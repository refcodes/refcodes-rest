module org.refcodes.rest {
	requires org.refcodes.textual;
	requires transitive jdk.httpserver;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.controlflow;
	requires transitive org.refcodes.data;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.io;
	requires transitive org.refcodes.matcher;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.web;
	requires transitive org.refcodes.observer;
	requires transitive org.refcodes.runtime;
	requires transitive org.refcodes.security;
	requires java.logging;

	exports org.refcodes.rest;
}
