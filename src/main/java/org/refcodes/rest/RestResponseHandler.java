// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.component.ConnectionStatus;
import org.refcodes.component.ConnectionStatusAccessor;
import org.refcodes.component.LinkComponent.LinkComponentBuilder;
import org.refcodes.data.DaemonLoopSleepTime;
import org.refcodes.exception.Trap;
import org.refcodes.textual.VerboseTextBuilder;
import org.refcodes.web.FormFields;
import org.refcodes.web.HeaderFields;
import org.refcodes.web.HeaderFieldsAccessor.HeaderFieldsBuilder;
import org.refcodes.web.HeaderFieldsAccessor.HeaderFieldsProperty;
import org.refcodes.web.HttpClientRequest;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpMethodAccessor.HttpMethodBuilder;
import org.refcodes.web.HttpMethodAccessor.HttpMethodProperty;
import org.refcodes.web.HttpRequestBuilder;
import org.refcodes.web.HttpResponseException;
import org.refcodes.web.HttpStatusCode;
import org.refcodes.web.MediaTypeFactoryLookup;
import org.refcodes.web.QueryFieldsAccessor.QueryFieldsProperty;
import org.refcodes.web.RedirectDepthAccessor.RedirectDepthBuilder;
import org.refcodes.web.RequestHeaderFields;
import org.refcodes.web.Url;

/**
 * A {@link RestResponseHandler} describes a REST request and the
 * {@link RestResponseConsumer} in charge for handling a REST response. The
 * {@link RestResponseHandler} describes the {@link HttpMethod} to be used, the
 * targeted URL, the {@link HeaderFields} as well as the Query-Fields and the
 * request {@link Object} or {@link InputStream}. It provides builder
 * functionality and <code>lambda</code> support for handling the responses
 * addressed to this {@link RestResponseHandler}. The <code>lambda</code>
 * defined as {@link RestResponseConsumer} acts as the single listener to this
 * {@link RestResponseHandler} responsible for handling the responses for which
 * this {@link RestResponseHandler} is responsible. The locator to which a
 * {@link RestResponseHandler} targets for is defined by the {@link #getUrl()}
 * property.
 */
public class RestResponseHandler extends HttpClientRequest implements RestResponseConsumer, QueryFieldsProperty, HeaderFieldsProperty<RequestHeaderFields>, RedirectDepthBuilder<RestResponseHandler>, HttpRequestBuilder<RestResponseHandler>, HttpMethodProperty, HttpMethodBuilder<RestResponseHandler>, LinkComponentBuilder<RestResponseHandler>, ConnectionStatusAccessor, HeaderFieldsBuilder<RequestHeaderFields, RestResponseHandler> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( RestResponseHandler.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private RestResponseConsumer _responseObserver = null;
	private ConnectionStatus _connectionStatus = ConnectionStatus.NONE;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link RestResponseHandler}.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields the Header-Fields
	 * @param aRequest the request
	 * @param aResponseConsumer the response observer
	 * @param aMediaTypeFactoryLookup the media type factory lookup
	 */
	public RestResponseHandler( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer, MediaTypeFactoryLookup aMediaTypeFactoryLookup ) {
		this( aHttpMethod, aUrl, aHeaderFields, aRequest, HttpClientRequest.DEFAULT_REDIRECT_DEPTH, aResponseConsumer, aMediaTypeFactoryLookup );
	}

	/**
	 * Instantiates a new {@link RestResponseHandler}.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aResponseConsumer the response observer
	 * @param aMediaTypeFactoryLookup the media type factory lookup
	 */
	public RestResponseHandler( HttpMethod aHttpMethod, Url aUrl, RestResponseConsumer aResponseConsumer, MediaTypeFactoryLookup aMediaTypeFactoryLookup ) {
		this( aHttpMethod, aUrl, null, null, aResponseConsumer, aMediaTypeFactoryLookup );
	}

	/**
	 * Instantiates a new {@link RestResponseHandler}.
	 *
	 * @param aMediaTypeFactoryLookup the media type factory lookup
	 */
	public RestResponseHandler( MediaTypeFactoryLookup aMediaTypeFactoryLookup ) {
		this( null, null, null, null, null, aMediaTypeFactoryLookup );
	}

	/**
	 * Instantiates a new {@link RestResponseHandler}.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest the request
	 * @param aResponseConsumer the response observer
	 * @param aMediaTypeFactoryLookup the media type factory lookup
	 */
	public RestResponseHandler( HttpMethod aHttpMethod, Url aUrl, Object aRequest, RestResponseConsumer aResponseConsumer, MediaTypeFactoryLookup aMediaTypeFactoryLookup ) {
		this( aHttpMethod, aUrl, null, aRequest, aResponseConsumer, aMediaTypeFactoryLookup );
	}

	/**
	 * Instantiates a new {@link RestResponseHandler}.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aResponseConsumer the response observer
	 * @param aRedirectDepth The redirect depth provides the count of
	 *        HTTP-Request and HTTP-Response cycles where the response
	 *        represents a redirect as of
	 *        {@link HttpStatusCode#isRedirectStatus()}. A value of -1
	 *        represents the default behavior, e.g. using
	 *        {@link java.net.HttpURLConnection}'s redirection means.
	 * @param aMediaTypeFactoryLookup the media type factory lookup
	 */
	public RestResponseHandler( HttpMethod aHttpMethod, Url aUrl, RestResponseConsumer aResponseConsumer, int aRedirectDepth, MediaTypeFactoryLookup aMediaTypeFactoryLookup ) {
		this( aHttpMethod, aUrl, null, null, aRedirectDepth, aResponseConsumer, aMediaTypeFactoryLookup );
	}

	/**
	 * Instantiates a new {@link RestResponseHandler}.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest the request
	 * @param aRedirectDepth The redirect depth provides the count of
	 *        HTTP-Request and HTTP-Response cycles where the response
	 *        represents a redirect as of
	 *        {@link HttpStatusCode#isRedirectStatus()}. A value of -1
	 *        represents the default behavior, e.g. using
	 *        {@link java.net.HttpURLConnection}'s redirection means.
	 * @param aResponseConsumer the response observer
	 * @param aMediaTypeFactoryLookup the media type factory lookup
	 */
	public RestResponseHandler( HttpMethod aHttpMethod, Url aUrl, Object aRequest, int aRedirectDepth, RestResponseConsumer aResponseConsumer, MediaTypeFactoryLookup aMediaTypeFactoryLookup ) {
		this( aHttpMethod, aUrl, null, aRequest, aRedirectDepth, aResponseConsumer, aMediaTypeFactoryLookup );
	}

	/**
	 * Instantiates a new {@link RestResponseHandler}.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields the Header-Fields
	 * @param aRequest the request
	 * @param aRedirectDepth The redirect depth provides the count of
	 *        HTTP-Request and HTTP-Response cycles where the response
	 *        represents a redirect as of
	 *        {@link HttpStatusCode#isRedirectStatus()}. A value of -1
	 *        represents the default behavior, e.g. using
	 *        {@link java.net.HttpURLConnection}'s redirection means.
	 * @param aResponseConsumer the response observer
	 * @param aMediaTypeFactoryLookup the media type factory lookup
	 */
	public RestResponseHandler( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, int aRedirectDepth, RestResponseConsumer aResponseConsumer, MediaTypeFactoryLookup aMediaTypeFactoryLookup ) {
		super( aHttpMethod, aUrl instanceof org.refcodes.web.UrlBuilder ? aUrl : new org.refcodes.web.UrlBuilder( aUrl ), aHeaderFields, aRequest, aRedirectDepth, aMediaTypeFactoryLookup );
		_responseObserver = aResponseConsumer;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseHandler withRedirectDepth( int aRedirectDepth ) {
		setRedirectDepth( aRedirectDepth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setQueryFields( FormFields aQueryFields ) {
		final Url theUrl = getUrl();
		final org.refcodes.web.UrlBuilder theUrlBuilder;
		if ( theUrl instanceof UrlBuilder ) {
			theUrlBuilder = (org.refcodes.web.UrlBuilder) theUrl;
		}
		else {
			theUrlBuilder = new org.refcodes.web.UrlBuilder( theUrl );
		}
		theUrlBuilder.setQueryFields( aQueryFields );
		setUrl( theUrlBuilder );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FormFields getQueryFields() {
		return getUrl().getQueryFields();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseHandler withUrl( Url aUrl ) {
		setUrl( aUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseHandler withHttpMethod( HttpMethod aHttpMethod ) {
		setHttpMethod( aHttpMethod );
		return this;
	}

	/**
	 * Sets the request for the request property.
	 *
	 * @param <REQ> the generic type
	 * @param aRequest The request to be stored by the request property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public <REQ> RestResponseHandler withRequest( REQ aRequest ) {
		setRequest( aRequest );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onResponse( RestResponseEvent aResponse ) throws HttpResponseException {
		if ( _connectionStatus != ConnectionStatus.CLOSED ) {
			_responseObserver.onResponse( aResponse );
		}
		else {
			LOGGER.log( Level.WARNING, "Ignoring response <" + aResponse + "> as this rest endpoint is in status <" + _connectionStatus + ">, you may have closed it already?" );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpMethod getHttpMethod() {
		return _httpMethod;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpMethod( HttpMethod aHttpMethod ) {
		_httpMethod = aHttpMethod;
	}

	/**
	 * Retrieves the {@link RestResponseConsumer} to which any responses are
	 * delegated upon invocation of the {@link #onResponse(RestResponseEvent)}
	 * method.
	 * 
	 * @return The (user defined) {@link RestResponseConsumer} to handle
	 *         responses.
	 */
	public RestResponseConsumer getResponseObserver() {
		return _responseObserver;
	}

	/**
	 * Sets the {@link RestResponseConsumer} to which any responses are
	 * delegated upon invocation of the {@link #onResponse(RestResponseEvent)}
	 * method.
	 * 
	 * @param aLambda The (user defined) {@link RestResponseConsumer} to handle
	 *        responses, feel free to code it as <code>lambda</code> expression!
	 */
	public void setResponseObserver( RestResponseConsumer aLambda ) {
		_responseObserver = aLambda;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open() throws IOException {
		_connectionStatus = ConnectionStatus.OPENED;
		synchronized ( this ) {
			notifyAll();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_connectionStatus = ConnectionStatus.CLOSED;
		synchronized ( this ) {
			notifyAll();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConnectionStatus getConnectionStatus() {
		return _connectionStatus;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHeaderFields( RequestHeaderFields aHeaderFields ) {
		_headerFields = aHeaderFields;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getUrl() {
		return _url;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setUrl( Url aUrl ) {
		_url = aUrl;
	}

	/**
	 * Builder method for setting the {@link RestResponseConsumer}.
	 * 
	 * @param aLambda The (user defined) {@link RestResponseConsumer} to handle
	 *        responses, feel free to code it as <code>lambda</code> expression
	 * 
	 * @return The {@link RestResponseHandler} for the sake of a fluent API.
	 */
	public RestResponseHandler withResponseObserver( RestResponseConsumer aLambda ) {
		setResponseObserver( aLambda );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseHandler withHeaderFields( RequestHeaderFields aRequestHeaderFields ) {
		setHeaderFields( aRequestHeaderFields );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseHandler withOpen() throws IOException {
		open();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseHandler withClose() throws IOException {
		close();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseHandler withCloseQuietly() {
		closeQuietly();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseHandler withCloseIn( int aCloseMillis ) {
		closeIn( aCloseMillis );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + _httpMethod + ": " + _url.toHttpUrl() + "?" + new VerboseTextBuilder().withElements( _url.getQueryFields() ).toString() + ")@" + hashCode();
	}

	/**
	 * This is a convenience method for easily instantiating the according
	 * builder.
	 * 
	 * @param aRestClient the rest client to take care of the caller.
	 * 
	 * @return an instance (using a public implementation) of this builder
	 */
	public static RestResponseHandler build( RestfulClient aRestClient ) {
		return new RestResponseHandler( aRestClient );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The Class RequestDaemon.
	 */
	static class RestResponseHandlerDaemon implements Runnable {

		private final RestResponseHandler _responseHandler;
		private final RestRequestHandler _requestHandler;
		private final RestfulClient _source;

		/**
		 * Instantiates a new request daemon.
		 *
		 * @param aResponseHandler the rest response handler.
		 * @param aRequestHandler The handler technically executing the request.
		 * @param aSource the source
		 */
		protected RestResponseHandlerDaemon( RestResponseHandler aResponseHandler, RestRequestHandler aRequestHandler, RestfulClient aSource ) {
			if ( aRequestHandler == null ) {
				throw new IllegalArgumentException( "Unable to process your request <" + aResponseHandler.toString() + "> as no <" + RestRequestHandler.class.getSimpleName() + "> has been implemented, aborting!" );
			}
			_responseHandler = aResponseHandler;
			_requestHandler = aRequestHandler;
			_source = aSource;
		}

		/**
		 * Run.
		 */
		@Override
		public void run() {
			try {
				// |--> Wait till the connection is opened / closed:
				synchronized ( _responseHandler ) {
					while ( _responseHandler.getConnectionStatus() == ConnectionStatus.NONE ) {
						try {
							_responseHandler.wait( DaemonLoopSleepTime.MAX.getTimeMillis() );
						}
						catch ( InterruptedException e ) {}
						if ( _responseHandler.getConnectionStatus() == ConnectionStatus.NONE ) {
							LOGGER.log( Level.WARNING, "Your handler's <" + _responseHandler + "> connection status is still <" + _responseHandler.getConnectionStatus() + "> after <" + DaemonLoopSleepTime.MAX.getTimeMillis() + "> ms, execution of your request starts not earlier than you calling the #open() method." );
						}
					}
				}
				// Wait till the connection is opened / closed <--|
				if ( _responseHandler.getConnectionStatus() != ConnectionStatus.OPENED ) {
					throw new IllegalStateException( "Aborting your request as of your handler <" + _responseHandler + "> connection status is <" + _responseHandler.getConnectionStatus() + "> although it is expected to e <" + ConnectionStatus.OPENED + ">." );
				}
				final RestResponse theResponse = _requestHandler.doRequest( _responseHandler );
				_responseHandler.onResponse( new RestResponseEvent( theResponse, _source ) );
			}
			catch ( HttpResponseException e ) {
				LOGGER.log( Level.SEVERE, Trap.asMessage( e ), e );
			}
		}
	}
}
