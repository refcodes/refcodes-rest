// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.web.HttpServerResponse;
import org.refcodes.web.HttpStatusCode;

/**
 * A lambda "catch-all" for handling exceptions during HTTP-Request processing.
 */
@FunctionalInterface
public interface HttpExceptionHandler {

	/**
	 * Invoked upon thrown {@link Exception} instances whilst processing
	 * HTTP-Requests. Process the HTTP-Response as of what you get from the
	 * HTTP-Request
	 * 
	 * @param aRequestEvent The HTTP-Request related to the {@link Exception}.
	 * @param aHttpServerResponse The HTTP-Response to be worked with.
	 * @param aException The {@link Exception} involved in the problem.
	 * @param aHttpStatusCode The HTTP-Error-Code (if known) identified.
	 */
	void onHttpError( RestRequestEvent aRequestEvent, HttpServerResponse aHttpServerResponse, Exception aException, HttpStatusCode aHttpStatusCode );

}
