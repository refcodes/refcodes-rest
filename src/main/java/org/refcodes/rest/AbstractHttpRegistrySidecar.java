// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.refcodes.component.InitializeException;
import org.refcodes.component.LifecycleMachine.ManualLifecycleMachine;
import org.refcodes.component.LifecycleStatus;
import org.refcodes.component.PauseException;
import org.refcodes.component.ResumeException;
import org.refcodes.component.StartException;
import org.refcodes.component.StopException;
import org.refcodes.data.Scheme;
import org.refcodes.net.IpAddress;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.web.Url;

/**
 * Abstract class for easily decorating a {@link HttpRegistrySidecar}.
 *
 * @param <DESC> the generic type
 * @param <B> the generic type
 */
public abstract class AbstractHttpRegistrySidecar<DESC extends HttpServerDescriptor, B extends HttpRegistrySidecar<DESC, B>> implements HttpRegistrySidecar<DESC, B> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected ManualLifecycleMachine _lifeCycleAutomaton = new ManualLifecycleMachine();
	private Url _serviceRegistryUrl;
	private DESC _serverDescriptor;
	private TrustStoreDescriptor _storeDescriptor;
	private String _alias;
	private String _instanceId;
	private String _host;
	private String _virtualHost;
	private int[] _ipAddress;
	private String _pingPath;
	protected RestRequestConsumer _pingRequestObserver = null;

	// /////////////////////////////////////////////////////////////////////////
	// LIFECYCLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isRunning() {
		return _lifeCycleAutomaton.isRunning();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isInitialized() {
		return _lifeCycleAutomaton.isInitialized();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LifecycleStatus getLifecycleStatus() {
		return _lifeCycleAutomaton.getLifecycleStatus();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void pause() throws PauseException {
		_lifeCycleAutomaton.pause();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop() throws StopException {
		_lifeCycleAutomaton.stop();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() throws StartException {
		_lifeCycleAutomaton.start();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void resume() throws ResumeException {
		_lifeCycleAutomaton.resume();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize() throws InitializeException {
		_lifeCycleAutomaton.initialize();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStoppable() {
		return _lifeCycleAutomaton.isStoppable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isPausable() {
		return _lifeCycleAutomaton.isPausable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStartable() {
		return _lifeCycleAutomaton.isStartable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isResumable() {
		return _lifeCycleAutomaton.isResumable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStopped() {
		return _lifeCycleAutomaton.isStopped();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isPaused() {
		return _lifeCycleAutomaton.isPaused();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isInitalizable() {
		return _lifeCycleAutomaton.isInitalizable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		_lifeCycleAutomaton.destroy();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isDestroyable() {
		return _lifeCycleAutomaton.isDestroyable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isDestroyed() {
		return _lifeCycleAutomaton.isDestroyed();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setInstanceId( String aInstanceId ) {
		_instanceId = aInstanceId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getInstanceId() {
		return _instanceId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHost( String aHost ) {
		_host = aHost;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getHost() {
		return _host;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getVirtualHost() {
		return _virtualHost;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setVirtualHost( String aVirtualHost ) {
		_virtualHost = aVirtualHost;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int[] getIpAddress() {
		return _ipAddress;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setIpAddress( int[] aIpAddress ) {
		_ipAddress = aIpAddress;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TrustStoreDescriptor getTrustStoreDescriptor() {
		return _storeDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTrustStoreDescriptor( TrustStoreDescriptor aTrustStoreDescriptor ) {
		_storeDescriptor = aTrustStoreDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DESC getHttpServerDescriptor() {
		return _serverDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpServerDescriptor( DESC aServerDescriptor ) {
		_serverDescriptor = aServerDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getHttpRegistryUrl() {
		return _serviceRegistryUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpRegistryUrl( Url aRegistryUrl ) {
		_serviceRegistryUrl = aRegistryUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAlias() {
		return _alias;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setAlias( String aAlias ) {
		_alias = aAlias;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getPingPath() {
		return _pingPath;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPingPath( String aPingPath ) {
		_pingPath = aPingPath;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Resolves the property from the provided value and the provided property
	 * and the and sets the property in case the provided value is not null.
	 * 
	 * @param aHost The value to be used when not null.
	 * @param aProperty The property to be used when the value is null and which
	 *        is to be set when the value is not null.
	 * 
	 * @return The value when not null, else the value of the provided property.
	 * 
	 * @throws UnknownHostException thrown in case the local machine's host
	 *         cannot be resolved when the provided host is null.
	 */
	protected static String toHost( String aHost, HostProperty aProperty ) throws UnknownHostException {
		if ( aHost != null ) {
			aProperty.setHost( aHost );
		}
		else {
			aHost = aProperty.getHost();
			if ( aHost == null ) {
				aHost = InetAddress.getLocalHost().getHostName();
				aProperty.setHost( aHost );
			}
		}
		return aHost;
	}

	/**
	 * Resolves the property from the provided value and the provided property
	 * and the and sets the property in case the provided value is not null.
	 * 
	 * @param aVirtualHost The value to be used when not null.
	 * @param aProperty The property to be used when the value is null and which
	 *        is to be set when the value is not null.
	 * 
	 * @return The value when not null, else the value of the provided property.
	 */
	protected static String toVirtualHost( String aVirtualHost, VirtualHostProperty aProperty ) {
		if ( aVirtualHost != null ) {
			aProperty.setVirtualHost( aVirtualHost );
		}
		else {
			aVirtualHost = aProperty.getVirtualHost();
		}
		return aVirtualHost;
	}

	/**
	 * Resolves the property from the provided value and the provided property
	 * and the and sets the property in case the provided value is not null.
	 * 
	 * @param aIpAddress The value to be used when not null.
	 * @param aProperty The property to be used when the value is null and which
	 *        is to be set when the value is not null.
	 * 
	 * @return The value when not null, else the value of the provided property.
	 * 
	 * @throws IOException thrown in case the local machine's IP-Address cannot
	 *         be resolved when the provided IP-Address is null.
	 */
	protected static int[] toIpAddress( int[] aIpAddress, IpAddressProperty aProperty ) throws IOException {
		if ( aIpAddress != null ) {
			aProperty.setIpAddress( aIpAddress );
		}
		else {
			aIpAddress = IpAddress.toHostIpAddress();
			aProperty.setIpAddress( aIpAddress );
		}
		return aIpAddress;
	}

	/**
	 * Resolves the property from the provided value and the provided property
	 * and the and sets the property in case the provided value is not null.
	 * 
	 * @param aAlias The value to be used when not null.
	 * @param aProperty The property to be used when the value is null and which
	 *        is to be set when the value is not null.
	 * 
	 * @return The value when not null, else the value of the provided property.
	 */
	protected static String toAlias( String aAlias, AliasProperty aProperty ) {
		if ( aAlias != null ) {
			aProperty.setAlias( aAlias );
		}
		else {
			aAlias = aProperty.getAlias();
		}
		return aAlias;
	}

	/**
	 * Resolves the property from the provided value and the provided property
	 * and the and sets the property in case the provided value is not null.
	 * 
	 * @param aScheme The value to be used when not null.
	 * @param aProperty The property to be used when the value is null and which
	 *        is to be set when the value is not null.
	 * 
	 * @return The value when not null, else the value of the provided property.
	 */
	protected static Scheme toScheme( Scheme aScheme, SchemeProperty aProperty ) {
		if ( aScheme != null ) {
			aProperty.setScheme( aScheme );
		}
		else {
			aScheme = aProperty.getScheme();
		}
		return aScheme;
	}

	/**
	 * Resolves the property from the provided value and the provided property
	 * and the and sets the property in case the provided value is not null.
	 * 
	 * @param aPort The value to be used when not null.
	 * @param aProperty The property to be used when the value is null and which
	 *        is to be set when the value is not null.
	 * 
	 * @return The value when not null, else the value of the provided property.
	 */
	protected static int toPort( int aPort, PortProperty aProperty ) {
		if ( aPort != -1 ) {
			aProperty.setPort( aPort );
		}
		else {
			aPort = aProperty.getPort();
		}
		return aPort;
	}

	/**
	 * Resolves the property from the provided value and the provided property
	 * and the and sets the property in case the provided value is not null.
	 * 
	 * @param aInstanceId The value to be used when not null.
	 * @param aHost The value to be used when instance TID is null.
	 * @param aProperty The property to be used when the value is null and which
	 *        is to be set when the value is not null.
	 * 
	 * @return The value when not null, else the value of the provided property.
	 */
	protected static String toInstanceId( String aInstanceId, String aHost, InstanceIdProperty aProperty ) {
		if ( aInstanceId != null ) {
			aProperty.setInstanceId( aInstanceId );
		}
		else {
			aInstanceId = aProperty.getInstanceId();
			if ( aInstanceId == null ) {
				aInstanceId = aHost;
				aProperty.setInstanceId( aInstanceId );
			}
		}
		return aInstanceId;
	}

	/**
	 * Resolves the property from the provided value and the provided property
	 * and the and sets the property in case the provided value is not null.
	 * 
	 * @param aPingPath The value to be used when not null.
	 * @param aDefaultPingPath The value to be used when the ping path is null.
	 * @param aProperty The property to be used when the value is null and which
	 *        is to be set when the value is not null.
	 * 
	 * @return The value when not null, else the value of the provided property.
	 */
	protected static String toPingPath( String aPingPath, String aDefaultPingPath, PingPathProperty aProperty ) {
		if ( aPingPath != null ) {
			aProperty.setPingPath( aPingPath );
		}
		else {
			aPingPath = aProperty.getPingPath();
			if ( aPingPath == null ) {
				aPingPath = aDefaultPingPath;
				aProperty.setPingPath( aPingPath );
			}
		}
		return aPingPath;
	}

	/**
	 * Resolves the property from the provided value and the provided property
	 * and the and sets the property in case the provided value is not null.
	 * 
	 * @param aStoreDescriptor The value to be used when not null.
	 * @param aProperty The property to be used when the value is null and which
	 *        is to be set when the value is not null.
	 * 
	 * @return The value when not null, else the value of the provided property.
	 */
	protected static TrustStoreDescriptor toTrustStoreDescriptor( TrustStoreDescriptor aStoreDescriptor, TrustStoreDescriptorProperty aProperty ) {
		if ( aStoreDescriptor != null ) {
			aProperty.setTrustStoreDescriptor( aStoreDescriptor );
		}
		else {
			aStoreDescriptor = aProperty.getTrustStoreDescriptor();
		}
		return aStoreDescriptor;
	}

	/**
	 * Resolves the property from the provided value and the provided property
	 * and the and sets the property in case the provided value is not null.
	 * 
	 * @param aRegistryUrl The value to be used when not null.
	 * @param aProperty The property to be used when the value is null and which
	 *        is to be set when the value is not null.
	 * 
	 * @return The value when not null, else the value of the provided property.
	 */
	protected static Url toHttpRegistryUrl( Url aRegistryUrl, HttpRegistryUrlProperty aProperty ) {
		if ( aRegistryUrl != null ) {
			aProperty.setHttpRegistryUrl( aRegistryUrl );
		}
		else {
			aRegistryUrl = aProperty.getHttpRegistryUrl();
		}
		return aRegistryUrl;
	}

	/**
	 * Resolves the property from the provided value and the provided property
	 * and the and sets the property in case the provided value is not null.
	 * 
	 * @param <DESC> The type of the server discovery descriptor (the object
	 *        describing your service and locating the service registry).
	 * @param aServerDescriptor The value to be used when not null.
	 * @param aProperty The property to be used when the value is null and which
	 *        is to be set when the value is not null.
	 * 
	 * @return The value when not null, else the value of the provided property.
	 */
	protected static <DESC extends HttpServerDescriptor> DESC toHttpServerDescriptor( DESC aServerDescriptor, HttpServerDescriptorProperty<DESC> aProperty ) {
		if ( aServerDescriptor != null ) {
			aProperty.setHttpServerDescriptor( aServerDescriptor );
		}
		else {
			aServerDescriptor = aProperty.getHttpServerDescriptor();
		}
		return aServerDescriptor;
	}
}
