// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.data.Scheme;
import org.refcodes.mixin.PortAccessor.PortBuilder;
import org.refcodes.mixin.PortAccessor.PortProperty;
import org.refcodes.web.SchemeAccessor.SchemeBuilder;
import org.refcodes.web.SchemeAccessor.SchemeProperty;

/**
 * The {@link HttpRegistrySidecar} describes the functionality required in order
 * to register a service at a service registry and discovery service. This type
 * is intended to be used by different separate hierarchy branches by providing
 * the generic type &lt;B&gt;, ensuring a coherent type hierarchy for each
 * branch.
 *
 * @param <DESC> The type of the server discovery descriptor (the object
 *        describing your service and locating the service registry).
 * @param <B> In order to implement the builder pattern with a coherent type
 *        hierarchy.
 */
public interface HttpRegistrySidecar<DESC extends HttpServerDescriptor, B extends HttpRegistrySidecar<DESC, B>> extends HttpRegistry<DESC, B>, PortProperty, PortBuilder<B>, SchemeProperty, SchemeBuilder<B> {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withPort( int aPort ) {
		setPort( aPort );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withScheme( Scheme aScheme ) {
		setScheme( aScheme );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withProtocol( String aProtocol ) {
		setProtocol( aProtocol );
		return (B) this;
	}
}
