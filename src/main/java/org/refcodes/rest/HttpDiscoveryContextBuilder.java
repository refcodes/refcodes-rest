// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.security.KeyStore;

import org.refcodes.rest.HttpDiscoveryUrlAccessor.HttpDiscoveryUrlBuilder;
import org.refcodes.rest.HttpDiscoveryUrlAccessor.HttpDiscoveryUrlProperty;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.security.TrustStoreDescriptorAccessor.TrustStoreDescriptorBuilder;
import org.refcodes.security.TrustStoreDescriptorAccessor.TrustStoreDescriptorProperty;
import org.refcodes.web.LoadBalancingStrategy;
import org.refcodes.web.LoadBalancingStrategyAccessor.LoadBalancingStrategyBuilder;
import org.refcodes.web.LoadBalancingStrategyAccessor.LoadBalancingStrategyProperty;
import org.refcodes.web.Url;

/**
 * The {@link HttpDiscoveryContextBuilder} implements the
 * {@link HttpDiscoveryContext} with builder functionality as of the builder
 * pattern.
 */
public class HttpDiscoveryContextBuilder implements LoadBalancingStrategyProperty, LoadBalancingStrategyBuilder<HttpDiscoveryContextBuilder>, HttpDiscoveryUrlProperty, HttpDiscoveryUrlBuilder<HttpDiscoveryContextBuilder>, TrustStoreDescriptorProperty, TrustStoreDescriptorBuilder<HttpDiscoveryContextBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Url _discoveryUrl;
	private TrustStoreDescriptor _trustStoreDescriptor;
	private LoadBalancingStrategy _strategy;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link HttpDiscoveryContextBuilder} context.
	 * 
	 * @param aDiscoveryUrl The {@link Url} pointing to the service discovery
	 *        registry.
	 */
	public HttpDiscoveryContextBuilder( Url aDiscoveryUrl ) {
		this( aDiscoveryUrl, null );
	}

	/**
	 * Constructs the {@link HttpDiscoveryContextBuilder} context.
	 * 
	 * @param aDiscoveryUrl The {@link Url} pointing to the service discovery
	 *        registry.
	 * @param aStoreDescriptor The descriptor describing the truststore
	 *        ({@link KeyStore}) required for establishing an HTTPS connection
	 *        to the registry.
	 */
	public HttpDiscoveryContextBuilder( Url aDiscoveryUrl, TrustStoreDescriptor aStoreDescriptor ) {
		_discoveryUrl = aDiscoveryUrl;
		_trustStoreDescriptor = aStoreDescriptor;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpDiscoveryContextBuilder withLoadBalancingStrategy( LoadBalancingStrategy aStrategy ) {
		setLoadBalancingStrategy( aStrategy );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpDiscoveryContextBuilder withHttpDiscoveryUrl( Url aUrl ) {
		setHttpDiscoveryUrl( aUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpDiscoveryContextBuilder withTrustStoreDescriptor( TrustStoreDescriptor aStoreDescriptor ) {
		setTrustStoreDescriptor( aStoreDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getHttpDiscoveryUrl() {
		return _discoveryUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TrustStoreDescriptor getTrustStoreDescriptor() {
		return _trustStoreDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpDiscoveryUrl( Url aUrl ) {
		_discoveryUrl = aUrl;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTrustStoreDescriptor( TrustStoreDescriptor aTrustStoreDescriptor ) {
		_trustStoreDescriptor = aTrustStoreDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoadBalancingStrategy getLoadBalancingStrategy() {
		return _strategy;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setLoadBalancingStrategy( LoadBalancingStrategy aStrategy ) {
		_strategy = aStrategy;
	}
}
