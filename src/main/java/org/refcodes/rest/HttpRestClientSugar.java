package org.refcodes.rest;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyStore;

import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.web.HttpServerContext;
import org.refcodes.web.Url;

/**
 * Declarative syntactic sugar which may be statically imported in order to
 * allow declarative definitions of REST client functionality:
 * <code>import static org.refcodes.rest.HttpRestClientSugar.*; </code>
 */
public class HttpRestClientSugar extends RestDeleteClientSugar {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	private HttpRestClientSugar() {
		throw new IllegalStateException( "Utility class" );
	}

	/**
	 * See also {@link RestfulHttpClient#open()}.
	 *
	 * @throws IOException thrown in case something went wrong.
	 */
	public static void open() throws IOException {
		HttpRestClientSingleton.getInstance().open();
	}

	/**
	 * See also {@link RestfulHttpClient#open(Url, TrustStoreDescriptor)}.
	 *
	 * @param aStoreDescriptor The store descriptor describing your
	 *        {@link KeyStore} required by HTTPS.
	 * 
	 * @throws IOException thrown in case something went wrong.
	 */
	public static void open( TrustStoreDescriptor aStoreDescriptor ) throws IOException {
		HttpRestClientSingleton.getInstance().open( aStoreDescriptor );
	}

	/**
	 * See also {@link RestfulHttpClient#open(Url)}.
	 *
	 * @param aBaseUrl The base {@link Url} to be used.
	 * 
	 * @throws IOException thrown in case something went wrong.
	 */
	public static void open( Url aBaseUrl ) throws IOException {
		HttpRestClientSingleton.getInstance().open( aBaseUrl );
	}

	/**
	 * See also {@link RestfulHttpClient#open(Url, TrustStoreDescriptor)}.
	 *
	 * @param aBaseUrl The base {@link Url} to be used.
	 * @param aStoreDescriptor The store descriptor describing your
	 *        {@link KeyStore} required by HTTPS.
	 * 
	 * @throws IOException thrown in case something went wrong.
	 */
	public static void open( Url aBaseUrl, TrustStoreDescriptor aStoreDescriptor ) throws IOException {
		HttpRestClientSingleton.getInstance().open( aBaseUrl, aStoreDescriptor );
	}

	/**
	 * See {@link RestfulHttpServer#open(Object)}.
	 *
	 * @param aCtx The context describing the parameters required to open the
	 *        connection.
	 * 
	 * @return The {@link RestfulHttpServer}
	 * 
	 * @throws IOException thrown in case something went wrong.
	 */
	public static RestfulHttpServer open( HttpServerContext aCtx ) throws IOException {
		HttpRestServerSingleton.getInstance().open( aCtx );
		return HttpRestServerSingleton.getInstance();
	}

	/**
	 * See also {@link RestfulHttpClient#withBaseUrl(String)}.
	 *
	 * @param aBaseUrl The base url
	 * 
	 * @return The HTTP rest client
	 * 
	 * @throws MalformedURLException the malformed URL exception
	 */
	public static RestfulHttpClient withBaseUrl( String aBaseUrl ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().withBaseUrl( aBaseUrl );
	}

	/**
	 * See also {@link RestfulHttpClient#withBaseUrl(URL)}.
	 *
	 * @param aBaseUrl The base url
	 * 
	 * @return The HTTP rest client
	 */
	public static RestfulHttpClient withBaseUrl( URL aBaseUrl ) {
		return HttpRestClientSingleton.getInstance().withBaseUrl( aBaseUrl );
	}

	/**
	 * See also {@link RestfulHttpClient#withUserAgent(String)}.
	 *
	 * @param aUserAgent The user agent
	 * 
	 * @return The HTTP rest client
	 * 
	 * @throws MalformedURLException the malformed URL exception
	 */
	public static RestfulHttpClient withUserAgent( String aUserAgent ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().withUserAgent( aUserAgent );
	}

	// /////////////////////////////////////////////////////////////////////////
	// REQUEST BUILDER:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// SYNCHRONOUS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// ASYNCHRONOUS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// PROTOCOL + HOST + PORT + PATH:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// SYNCHRONOUS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// ASYNCHRONOUS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// PROTOCOL + HOST + PATH:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// SYNCHRONOUS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// ASYNCHRONOUS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// PROTOCOL (STRING) + HOST + PORT + PATH:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// SYNCHRONOUS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// ASYNCHRONOUS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// PROTOCOL (STRING) + HOST + PATH:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// SYNCHRONOUS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// ASYNCHRONOUS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

}
