// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "HTTP://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("HTTP://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("HTTP://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("HTTP://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManagerFactory;

import org.refcodes.component.ConnectionStatus;
import org.refcodes.controlflow.ControlFlowUtility;
import org.refcodes.controlflow.ThreadingModel;
import org.refcodes.data.Delimiter;
import org.refcodes.data.LatencySleepTime;
import org.refcodes.data.Literal;
import org.refcodes.data.Scheme;
import org.refcodes.exception.MarshalException;
import org.refcodes.exception.Trap;
import org.refcodes.exception.UnhandledEnumBugException;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.security.KeyStoreDescriptor;
import org.refcodes.web.AuthType;
import org.refcodes.web.BadResponseException;
import org.refcodes.web.BasicAuthCredentials;
import org.refcodes.web.BasicAuthObserver;
import org.refcodes.web.BasicAuthRequiredException;
import org.refcodes.web.BasicAuthResponse;
import org.refcodes.web.ContentType;
import org.refcodes.web.FormMediaTypeFactory;
import org.refcodes.web.HeaderField;
import org.refcodes.web.HeaderFields;
import org.refcodes.web.HttpBodyMap;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpServerContext;
import org.refcodes.web.HttpServerRequest;
import org.refcodes.web.HttpServerResponse;
import org.refcodes.web.HttpStatusCode;
import org.refcodes.web.HttpStatusException;
import org.refcodes.web.HttpsConnectionRequestObserver;
import org.refcodes.web.JsonMediaTypeFactory;
import org.refcodes.web.MediaType;
import org.refcodes.web.MediaTypeFactory;
import org.refcodes.web.PostHttpServerInterceptor;
import org.refcodes.web.PreHttpServerInterceptor;
import org.refcodes.web.RequestHeaderFields;
import org.refcodes.web.TextMediaTypeFactory;
import org.refcodes.web.TransportLayerProtocol;
import org.refcodes.web.UnsupportedMediaTypeException;
import org.refcodes.web.Url;
import org.refcodes.web.UrlBuilder;
import org.refcodes.web.XmlMediaTypeFactory;

import com.sun.net.httpserver.Authenticator;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpPrincipal;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;

/**
 * Implementation of the {@link RestfulHttpServer} interface using the
 * {@link RestfulHttpServer} defined in the <code>com.sun.net.httpserver</code>
 * package. The {@link HttpRestServer} is being initialized with some common
 * {@link MediaTypeFactory} instances (as implemented by the
 * {@link AbstractRestfulServer}). At the time of writing this document the
 * {@link MediaTypeFactory} instances being preconfigured are:
 * <ul>
 * <li>{@link JsonMediaTypeFactory}</li>
 * <li>{@link XmlMediaTypeFactory}</li>
 * <li>{@link TextMediaTypeFactory}</li>
 * <li>{@link FormMediaTypeFactory}</li>
 * </ul>
 * The {@link HttpRestServer} supports HTTP as well as HTTPS protocols as being
 * based on the {@link RestfulHttpServer} as well as on the {@link HttpsServer}.
 * For opening up an HTTPS connection, refer to the methods such as
 * {@link #open(String, KeyStoreDescriptor, int)} or
 * {@link #open(KeyStoreDescriptor, int)} and the like.
 * 
 * Set the system property {@link SystemProperty#LOG_DEBUG} to true (set when
 * invoking the JRA by passing the argument <code>-Dlog.debug=true</code> to the
 * <code>java</code> executable) to log additional erroneous situations e.g.
 * related to content types and accept types alongside marshaling and
 * unmarshaling.
 */
public class HttpRestServer extends AbstractRestfulServer implements RestfulHttpServer {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( HttpRestServer.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	protected static final String CONTEXT_PATH = Delimiter.PATH.getChar() + "";
	private static final String ANONYMOUS = "anonymous";
	private static final long NO_RESPONSE_BODY = -1L;
	private static final long CHUNCKED_ENCODING = 0L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private com.sun.net.httpserver.HttpServer _httpServer = null;
	private int _port = -1;
	private HttpsConnectionRequestObserver _httpsConnectionRequestObserver = null;
	private ExecutorService _executorService;
	private ConnectionStatus _connectionStatus = ConnectionStatus.NONE;
	private HttpBasicAuthenticator _httpBasicAuthenticator = null;
	private HttpContext _httpContext;
	private Scheme _scheme = null;
	private String _protocol = null;
	private KeyStoreDescriptor _keyStoreDescriptor = null;
	private int _maxConnections = -1;
	private HttpExceptionHandling _httpExceptionHandling = HttpExceptionHandling.REPLACE;
	private HttpExceptionHandler _httpExceptionHandler = null;
	private final List<PreHttpServerInterceptor> _preHttpInterceptos = new ArrayList<>();
	private final List<PostHttpServerInterceptor> _postHttpInterceptos = new ArrayList<>();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link HttpRestServer}. Use {@link #open(int)} or similar to
	 * make it listen on your port. The provided {@link ThreadingModel} defines
	 * whether to use a single-threaded or a multi-threaded threading model.
	 * Threads are created as daemon threads. For more control on thread
	 * generation use the constructor {@link #HttpRestServer(ExecutorService)}.
	 *
	 * @param aThreadingModel Specifies to either use a single-threaded
	 *        threading model (no {@link ExecutorService}) or a multi-threaded
	 *        threading model with a default {@link ExecutorService} as of
	 *        {@link ControlFlowUtility#createCachedExecutorService(boolean)}.
	 */
	public HttpRestServer( ThreadingModel aThreadingModel ) {
		this( aThreadingModel, false );
	}

	/**
	 * Constructs a {@link HttpRestServer}. Use {@link #open(int)} or similar to
	 * make it listen on your port. The provided {@link ThreadingModel} defines
	 * whether to use a single-threaded or a multi-threaded threading model.
	 * Threads are created as daemon threads. For more control on thread
	 * generation use the constructor {@link #HttpRestServer(ExecutorService)}.
	 * 
	 * @param aThreadingModel Specifies to either use a single-threaded
	 *        threading model (no {@link ExecutorService}) or a multi-threaded
	 *        threading model with a default {@link ExecutorService} as of
	 *        {@link ControlFlowUtility#createCachedExecutorService(boolean)}.
	 * @param isVerbose When true, any unknown content- and accept-types are
	 *        logged.
	 */
	public HttpRestServer( ThreadingModel aThreadingModel, boolean isVerbose ) {
		this( aThreadingModel == ThreadingModel.SINGLE ? null : ControlFlowUtility.createCachedExecutorService( true ), isVerbose );
	}

	/**
	 * Constructs a {@link HttpRestServer}. Use {@link #open(int)} or similar to
	 * make it listen on your port. Uses a multi threaded threading model with a
	 * default {@link ExecutorService} as of
	 * {@link ControlFlowUtility#createCachedExecutorService(boolean)}. Threads
	 * are created as daemon threads. For more control on thread generation use
	 * the constructor {@link #HttpRestServer(ExecutorService)}.
	 */
	public HttpRestServer() {
		this( false );
	}

	/**
	 * Constructs a {@link HttpRestServer}. Use {@link #open(int)} or similar to
	 * make it listen on your port. Uses a multi threaded threading model with a
	 * default {@link ExecutorService} as of
	 * {@link ControlFlowUtility#createCachedExecutorService(boolean)}. Threads
	 * are created as daemon threads. For more control on thread generation use
	 * the constructor {@link #HttpRestServer(ExecutorService)}.
	 * 
	 * @param isVerbose When true, any unknown content- and accept-types are
	 *        logged.
	 */
	public HttpRestServer( boolean isVerbose ) {
		this( ControlFlowUtility.createCachedExecutorService( true ), isVerbose );
	}

	/**
	 * Constructs a {@link HttpRestServer}. Use {@link #open(int)} or similar to
	 * make it listen on your port. Uses a multi threaded threading model.
	 *
	 * @param aExecutorService An executor service to be used when creating
	 *        {@link Thread}s.
	 */
	public HttpRestServer( ExecutorService aExecutorService ) {
		this( aExecutorService, false );
	}

	/**
	 * Constructs a {@link HttpRestServer}. Use {@link #open(int)} or similar to
	 * make it listen on your port. Uses a multi threaded threading model.
	 * 
	 * @param aExecutorService An executor service to be used when creating
	 *        {@link Thread}s.
	 * @param isVerbose When true, any unknown content- and accept-types are
	 *        logged.
	 */
	public HttpRestServer( ExecutorService aExecutorService, boolean isVerbose ) {
		super( aExecutorService, isVerbose );
		_executorService = aExecutorService;
		_httpExceptionHandler = new DefaultErrorHandler();
	}

	// /////////////////////////////////////////////////////////////////////////
	// LIFECYCLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void close() throws IOException {
		if ( _connectionStatus != ConnectionStatus.OPENED ) {
			throw new IOException( "Connection is in status <" + _connectionStatus + ">. Open the connection before closing!" );
		}
		try {
			if ( _httpServer != null ) {
				_httpServer.stop( LatencySleepTime.MIN.getTimeMillis() / 1000 );
				_httpServer.removeContext( CONTEXT_PATH );
				_httpServer = null;
			}
		}
		finally {
			_connectionStatus = ConnectionStatus.CLOSED;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open( String aProtocol, KeyStoreDescriptor aStoreDescriptor, int aPort, int aMaxConnections ) throws IOException {
		if ( aPort < 0 ) {
			aPort = getPort();
		}
		if ( aPort < 0 ) {
			throw new IOException( "You must provide a valid port via 'setPort( aPort )' before you can invoke a port-less 'open' method!" );
		}
		_port = aPort;
		if ( aProtocol == null ) {
			aProtocol = toProtocol();
		}
		final Scheme theScheme = Scheme.fromProtocol( aProtocol );
		if ( theScheme == Scheme.HTTPS ) {
			aProtocol = TransportLayerProtocol.TLS.name();
		}
		if ( aProtocol == null && aStoreDescriptor != null ) {
			if ( SystemProperty.LOG_DEBUG.isEnabled() || _isVerbose ) {
				LOGGER.log( Level.INFO, "You did not provide a protocol such as <" + TransportLayerProtocol.TLS + "> or <" + Scheme.HTTPS.toProtocol() + ">, falling back <" + TransportLayerProtocol.TLS + ">." );
			}
			aProtocol = TransportLayerProtocol.TLS.name();
		}
		if ( aStoreDescriptor == null ) {
			aStoreDescriptor = getKeyStoreDescriptor();
		}
		if ( aMaxConnections < 0 ) {
			aMaxConnections = getMaxConnections();
		}
		// HTTP |-->
		if ( ( theScheme == null && aProtocol == null ) || theScheme == Scheme.HTTP ) {
			try {
				final com.sun.net.httpserver.HttpServer theHttpServer = com.sun.net.httpserver.HttpServer.create();
				theHttpServer.bind( new InetSocketAddress( aPort ), aMaxConnections );
				open( theHttpServer );
			}
			catch ( IOException e ) {
				throw new IOException( "Unable to bind to port <" + aPort + ">!", e );
			}
		}
		// HTTP <--|
		// HTTPS |-->
		else {
			try {
				final InetSocketAddress theAddress = new InetSocketAddress( aPort );
				final SSLContext theSSLContext = SSLContext.getInstance( aProtocol ); // "TLS"
				final KeyStore theKeyStore = KeyStore.getInstance( aStoreDescriptor.getStoreType().name() ); // "JKS"
				final FileInputStream theKeystoreInputStream = new FileInputStream( aStoreDescriptor.getStoreFile() );
				final KeyManagerFactory theKeyManagerFactory = KeyManagerFactory.getInstance( KeyManagerFactory.getDefaultAlgorithm() );
				final TrustManagerFactory theTrustManagerFactory = TrustManagerFactory.getInstance( TrustManagerFactory.getDefaultAlgorithm() );
				final HttpsServer theHttpsServer = HttpsServer.create( theAddress, aMaxConnections );
				theKeyStore.load( theKeystoreInputStream, aStoreDescriptor.getStorePassword().toCharArray() );
				theKeyManagerFactory.init( theKeyStore, aStoreDescriptor.getKeyPassword().toCharArray() );
				theTrustManagerFactory.init( theKeyStore );
				theSSLContext.init( theKeyManagerFactory.getKeyManagers(), theTrustManagerFactory.getTrustManagers(), null );
				theHttpsServer.setHttpsConfigurator( new HttpsRestConfigurator( theSSLContext ) );
				theHttpsServer.setExecutor( null ); // creates default executor
				open( theHttpsServer );
			}
			catch ( IOException | NoSuchAlgorithmException | KeyStoreException | CertificateException | UnrecoverableKeyException | KeyManagementException e ) {
				throw new IOException( "Unable to bind to port <" + aPort + ">: " + Trap.asMessage( e ), e );
			}
		}
		// HTTPS <--|
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConnectionStatus getConnectionStatus() {
		return _connectionStatus;
	}

	// /////////////////////////////////////////////////////////////////////////
	// MEHTODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpExceptionHandler getHttpExceptionHandler() {
		return _httpExceptionHandler;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpExceptionHandler( HttpExceptionHandler aHttpErrorHandler ) {
		_httpExceptionHandler = aHttpErrorHandler;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpExceptionHandling getHttpExceptionHandling() {
		return _httpExceptionHandling;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpExceptionHandling( HttpExceptionHandling aHttpErrorHandling ) {
		_httpExceptionHandling = aHttpErrorHandling;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestfulHttpServer onConnectionRequest( HttpsConnectionRequestObserver aObserver ) {
		_httpsConnectionRequestObserver = aObserver;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestfulHttpServer onBasicAuthRequest( BasicAuthObserver aBasicAuthObserver ) {
		final HttpContext theHttpContext = _httpContext;
		HttpBasicAuthenticator theHttpBasicAuthenticator = null;
		if ( aBasicAuthObserver != null ) {
			theHttpBasicAuthenticator = new HttpBasicAuthenticator( aBasicAuthObserver );
			if ( theHttpContext != null ) {
				theHttpContext.setAuthenticator( theHttpBasicAuthenticator );
			}
		}
		_httpBasicAuthenticator = theHttpBasicAuthenticator;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPort( int aPort ) {
		_port = aPort;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPort() {
		return _port;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setScheme( Scheme aScheme ) {
		_scheme = aScheme;
		_protocol = null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Scheme getScheme() {
		return _scheme;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toProtocol() {
		return _scheme != null ? _scheme.toProtocol() : _protocol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setProtocol( String aProtocol ) {
		final Scheme theScheme = Scheme.fromProtocol( aProtocol );
		if ( theScheme != null ) {
			_scheme = theScheme;
			_protocol = null;
		}
		else {
			_protocol = aProtocol;

			_scheme = null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public KeyStoreDescriptor getKeyStoreDescriptor() {
		return _keyStoreDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setKeyStoreDescriptor( KeyStoreDescriptor aKeyStoreDescriptor ) {
		_keyStoreDescriptor = aKeyStoreDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getMaxConnections() {
		return _maxConnections;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMaxConnections( int aMaxConnections ) {
		_maxConnections = aMaxConnections;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INTERCEPTOS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasPreHttpInterceptor( PreHttpServerInterceptor aPreInterceptor ) {
		return _preHttpInterceptos.contains( aPreInterceptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addPreHttpInterceptor( PreHttpServerInterceptor aPreInterceptor ) {
		if ( !_preHttpInterceptos.contains( aPreInterceptor ) ) {
			return _preHttpInterceptos.add( aPreInterceptor );
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean removePreHttpInterceptor( PreHttpServerInterceptor aPreInterceptor ) {
		return _preHttpInterceptos.remove( aPreInterceptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasPostHttpInterceptor( PostHttpServerInterceptor aPostInterceptor ) {
		return _postHttpInterceptos.contains( aPostInterceptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addPostHttpInterceptor( PostHttpServerInterceptor aPostInterceptor ) {
		if ( !_postHttpInterceptos.contains( aPostInterceptor ) ) {
			return _postHttpInterceptos.add( aPostInterceptor );
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean removePostHttpInterceptor( PostHttpServerInterceptor aPostInterceptor ) {
		return _postHttpInterceptos.remove( aPostInterceptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withRealm( String aRealm ) {
		setRealm( aRealm );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withCloseUnchecked() {
		closeUnchecked();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withObserversActive( boolean isActive ) {
		setObserversActive( isActive );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withEnableObservers() {
		enableObservers();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withDisableObservers() {
		disableObservers();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withOnHttpException( HttpExceptionHandler aHttpExceptionHandler ) {
		onHttpException( aHttpExceptionHandler );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withOpenUnchecked( HttpServerContext aConnection ) {
		openUnchecked( aConnection );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withHttpExceptionHandler( HttpExceptionHandler aHttpErrorHandler ) {
		setHttpExceptionHandler( aHttpErrorHandler );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withHttpExceptionHandling( HttpExceptionHandling aHttpErrorHandling ) {
		setHttpExceptionHandling( aHttpErrorHandling );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withBaseLocator( String aBaseLocator ) {
		setBaseLocator( aBaseLocator );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withClose() throws IOException {
		close();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withCloseQuietly() {
		closeQuietly();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withCloseIn( int aCloseMillis ) {
		closeIn( aCloseMillis );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withOpen( HttpServerContext aConnection ) throws IOException {
		open( aConnection );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withOpen( int aPort ) throws IOException {
		open( aPort );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withPort( int aPort ) {
		setPort( aPort );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withScheme( Scheme aScheme ) {
		setScheme( aScheme );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withProtocol( String aProtocol ) {
		setProtocol( aProtocol );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withKeyStoreDescriptor( KeyStoreDescriptor aKeyStoreDescriptor ) {
		setKeyStoreDescriptor( aKeyStoreDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestServer withMaxConnections( int aMaxConnections ) {
		setMaxConnections( aMaxConnections );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void preIntercept( HttpServerRequest aRequest, HttpServerResponse aResponse ) {
		for ( PreHttpServerInterceptor eInterceptor : _preHttpInterceptos ) {
			eInterceptor.preIntercept( aRequest, aResponse );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void postIntercept( HttpServerRequest aRequest, HttpServerResponse aResponse ) {
		for ( PostHttpServerInterceptor eInterceptor : _postHttpInterceptos ) {
			eInterceptor.postIntercept( aRequest, aResponse );
		}
	}

	/**
	 * Gets the HTTP server.
	 *
	 * @return the HTTP server
	 */
	protected com.sun.net.httpserver.HttpServer getHttpServer() {
		return _httpServer;
	}

	/**
	 * A hook to be used when using custom {@link RestfulHttpServer} (
	 * {@link HttpsServer}) by custom open(...) methods of sub-classes of this
	 * {@link HttpRestServer}. E.g {@link HttpRestServer} uses this hook to
	 * pre-configure a {@link HttpsServer} for HTTPS. The passed
	 * {@link RestfulHttpServer} ( {@link HttpsServer}) must already be bound to
	 * a port and enabled with the number of concurrent connections as of
	 * {@link com.sun.net.httpserver.HttpServer#bind(InetSocketAddress, int)}.
	 * 
	 * @param aHttpServer The {@link RestfulHttpServer} to be used. E.g. an
	 *        {@link HttpsServer} might be used to enable HTTPS.
	 * 
	 * @throws IOException in case opening with the provided
	 *         {@link RestfulHttpServer} fails.
	 */
	protected void open( com.sun.net.httpserver.HttpServer aHttpServer ) throws IOException {
		if ( _connectionStatus == ConnectionStatus.OPENED ) {
			throw new IOException( "Connection is still in status <" + _connectionStatus + ">. Close the connection before reopening!" );
		}
		if ( _executorService != null ) {
			aHttpServer.setExecutor( _executorService );
		}
		final HttpContext theHttpContext = aHttpServer.createContext( CONTEXT_PATH, new EndpointHttpHandler() );
		final HttpBasicAuthenticator theHttpBasicAuthenticator = _httpBasicAuthenticator;
		if ( theHttpBasicAuthenticator != null ) {
			theHttpContext.setAuthenticator( theHttpBasicAuthenticator );
		}
		aHttpServer.start();
		_httpServer = aHttpServer;
		_httpContext = theHttpContext;
		_connectionStatus = ConnectionStatus.OPENED;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private Authenticator.Result toBasicAuthFailure( HttpExchange aHttpExchange ) {
		return new Authenticator.Failure( HttpStatusCode.UNAUTHORIZED.getStatusCode() );
	}

	private Authenticator.Success toBasicAuthSuccess( String aIdentity ) {
		return new Authenticator.Success( new HttpPrincipal( aIdentity, getRealm() ) );
	}

	//	private Authenticator.Success toBasicAuthSkip( String aIdentity ) {
	//		return new Authenticator.Success( new HttpPrincipal( aIdentity, getRealm() ) );
	//	}

	private Authenticator.Result toBasicAuthRequired( HttpExchange aHttpExchange ) {
		final Headers theHeaders = aHttpExchange.getResponseHeaders();
		doBasicAuthRequired( theHeaders );
		return new Authenticator.Retry( HttpStatusCode.UNAUTHORIZED.getStatusCode() );
	}

	private void doBasicAuthRequired( Headers aHeaders ) {
		aHeaders.set( HeaderField.WWW_AUTHENTICATE.getName(), HeaderFields.BASIC_REALM + "=\"" + getRealm() + "\"" );
	}

	///////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	///////////////////////////////////////////////////////////////////////////

	/**
	 * Extension of the {@link com.sun.net.httpserver.Authenticator} for doing
	 * custom HTTP Basic-Authentication.
	 */
	private class HttpBasicAuthenticator extends Authenticator {

		private final BasicAuthObserver _basicAuthObserver;

		/**
		 * Instantiates a new HTTP basic authenticator.
		 *
		 * @param aBasicAuthObserver the basic auth observer
		 */
		public HttpBasicAuthenticator( BasicAuthObserver aBasicAuthObserver ) {
			_basicAuthObserver = aBasicAuthObserver;
		}

		/**
		 * Authenticate.
		 *
		 * @param aHttpExchange The {@link HttpExchange} doing the "physical"
		 *        HTTP-Response.
		 * 
		 * @return the result
		 */
		@Override
		public Result authenticate( HttpExchange aHttpExchange ) {
			final Headers theRequestHeaders = aHttpExchange.getRequestHeaders();
			final String theAuthHeader = theRequestHeaders.getFirst( HeaderField.AUTHORIZATION.getName() );
			BasicAuthCredentials theCredentials = null;
			if ( theAuthHeader == null ) {
				final BasicAuthResponse theBasicAuthResponse = _basicAuthObserver.onBasicAuthRequest( aHttpExchange.getLocalAddress(), aHttpExchange.getRemoteAddress(), HttpMethod.fromHttpMethod( aHttpExchange.getRequestMethod() ), aHttpExchange.getRequestURI().getPath(), theCredentials, getRealm() );
				if ( theBasicAuthResponse != BasicAuthResponse.BASIC_AUTH_SUCCESS ) {
					return toBasicAuthRequired( aHttpExchange );
				}
				return new Authenticator.Success( new HttpPrincipal( ANONYMOUS, getRealm() ) );
			}
			else {
				int theMarker = theAuthHeader.indexOf( ' ' );
				if ( theMarker == -1 || !theAuthHeader.substring( 0, theMarker ).equals( AuthType.BASIC.getName() ) ) {
					return toBasicAuthFailure( aHttpExchange );
				}
				final byte[] theCredentialChars = Base64.getDecoder().decode( theAuthHeader.substring( theMarker + 1 ) );
				final String theCredentialsText = new String( theCredentialChars );
				theMarker = theCredentialsText.indexOf( ':' );
				theCredentials = new BasicAuthCredentials( theCredentialsText.substring( 0, theMarker ), theCredentialsText.substring( theMarker + 1 ) );
			}
			final BasicAuthResponse theBasicAuthResponse = _basicAuthObserver.onBasicAuthRequest( aHttpExchange.getLocalAddress(), aHttpExchange.getRemoteAddress(), HttpMethod.fromHttpMethod( aHttpExchange.getRequestMethod() ), aHttpExchange.getRequestURI().getPath(), theCredentials, getRealm() );
			if ( theBasicAuthResponse == null ) {
				throw new NullPointerException( "Your <HttpBasicAuthenticator> instance must return an element of type <BasicAuthResponse> and not null." );
			}
			return switch ( theBasicAuthResponse ) {
			case BASIC_AUTH_SUCCESS -> toBasicAuthSuccess( theCredentials.getIdentity() );
			case BASIC_AUTH_REQUIRED -> toBasicAuthRequired( aHttpExchange );
			case BASIC_AUTH_FAILURE -> toBasicAuthFailure( aHttpExchange );
			// case BASIC_AUTH_SKIP -> toBasicAuthSkip( theCredentials.getIdentity() );
			default -> throw new UnhandledEnumBugException( theBasicAuthResponse );
			};
		}
	}

	/**
	 * Main {@link com.sun.net.httpserver.HttpHandler} managing the dispatch of
	 * incoming requests to the registered {@link RestRequestConsumer} instances
	 * depending on the according {@link RestEndpoint}'s Locator-Pattern, HTTP
	 * method and so on.
	 */
	private class EndpointHttpHandler implements HttpHandler {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void handle( HttpExchange aHttpExchange ) throws IOException {
			final HttpMethod theHttpMethod = HttpMethod.fromHttpMethod( aHttpExchange.getRequestMethod() );
			if ( theHttpMethod != null ) {
				final URI theRequestURI = aHttpExchange.getRequestURI();
				final HttpServerResponse theHttpServerResponse = new HttpServerResponse( HttpRestServer.this );
				final InetSocketAddress theLocalAddress = aHttpExchange.getLocalAddress();
				final InetSocketAddress theRemoteAddress = aHttpExchange.getRemoteAddress();
				final Headers theRequestHeaders = aHttpExchange.getRequestHeaders();
				final RequestHeaderFields theRequestHeaderFields = new RequestHeaderFields( theRequestHeaders );
				final UrlBuilder theUrl = new UrlBuilder( theRequestURI.toString() );
				if ( theUrl.getScheme() == null ) {
					Scheme theScheme = _scheme;
					if ( theScheme == null ) {
						theScheme = TransportLayerProtocol.toScheme( _protocol );
					}
					theUrl.setScheme( theScheme );
				}
				if ( theUrl.getHost() == null ) {
					theUrl.setHost( Literal.LOCALHOST.getValue() );
				}
				if ( theUrl.getPort() == -1 ) {
					theUrl.setPort( _port );
				}
				try {
					onHttpRequest( aHttpExchange, theLocalAddress, theRemoteAddress, theHttpMethod, theUrl, theRequestHeaderFields, aHttpExchange.getRequestBody(), theHttpServerResponse );
				}
				catch ( BasicAuthRequiredException e ) {
					doBasicAuthRequired( aHttpExchange.getResponseHeaders() );
					if ( SystemProperty.LOG_DEBUG.isEnabled() || _isVerbose ) {
						LOGGER.log( Level.INFO, "Required HTTP Basic-Authentication with status <" + e.getStatusCode() + "> with code <" + e.getStatusCode().getStatusCode() + "> for request URL <" + theRequestURI + "> with request method <" + aHttpExchange.getRequestMethod() + ">: " + Trap.asMessage( e ) );
					}
					onHttpException( aHttpExchange, theLocalAddress, theRemoteAddress, theHttpMethod, theUrl, theRequestHeaderFields, theHttpServerResponse, e, e.getStatusCode() );
				}
				catch ( HttpStatusException e ) {
					if ( SystemProperty.LOG_DEBUG.isEnabled() || _isVerbose ) {
						LOGGER.log( Level.WARNING, "Responding status <" + e.getStatusCode() + "> with code <" + e.getStatusCode().getStatusCode() + "> for request URL <" + theRequestURI + "> with request method <" + aHttpExchange.getRequestMethod() + "> as of: " + Trap.asMessage( e ), e );
					}
					onHttpException( aHttpExchange, theLocalAddress, theRemoteAddress, theHttpMethod, theUrl, theRequestHeaderFields, theHttpServerResponse, e, e.getStatusCode() );
				}
				catch ( Exception e ) {
					if ( SystemProperty.LOG_DEBUG.isEnabled() || _isVerbose ) {
						LOGGER.log( Level.WARNING, "Bad request <" + e.getClass().getName() + "> for request URL <" + theRequestURI + "> with request method <" + aHttpExchange.getRequestMethod() + "> as of: " + Trap.asMessage( e ), e );
					}
					onHttpException( aHttpExchange, theLocalAddress, theRemoteAddress, theHttpMethod, theUrl, theRequestHeaderFields, theHttpServerResponse, e, HttpStatusCode.BAD_REQUEST );
				}
			}
			else {
				LOGGER.log( Level.WARNING, "Unknown HTTP-Method <" + aHttpExchange.getRequestMethod() + "> when querying resource locator <" + aHttpExchange.getLocalAddress() + ">." );
				aHttpExchange.sendResponseHeaders( HttpStatusCode.INTERNAL_SERVER_ERROR.getStatusCode(), NO_RESPONSE_BODY );
			}
		}

		/**
		 * Invokes the registered {@link HttpExceptionHandler} instance (if any)
		 * upon an Exception whilst processing an incoming HTTP-Request.
		 * 
		 * @param aHttpExchange The {@link HttpExchange} doing the "physical"
		 *        HTTP-Response.
		 * @param aLocalAddress The host and port of your REST service.
		 * @param aRemoteAddress The host and port for the caller.
		 * @param aHttpMethod The {@link HttpMethod} of the request.
		 * @param aUrl The {@link Url} from which to take the URL specific data.
		 * @param aRequestHeaderFields The Header-Fields ({@link HeaderFields})
		 *        belonging to the request.
		 * @param aHttpInputStream The body passed by the request.
		 * @param aException The cause of the problems.
		 * @param aHttpServerResponse The {@link HttpServerResponse} to be
		 *        worked with
		 * @param aHttpStatusCode The status code to be set for the response.
		 * 
		 * @return A {@link HttpServerResponse} instance to by used by the
		 *         extension to produce an according HTTP-Response.
		 * 
		 * @throws HttpStatusException thrown in case of an {@link RestEndpoint}
		 *         responsible for the given request encountered a problem or
		 *         none {@link RestEndpoint} felt responsible to produce a
		 *         {@link HttpServerResponse}.
		 * @throws IOException Thrown in case even the {@link HttpExchange}
		 *         failed to process.
		 * @throws MarshalException Thrown when marshaling / serializing the
		 *         response failed.
		 * @throws UnsupportedMediaTypeException Thrown in case a Media-Type has
		 *         been provided which is not supported.
		 */
		protected void onHttpException( HttpExchange aHttpExchange, InetSocketAddress aLocalAddress, InetSocketAddress aRemoteAddress, HttpMethod aHttpMethod, UrlBuilder aUrl, RequestHeaderFields aRequestHeaderFields, HttpServerResponse aHttpServerResponse, Exception aException, HttpStatusCode aHttpStatusCode ) throws IOException {
			aHttpServerResponse.setHttpStatusCode( aHttpStatusCode );
			if ( _httpExceptionHandler != null ) {
				try {
					final RestRequestEvent theRestRequestEvent = new RestRequestEvent( aLocalAddress, aRemoteAddress, aHttpMethod, aUrl, null, aRequestHeaderFields, aHttpExchange.getRequestBody(), HttpRestServer.this );
					_httpExceptionHandler.onHttpError( theRestRequestEvent, aHttpServerResponse, aException, aHttpStatusCode );
					finalizeMediaType( aHttpServerResponse );
					doHttpResponse( aHttpExchange, aRequestHeaderFields, aHttpServerResponse, aHttpStatusCode );
				}
				catch ( Exception failed ) {
					finalizeHttpExceptionResponse( aHttpExchange, aException, !aHttpServerResponse.getHttpStatusCode().isErrorStatus() ? aHttpStatusCode : aHttpServerResponse.getHttpStatusCode() );
				}
			}
			else {
				finalizeHttpExceptionResponse( aHttpExchange, aException, aHttpStatusCode );
			}
		}

		/**
		 * If an unsupported Media-Type is set, then the first supported
		 * Media-Type is set. We assume that we already have (if any) taken over
		 * the Media-Type from the according request. Now we finalize for
		 * erroneous situations to make sure we support the Media-Type
		 * 
		 * @param aHttpServerResponse The {@link HttpServerResponse} for which
		 *        to finalize the Media-Type.
		 */
		protected void finalizeMediaType( HttpServerResponse aHttpServerResponse ) {
			if ( aHttpServerResponse.getResponse() != null ) {
				final ContentType theContentType = aHttpServerResponse.getHeaderFields().getContentType();
				if ( theContentType == null || !HttpRestServer.this.hasMediaTypeFactory( theContentType.getMediaType() ) ) {
					final String theTypeText = theContentType != null ? theContentType.toHttpMediaType() : null;
					if ( SystemProperty.LOG_DEBUG.isEnabled() || _isVerbose ) {
						LOGGER.log( Level.WARNING, "Unsupported Content-Type <" + theTypeText + "> detected in HTTP-Server-Response, trying fallback ..." );
					}
					if ( HttpRestServer.this.getFactoryMediaTypes().length == 0 ) {
						if ( SystemProperty.LOG_DEBUG.isEnabled() || _isVerbose ) {
							LOGGER.log( Level.WARNING, "No fallback Content-Type detected for HTTP-Server-Response as no Media-Type-Factories have been configured ..." );
						}
					}
					else if ( HttpRestServer.this.getFactoryMediaTypes().length != 0 ) {
						final MediaType theMediaType = HttpRestServer.this.getFactoryMediaTypes()[0];
						if ( SystemProperty.LOG_DEBUG.isEnabled() || _isVerbose ) {
							LOGGER.log( Level.INFO, "Using fallback Content-Type <" + theMediaType.toHttpMediaType() + "> for HTTP-Server-Response ..." );
						}
						aHttpServerResponse.getHeaderFields().putContentType( theMediaType );
					}
				}
			}
		}

		/**
		 * Does the HTTP-Response handling such as content negotiation, body
		 * construction and status code processing.
		 * 
		 * @param aHttpExchange The {@link HttpExchange} doing the "physical"
		 *        HTTP-Response.
		 * @param aRequestHeaderFields The Header-Fields of the original
		 *        request.
		 * @param aHttpServerResponse The constructed {@link HttpServerResponse}
		 *        to be sent back to the client.
		 * @param aHttpStatusCode The status code to be set for the response.
		 * 
		 * @throws IOException Thrown in case even the {@link HttpExchange}
		 *         failed to process.
		 * @throws MarshalException Thrown when marshaling / serializing the
		 *         response failed.
		 * @throws UnsupportedMediaTypeException Thrown in case a Media-Type has
		 *         been provided which is not supported.
		 */
		protected void doHttpResponse( HttpExchange aHttpExchange, RequestHeaderFields aRequestHeaderFields, HttpServerResponse aHttpServerResponse, HttpStatusCode aHttpStatusCode ) throws IOException, MarshalException, UnsupportedMediaTypeException {
			final Headers theResponseHeaders = aHttpExchange.getResponseHeaders();
			for ( String eKey : aHttpServerResponse.getHeaderFields().keySet() ) {
				theResponseHeaders.put( eKey, aHttpServerResponse.getHeaderFields().get( eKey ) );
			}
			final Object theResponse = aHttpServerResponse.getResponse();
			if ( ( theResponse instanceof InputStream ) ) {
				aHttpExchange.sendResponseHeaders( aHttpStatusCode.getStatusCode(), CHUNCKED_ENCODING );
				final InputStream theInputStream = (InputStream) theResponse;
				HttpRestClient.pipe( theInputStream, aHttpExchange.getResponseBody() );
				aHttpExchange.getResponseBody().flush();
			}
			else {
				byte[] theBytes = null;
				if ( theResponse != null ) {
					try {
						theBytes = aHttpServerResponse.toHttpBody().getBytes();
					}
					catch ( BadResponseException e ) {
						if ( SystemProperty.LOG_DEBUG.isEnabled() || _isVerbose ) {
							LOGGER.log( Level.WARNING, "Trying fallback procedure as of: " + Trap.asMessage( e ), e );
						}
						theBytes = toResponseBody( aHttpServerResponse.getResponse(), aRequestHeaderFields, aHttpServerResponse.getHeaderFields() );
					}
				}
				if ( theBytes != null && theBytes.length != 0 ) {
					aHttpExchange.sendResponseHeaders( aHttpStatusCode.getStatusCode(), theBytes.length );
					aHttpExchange.getResponseBody().write( theBytes );
					aHttpExchange.getResponseBody().flush();
				}
				else {
					aHttpExchange.sendResponseHeaders( aHttpStatusCode.getStatusCode(), NO_RESPONSE_BODY );
				}
			}
			try {
				aHttpExchange.getResponseBody().close();
			}
			// Seems to be a known issue in case of NO_CONTENT <403>, see "https://github.com/http4k/http4k/issues/598" |-->
			catch ( AssertionError ignore ) {
				if ( SystemProperty.LOG_DEBUG.isEnabled() || _isVerbose ) {
					LOGGER.log( Level.WARNING, "Encountered an assertion error as of: " + Trap.asMessage( ignore ), ignore );
				}
			}
			// Seems to be a known issue in case of NO_CONTENT <403>, see "https://github.com/http4k/http4k/issues/598" <--|
		}

		/**
		 * If nothing more is possible, finish with minimum means to get
		 * response through to the client.
		 * 
		 * @param aHttpExchange The {@link HttpExchange} doing the "physical"
		 *        HTTP-Response.
		 * @param aException The cause of the problems.
		 * @param aHttpStatusCode The status code to be set for the
		 *        HTTP-Response.
		 * 
		 * @throws IOException Thrown in case even the {@link HttpExchange}
		 *         failed to process.
		 */
		protected void finalizeHttpExceptionResponse( HttpExchange aHttpExchange, Exception aException, HttpStatusCode aHttpStatusCode ) throws IOException {
			LOGGER.log( Level.WARNING, "Unable to fully satisfy <" + getHttpExceptionHandling() + "> mode, falling back to <" + HttpExceptionHandling.EMPTY + "> as of: " + Trap.asMessage( aException ), aException );
			aHttpExchange.sendResponseHeaders( aHttpStatusCode.getStatusCode(), NO_RESPONSE_BODY );
			aHttpExchange.getResponseBody().close();
		}

		private void onHttpRequest( HttpExchange aHttpExchange, InetSocketAddress aLocalAddress, InetSocketAddress aRemoteAddress, HttpMethod aHttpMethod, UrlBuilder aUrl, RequestHeaderFields aRequestHeaderFields, InputStream aHttpInputStream, HttpServerResponse aHttpServerResponse ) throws HttpStatusException, MarshalException, IOException {
			HttpRestServer.super.onHttpRequest( aLocalAddress, aRemoteAddress, aHttpMethod, aUrl, aRequestHeaderFields, aHttpInputStream, aHttpServerResponse );
			HttpStatusCode theHttpStatusCode = aHttpServerResponse.getHttpStatusCode();
			if ( theHttpStatusCode == null ) {
				theHttpStatusCode = aHttpServerResponse.getResponse() == null ? HttpStatusCode.NO_CONTENT : HttpStatusCode.OK;
			}
			doHttpResponse( aHttpExchange, aRequestHeaderFields, aHttpServerResponse, theHttpStatusCode );
		}
	}

	private class DefaultErrorHandler implements HttpExceptionHandler {

		@Override
		public void onHttpError( RestRequestEvent aRequestEvent, HttpServerResponse aHttpServerResponse, Exception aException, HttpStatusCode aHttpStatusCode ) {
			final HttpBodyMap theBodyMap;
			switch ( getHttpExceptionHandling() ) {
			case EMPTY -> {
				aHttpServerResponse.setResponse( null );
				aHttpServerResponse.getHeaderFields().clear();
			}
			case KEEP -> {
			}
			case MERGE -> {
				theBodyMap = toResponseBodyMap( aHttpServerResponse );
				if ( !theBodyMap.hasStatusCode() ) {
					theBodyMap.putStatusCode( aHttpStatusCode );
				}
				if ( !theBodyMap.hasStatusAlias() ) {
					theBodyMap.putStatusAlias( aHttpStatusCode );
				}
				if ( !theBodyMap.hasStatusException() ) {
					theBodyMap.putStatusException( aException );
				}
				if ( !theBodyMap.hasStatusMessage() && aException.getMessage() != null && aException.getMessage().length() > 0 ) {
					theBodyMap.putStatusMessage( aException.getMessage() );
				}
				if ( !theBodyMap.hasStatusTimeStamp() ) {
					theBodyMap.putStatusTimeStamp();
				}
				aHttpServerResponse.setResponse( theBodyMap );
			}
			case UPDATE -> {
				theBodyMap = toResponseBodyMap( aHttpServerResponse );
				theBodyMap.putStatusCode( aHttpStatusCode );
				theBodyMap.putStatusAlias( aHttpStatusCode );
				theBodyMap.putStatusException( aException );
				theBodyMap.putStatusMessage( aException.getMessage() );
				theBodyMap.putStatusTimeStamp();
				aHttpServerResponse.setResponse( theBodyMap );
			}
			case REPLACE -> {
				theBodyMap = new HttpBodyMap();
				theBodyMap.putStatusCode( aHttpStatusCode );
				theBodyMap.putStatusAlias( aHttpStatusCode );
				theBodyMap.putStatusException( aException );
				theBodyMap.putStatusMessage( aException.getMessage() );
				theBodyMap.putStatusTimeStamp();
				aHttpServerResponse.setResponse( theBodyMap );
			}
			default -> throw new UnhandledEnumBugException( getHttpExceptionHandling() );
			}
		}

		/**
		 * Creates a {@link HttpBodyMap} from the provided HTTP-Response.
		 * Usually used in erroneous situations. In case
		 * {@link HttpServerResponse#getResponse()} returns a
		 * {@link HttpBodyMap}, then this is returned. In case
		 * {@link HttpServerResponse#getResponse()} returns an
		 * {@link InputStream}, then a new {@link HttpBodyMap} is returned: This
		 * is to prevent providing the stream's data in an exceptional state.
		 * 
		 * @param aHttpServerResponse The {@link HttpServerResponse} from which
		 *        to extract the response representing {@link HttpBodyMap}.
		 * 
		 * @return The {@link HttpBodyMap} representation of the HTTP-Response's
		 *         response object.
		 */
		protected HttpBodyMap toResponseBodyMap( HttpServerResponse aHttpServerResponse ) {
			HttpBodyMap theHttpBodyMap = null;
			final Object theResponse = aHttpServerResponse.getResponse();
			if ( theResponse != null ) {
				if ( theResponse instanceof HttpBodyMap ) {
					theHttpBodyMap = (HttpBodyMap) theResponse;
				}
				else if ( !( theResponse instanceof InputStream ) ) {
					theHttpBodyMap = new HttpBodyMap( aHttpServerResponse.getResponse() );
				}
			}
			if ( theHttpBodyMap == null ) {
				theHttpBodyMap = new HttpBodyMap();
			}
			return theHttpBodyMap;
		}
	}

	/**
	 * This class is used to configure the HTTPS parameters for each incoming
	 * HTTPS connection in order to change the default configuration:.
	 */
	private class HttpsRestConfigurator extends HttpsConfigurator {

		/**
		 * {@inheritDoc}
		 */
		public HttpsRestConfigurator( SSLContext aSSLContext ) {
			super( aSSLContext );
		}

		/**
		 * Not sure on how to proceed with
		 * {@link HttpsParameters#setNeedClientAuth(boolean)}. Do we need to
		 * propagate such a flag back from
		 * {@link HttpsConnectionRequestObserver} to the
		 * {@link HttpsParameters#setNeedClientAuth(boolean)}?
		 *
		 * @param aHttpsParams the the https params
		 */
		@Override
		public void configure( HttpsParameters aHttpsParams ) {
			final HttpsConnectionRequestObserver theObserver = _httpsConnectionRequestObserver;
			if ( theObserver != null ) {
				final InetSocketAddress theRemoteAddress = aHttpsParams.getClientAddress();
				final com.sun.net.httpserver.HttpServer theServer = getHttpServer();
				InetSocketAddress theLocalAddress = null;
				if ( theServer != null ) {
					theLocalAddress = theServer.getAddress();
				}
				else {
					LOGGER.log( Level.WARNING, "Unable to determine the local address for remote address <" + theRemoteAddress.toString() + ">, the server might have been closed in the meantime." );
				}
				final SSLContext theSSLContext = getSSLContext();
				final SSLParameters theSSLParams = theSSLContext.getDefaultSSLParameters();
				theObserver.onHttpsConnectionRequest( theLocalAddress, theRemoteAddress, theSSLParams );
				aHttpsParams.setSSLParameters( theSSLParams );
			}
		}
	}
}
