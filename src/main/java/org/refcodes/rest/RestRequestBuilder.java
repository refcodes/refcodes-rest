// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.textual.VerboseTextBuilder;
import org.refcodes.web.HeaderFieldsAccessor.HeaderFieldsBuilder;
import org.refcodes.web.HttpClientRequest;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpMethodAccessor.HttpMethodBuilder;
import org.refcodes.web.HttpMethodAccessor.HttpMethodProperty;
import org.refcodes.web.HttpRequestBuilder;
import org.refcodes.web.HttpResponseException;
import org.refcodes.web.HttpStatusCode;
import org.refcodes.web.RedirectDepthAccessor.RedirectDepthBuilder;
import org.refcodes.web.RequestHeaderFields;
import org.refcodes.web.Url;

/**
 * The implementation of the {@link RestResponseHandler} interface as good old
 * POJO for use by different {@link RestfulClient} implementations.
 */
public class RestRequestBuilder extends RestRequest implements RedirectDepthBuilder<RestRequestBuilder>, HttpRequestBuilder<RestRequestBuilder>, HttpMethodProperty, HttpMethodBuilder<RestRequestBuilder>, HeaderFieldsBuilder<RequestHeaderFields, RestRequestBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private RestfulClient _restClient;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new rest request builder impl.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields the Header-Fields
	 * @param aRequest the request
	 * @param aRestClient the rest client
	 */
	protected RestRequestBuilder( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, RestfulClient aRestClient ) {
		this( aHttpMethod, aUrl instanceof org.refcodes.web.UrlBuilder ? aUrl : new org.refcodes.web.UrlBuilder( aUrl ), aHeaderFields, aRequest, HttpClientRequest.DEFAULT_REDIRECT_DEPTH, aRestClient );
	}

	/**
	 * Instantiates a new rest request builder impl.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRestClient the rest client
	 */
	protected RestRequestBuilder( HttpMethod aHttpMethod, Url aUrl, RestfulClient aRestClient ) {
		this( aHttpMethod, aUrl, null, null, aRestClient );
	}

	/**
	 * Instantiates a new rest request builder impl.
	 *
	 * @param aRestClient the rest client
	 */
	protected RestRequestBuilder( RestfulClient aRestClient ) {
		this( null, null, null, null, aRestClient );
	}

	/**
	 * Instantiates a new rest request builder impl.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest the request
	 * @param aRestClient the rest client
	 */
	protected RestRequestBuilder( HttpMethod aHttpMethod, Url aUrl, Object aRequest, RestfulClient aRestClient ) {
		this( aHttpMethod, aUrl, null, aRequest, aRestClient );
	}

	/**
	 * Instantiates a new rest request builder impl.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRedirectDepth The redirect depth provides the count of
	 *        HTTP-Request and HTTP-Response cycles where the response
	 *        represents a redirect as of
	 *        {@link HttpStatusCode#isRedirectStatus()}. A value of -1
	 *        represents the default behavior, e.g. using
	 *        {@link java.net.HttpURLConnection}'s redirection means.
	 * @param aRestClient the rest client
	 */
	protected RestRequestBuilder( HttpMethod aHttpMethod, Url aUrl, int aRedirectDepth, RestfulClient aRestClient ) {
		this( aHttpMethod, aUrl, null, null, aRedirectDepth, aRestClient );
	}

	/**
	 * Instantiates a new rest request builder impl.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest the request
	 * @param aRedirectDepth The redirect depth provides the count of
	 *        HTTP-Request and HTTP-Response cycles where the response
	 *        represents a redirect as of
	 *        {@link HttpStatusCode#isRedirectStatus()}. A value of -1
	 *        represents the default behavior, e.g. using
	 *        {@link java.net.HttpURLConnection}'s redirection means.
	 * @param aRestClient the rest client
	 */
	protected RestRequestBuilder( HttpMethod aHttpMethod, Url aUrl, Object aRequest, int aRedirectDepth, RestfulClient aRestClient ) {
		this( aHttpMethod, aUrl, null, aRequest, aRedirectDepth, aRestClient );
	}

	/**
	 * Instantiates a new rest request builder impl.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields the Header-Fields
	 * @param aRequest the request
	 * @param aRedirectDepth The redirect depth provides the count of
	 *        HTTP-Request and HTTP-Response cycles where the response
	 *        represents a redirect as of
	 *        {@link HttpStatusCode#isRedirectStatus()}. A value of -1
	 *        represents the default behavior, e.g. using
	 *        {@link java.net.HttpURLConnection}'s redirection means.
	 * @param aRestClient the rest client
	 */
	protected RestRequestBuilder( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, int aRedirectDepth, RestfulClient aRestClient ) {
		super( aHttpMethod, aUrl instanceof UrlBuilder ? aUrl : new org.refcodes.web.UrlBuilder( aUrl ), aHeaderFields, aRequest, aRedirectDepth, aRestClient );
		_restClient = aRestClient;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestRequestBuilder withUrl( Url aUrl ) {
		setUrl( aUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestRequestBuilder withHttpMethod( HttpMethod aHttpMethod ) {
		setHttpMethod( aHttpMethod );
		return this;
	}

	/**
	 * Sets the request for the request property.
	 *
	 * @param <REQ> the generic type
	 * @param aRequest The request to be stored by the request property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public <REQ> RestRequestBuilder withRequest( REQ aRequest ) {
		setRequest( aRequest );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestRequestBuilder withHeaderFields( RequestHeaderFields aRequestHeaderFields ) {
		setHeaderFields( aRequestHeaderFields );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestRequestBuilder withRedirectDepth( int aRedirectDepth ) {
		setRedirectDepth( aRedirectDepth );
		return this;
	}

	/**
	 * This is a convenience method for easily instantiating the according
	 * builder.
	 * 
	 * @param aRestClient the rest client
	 * 
	 * @return an instance (using a public implementation) of this builder
	 */
	@Override
	public RestRequestBuilder build( RestfulClient aRestClient ) {
		return new RestRequestBuilder( aRestClient );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.refcodes.web.UrlBuilder getUrl() {
		return (org.refcodes.web.UrlBuilder) _url;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setUrl( Url aUrl ) {
		_url = aUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpMethod getHttpMethod() {
		return _httpMethod;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpMethod( HttpMethod aHttpMethod ) {
		_httpMethod = aHttpMethod;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + _httpMethod + ": " + _url.toHttpUrl() + "?" + new VerboseTextBuilder().withElements( _url.getQueryFields() ).toString() + ")@" + hashCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHeaderFields( RequestHeaderFields aHeaderFields ) {
		_headerFields = aHeaderFields;
	}

	/**
	 * Sends the request and returns synchronously the according
	 * {@link RestResponse}.
	 *
	 * @return the rest response
	 * 
	 * @throws HttpResponseException the http response exception
	 */
	@Override
	public RestResponse toRestResponse() throws HttpResponseException {
		return _restClient.doRequest( getHttpMethod(), getUrl(), getHeaderFields(), getRequest(), getRedirectDepth() );
	}
}
