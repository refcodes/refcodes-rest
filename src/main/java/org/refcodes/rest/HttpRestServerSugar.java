package org.refcodes.rest;

import java.io.IOException;
import java.security.KeyStore;
import java.util.regex.Pattern;

import org.refcodes.security.KeyStoreDescriptor;
import org.refcodes.web.BasicAuthObserver;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpServerContext;

/**
 * Declarative syntactic sugar which may be statically imported in order to
 * allow declarative definitions of RESTful server functionality:
 * <code>import static org.refcodes.rest.HttpRestServerSugar.*;</code>
 */
public class HttpRestServerSugar {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	private HttpRestServerSugar() {
		throw new IllegalStateException( "Utility class" );
	}

	/**
	 * See {@link RestfulHttpServer}.
	 *
	 * @return The {@link RestfulHttpServer}
	 */
	public static RestfulHttpServer getHttpRestServer() {
		return HttpRestServerSingleton.getInstance();
	}

	/**
	 * See {@link RestfulHttpServer#close()}.
	 *
	 * @return The {@link RestfulHttpServer}
	 * 
	 * @throws IOException the close exception
	 */
	public static RestfulHttpServer close() throws IOException {
		HttpRestServerSingleton.getInstance().close();
		return HttpRestServerSingleton.getInstance();
	}

	/**
	 * See {@link RestfulHttpServer#closeIn(int)}.
	 *
	 * @param aCloseMillis The close in millis
	 * 
	 * @return The {@link RestfulHttpServer}
	 */
	public static RestfulHttpServer closeIn( int aCloseMillis ) {
		HttpRestServerSingleton.getInstance().closeIn( aCloseMillis );
		return HttpRestServerSingleton.getInstance();
	}

	/**
	 * See {@link RestfulHttpServer#closeQuietly()}.
	 *
	 * @return The {@link RestfulHttpServer}
	 */
	public static RestfulHttpServer closeQuietly() {
		HttpRestServerSingleton.getInstance().closeQuietly();
		return HttpRestServerSingleton.getInstance();
	}

	/**
	 * See {@link RestfulHttpServer#onHttpException(HttpExceptionHandler)}.
	 *
	 * @param aHttpExceptionHandler the http exception handler
	 * 
	 * @return The {@link RestfulHttpServer}
	 */
	public static RestfulHttpServer onHttpException( HttpExceptionHandler aHttpExceptionHandler ) {
		HttpRestServerSingleton.getInstance().onHttpException( aHttpExceptionHandler );
		return HttpRestServerSingleton.getInstance();
	}

	/**
	 * See {@link RestfulHttpServer#onBasicAuthRequest(BasicAuthObserver)}.
	 *
	 * @param aObserver The observer
	 * 
	 * @return The {@link RestfulHttpServer}
	 */
	public static RestfulHttpServer onBasicAuthRequest( BasicAuthObserver aObserver ) {
		return HttpRestServerSingleton.getInstance().onBasicAuthRequest( aObserver );
	}

	/**
	 * See {@link RestfulHttpServer#onDelete(String, RestRequestConsumer)}.
	 *
	 * @param aLocatorPathPattern The locator pattern
	 * @param aRequestConsumer The request observer
	 * 
	 * @return The {@link RestEndpointBuilder}
	 */
	public static RestEndpointBuilder onDelete( String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		return HttpRestServerSingleton.getInstance().onDelete( aLocatorPathPattern, aRequestConsumer );
	}

	/**
	 * See {@link RestfulHttpServer#onGet(String, RestRequestConsumer)}.
	 *
	 * @param aLocatorPathPattern The locator pattern
	 * @param aRequestConsumer The request observer
	 * 
	 * @return The {@link RestEndpointBuilder}
	 */
	public static RestEndpointBuilder onGet( String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		return HttpRestServerSingleton.getInstance().onGet( aLocatorPathPattern, aRequestConsumer );
	}

	/**
	 * See {@link RestfulHttpServer#onPost(String, RestRequestConsumer)}.
	 *
	 * @param aLocatorPathPattern The locator pattern
	 * @param aRequestConsumer The request observer
	 * 
	 * @return The {@link RestEndpointBuilder} See
	 *         {@link RestfulHttpServer#onPost(String, RestRequestConsumer)}
	 */
	public static RestEndpointBuilder onPost( String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		return HttpRestServerSingleton.getInstance().onPost( aLocatorPathPattern, aRequestConsumer );
	}

	/**
	 * See {@link RestfulHttpServer#onPut(String, RestRequestConsumer)}.
	 *
	 * @param aLocatorPathPattern The locator pattern
	 * @param aRequestConsumer The request observer
	 * 
	 * @return The {@link RestEndpointBuilder}
	 */
	public static RestEndpointBuilder onPut( String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		return HttpRestServerSingleton.getInstance().onPut( aLocatorPathPattern, aRequestConsumer );
	}

	/**
	 * See
	 * {@link RestfulHttpServer#onRequest(HttpMethod, String, RestRequestConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method
	 * @param aLocatorPathPattern The locator pattern
	 * @param aRequestConsumer The request observer
	 * 
	 * @return The {@link RestEndpointBuilder}
	 */
	public static RestEndpointBuilder onRequest( HttpMethod aHttpMethod, String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		return HttpRestServerSingleton.getInstance().onRequest( aHttpMethod, aLocatorPathPattern, aRequestConsumer );
	}

	/**
	 * See {@link RestfulHttpServer#onRequest(String, RestRequestConsumer)}.
	 *
	 * @param aLocatorPathPattern The locator pattern
	 * @param aRequestConsumer The request observer
	 * 
	 * @return The {@link RestEndpointBuilder}
	 */
	public static RestEndpointBuilder onRequest( String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		return HttpRestServerSingleton.getInstance().onRequest( aLocatorPathPattern, aRequestConsumer );
	}

	/**
	 * See {@link RestfulHttpServer#onDelete(String, RestRequestConsumer)}.
	 *
	 * @param aLocatorRegExp The locator pattern
	 * @param aRequestConsumer The request observer
	 * 
	 * @return The {@link RestEndpointBuilder}
	 */
	public static RestEndpointBuilder onDelete( Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		return HttpRestServerSingleton.getInstance().onDelete( aLocatorRegExp, aRequestConsumer );
	}

	/**
	 * See {@link RestfulHttpServer#onGet(String, RestRequestConsumer)}.
	 *
	 * @param aLocatorRegExp The locator pattern
	 * @param aRequestConsumer The request observer
	 * 
	 * @return The {@link RestEndpointBuilder}
	 */
	public static RestEndpointBuilder onGet( Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		return HttpRestServerSingleton.getInstance().onGet( aLocatorRegExp, aRequestConsumer );
	}

	/**
	 * See {@link RestfulHttpServer#onPost(String, RestRequestConsumer)}.
	 *
	 * @param aLocatorRegExp The locator pattern
	 * @param aRequestConsumer The request observer
	 * 
	 * @return The {@link RestEndpointBuilder} See
	 *         {@link RestfulHttpServer#onPost(String, RestRequestConsumer)}
	 */
	public static RestEndpointBuilder onPost( Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		return HttpRestServerSingleton.getInstance().onPost( aLocatorRegExp, aRequestConsumer );
	}

	/**
	 * See {@link RestfulHttpServer#onPut(String, RestRequestConsumer)}.
	 *
	 * @param aLocatorRegExp The locator pattern
	 * @param aRequestConsumer The request observer
	 * 
	 * @return The {@link RestEndpointBuilder}
	 */
	public static RestEndpointBuilder onPut( Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		return HttpRestServerSingleton.getInstance().onPut( aLocatorRegExp, aRequestConsumer );
	}

	/**
	 * See
	 * {@link RestfulHttpServer#onRequest(HttpMethod, String, RestRequestConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method
	 * @param aLocatorRegExp The locator pattern
	 * @param aRequestConsumer The request observer
	 * 
	 * @return The {@link RestEndpointBuilder}
	 */
	public static RestEndpointBuilder onRequest( HttpMethod aHttpMethod, Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		return HttpRestServerSingleton.getInstance().onRequest( aHttpMethod, aLocatorRegExp, aRequestConsumer );
	}

	/**
	 * See {@link RestfulHttpServer#onRequest(String, RestRequestConsumer)}.
	 *
	 * @param aLocatorRegExp The locator pattern
	 * @param aRequestConsumer The request observer
	 * 
	 * @return The {@link RestEndpointBuilder}
	 */
	public static RestEndpointBuilder onRequest( Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		return HttpRestServerSingleton.getInstance().onRequest( aLocatorRegExp, aRequestConsumer );
	}

	/**
	 * See {@link RestfulHttpServer#open(Object)}.
	 *
	 * @param aCtx The context describing the parameters required to open the
	 *        connection.
	 * 
	 * @return The {@link RestfulHttpServer}
	 * 
	 * @throws IOException thrown in case the open operation caused problems.
	 */
	public static RestfulHttpServer open( HttpServerContext aCtx ) throws IOException {
		HttpRestServerSingleton.getInstance().open( aCtx );
		return HttpRestServerSingleton.getInstance();
	}

	/**
	 * See {@link RestfulHttpServer#open(int)}.
	 *
	 * @param aPort The port
	 * 
	 * @return The {@link RestfulHttpServer}
	 * 
	 * @throws IOException thrown in case the open operation caused problems.
	 */
	public static RestfulHttpServer open( int aPort ) throws IOException {
		HttpRestServerSingleton.getInstance().open( aPort );
		return HttpRestServerSingleton.getInstance();
	}

	/**
	 * See {@link RestfulHttpServer#open(int, int)}.
	 *
	 * @param aPort The port
	 * @param aMaxConnections The max connections
	 * 
	 * @return The {@link RestfulHttpServer}
	 * 
	 * @throws IOException thrown in case the open operation caused problems.
	 */
	public static RestfulHttpServer open( int aPort, int aMaxConnections ) throws IOException {
		HttpRestServerSingleton.getInstance().open( aPort, aMaxConnections );
		return HttpRestServerSingleton.getInstance();
	}

	/**
	 * See {@link RestfulHttpServer#open(KeyStoreDescriptor, int)}.
	 *
	 * @param aStoreDescriptor The store descriptor describing your
	 *        {@link KeyStore} required by HTTP.
	 * @param aPort The port
	 * 
	 * @return The {@link RestfulHttpServer}
	 * 
	 * @throws IOException thrown in case the open operation caused problems.
	 */
	public static RestfulHttpServer open( KeyStoreDescriptor aStoreDescriptor, Integer aPort ) throws IOException {
		HttpRestServerSingleton.getInstance().open( aStoreDescriptor, aPort );
		return HttpRestServerSingleton.getInstance();
	}

	/**
	 * See {@link RestfulHttpServer#open(KeyStoreDescriptor, int, int )}.
	 *
	 * @param aStoreDescriptor The store descriptor describing your
	 *        {@link KeyStore} required by HTTP.
	 * @param aPort The port
	 * @param aMaxConnections The max connections
	 * 
	 * @return The {@link RestfulHttpServer}
	 * 
	 * @throws IOException thrown in case the open operation caused problems.
	 */
	public static RestfulHttpServer open( KeyStoreDescriptor aStoreDescriptor, Integer aPort, int aMaxConnections ) throws IOException {
		HttpRestServerSingleton.getInstance().open( aStoreDescriptor, aPort, aMaxConnections );
		return HttpRestServerSingleton.getInstance();
	}

	/**
	 * See {@link RestfulHttpServer#open(String, KeyStoreDescriptor, int)}.
	 *
	 * @param aSecureSocketProtocol The secure socket protocol
	 * @param aStoreDescriptor The store descriptor describing your
	 *        {@link KeyStore} required by HTTP.
	 * @param aPort The port
	 * 
	 * @return The {@link RestfulHttpServer}
	 * 
	 * @throws IOException thrown in case the open operation caused problems.
	 */
	public static RestfulHttpServer open( String aSecureSocketProtocol, KeyStoreDescriptor aStoreDescriptor, Integer aPort ) throws IOException {
		HttpRestServerSingleton.getInstance().open( aSecureSocketProtocol, aStoreDescriptor, aPort );
		return HttpRestServerSingleton.getInstance();
	}

	/**
	 * See {@link RestfulHttpServer#open(String, KeyStoreDescriptor, int, int)}.
	 *
	 * @param aSecureSocketProtocol The secure socket protocol
	 * @param aStoreDescriptor The store descriptor describing your
	 *        {@link KeyStore} required by HTTP.
	 * @param aPort The port
	 * @param aMaxConnections The max connections
	 * 
	 * @return The {@link RestfulHttpServer}
	 * 
	 * @throws IOException thrown in case the open operation caused problems.
	 */
	public static RestfulHttpServer open( String aSecureSocketProtocol, KeyStoreDescriptor aStoreDescriptor, Integer aPort, int aMaxConnections ) throws IOException {
		HttpRestServerSingleton.getInstance().open( aSecureSocketProtocol, aStoreDescriptor, aPort, aMaxConnections );
		return HttpRestServerSingleton.getInstance();
	}

	/**
	 * See {@link RestfulHttpServer#unsubscribeObserver(Object)}.
	 *
	 * @param aEndpoint The endpoint
	 * 
	 * @return true, if successful
	 */
	public static boolean unsubscribeObserver( RestEndpoint aEndpoint ) {
		return HttpRestServerSingleton.getInstance().unsubscribeObserver( aEndpoint );
	}

	/**
	 * See {@link RestfulHttpServer#withBaseLocator(String)}.
	 *
	 * @param aBaseLocator The base locator
	 * 
	 * @return The {@link RestfulHttpServer}
	 */
	public static RestfulHttpServer withBaseLocator( String aBaseLocator ) {
		return HttpRestServerSingleton.getInstance().withBaseLocator( aBaseLocator );
	}

	/**
	 * See {@link RestfulHttpServer#withRealm(String)}.
	 *
	 * @param aRealm The realm
	 * 
	 * @return The {@link RestfulHttpServer}
	 */
	public static RestfulHttpServer withRealm( String aRealm ) {
		return HttpRestServerSingleton.getInstance().withRealm( aRealm );
	}
}
