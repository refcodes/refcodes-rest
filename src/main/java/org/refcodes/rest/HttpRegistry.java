// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.component.Configurable;
import org.refcodes.component.InitializeException;
import org.refcodes.component.LifecycleComponent.LifecycleAutomaton;
import org.refcodes.component.PauseException;
import org.refcodes.component.ResumeException;
import org.refcodes.component.StartException;
import org.refcodes.component.StopException;
import org.refcodes.data.Scheme;
import org.refcodes.mixin.AliasAccessor.AliasBuilder;
import org.refcodes.mixin.AliasAccessor.AliasProperty;
import org.refcodes.mixin.InstanceIdAccessor.InstanceIdBuilder;
import org.refcodes.mixin.InstanceIdAccessor.InstanceIdProperty;
import org.refcodes.net.IpAddressAccessor.IpAddressBuilder;
import org.refcodes.net.IpAddressAccessor.IpAddressProperty;
import org.refcodes.rest.HttpRegistryUrlAccessor.HttpRegistryUrlBuilder;
import org.refcodes.rest.HttpRegistryUrlAccessor.HttpRegistryUrlProperty;
import org.refcodes.rest.HttpServerDescriptorAccessor.HttpServerDescriptorBuilder;
import org.refcodes.rest.HttpServerDescriptorAccessor.HttpServerDescriptorProperty;
import org.refcodes.rest.PingPathAccessor.PingPathBuilder;
import org.refcodes.rest.PingPathAccessor.PingPathProperty;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.security.TrustStoreDescriptorAccessor.TrustStoreDescriptorBuilder;
import org.refcodes.security.TrustStoreDescriptorAccessor.TrustStoreDescriptorProperty;
import org.refcodes.web.HostAccessor.HostBuilder;
import org.refcodes.web.HostAccessor.HostProperty;
import org.refcodes.web.Url;
import org.refcodes.web.VirtualHostAccessor.VirtualHostBuilder;
import org.refcodes.web.VirtualHostAccessor.VirtualHostProperty;

/**
 * The {@link HttpRegistry} describes the functionality required in order to
 * register a service at a service registry and discovery service. This type is
 * intended to be used by different separate hierarchy branches by providing the
 * generic type &lt;B&gt;, ensuring a coherent type hierarchy for each branch.
 *
 * @param <DESC> The type of the server discovery descriptor (the object
 *        describing your service and locating the service registry).
 * @param <B> In order to implement the builder pattern with a coherent type
 *        hierarchy.
 */
public interface HttpRegistry<DESC extends HttpServerDescriptor, B extends HttpRegistry<DESC, B>> extends Configurable<HttpRegistryContext<DESC>>, HttpServerDescriptorProperty<DESC>, HttpServerDescriptorBuilder<DESC, B>, PingPathProperty, PingPathBuilder<B>, HostProperty, HostBuilder<B>, VirtualHostProperty, VirtualHostBuilder<B>, IpAddressProperty, IpAddressBuilder<B>, InstanceIdProperty, InstanceIdBuilder<B>, AliasProperty, AliasBuilder<B>, LifecycleAutomaton, HttpRegistryUrlProperty, HttpRegistryUrlBuilder<B>, TrustStoreDescriptorProperty, TrustStoreDescriptorBuilder<B>, HttpServerDescriptorFactory<DESC> {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withAlias( String aAlias ) {
		setAlias( aAlias );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withPingPath( String aPingPath ) {
		setPingPath( aPingPath );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withHttpRegistryUrl( Url aUrl ) {
		setHttpRegistryUrl( aUrl );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withHttpServerDescriptor( DESC aServerDescriptor ) {
		setHttpServerDescriptor( aServerDescriptor );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withHost( String aHost ) {
		setHost( aHost );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withVirtualHost( String aVirtualHost ) {
		setVirtualHost( aVirtualHost );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	default B withIpAddress( int[] aIpAddress ) {
		setIpAddress( aIpAddress );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withCidrNotation( String aCidrNotation ) {
		fromCidrNotation( aCidrNotation );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withTrustStoreDescriptor( TrustStoreDescriptor aStoreDescriptor ) {
		setTrustStoreDescriptor( aStoreDescriptor );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withInstanceId( String aInstanceId ) {
		setInstanceId( aInstanceId );
		return (B) this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// LIFECYCLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Initializes the {@link HttpRegistry} by registering it at the service
	 * registry with a status such as "starting" or "initializing" or
	 * "not-ready-yet". {@inheritDoc}
	 */
	@Override
	default void initialize() throws InitializeException {
		initialize( null, null, null );
	}

	/**
	 * Initializes the {@link HttpRegistry} by registering it at the service
	 * registry with a status such as "starting" or "initializing" or
	 * "not-ready-yet". {@inheritDoc}
	 * 
	 * @param aRegistryContext The context providing the descriptor of the
	 *        server to be registered and the {@link Url} of the service
	 *        registry to be used as well as the required truststore.
	 * 
	 * @throws InitializeException thrown in case initializing a component
	 *         caused problems. Usually a method similar to "initialize()"
	 *         throws such an exception.
	 */
	@Override
	default void initialize( HttpRegistryContext<DESC> aRegistryContext ) throws InitializeException {
		initialize( aRegistryContext.getHttpServerDescriptor(), aRegistryContext.getHttpRegistryUrl(), aRegistryContext.getTrustStoreDescriptor() );
	}

	/**
	 * Initializes the {@link HttpRegistry} by registering it at the service
	 * registry with a status such as "starting" or "initializing" or
	 * "not-ready-yet". {@inheritDoc}
	 * 
	 * @param aServerDescriptor The descriptor of the server to be registered.
	 * @param aRegistryUrl The URL of the service registry to be used.
	 * 
	 * @throws InitializeException thrown in case initializing a component
	 *         caused problems. Usually a method similar to "initialize()"
	 *         throws such an exception.
	 */
	default void initialize( DESC aServerDescriptor, Url aRegistryUrl ) throws InitializeException {
		initialize( aServerDescriptor, aRegistryUrl, null );
	}

	/**
	 * Initializes the {@link HttpRegistry} by registering it at the service
	 * registry with a status such as "starting" or "initializing" or
	 * "not-ready-yet". {@inheritDoc}
	 * 
	 * @param aServerDescriptor The descriptor of the server to be registered.
	 * @param aRegistryUrl The URL of the service registry to be used.
	 * @param aStoreDescriptor The descriptor describing the truststore for
	 *        (optionally) opening an HTTPS connection to the registry server.
	 * 
	 * @throws InitializeException thrown in case initializing a component
	 *         caused problems. Usually a method similar to "initialize()"
	 *         throws such an exception.
	 */
	void initialize( DESC aServerDescriptor, Url aRegistryUrl, TrustStoreDescriptor aStoreDescriptor ) throws InitializeException;

	/**
	 * Initializes the {@link HttpRegistry} by registering it at the service
	 * registry with a status such as "starting" or "initializing" or
	 * "not-ready-yet". {@inheritDoc}
	 * 
	 * @param aAlias The name ("alias") which identifies the server in the
	 *        registry.
	 * @param aInstanceId The TID for the instance when being registered at the
	 *        service registry. If omitted, then the host name is used.
	 * @param aScheme The {@link Scheme} to which this server is being attached
	 *        (HTTP or HTTPS).
	 * @param aPort The port of your service being registered.
	 * @param aRegistryUrl The registry server where to register.
	 * 
	 * @throws InitializeException thrown in case initializing a component
	 *         caused problems. Usually a method similar to "initialize()"
	 *         throws such an exception.
	 */
	default void initialize( String aAlias, String aInstanceId, Scheme aScheme, int aPort, Url aRegistryUrl ) throws InitializeException {
		initialize( aAlias, aInstanceId, aScheme, null, null, null, aPort, null, aRegistryUrl );
	}

	/**
	 * Initializes the {@link HttpRegistry} by registering it at the service
	 * registry with a status such as "starting" or "initializing" or
	 * "not-ready-yet". {@inheritDoc}
	 * 
	 * @param aAlias The name ("alias") which identifies the server in the
	 *        registry.
	 * @param aScheme The {@link Scheme} to which this server is being attached
	 *        (HTTP or HTTPS).
	 * @param aHost The host name to be used to address this server. If omitted,
	 *        then the system's host name should be used.
	 * @param aPort The port of your service being registered.
	 * @param aRegistryUrl The registry server where to register.
	 * 
	 * @throws InitializeException thrown in case initializing a component
	 *         caused problems. Usually a method similar to "initialize()"
	 *         throws such an exception.
	 */
	default void initialize( String aAlias, Scheme aScheme, String aHost, int aPort, Url aRegistryUrl ) throws InitializeException {
		initialize( aAlias, null, aScheme, aHost, null, null, aPort, null, aRegistryUrl );
	}

	/**
	 * Initializes the {@link HttpRegistry} by registering it at the service
	 * registry with a status such as "starting" or "initializing" or
	 * "not-ready-yet". {@inheritDoc}
	 * 
	 * @param aAlias The name ("alias") which identifies the server in the
	 *        registry.
	 * @param aScheme The {@link Scheme} to which this server is being attached
	 *        (HTTP or HTTPS).
	 * @param aHost The host name to be used to address this server. If omitted,
	 *        then the system's host name should be used.
	 * @param aVirtualHost The virtual host name to be used for resolving.
	 * @param aIpAddress The IP-Address identifying the host.
	 * @param aPort The port of your service being registered.
	 * @param aPingPath The path to use as health-check end-point by this
	 *        server.
	 * @param aRegistryUrl The registry server where to register.
	 * 
	 * @throws InitializeException thrown in case initializing a component
	 *         caused problems. Usually a method similar to "initialize()"
	 *         throws such an exception.
	 */
	default void initialize( String aAlias, Scheme aScheme, String aHost, String aVirtualHost, int[] aIpAddress, int aPort, String aPingPath, Url aRegistryUrl ) throws InitializeException {
		initialize( toHttpServerDescriptor( aAlias, aScheme, aHost, aVirtualHost, aIpAddress, aPort, aPingPath ), aRegistryUrl );
	}

	/**
	 * Initializes the {@link HttpRegistry} by registering it at the service
	 * registry with a status such as "starting" or "initializing" or
	 * "not-ready-yet". {@inheritDoc}
	 * 
	 * @param aAlias The name ("alias") which identifies the server in the
	 *        registry.
	 * @param aInstanceId The TID for the instance when being registered at the
	 *        service registry. If omitted, then the host name is used.
	 * @param aScheme The {@link Scheme} to which this server is being attached
	 *        (HTTP or HTTPS).
	 * @param aHost The host name to be used to address this server. If omitted,
	 *        then the system's host name should be used.
	 * @param aVirtualHost The virtual host name to be used for resolving.
	 * @param aIpAddress The IP-Address identifying the host.
	 * @param aPort The port of your service being registered.
	 * @param aPingPath The path to use as health-check end-point by this
	 *        server.
	 * @param aRegistryUrl The registry server where to register.
	 * 
	 * @throws InitializeException thrown in case initializing a component
	 *         caused problems. Usually a method similar to "initialize()"
	 *         throws such an exception.
	 */
	default void initialize( String aAlias, String aInstanceId, Scheme aScheme, String aHost, String aVirtualHost, int[] aIpAddress, int aPort, String aPingPath, Url aRegistryUrl ) throws InitializeException {
		initialize( toHttpServerDescriptor( aAlias, aInstanceId, aScheme, aHost, aVirtualHost, aIpAddress, aPort, aPingPath ), aRegistryUrl );
	}

	/**
	 * Some "up-and-running" status is communicated to the registry server. May
	 * also start a "ping" or "heartbeat" daemon. {@inheritDoc}
	 */
	@Override
	void start() throws StartException;

	/**
	 * Some "deactivated" status is communicated to the registry server.
	 * {@inheritDoc}
	 */
	@Override
	void pause() throws PauseException;

	/**
	 * Some "up-and-running (again)" status is communicated to the registry
	 * server. {@inheritDoc}
	 */
	@Override
	void resume() throws ResumeException;

	/**
	 * Some "out-of-order" status is communicated to the registry server. Also
	 * stops a "ping" or "heartbeat" daemon. {@inheritDoc}
	 */
	@Override
	void stop() throws StopException;

	/**
	 * This server is taken (removed) from the registry server. Also stops a
	 * "ping" or "heartbeat" daemon. Finally the connection is closed.
	 * {@inheritDoc}
	 */
	@Override
	void destroy();
}
