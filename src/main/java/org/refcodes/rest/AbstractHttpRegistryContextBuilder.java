// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.security.KeyStore;

import org.refcodes.rest.HttpRegistryContext.HttpRegistryContextBuilder;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.web.Url;

/**
 * The Class AbstractHttpRegistryContextBuilder.
 *
 * @param <DESC> the generic type
 */
public abstract class AbstractHttpRegistryContextBuilder<DESC extends HttpServerDescriptor> implements HttpRegistryContextBuilder<DESC> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Url _registryUrl;
	private TrustStoreDescriptor _trustStoreDescriptor;
	private DESC _serverDescriptor;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link AbstractHttpRegistryContextBuilder} context.
	 * 
	 * @param aRegistryUrl The {@link Url} pointing to the service discovery
	 *        registry.
	 * @param aServerDescriptor The descriptor describing the server ("service")
	 *        to be discovered.
	 */
	public AbstractHttpRegistryContextBuilder( Url aRegistryUrl, DESC aServerDescriptor ) {
		this( aRegistryUrl, null, aServerDescriptor );
	}

	/**
	 * Constructs the {@link AbstractHttpRegistryContextBuilder} context.
	 * 
	 * @param aRegistryUrl The {@link Url} pointing to the service discovery
	 *        registry.
	 * @param aStoreDescriptor The descriptor describing the truststore
	 *        ({@link KeyStore}) required for establishing an HTTPS connection
	 *        to the registry.
	 * @param aServerDescriptor The descriptor describing the server ("service")
	 *        to be discovered.
	 */
	public AbstractHttpRegistryContextBuilder( Url aRegistryUrl, TrustStoreDescriptor aStoreDescriptor, DESC aServerDescriptor ) {
		_registryUrl = aRegistryUrl;
		_serverDescriptor = aServerDescriptor;
		_trustStoreDescriptor = aStoreDescriptor;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getHttpRegistryUrl() {
		return _registryUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DESC getHttpServerDescriptor() {
		return _serverDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TrustStoreDescriptor getTrustStoreDescriptor() {
		return _trustStoreDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpRegistryUrl( Url aRegistryUrl ) {
		_registryUrl = aRegistryUrl;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpServerDescriptor( DESC aServerDescriptor ) {
		_serverDescriptor = aServerDescriptor;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTrustStoreDescriptor( TrustStoreDescriptor aTrustStoreDescriptor ) {
		_trustStoreDescriptor = aTrustStoreDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRegistryContextBuilder<DESC> withHttpRegistryUrl( Url aUrl ) {
		setHttpRegistryUrl( aUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRegistryContextBuilder<DESC> withHttpServerDescriptor( DESC aServerDescriptor ) {
		setHttpServerDescriptor( aServerDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRegistryContextBuilder<DESC> withTrustStoreDescriptor( TrustStoreDescriptor aStoreDescriptor ) {
		setTrustStoreDescriptor( aStoreDescriptor );
		return this;
	}
}
