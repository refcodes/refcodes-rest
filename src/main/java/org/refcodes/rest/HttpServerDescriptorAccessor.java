// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

/**
 * Provides an accessor for a {@link HttpServerDescriptor} property.
 * 
 * @param <DESC> The {@link HttpServerDescriptor} (sub-)type of the truststore
 *        descriptor.
 */
public interface HttpServerDescriptorAccessor<DESC extends HttpServerDescriptor> {

	/**
	 * Retrieves the {@link HttpServerDescriptor} from the
	 * {@link HttpServerDescriptor} property.
	 * 
	 * @return The {@link HttpServerDescriptor} stored by the
	 *         {@link HttpServerDescriptor} property.
	 */
	DESC getHttpServerDescriptor();

	/**
	 * Provides a mutator for a {@link HttpServerDescriptor} property.
	 * 
	 * @param <DESC> The {@link HttpServerDescriptor} (sub-)type of the
	 *        truststore descriptor.
	 */
	public interface HttpServerDescriptorMutator<DESC extends HttpServerDescriptor> {

		/**
		 * Sets the {@link HttpServerDescriptor} for the
		 * {@link HttpServerDescriptor} property.
		 * 
		 * @param aServerDescriptor The {@link HttpServerDescriptor} to be
		 *        stored by the {@link HttpServerDescriptor} property.
		 */
		void setHttpServerDescriptor( DESC aServerDescriptor );
	}

	/**
	 * Provides a builder method for a {@link HttpServerDescriptor} property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <DESC> The {@link HttpServerDescriptor} (sub-)type of the
	 *        truststore descriptor.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface HttpServerDescriptorBuilder<DESC extends HttpServerDescriptor, B extends HttpServerDescriptorBuilder<DESC, B>> {

		/**
		 * Sets the {@link HttpServerDescriptor} for the
		 * {@link HttpServerDescriptor} property.
		 * 
		 * @param aServerDescriptor The {@link HttpServerDescriptor} to be
		 *        stored by the {@link HttpServerDescriptor} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withHttpServerDescriptor( DESC aServerDescriptor );
	}

	/**
	 * Provides a {@link HttpServerDescriptor} property.
	 * 
	 * @param <DESC> The {@link HttpServerDescriptor} (sub-)type of the
	 *        truststore descriptor.
	 */
	public interface HttpServerDescriptorProperty<DESC extends HttpServerDescriptor> extends HttpServerDescriptorAccessor<DESC>, HttpServerDescriptorMutator<DESC> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setHttpServerDescriptor(HttpServerDescriptor)} and returns
		 * the very same value (getter).
		 * 
		 * @param aHttpServerDescriptor The value to set (via
		 *        {@link #setHttpServerDescriptor(HttpServerDescriptor)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default DESC letHttpServerDescriptor( DESC aHttpServerDescriptor ) {
			setHttpServerDescriptor( aHttpServerDescriptor );
			return aHttpServerDescriptor;
		}
	}
}
