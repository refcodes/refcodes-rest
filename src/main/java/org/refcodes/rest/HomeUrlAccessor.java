// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.web.Url;

/**
 * Provides an accessor for a home {@link Url} property.
 */
public interface HomeUrlAccessor {

	/**
	 * Retrieves the home {@link Url} from the home {@link Url} property.
	 * 
	 * @return The home {@link Url} stored by the home {@link Url} property.
	 */
	Url getHomeUrl();

	/**
	 * Provides a mutator for a home {@link Url} property.
	 */
	public interface HomeUrlMutator {

		/**
		 * Sets the home {@link Url} for the home {@link Url} property.
		 * 
		 * @param aHomeUrl The home {@link Url} to be stored by the home
		 *        {@link Url} property.
		 */
		void setHomeUrl( Url aHomeUrl );
	}

	/**
	 * Provides a mutator for an home {@link Url} property.
	 * 
	 * @param <B> The builder which implements the {@link HomeUrlBuilder}.
	 */
	public interface HomeUrlBuilder<B extends HomeUrlBuilder<?>> {

		/**
		 * Sets the home {@link Url} to use and returns this builder as of the
		 * builder pattern.
		 * 
		 * @param aHomeUrl The home {@link Url} to be stored by the home
		 *        {@link Url} property.
		 * 
		 * @return This {@link HomeUrlBuilder} instance to continue
		 *         configuration.
		 */
		B withHomeUrl( Url aHomeUrl );
	}

	/**
	 * Provides a home {@link Url} property.
	 */
	public interface HomeUrlProperty extends HomeUrlAccessor, HomeUrlMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Url} (setter) as
		 * of {@link #setHomeUrl(Url)} and returns the very same value (getter).
		 * 
		 * @param aUrl The {@link Url} to set (via {@link #setHomeUrl(Url)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Url letHomeUrl( Url aUrl ) {
			setHomeUrl( aUrl );
			return aUrl;
		}
	}
}
