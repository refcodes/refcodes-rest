// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.web.BasicAuthCredentialsAccessor.BasicAuthCredentialsBuilder;
import org.refcodes.web.BasicAuthCredentialsAccessor.BasicAuthCredentialsProperty;
import org.refcodes.web.FormMediaTypeFactory;
import org.refcodes.web.JsonMediaTypeFactory;
import org.refcodes.web.MediaType;
import org.refcodes.web.MediaTypeFactory;
import org.refcodes.web.MediaTypeFactoryLookup.MutableMediaTypeFactoryLookup;
import org.refcodes.web.OauthTokenAccessor.OauthTokenBuilder;
import org.refcodes.web.OauthTokenAccessor.OauthTokenProperty;
import org.refcodes.web.TextMediaTypeFactory;
import org.refcodes.web.UserAgentAccessor.UserAgentBuilder;
import org.refcodes.web.UserAgentAccessor.UserAgentProperty;
import org.refcodes.web.XmlMediaTypeFactory;

/**
 * A client to send requests for communicating with a RESTful server such as the
 * {@link RestfulHttpServer}. For marshaling and unmarshaling HTTP bodies you
 * have to register the according {@link MediaTypeFactory} instances being
 * capable of handling the according {@link MediaType} definitions. Examples for
 * ready to use {@link MediaTypeFactory} implementations are
 * <ul>
 * <li>{@link JsonMediaTypeFactory},
 * <li>{@link XmlMediaTypeFactory}
 * <li>{@link FormMediaTypeFactory}
 * <li>{@link TextMediaTypeFactory}
 * </ul>
 * Implementations of the {@link RestfulClient} interface, such as the
 * {@link HttpRestClient}, are initialized with some common
 * {@link MediaTypeFactory} instances. You may set an individual User-Agent via
 * {@link #setUserAgent(String)}, not setting it will result in the default
 * User-Agent to be set as coded into the {@link RestfulClient} implementations.
 */
public interface RestfulClient extends BasicAuthCredentialsProperty, BasicAuthCredentialsBuilder<RestfulClient>, OauthTokenProperty, OauthTokenBuilder<RestfulClient>, RestGetClient, RestPostClient, RestDeleteClient, RestPutClient, MutableMediaTypeFactoryLookup, UserAgentProperty, UserAgentBuilder<RestfulClient> {}
