// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.io.InputStream;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.controlflow.ExecutionStrategy;
import org.refcodes.data.Delimiter;
import org.refcodes.exception.MarshalException;
import org.refcodes.exception.VetoException;
import org.refcodes.io.ReplayInputStream;
import org.refcodes.matcher.PathMatcher;
import org.refcodes.matcher.RegExpMatcher;
import org.refcodes.matcher.WildcardMatcher;
import org.refcodes.mixin.WildcardSubstitutes;
import org.refcodes.observer.AbstractObservable;
import org.refcodes.runtime.Host;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.VerboseTextBuilder;
import org.refcodes.web.BasicAuthRequiredException;
import org.refcodes.web.ContentType;
import org.refcodes.web.FormMediaTypeFactory;
import org.refcodes.web.HeaderField;
import org.refcodes.web.HeaderFields;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpRequest;
import org.refcodes.web.HttpServerRequest;
import org.refcodes.web.HttpServerResponse;
import org.refcodes.web.HttpStatusException;
import org.refcodes.web.InternalServerErrorException;
import org.refcodes.web.JsonMediaTypeFactory;
import org.refcodes.web.MediaType;
import org.refcodes.web.MediaTypeFactory;
import org.refcodes.web.NotFoundException;
import org.refcodes.web.RequestHeaderFields;
import org.refcodes.web.ResponseHeaderFields;
import org.refcodes.web.TextMediaTypeFactory;
import org.refcodes.web.UnsupportedMediaTypeException;
import org.refcodes.web.Url;
import org.refcodes.web.XmlMediaTypeFactory;

/**
 * Implementation of the base functionality of the {@link RestfulServer}
 * interface omitting the HTTP handling part being the foundation for various
 * {@link RestfulServer} implementations such as {@link HttpRestServer} or
 * {@link LoopbackRestServer}. The {@link AbstractRestfulServer} is
 * preconfigured with the following {@link MediaTypeFactory} instances:
 * <ul>
 * <li>{@link JsonMediaTypeFactory}</li>
 * <li>{@link XmlMediaTypeFactory}</li>
 * <li>{@link TextMediaTypeFactory}</li>
 * <li>{@link FormMediaTypeFactory}</li>
 * </ul>
 * In your sub-classes, overwrite the method {@link #initMedaTypeFactories()},
 * therein calling {@link #addMediaTypeFactory(MediaTypeFactory)} to add (by
 * also invoking super's {@link #initMedaTypeFactories()}) or to set your own
 * (without invoking super's {@link #initMedaTypeFactories()})
 * {@link MediaTypeFactory} instances.
 * 
 * Set the system property {@link SystemProperty#LOG_DEBUG} to true (set when
 * invoking the JRA by passing the argument <code>-Dlog.debug=true</code> to the
 * <code>java</code> executable) to log additional erroneous situations e.g.
 * related to content types and accept types alongside marshaling and
 * unmarshaling.
 */
public abstract class AbstractRestfulServer extends AbstractObservable<RestEndpoint, HttpRequest> implements RestfulServer {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( AbstractRestfulServer.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Map<MediaType, MediaTypeFactory> _mediaTypeFacotries = new LinkedHashMap<>();
	private final Map<WildcardMatcher, List<RestEndpoint>> _matcherEndpoints = new LinkedHashMap<>();
	private String _realm = Host.getComputerName();
	private String _baseLocator = null;
	protected boolean _isVerbose;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link AbstractRestfulServer} preconfigured with
	 * {@link MediaTypeFactory} instances for JSON and REST.
	 */
	public AbstractRestfulServer() {
		this( false );
	}

	/**
	 * Constructs a {@link AbstractRestfulServer} preconfigured with
	 * {@link MediaTypeFactory} instances for JSON and REST.
	 * 
	 * @param isVerbose When true, any unknown content- and accept-types are
	 *        logged.
	 */
	public AbstractRestfulServer( boolean isVerbose ) {
		initMedaTypeFactories();
		_isVerbose = isVerbose;
	}

	/**
	 * Constructs a {@link AbstractRestfulServer} preconfigured with
	 * {@link MediaTypeFactory} instances for JSON and REST.
	 * 
	 * @param aExecutorService An executor service to be used when creating
	 *        {@link Thread}s.
	 */
	public AbstractRestfulServer( ExecutorService aExecutorService ) {
		this( aExecutorService, false );
	}

	/**
	 * Constructs a {@link AbstractRestfulServer} preconfigured with
	 * {@link MediaTypeFactory} instances for JSON and REST.
	 * 
	 * @param aExecutorService An executor service to be used when creating
	 *        {@link Thread}s.
	 * @param isVerbose When true, any unknown content- and accept-types are
	 *        logged.
	 */
	public AbstractRestfulServer( ExecutorService aExecutorService, boolean isVerbose ) {
		super( aExecutorService );
		initMedaTypeFactories();
		_isVerbose = isVerbose;
	}

	/**
	 * Adds the default {@link MediaTypeFactory} instances. Can be overridden.
	 */
	protected void initMedaTypeFactories() {
		addMediaTypeFactory( new JsonMediaTypeFactory() );
		addMediaTypeFactory( new XmlMediaTypeFactory() );
		addMediaTypeFactory( new FormMediaTypeFactory() );
		addMediaTypeFactory( new TextMediaTypeFactory() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setObserversActive( boolean isActive ) {
		super.setObserversActive( isActive );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isObserversActive() {
		return super.isObserversActive();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<RestEndpoint> observers() {
		return super.observers();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getRealm() {
		return _realm;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRealm( String aRealm ) {
		_realm = aRealm;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getBaseLocator() {
		return _baseLocator;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBaseLocator( String aBaseLocator ) {
		if ( !aBaseLocator.startsWith( Delimiter.PATH.getChar() + "" ) ) {
			throw new IllegalArgumentException( "Your provided base locator <" + aBaseLocator + "> is not an absolute locator, it has to start with a slash (\"" + Delimiter.PATH.getChar() + "\") character to be an absolute locator." );
		}
		_baseLocator = aBaseLocator;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean subscribeObserver( RestEndpoint aObserver ) {
		if ( super.subscribeObserver( aObserver ) ) {
			final WildcardMatcher theMatcher;
			if ( aObserver.getLocatorPathPattern() != null ) {
				theMatcher = new PathMatcher( aObserver.getLocatorPathPattern() );
			}
			else if ( aObserver.getLocatorRegExp() != null ) {
				theMatcher = new RegExpMatcher( aObserver.getLocatorRegExp() );
			}
			else {
				return false;
			}
			List<RestEndpoint> theEndpoints = _matcherEndpoints.get( theMatcher );
			if ( theEndpoints == null ) {
				theEndpoints = new ArrayList<>();
				_matcherEndpoints.put( theMatcher, theEndpoints );
			}
			theEndpoints.add( aObserver );
			return true;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean unsubscribeObserver( RestEndpoint aObserver ) {
		if ( super.unsubscribeObserver( aObserver ) ) {
			final Iterator<WildcardMatcher> eMatchers = _matcherEndpoints.keySet().iterator();
			List<RestEndpoint> eObservers;
			Iterator<RestEndpoint> eEndpoints;
			while ( eMatchers.hasNext() ) {
				eObservers = _matcherEndpoints.get( eMatchers.next() );
				eEndpoints = eObservers.iterator();
				while ( eEndpoints.hasNext() ) {
					if ( aObserver == eEndpoints.next() ) {
						eEndpoints.remove();
					}
				}
				if ( eObservers.isEmpty() ) {
					eMatchers.remove();
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean addMediaTypeFactory( MediaTypeFactory aMediaTypeFactory ) {
		boolean hasAddedAny = false;
		for ( MediaType eMediaType : aMediaTypeFactory.getMediaTypes() ) {
			if ( !_mediaTypeFacotries.containsKey( eMediaType ) ) {
				_mediaTypeFacotries.put( eMediaType, aMediaTypeFactory );
				hasAddedAny = true;
			}
		}
		return hasAddedAny;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MediaTypeFactory toMediaTypeFactory( MediaType aMediaType ) {
		return _mediaTypeFacotries.get( aMediaType );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MediaType[] getFactoryMediaTypes() {
		return _mediaTypeFacotries.keySet().toArray( new MediaType[_mediaTypeFacotries.size()] );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		_matcherEndpoints.clear();
		_matcherEndpoints.clear();
		super.dispose();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Extensions of this class disect an incoming request and pass it to this
	 * method for doing the actual invocation of the registered
	 * {@link RestEndpoint} instances. An extension might call this method from
	 * inside an event (request) handler.
	 * 
	 * @param aLocalAddress The host and port of your REST service.
	 * @param aRemoteAddress The host and port for the caller.
	 * @param aHttpMethod The {@link HttpMethod} of the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequestHeaderFields The Header-Fields ({@link HeaderFields})
	 *        belonging to the request.
	 * @param aHttpInputStream The body passed by the request.
	 * @param aHttpServerResponse A {@link HttpServerResponse} instance to be
	 *        used by the extension to produce an according HTTP-Response.
	 * 
	 * @throws HttpStatusException thrown in case of an {@link RestEndpoint}
	 *         responsible for the given request encountered a problem or none
	 *         {@link RestEndpoint} felt responsible to produce a
	 *         {@link HttpServerResponse}.
	 */
	protected void onHttpRequest( InetSocketAddress aLocalAddress, InetSocketAddress aRemoteAddress, HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aRequestHeaderFields, InputStream aHttpInputStream, HttpServerResponse aHttpServerResponse ) throws HttpStatusException {
		String theLocator = aUrl.getPath();
		RestEndpoint theEndpoint = null;
		final ContentType theMediaType = toNegotiatedContenType( aRequestHeaderFields );
		if ( theMediaType != null ) {
			aHttpServerResponse.getHeaderFields().putContentType( theMediaType );
		}
		if ( _baseLocator == null || theLocator.toLowerCase().startsWith( _baseLocator.toLowerCase() ) ) {
			if ( _baseLocator != null ) {
				theLocator = theLocator.substring( _baseLocator.length() );
			}
			RestRequestEvent eRestRequestEvent;
			Object eResponse = null;
			List<RestEndpoint> eEndpoints;
			WildcardSubstitutes eWildcardSubstitutes;
			for ( WildcardMatcher eMatcher : _matcherEndpoints.keySet() ) {
				eWildcardSubstitutes = eMatcher.toWildcardSubstitutes( theLocator );
				if ( eWildcardSubstitutes != null ) {
					eEndpoints = _matcherEndpoints.get( eMatcher );
					if ( eEndpoints.size() != 0 ) {
						if ( !aHttpInputStream.markSupported() ) {
							aHttpInputStream = new ReplayInputStream( aHttpInputStream );
						}
						final HttpServerRequest theHttpServerRequest = new HttpServerRequest( aHttpMethod, aUrl, aRequestHeaderFields, aHttpInputStream, this );
						for ( RestEndpoint eEndpoint : eEndpoints ) {
							if ( eEndpoint.getHttpMethod() == null || eEndpoint.getHttpMethod() == aHttpMethod ) {
								if ( theEndpoint == null ) {
									theEndpoint = eEndpoint;
								}
								eRestRequestEvent = new RestRequestEvent( aLocalAddress, aRemoteAddress, aHttpMethod, aUrl, eWildcardSubstitutes, aRequestHeaderFields, aHttpInputStream, this );
								try {
									preIntercept( theHttpServerRequest, aHttpServerResponse );
									eEndpoint.onRequest( eRestRequestEvent, aHttpServerResponse );
									postIntercept( theHttpServerRequest, aHttpServerResponse );
								}
								catch ( BasicAuthRequiredException e ) {
									aHttpServerResponse.getHeaderFields().putBasicAuthRequired( getRealm() );
									throw e;
								}
								if ( eResponse == null ) {
									eResponse = aHttpServerResponse.getResponse();
								}
								else if ( eResponse != null ) {
									if ( SystemProperty.LOG_DEBUG.isEnabled() || _isVerbose ) {
										LOGGER.log( Level.WARNING, "An endpoint of type <" + eEndpoint.getClass().getName() + "> (" + eEndpoint + ") would overwrite the response already produced by an endpoint of type <" + theEndpoint.getClass().getName() + "> (" + theEndpoint + " )" );
									}
									throw new InternalServerErrorException( "Unambiguous responsibility detected for handling resource locator <" + theLocator + "> with HTTP-Method <" + aHttpMethod + ">." );
								}
							}
						}
					}
				}
			}
		}
		if ( theEndpoint == null ) {
			throw new NotFoundException( "There is none endpoint for handling resource locator <" + theLocator + "> with HTTP-Method <" + aHttpMethod + ">." );
		}
	}

	/**
	 * Determines the best fitting respone's {@link ContentType}. The default
	 * Content-Type-Negotiation implementation of this method makes use of the
	 * {@link RequestHeaderFields} and matches them against the supported
	 * {@link MediaType} types ( retrieved via {@link #getFactoryMediaTypes()}).
	 * May be overwritten to enforce another Content-Type-Negotiation strategy.
	 * 
	 * @param aRequestHeaderFields The request's {@link HeaderField} instance to
	 *        use when determining the best fitting respone's
	 *        {@link ContentType}.
	 * 
	 * @return The best fitting (as of the implemented Content-Type-Negotiation
	 *         strategy) Content-Type to be used for the response.
	 */
	protected ContentType toNegotiatedContenType( RequestHeaderFields aRequestHeaderFields ) {
		ContentType theMediatype = null;
		out: {
			final List<ContentType> theRequestAcceptTypes = aRequestHeaderFields.getAcceptTypes();
			if ( theRequestAcceptTypes != null ) {
				for ( ContentType theContentType : theRequestAcceptTypes ) {
					if ( hasMediaTypeFactory( theContentType.getMediaType() ) ) {
						theMediatype = theContentType;
						break out;
					}
				}
			}

			// |--> Any unknown Accept-Types?
			final List<String> theUnkonwnAcceptTypes = aRequestHeaderFields.getUnknownAcceptTypes();
			if ( ( SystemProperty.LOG_DEBUG.isEnabled() || _isVerbose ) && theUnkonwnAcceptTypes != null && theUnkonwnAcceptTypes.size() != 0 ) {
				LOGGER.log( Level.WARNING, "Unable to resolve unknown request's Header-Field <" + HeaderField.ACCEPT.getName() + ">: " + new VerboseTextBuilder().withElements( theUnkonwnAcceptTypes ).toString() );
			}
			// Any unknown Accept-Types? <--|

			final ContentType theRequestContentType = aRequestHeaderFields.getContentType();
			if ( theRequestContentType != null && hasMediaTypeFactory( theRequestContentType.getMediaType() ) ) {
				theMediatype = theRequestContentType;
			}
			// |--> Any unknown Content-Types?
			final List<String> theUnkonwnContentTypes = aRequestHeaderFields.getUnknownContentTypes();
			if ( ( SystemProperty.LOG_DEBUG.isEnabled() || _isVerbose ) && theUnkonwnContentTypes != null && theUnkonwnContentTypes.size() != 0 ) {
				LOGGER.log( Level.WARNING, "Unable to resolve unknown request's Header-Field <" + HeaderField.CONTENT_TYPE.getName() + ">: " + new VerboseTextBuilder().withElements( theUnkonwnContentTypes ).toString() );
			}
			// Any unknown Content-Types? <--|
		}
		return theMediatype;
	}

	/**
	 * Creates a {@link String} {@link MediaType} encoded as of the
	 * {@link HeaderField#CONTENT_TYPE} from the response header or if not set
	 * as of the {@link HeaderField#ACCEPT} from the request header or if not
	 * set as of the {@link HeaderField#CONTENT_TYPE} from the request header.
	 * 
	 * @param aResponse The response which to encode as of the detected
	 *        {@link MediaType}s.
	 * @param aRequestHeaderFields The Header-Fields from the request.
	 * @param aResponseHeaderFields The Header-Fields from the response.
	 * 
	 * @return An accordingly encoded response as byte array.
	 * 
	 * @throws MarshalException thrown when marshaling / serializing an object
	 *         failed.
	 * @throws UnsupportedMediaTypeException thrown in case none of the
	 *         identified media types is supported, e.g. no required
	 *         {@link MediaTypeFactory} has been registered as of
	 *         {@link #addMediaTypeFactory(MediaTypeFactory)}.
	 */
	protected byte[] toResponseBody( Object aResponse, RequestHeaderFields aRequestHeaderFields, ResponseHeaderFields aResponseHeaderFields ) throws MarshalException, UnsupportedMediaTypeException {

		if ( aResponse == null ) {
			return new byte[] {};
		}

		// 1. Response Content-Type:
		String theResponseBody = toMarshaled( aResponse, aResponseHeaderFields.getContentType(), aResponseHeaderFields );
		if ( theResponseBody != null ) {
			if ( SystemProperty.LOG_DEBUG.isEnabled() || _isVerbose ) {
				LOGGER.log( Level.INFO, "Auto-determined Response-Header's <" + HeaderField.CONTENT_TYPE.getName() + "> Media-Type <" + aResponseHeaderFields.getContentType().toHttpMediaType() + "> for the response." );
			}
			return theResponseBody.getBytes();
		}
		// 2. Request Accept-Types:
		final List<ContentType> theAcceptTypes = aRequestHeaderFields.getAcceptTypes();
		if ( theAcceptTypes != null ) {
			for ( ContentType eContentType : theAcceptTypes ) {
				theResponseBody = toMarshaled( aResponse, eContentType, aResponseHeaderFields );
				if ( theResponseBody != null ) {
					if ( SystemProperty.LOG_DEBUG.isEnabled() || _isVerbose ) {
						LOGGER.log( Level.INFO, "Auto-determined Request-Header's <" + HeaderField.ACCEPT.getName() + "> Media-Type <" + eContentType.toHttpMediaType() + "> for the response." );
					}
					return theResponseBody.getBytes();
				}
			}
		}
		// 3. Request Content-Type:
		theResponseBody = toMarshaled( aResponse, aRequestHeaderFields.getContentType(), aResponseHeaderFields );
		if ( theResponseBody != null ) {
			if ( SystemProperty.LOG_DEBUG.isEnabled() || _isVerbose ) {
				LOGGER.log( Level.INFO, "Auto-determined Request-Header's <" + HeaderField.CONTENT_TYPE.getName() + "> Media-Type <" + aRequestHeaderFields.getContentType().toHttpMediaType() + "> for the response." );
			}
			return theResponseBody.getBytes();
		}
		// 4. Some detectable Header-Field types were provided, but no factory:
		if ( aResponseHeaderFields.getContentType() != null || aRequestHeaderFields.getContentType() != null || ( aRequestHeaderFields.getAcceptTypes() != null && aRequestHeaderFields.getAcceptTypes().size() != 0 ) ) {
			throw new UnsupportedMediaTypeException( "No Media-Type factory found for request ACCEPT types <" + new VerboseTextBuilder().withElements( aRequestHeaderFields.getAcceptTypes() ) + "> or response CONTENT-TYPE <" + aResponseHeaderFields.getContentType() + "> or request CONTENT type <" + aRequestHeaderFields.getContentType() + ">." );
		}
		// 5. No Header-Field types provided, using Media-Type factories:
		//	MediaType[] theMediaTypes = getFactoryMediaTypes();
		//	if ( theMediaTypes != null && theMediaTypes.length != 0 ) {
		//		theResponseBody = toMarshaled( aResponse, theMediaTypes[0], Encoding.UTF_8.getCode(), aResponseHeaderFields );
		//		if ( theResponseBody != null ) {
		//			if ( SystemProperty.LOG_DEBUG.isEnabled() || _isVerbose ) {
		// 				LOGGER.log( Level.INFO,  "Auto-configured first supported fallback Media-Type <" + theMediaTypes[0].toHttpMediaType() + "> for the response." );
		// 			}
		//			return theResponseBody.getBytes();
		//		}
		//	}

		// 6. No Header-Field types and no Media-Type factories found
		throw new UnsupportedMediaTypeException( "No Media-Type in HTTP-Request detected." );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean fireEvent( HttpRequest aEvent, RestEndpoint aObserver, ExecutionStrategy aExecutionStrategy ) throws VetoException {
		throw new UnsupportedOperationException( "As the #onHttpRequest method takes care of observer invocation." );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INTERCEPTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Invoked to pre-process a {@link HttpServerRequest} alongside a
	 * {@link HttpServerResponse}.
	 *
	 * @param aRequest The {@link HttpServerRequest} to pre-process.
	 * @param aResponse The {@link HttpServerResponse} to post-process.
	 */
	protected void preIntercept( HttpServerRequest aRequest, HttpServerResponse aResponse ) {}

	/**
	 * Invoked to post-process a {@link HttpServerRequest} alongside a
	 * {@link HttpServerResponse}.
	 *
	 * @param aRequest The {@link HttpServerRequest} to post-process.
	 * @param aResponse The {@link HttpServerResponse} to post-process.
	 */
	protected void postIntercept( HttpServerRequest aRequest, HttpServerResponse aResponse ) {}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private String toMarshaled( Object aResponse, MediaType aMediaType, Map<String, String> aMediaTypeParams, ResponseHeaderFields aResponseHeaderFields ) {
		if ( aMediaTypeParams != null && aMediaTypeParams.isEmpty() ) {
			aMediaTypeParams = null;
		}
		final MediaTypeFactory theFactory = toMediaTypeFactory( aMediaType );
		if ( theFactory != null ) {
			try {
				final String theMarshaled = theFactory.toMarshaled( aResponse, aMediaTypeParams );
				final ContentType theContentType = new ContentType( aMediaType, aMediaTypeParams );
				aResponseHeaderFields.putContentType( theContentType );
				return theMarshaled;
			}
			catch ( Exception e ) {}
		}
		return null;
	}

	private String toMarshaled( Object aResponse, ContentType contentType, ResponseHeaderFields aResponseHeaderFields ) {
		return toMarshaled( aResponse, contentType != null ? contentType.getMediaType() : null, contentType, aResponseHeaderFields );
	}

	//	private String toMarshaled( Object aResponse, MediaType aMediaType, String aCharset, ResponseHeaderFields aResponseHeaderFields ) {
	//		if ( aCharset == null ) { return toMarshaled( aResponse, aMediaType, (Map<String, String>) null, aResponseHeaderFields ); }
	//		return toMarshaled( aResponse, aMediaType, new PropertiesBuilderImpl().withPut( MediaTypeParameter.CHARSET.getName(), aCharset ), aResponseHeaderFields );
	//	}
}
