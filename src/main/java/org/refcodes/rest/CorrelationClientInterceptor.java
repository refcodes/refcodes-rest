// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.runtime.Correlation;
import org.refcodes.web.HttpClientInterceptor;
import org.refcodes.web.HttpClientRequest;
import org.refcodes.web.HttpClientResponse;

/**
 * The {@link CorrelationClientInterceptor} manages (adds) correlation IDs for
 * request and session.
 */
public class CorrelationClientInterceptor implements HttpClientInterceptor {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void preIntercept( HttpClientRequest aRequest, HttpClientResponse aResponse ) {
		String theRequestId = aRequest.getHeaderFields().getRequestId();
		if ( theRequestId != null && theRequestId.length() != 0 ) {
			Correlation.REQUEST.setId( theRequestId );
		}
		else {
			theRequestId = Correlation.REQUEST.pullId();
		}
		aResponse.getHeaderFields().putRequestId( theRequestId );
		String theSessionId = aRequest.getHeaderFields().getSessionId();
		if ( theSessionId != null && theSessionId.length() != 0 ) {
			Correlation.SESSION.setId( theSessionId );
		}
		else {
			theSessionId = Correlation.SESSION.pullId();
		}
		aResponse.getHeaderFields().putSessionId( theSessionId );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void postIntercept( HttpClientRequest aRequest, HttpClientResponse aResponse ) {
		String theRequestId = aResponse.getHeaderFields().getRequestId();
		if ( theRequestId != null && theRequestId.length() != 0 ) {
			Correlation.REQUEST.setId( theRequestId );
		}
		else {
			theRequestId = aRequest.getHeaderFields().getRequestId();
			if ( theRequestId == null || theRequestId.isEmpty() ) {
				theRequestId = Correlation.REQUEST.pullId();
			}
			aResponse.getHeaderFields().putRequestId( theRequestId );
		}
		String theSessionId = aResponse.getHeaderFields().getSessionId();
		if ( theSessionId != null && theSessionId.length() != 0 ) {
			Correlation.SESSION.setId( theSessionId );
		}
		else {
			theSessionId = aRequest.getHeaderFields().getSessionId();
			if ( theSessionId == null || theSessionId.isEmpty() ) {
				theSessionId = Correlation.SESSION.pullId();
			}
			aResponse.getHeaderFields().putSessionId( theSessionId );
		}
	}
}
