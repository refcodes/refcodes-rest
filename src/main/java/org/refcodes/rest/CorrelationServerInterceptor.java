// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.runtime.Correlation;
import org.refcodes.web.HttpServerInterceptor;
import org.refcodes.web.HttpServerRequest;
import org.refcodes.web.HttpServerResponse;

/**
 * The {@link CorrelationServerInterceptor} manages (adds) correlation IDs for
 * request and session.
 */
public class CorrelationServerInterceptor implements HttpServerInterceptor {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void preIntercept( HttpServerRequest aRequest, HttpServerResponse aResponse ) {
		String theRequestId = aRequest.getHeaderFields().getRequestId();
		if ( theRequestId != null && theRequestId.length() != 0 ) {
			Correlation.REQUEST.setId( theRequestId );
		}
		else {
			theRequestId = Correlation.REQUEST.pullId();
		}
		aResponse.getHeaderFields().putRequestId( theRequestId );
		String theSessionId = aRequest.getHeaderFields().getSessionId();
		if ( theSessionId != null && theSessionId.length() != 0 ) {
			Correlation.SESSION.setId( theSessionId );
		}
		else {
			theSessionId = Correlation.SESSION.pullId();
		}
		aResponse.getHeaderFields().putSessionId( theSessionId );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void postIntercept( HttpServerRequest aRequest, HttpServerResponse aResponse ) {
		String theRequestId = aResponse.getHeaderFields().getRequestId();
		if ( theRequestId != null && theRequestId.length() != 0 ) {
			Correlation.REQUEST.setId( theRequestId );
		}
		else {
			theRequestId = aRequest.getHeaderFields().getRequestId();
			if ( theRequestId == null || theRequestId.isEmpty() ) {
				theRequestId = Correlation.REQUEST.pullId();
			}
			aResponse.getHeaderFields().putRequestId( theRequestId );
		}
		String theSessionId = aResponse.getHeaderFields().getSessionId();
		if ( theSessionId != null && theSessionId.length() != 0 ) {
			Correlation.SESSION.setId( theSessionId );
		}
		else {
			theSessionId = aRequest.getHeaderFields().getSessionId();
			if ( theSessionId == null || theSessionId.isEmpty() ) {
				theSessionId = Correlation.SESSION.pullId();
			}
			aResponse.getHeaderFields().putSessionId( theSessionId );
		}
	}
}
