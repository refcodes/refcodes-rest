// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.component.ConnectionStatus;
import org.refcodes.component.ConnectionStatusAccessor;
import org.refcodes.component.LinkComponent.LinkComponentBuilder;
import org.refcodes.data.DaemonLoopSleepTime;
import org.refcodes.exception.Trap;
import org.refcodes.io.IOResultAccessor;
import org.refcodes.mixin.ResponseAccessor;
import org.refcodes.textual.VerboseTextBuilder;
import org.refcodes.web.FormFields;
import org.refcodes.web.HeaderFieldsAccessor.HeaderFieldsBuilder;
import org.refcodes.web.HeaderFieldsAccessor.HeaderFieldsProperty;
import org.refcodes.web.HttpClientRequest;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpMethodAccessor.HttpMethodBuilder;
import org.refcodes.web.HttpMethodAccessor.HttpMethodProperty;
import org.refcodes.web.HttpRequestBuilder;
import org.refcodes.web.HttpResponseException;
import org.refcodes.web.HttpStatusCode;
import org.refcodes.web.MediaTypeFactoryLookup;
import org.refcodes.web.QueryFieldsAccessor.QueryFieldsProperty;
import org.refcodes.web.RedirectDepthAccessor.RedirectDepthBuilder;
import org.refcodes.web.RequestHeaderFields;
import org.refcodes.web.Url;

/**
 * A {@link RestResponseResult} handles a REST request asynchronously on the
 * {@link RestfulClient} instance's side to do the actual technical
 * implementation of sending that request (or mocking the send-out of a
 * request).
 */
public class RestResponseResult extends HttpClientRequest implements RestResponseConsumer, QueryFieldsProperty, HeaderFieldsProperty<RequestHeaderFields>, RedirectDepthBuilder<RestResponseResult>, HttpRequestBuilder<RestResponseResult>, HttpMethodProperty, HttpMethodBuilder<RestResponseResult>, LinkComponentBuilder<RestResponseResult>, ConnectionStatusAccessor, HeaderFieldsBuilder<RequestHeaderFields, RestResponseResult>, IOResultAccessor<RestResponse, InterruptedException>, ResponseAccessor<RestResponse> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( RestResponseResult.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private RestResponse _response = null;
	private ConnectionStatus _connectionStatus = ConnectionStatus.NONE;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link RestResponseResult}.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest the request
	 * @param aRedirectDepth The redirect depth provides the count of
	 *        HTTP-Request and HTTP-Response cycles where the response
	 *        represents a redirect as of
	 *        {@link HttpStatusCode#isRedirectStatus()}. A value of -1
	 *        represents the default behavior, e.g. using
	 *        {@link java.net.HttpURLConnection}'s redirection means.
	 * @param aMediaTypeFactoryLookup the media type factory lookup
	 */
	public RestResponseResult( HttpMethod aHttpMethod, Url aUrl, Object aRequest, int aRedirectDepth, MediaTypeFactoryLookup aMediaTypeFactoryLookup ) {
		super( aHttpMethod, aUrl, aRequest, aRedirectDepth, aMediaTypeFactoryLookup );
	}

	/**
	 * Instantiates a new {@link RestResponseResult}.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest the request
	 * @param aMediaTypeFactoryLookup the media type factory lookup
	 */
	public RestResponseResult( HttpMethod aHttpMethod, Url aUrl, Object aRequest, MediaTypeFactoryLookup aMediaTypeFactoryLookup ) {
		super( aHttpMethod, aUrl, aRequest, aMediaTypeFactoryLookup );
	}

	/**
	 * Instantiates a new {@link RestResponseResult}.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields the Header-Fields
	 * @param aRequest the request
	 * @param aRedirectDepth The redirect depth provides the count of
	 *        HTTP-Request and HTTP-Response cycles where the response
	 *        represents a redirect as of
	 *        {@link HttpStatusCode#isRedirectStatus()}. A value of -1
	 *        represents the default behavior, e.g. using
	 *        {@link java.net.HttpURLConnection}'s redirection means.
	 * @param aMediaTypeFactoryLookup the media type factory lookup
	 */
	public RestResponseResult( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, int aRedirectDepth, MediaTypeFactoryLookup aMediaTypeFactoryLookup ) {
		super( aHttpMethod, aUrl, aHeaderFields, aRequest, aRedirectDepth, aMediaTypeFactoryLookup );
	}

	/**
	 * Instantiates a new {@link RestResponseResult}.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields the Header-Fields
	 * @param aRequest the request
	 * @param aMediaTypeFactoryLookup the media type factory lookup
	 */
	public RestResponseResult( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, MediaTypeFactoryLookup aMediaTypeFactoryLookup ) {
		super( aHttpMethod, aUrl, aHeaderFields, aRequest, aMediaTypeFactoryLookup );
	}

	/**
	 * Instantiates a new {@link RestResponseResult}.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRedirectDepth The redirect depth provides the count of
	 *        HTTP-Request and HTTP-Response cycles where the response
	 *        represents a redirect as of
	 *        {@link HttpStatusCode#isRedirectStatus()}. A value of -1
	 *        represents the default behavior, e.g. using
	 *        {@link java.net.HttpURLConnection}'s redirection means.
	 * @param aMediaTypeFactoryLookup the media type factory lookup
	 */
	public RestResponseResult( HttpMethod aHttpMethod, Url aUrl, int aRedirectDepth, MediaTypeFactoryLookup aMediaTypeFactoryLookup ) {
		super( aHttpMethod, aUrl, aRedirectDepth, aMediaTypeFactoryLookup );
	}

	/**
	 * Instantiates a new {@link RestResponseResult}.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aMediaTypeFactoryLookup the media type factory lookup
	 */
	public RestResponseResult( HttpMethod aHttpMethod, Url aUrl, MediaTypeFactoryLookup aMediaTypeFactoryLookup ) {
		super( aHttpMethod, aUrl, aMediaTypeFactoryLookup );
	}

	/**
	 * Instantiates a new {@link RestResponseResult}.
	 *
	 * @param aMediaTypeFactoryLookup the media type factory lookup
	 */
	public RestResponseResult( MediaTypeFactoryLookup aMediaTypeFactoryLookup ) {
		super( null, null, null, null, aMediaTypeFactoryLookup );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the expected {@link RestResponse} result, if none result is
	 * available it blocks until one is available (or this instance is notified
	 * via {@link #notifyAll()} or one of the {@link #close()} methods). To test
	 * for a result, call {@link #hasResult()}. The method {@link #getResult()}
	 * does not block and returns null if there is no result available yet.
	 *
	 * @return The {@link RestResponse} as soon as it is available.
	 * 
	 * @throws InterruptedException thrown in case this blocking method is
	 *         interrupted by invoking {@link Object#notifyAll()}.
	 */
	@Override
	public RestResponse getResult() throws InterruptedException {
		if ( !hasResult() ) {
			synchronized ( this ) {
				if ( !hasResult() ) {
					try {
						wait();
					}
					catch ( InterruptedException e ) {
						if ( !hasResult() ) {
							throw e;
						}
					}
				}
			}
		}
		return getResponse();
	}

	/**
	 * This method retrieves the received {@link RestResponse}. It does not
	 * block and returns null if there is no result available yet. To block till
	 * a result is available, please call {@link #getResult()}. {@inheritDoc}
	 */
	@Override
	public RestResponse getResponse() {
		return _response;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpMethod getHttpMethod() {
		return _httpMethod;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpMethod( HttpMethod aHttpMethod ) {
		_httpMethod = aHttpMethod;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open() throws IOException {
		_connectionStatus = ConnectionStatus.OPENED;
		synchronized ( this ) {
			notifyAll();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_connectionStatus = ConnectionStatus.CLOSED;
		synchronized ( this ) {
			notifyAll();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConnectionStatus getConnectionStatus() {
		return _connectionStatus;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHeaderFields( RequestHeaderFields aHeaderFields ) {
		_headerFields = aHeaderFields;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getUrl() {
		return _url;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setUrl( Url aUrl ) {
		_url = aUrl;
	}

	/**
	 * Tests whether there is already a HTTP-Response.
	 *
	 * @return True in case there is a HTTP-Response, else false.
	 */
	@Override
	public boolean hasResult() {
		return getResponse() != null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setQueryFields( FormFields aQueryFields ) {
		final Url theUrl = getUrl();
		final org.refcodes.web.UrlBuilder theUrlBuilder;
		if ( theUrl instanceof UrlBuilder ) {
			theUrlBuilder = (org.refcodes.web.UrlBuilder) theUrl;
		}
		else {
			theUrlBuilder = new org.refcodes.web.UrlBuilder( theUrl );
		}
		theUrlBuilder.setQueryFields( aQueryFields );
		setUrl( theUrlBuilder );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FormFields getQueryFields() {
		return getUrl().getQueryFields();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseResult withUrl( Url aUrl ) {
		setUrl( aUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseResult withHttpMethod( HttpMethod aHttpMethod ) {
		setHttpMethod( aHttpMethod );
		return this;
	}

	/**
	 * Sets the request for the request property.
	 *
	 * @param <REQ> the generic type
	 * @param aRequest The request to be stored by the request property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public <REQ> RestResponseResult withRequest( REQ aRequest ) {
		setRequest( aRequest );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseResult withHeaderFields( RequestHeaderFields aRequestHeaderFields ) {
		setHeaderFields( aRequestHeaderFields );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseResult withRedirectDepth( int aRedirectDepth ) {
		setRedirectDepth( aRedirectDepth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseResult withOpen() throws IOException {
		open();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseResult withClose() throws IOException {
		close();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseResult withCloseQuietly() {
		closeQuietly();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseResult withCloseIn( int aCloseMillis ) {
		closeIn( aCloseMillis );
		return this;
	}

	/**
	 * This is a convenience method for easily instantiating the according
	 * builder.
	 * 
	 * @param aRestClient the rest client to take care of the caller.
	 * 
	 * @return an instance (using a public implementation) of this builder
	 */
	public RestResponseResult build( RestfulClient aRestClient ) {
		return new RestResponseResult( aRestClient );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + _httpMethod + ": " + _url.toHttpUrl() + "?" + new VerboseTextBuilder().withElements( _url.getQueryFields() ).toString() + ")@" + hashCode();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onResponse( RestResponseEvent aResponse ) throws HttpResponseException {
		if ( getConnectionStatus() != ConnectionStatus.CLOSED ) {
			_response = aResponse;
			synchronized ( this ) {
				notifyAll();
			}
		}
		else {
			LOGGER.log( Level.WARNING, "Ignoring response <" + aResponse + "> as this rest endpoint is in status <" + _connectionStatus + ">, you may have closed it already?" );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The Class RequestDaemon.
	 */
	static class RestResponseResultDaemon implements Runnable {

		private final RestResponseResult _responseResult;
		private final RestRequestHandler _requestHandler;
		private final RestfulClient _source;

		/**
		 * Instantiates a new request daemon.
		 *
		 * @param aRestResponseResult the rest response result.
		 * @param aRequestHandler The handler technically executing the request.
		 * @param aSource the source
		 */
		protected RestResponseResultDaemon( RestResponseResult aRestResponseResult, RestRequestHandler aRequestHandler, RestfulClient aSource ) {
			if ( aRequestHandler == null ) {
				throw new IllegalArgumentException( "Unable to process your request <" + aRestResponseResult.toString() + "> as no <" + RestRequestHandler.class.getSimpleName() + "> has been implemented, aborting!" );
			}
			_responseResult = aRestResponseResult;
			_requestHandler = aRequestHandler;
			_source = aSource;
		}

		/**
		 * Run.
		 */
		@Override
		public void run() {
			try {
				// |--> Wait till the connection is opened / closed:
				synchronized ( _responseResult ) {
					while ( _responseResult.getConnectionStatus() == ConnectionStatus.NONE ) {
						try {
							_responseResult.wait( DaemonLoopSleepTime.MAX.getTimeMillis() );
						}
						catch ( InterruptedException e ) {}
						if ( _responseResult.getConnectionStatus() == ConnectionStatus.NONE ) {
							LOGGER.log( Level.WARNING, "Your result's <" + _responseResult + "> connection status is still <" + _responseResult.getConnectionStatus() + "> after <" + DaemonLoopSleepTime.MAX.getTimeMillis() + "> ms, execution of your request starts not earlier than you calling the #open() method." );
						}
					}
				}
				// Wait till the connection is opened / closed <--|

				if ( _responseResult.getConnectionStatus() != ConnectionStatus.OPENED ) {
					throw new IllegalStateException( "Aborting your request as of your result <" + _responseResult + "> connection status is <" + _responseResult.getConnectionStatus() + "> although it is expected to e <" + ConnectionStatus.OPENED + ">." );
				}
				final RestResponse theResponse = _requestHandler.doRequest( _responseResult );
				_responseResult.onResponse( new RestResponseEvent( theResponse, _source ) );
			}
			catch ( HttpResponseException e ) {
				LOGGER.log( Level.SEVERE, Trap.asMessage( e ), e );
			}
		}
	}
}
