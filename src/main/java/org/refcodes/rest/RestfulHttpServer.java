// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.io.IOException;
import java.security.KeyStore;

import javax.net.ssl.SSLContext;

import org.refcodes.component.ConnectionComponent.ConnectionComponentBuilder;
import org.refcodes.component.ConnectionStatusAccessor;
import org.refcodes.component.LinkComponent;
import org.refcodes.data.Scheme;
import org.refcodes.io.MaxConnectionsAccessor.MaxConnectionsBuilder;
import org.refcodes.io.MaxConnectionsAccessor.MaxConnectionsProperty;
import org.refcodes.mixin.PortAccessor.PortBuilder;
import org.refcodes.mixin.PortAccessor.PortProperty;
import org.refcodes.rest.HttpExceptionHandlerAccessor.HttpExceptionHandlerBuilder;
import org.refcodes.rest.HttpExceptionHandlerAccessor.HttpExceptionHandlerProperty;
import org.refcodes.rest.HttpExceptionHandlingAccessor.HttpExceptionHandlingBuilder;
import org.refcodes.rest.HttpExceptionHandlingAccessor.HttpExceptionHandlingProperty;
import org.refcodes.security.KeyStoreDescriptor;
import org.refcodes.security.KeyStoreDescriptorAccessor.KeyStoreDescriptorBuilder;
import org.refcodes.security.KeyStoreDescriptorAccessor.KeyStoreDescriptorProperty;
import org.refcodes.web.BasicAuthObservable;
import org.refcodes.web.HttpServerContext;
import org.refcodes.web.HttpServerInterceptable;
import org.refcodes.web.HttpsConnectionRequestObservable;
import org.refcodes.web.SchemeAccessor.SchemeBuilder;
import org.refcodes.web.SchemeAccessor.SchemeProperty;

/**
 * Extends a {@link RestfulServer} to be capable of opening a server socket on
 * the local host with the provided port number via {@link #open(Object)} or
 * with an additional maximum number of connections via {@link #open(int, int)}.
 * To open a HTTPS port, use the methods such as:
 * {@link #open(KeyStoreDescriptor, int)} or
 * {@link #open(KeyStoreDescriptor, int, int)} or
 * {@link #open(String, KeyStoreDescriptor, int)} or
 * {@link #open(String, KeyStoreDescriptor, int, int)}. A
 * {@link RestfulHttpServer} can be shutdown via {@link #close()}.
 */
public interface RestfulHttpServer extends RestfulServer, HttpServerInterceptable, HttpExceptionHandlerProperty, HttpExceptionHandlerBuilder<RestfulHttpServer>, HttpExceptionHandlingProperty, HttpExceptionHandlingBuilder<RestfulHttpServer>, LinkComponent, ConnectionStatusAccessor, ConnectionComponentBuilder<HttpServerContext, RestfulHttpServer>, BasicAuthObservable<RestfulHttpServer>, HttpsConnectionRequestObservable<RestfulHttpServer>, PortProperty, PortBuilder<RestfulHttpServer>, SchemeProperty, SchemeBuilder<RestfulHttpServer>, KeyStoreDescriptorProperty, KeyStoreDescriptorBuilder<RestfulHttpServer>, MaxConnectionsProperty, MaxConnectionsBuilder<RestfulHttpServer> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpServer withHttpExceptionHandler( HttpExceptionHandler aHttpErrorHandler ) {
		setHttpExceptionHandler( aHttpErrorHandler );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpServer withHttpExceptionHandling( HttpExceptionHandling aHttpErrorHandling ) {
		setHttpExceptionHandling( aHttpErrorHandling );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpServer withRealm( String aRealm ) {
		setRealm( aRealm );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpServer withBaseLocator( String aBaseLocator ) {
		setBaseLocator( aBaseLocator );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpServer withClose() throws IOException {
		close();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpServer withCloseQuietly() {
		closeQuietly();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpServer withCloseIn( int aCloseMillis ) {
		closeIn( aCloseMillis );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpServer withOpen( HttpServerContext aConnection ) throws IOException {
		open( aConnection );
		return this;
	}

	/**
	 * Builder method for opening the {@link RestfulHttpServer}. Delegates to
	 * {@link #open(int)} and returns this method.
	 * 
	 * @param aPort The port on which to listen for requests.
	 * 
	 * @return This instance for applying method chaining.
	 * 
	 * @throws IOException thrown in case something went wrong such as the port
	 *         being already in use.
	 */
	default RestfulHttpServer withOpen( int aPort ) throws IOException {
		open( aPort );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpServer withPort( int aPort ) {
		setPort( aPort );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpServer withScheme( Scheme aScheme ) {
		setScheme( aScheme );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpServer withProtocol( String aProtocol ) {
		setProtocol( aProtocol );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpServer withKeyStoreDescriptor( KeyStoreDescriptor aKeyStoreDescriptor ) {
		setKeyStoreDescriptor( aKeyStoreDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpServer withMaxConnections( int aMaxConnections ) {
		setMaxConnections( aMaxConnections );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// OPEN:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void open() throws IOException {
		open( toProtocol(), getKeyStoreDescriptor(), getPort(), getMaxConnections() );
	}

	/**
	 * Opens the HTTP sever connection (socket) at the given port.
	 *
	 * @param aPort The port on which to listen for requests.
	 * 
	 * @throws IOException thrown in case something went wrong such as the port
	 *         being already in use.
	 */
	default void open( int aPort ) throws IOException {
		open( toProtocol(), getKeyStoreDescriptor(), aPort, getMaxConnections() );
	}

	/**
	 * Opens the HTTP sever connection (socket) at the given port allowing the
	 * given number of maximum connections at the same time.
	 * 
	 * @param aPort The port on which to listen for requests.
	 * @param aMaxConnections The number of maximum connections at the same
	 *        time.
	 * 
	 * @throws IOException thrown in case something went wrong such as the port
	 *         being already in use.
	 */
	default void open( int aPort, int aMaxConnections ) throws IOException {
		open( toProtocol(), getKeyStoreDescriptor(), aPort, aMaxConnections );
	}

	/**
	 * Same as {@link #open(String, KeyStoreDescriptor, int, int)} but:
	 * <ul>
	 * <li>The number of maximum connections at the same time is decided upon by
	 * the implementations's default value.
	 * </ul>
	 * 
	 * @param aProtocol The protocol to use, e.g. "TLS".
	 * @param aStoreDescriptor The store descriptor describing your
	 *        {@link KeyStore} required for HTTPS.
	 * @param aPort The port on which to listen for requests.
	 * 
	 * @throws IOException thrown in case something went wrong such as the port
	 *         being already in use.
	 */
	default void open( String aProtocol, KeyStoreDescriptor aStoreDescriptor, int aPort ) throws IOException {
		open( aProtocol, aStoreDescriptor, aPort, getMaxConnections() );
	}

	/**
	 * Same as {@link #open(String, KeyStoreDescriptor, int)} but:
	 * <ul>
	 * <li>The number of maximum connections at the same time is decided upon by
	 * the implementations's default value.
	 * </ul>
	 * 
	 * @param aProtocol The protocol to use, e.g. "TLS".
	 * @param aStoreDescriptor The store descriptor describing your
	 *        {@link KeyStore} required for HTTPS.
	 * 
	 * @throws IOException thrown in case something went wrong such as the port
	 *         being already in use.
	 */
	default void open( String aProtocol, KeyStoreDescriptor aStoreDescriptor ) throws IOException {
		open( aProtocol, aStoreDescriptor, getPort(), getMaxConnections() );
	}

	/**
	 * Same as {@link #open(String, KeyStoreDescriptor, int, int)} but:
	 * <ul>
	 * <li>The protocol for the underlying {@link SSLContext} used is set to
	 * "TLS".
	 * <li>The type of {@link KeyStore} is set to "JKS".
	 * <li>The password for the {@link KeyStore}'s key is assumed to be the same
	 * as the provided {@link KeyStore} password.
	 * </ul>
	 * 
	 * @param aStoreDescriptor The store descriptor describing your
	 *        {@link KeyStore} required for HTTPS.
	 * @param aPort The port on which to listen for requests.
	 * @param aMaxConnections The number of maximum connections at the same
	 *        time.
	 * 
	 * @throws IOException thrown in case something went wrong such as the port
	 *         being already in use.
	 */
	default void open( KeyStoreDescriptor aStoreDescriptor, int aPort, int aMaxConnections ) throws IOException {
		open( toProtocol(), aStoreDescriptor, aPort, aMaxConnections );
	}

	/**
	 * Same as {@link #open(String, KeyStoreDescriptor, int, int)} but:
	 * <ul>
	 * <li>The protocol for the underlying {@link SSLContext} used is set to
	 * "TLS".
	 * <li>The type of {@link KeyStore} is set to "JKS".
	 * </ul>
	 * 
	 * @param aStoreDescriptor The store descriptor describing your
	 *        {@link KeyStore} required for HTTPS.
	 * @param aPort The port on which to listen for requests.
	 * 
	 * @throws IOException thrown in case something went wrong such as the port
	 *         being already in use.
	 */
	default void open( KeyStoreDescriptor aStoreDescriptor, int aPort ) throws IOException {
		open( toProtocol(), aStoreDescriptor, aPort, getMaxConnections() );
	}

	/**
	 * Same as {@link #open(String, KeyStoreDescriptor, int)} but:
	 * <ul>
	 * <li>The protocol for the underlying {@link SSLContext} used is set to
	 * "TLS".
	 * <li>The type of {@link KeyStore} is set to "JKS".
	 * </ul>
	 * 
	 * @param aStoreDescriptor The store descriptor describing your
	 *        {@link KeyStore} required for HTTPS.
	 * 
	 * @throws IOException thrown in case something went wrong such as the port
	 *         being already in use.
	 */
	default void open( KeyStoreDescriptor aStoreDescriptor ) throws IOException {
		open( toProtocol(), aStoreDescriptor, getPort(), getMaxConnections() );
	}

	/**
	 * Opens the HTTPS sever connection (socket) at the given port allowing the
	 * given number of maximum connections at the same time using the provided
	 * HTTPS configuration parameters.
	 * 
	 * @param aScheme The {@link Scheme} to use, e.g. {@link Scheme#HTTPS}
	 *        defaults to "TLS".
	 * @param aStoreDescriptor The store descriptor describing your
	 *        {@link KeyStore} required for HTTPS.
	 * @param aPort The port on which to listen for requests.
	 * @param aMaxConnections The number of maximum connections at the same
	 *        time.
	 * 
	 * @throws IOException thrown in case something went wrong such as the port
	 *         being already in use.
	 */
	default void open( Scheme aScheme, KeyStoreDescriptor aStoreDescriptor, int aPort, int aMaxConnections ) throws IOException {
		open( aScheme.name(), aStoreDescriptor, aPort, aMaxConnections );
	}

	/**
	 * Same as {@link #open(String, KeyStoreDescriptor, int, int)} but:
	 * <ul>
	 * <li>The number of maximum connections at the same time is decided upon by
	 * the implementations's default value.
	 * </ul>
	 * 
	 * @param aScheme The {@link Scheme} to use, e.g. {@link Scheme#HTTPS}
	 *        defaults to "TLS".
	 * @param aStoreDescriptor The store descriptor describing your
	 *        {@link KeyStore} required for HTTPS.
	 * @param aPort The port on which to listen for requests.
	 * 
	 * @throws IOException thrown in case something went wrong such as the port
	 *         being already in use.
	 */
	default void open( Scheme aScheme, KeyStoreDescriptor aStoreDescriptor, int aPort ) throws IOException {
		open( aScheme.toProtocol(), aStoreDescriptor, aPort, getMaxConnections() );
	}

	/**
	 * Same as {@link #open(String, KeyStoreDescriptor, int)} but:
	 * <ul>
	 * <li>The number of maximum connections at the same time is decided upon by
	 * the implementations's default value.
	 * </ul>
	 * 
	 * @param aScheme The {@link Scheme} to use, e.g. {@link Scheme#HTTPS}
	 *        defaults to "TLS".
	 * @param aStoreDescriptor The store descriptor describing your
	 *        {@link KeyStore} required for HTTPS.
	 * 
	 * @throws IOException thrown in case something went wrong such as the port
	 *         being already in use.
	 */
	default void open( Scheme aScheme, KeyStoreDescriptor aStoreDescriptor ) throws IOException {
		open( aScheme.toProtocol(), aStoreDescriptor, getPort(), getMaxConnections() );
	}

	/**
	 * Opens the HTTPS sever connection (socket) at the given port allowing the
	 * given number of maximum connections at the same time using the provided
	 * HTTPS configuration parameters.
	 * 
	 * @param aProtocol The protocol to use, e.g. "TLS".
	 * @param aStoreDescriptor The store descriptor describing your
	 *        {@link KeyStore} required for HTTPS.
	 * @param aPort The port on which to listen for requests.
	 * @param aMaxConnections The number of maximum connections at the same
	 *        time.
	 * 
	 * @throws IOException thrown in case something went wrong such as the port
	 *         being already in use.
	 */
	void open( String aProtocol, KeyStoreDescriptor aStoreDescriptor, int aPort, int aMaxConnections ) throws IOException;

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void open( HttpServerContext aConnection ) throws IOException {
		open( aConnection.toProtocol(), aConnection.getKeyStoreDescriptor(), aConnection.getPort(), aConnection.getMaxConnections() );
	}
}
