package org.refcodes.rest;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyStore;

import org.refcodes.component.ConnectionComponent;
import org.refcodes.component.ConnectionStatusAccessor;
import org.refcodes.component.LinkComponent;
import org.refcodes.component.Openable.OpenBuilder;
import org.refcodes.security.StoreType;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.security.TrustStoreDescriptorAccessor.TrustStoreDescriptorBuilder;
import org.refcodes.security.TrustStoreDescriptorAccessor.TrustStoreDescriptorProperty;
import org.refcodes.web.BaseUrlAccessor.BaseUrlBuilder;
import org.refcodes.web.BaseUrlAccessor.BaseUrlProperty;
import org.refcodes.web.BasicAuthCredentials;
import org.refcodes.web.HttpClientContext;
import org.refcodes.web.HttpClientInterceptable;
import org.refcodes.web.OauthToken;
import org.refcodes.web.Url;

/**
 * Extends a {@link RestfulClient} to be capable of providing a User-Agent with
 * {@link #setUserAgent(String)} ({@link #withUserAgent(String)}) and to be
 * capable of using base URLs to be set with {@link #setBaseUrl(String)} (
 * {@link #withBaseUrl(String)}). This type is intended to be used by different
 * separate hierarchy branches by providing the generic type &lt;B&gt;, ensuring
 * a coherent type hierarchy for each branch. To prepare HTTPS connections, use
 * the methods such as: {@link #open(Url, TrustStoreDescriptor)} or
 * {@link #open(HttpClientContext)}. A {@link RestfulHttpClient} can be shutdown
 * via {@link #close()}.
 */
public interface RestfulHttpClient extends RestfulClient, HttpClientInterceptable, ConnectionStatusAccessor, ConnectionComponent<HttpClientContext>, LinkComponent, OpenBuilder<RestfulHttpClient>, BaseUrlProperty, BaseUrlBuilder<RestfulHttpClient>, TrustStoreDescriptorProperty, TrustStoreDescriptorBuilder<RestfulHttpClient> {

	String DEFAULT_KEYSTORE_TYPE = StoreType.JKS.name();
	String DEFAULT_SSL_PROTOCOL = "TLS";

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void open() throws IOException {
		open( getBaseUrl(), getTrustStoreDescriptor() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void open( HttpClientContext aCtx ) throws IOException {
		open( aCtx.getBaseUrl(), aCtx.getTrustStoreDescriptor() );
	}

	/**
	 * Configures the HTTPS client connection with the provided configuration
	 * parameters.
	 * 
	 * @param aStoreDescriptor The {@link TrustStoreDescriptor} pointing to your
	 *        {@link KeyStore}.
	 * 
	 * @throws IOException thrown in case something went wrong.
	 */
	default void open( TrustStoreDescriptor aStoreDescriptor ) throws IOException {
		open( getBaseUrl(), aStoreDescriptor );
	}

	/**
	 * Configures the HTTPS client connection with the provided configuration
	 * parameters.
	 * 
	 * @param aBaseUrl The base {@link Url} to be used.
	 * 
	 * @throws IOException thrown in case something went wrong.
	 */
	default void open( Url aBaseUrl ) throws IOException {
		open( aBaseUrl, getTrustStoreDescriptor() );
	}

	/**
	 * Configures the HTTPS client connection with the provided configuration
	 * parameters.
	 * 
	 * @param aBaseUrl The base {@link Url} to be used.
	 * @param aStoreDescriptor The {@link TrustStoreDescriptor} pointing to your
	 *        {@link KeyStore}.
	 * 
	 * @throws IOException thrown in case something went wrong.
	 */
	void open( Url aBaseUrl, TrustStoreDescriptor aStoreDescriptor ) throws IOException;

	/**
	 * Open the component's connection(s).
	 * 
	 * @return This instance as of the builder pattern.
	 * 
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 */
	@Override
	default RestfulHttpClient withOpen() throws IOException {
		open();
		return this;
	}

	/**
	 * Opens the component with the given connection.
	 * 
	 * @param aCtx The context used for opening the connection.
	 * 
	 * @return This instance as of the builder pattern.
	 * 
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 */
	default RestfulHttpClient withOpen( HttpClientContext aCtx ) throws IOException {
		open( aCtx );
		return this;
	}

	/**
	 * Configures the HTTPS client connection with the provided configuration
	 * parameters.
	 * 
	 * @param aStoreDescriptor The {@link TrustStoreDescriptor} pointing to your
	 *        {@link KeyStore}.
	 * 
	 * @return This instance as of the builder pattern.
	 * 
	 * @throws IOException thrown in case something went wrong.
	 */
	default RestfulHttpClient withOpen( TrustStoreDescriptor aStoreDescriptor ) throws IOException {
		open( getBaseUrl(), aStoreDescriptor );
		return this;
	}

	/**
	 * Configures the HTTPS client connection with the provided configuration
	 * parameters.
	 * 
	 * @param aBaseUrl The base {@link Url} to be used.
	 * 
	 * @return This instance as of the builder pattern.
	 * 
	 * @throws IOException thrown in case something went wrong.
	 */
	default RestfulHttpClient withOpen( Url aBaseUrl ) throws IOException {
		open( aBaseUrl, getTrustStoreDescriptor() );
		return this;
	}

	/**
	 * Configures the HTTPS client connection with the provided configuration
	 * parameters.
	 * 
	 * @param aBaseUrl The base {@link Url} to be used.
	 * @param aStoreDescriptor The {@link TrustStoreDescriptor} pointing to your
	 *        {@link KeyStore}.
	 * 
	 * @return This instance as of the builder pattern.
	 * 
	 * @throws IOException thrown in case something went wrong.
	 */
	default RestfulHttpClient withOpen( Url aBaseUrl, TrustStoreDescriptor aStoreDescriptor ) throws IOException {
		open( aBaseUrl, aStoreDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpClient withBaseUrl( String aBaseUrl ) throws MalformedURLException {
		setBaseUrl( aBaseUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpClient withBaseUrl( Url aBaseUrl ) {
		setBaseUrl( aBaseUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpClient withBaseUrl( URL aBaseURL ) {
		setBaseUrl( aBaseURL );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpClient withBasicAuthCredentials( BasicAuthCredentials aBasicAuthCredentials ) {
		setBasicAuthCredentials( aBasicAuthCredentials );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpClient withBasicAuthCredentials( String aUserName, String aSecret ) {
		setBasicAuthCredentials( aUserName, aSecret );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpClient withOAuthToken( OauthToken aOauthToken ) {
		setOauthToken( aOauthToken );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpClient withTrustStoreDescriptor( TrustStoreDescriptor aStoreDescriptor ) {
		setTrustStoreDescriptor( aStoreDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulHttpClient withUserAgent( String aUserAgent ) {
		setUserAgent( aUserAgent );
		return this;
	}
}
