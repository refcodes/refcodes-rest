///////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
///////////////////////////////////////////////////////////////////////////////
package org.refcodes.rest;

import org.refcodes.web.HttpClientRequest;
import org.refcodes.web.HttpResponseException;

/**
 * A {@link RestRequestHandler} handles a REST request on the
 * {@link RestfulClient} instance's side to do the actual technical
 * implementation of sending that request (or mocking the send-out of a
 * request).
 */
@FunctionalInterface
public interface RestRequestHandler {

	/**
	 * Hook receiving a prepared {@link RestResponseHandler} instance to be used
	 * to do the technical request with the technology chosen by the
	 * implementing sub-class.
	 * 
	 * @param aHttpClientRequest The prepared {@link HttpClientRequest}.
	 * 
	 * @return The resulting {@link RestResponse}.
	 * 
	 * @throws HttpResponseException thrown by a HTTP response handling system
	 *         in case of some unexpected response.
	 */
	RestResponse doRequest( HttpClientRequest aHttpClientRequest ) throws HttpResponseException;

}
