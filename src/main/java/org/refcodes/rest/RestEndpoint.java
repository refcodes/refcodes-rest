// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.util.regex.Pattern;

import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpMethodAccessor;

/**
 * An {@link RestEndpoint} subscribes to a {@link RestfulServer} (
 * {@link RestfulHttpServer}) and defines the target for a REST request.
 * Therefore the {@link RestEndpoint} describes the {@link HttpMethod}, the
 * locator (pattern) to which to respond as well as a
 * {@link RestRequestConsumer} responsible for processing the request. The
 * {@link RestRequestConsumer} is invoked as soon as a request with the given
 * {@link HttpMethod} for a locator matching the given Locator-Pattern is being
 * processed by the {@link RestfulServer} ( {@link RestfulHttpServer}). The
 * locator for which an {@link RestEndpoint} is responsible for is defined by
 * the {@link RestEndpoint}'s Locator-Pattern: A single asterisk ("*") matches
 * zero or more characters within a locator name. A double asterisk ("**")
 * matches zero or more characters across directory levels. A question mark
 * ("?") matches exactly one character within a locator name. The single
 * asterisk ("*"), the double asterisk ("**") and the question mark ("?") we
 * refer to as wildcard: You get an array with the substitutes of the wildcards
 * using the {@link RestRequestEvent}'s method
 * {@link RestRequestEvent#getWildcardReplacements()} inside the
 * {@link #onRequest(RestRequestEvent, org.refcodes.web.HttpServerResponse)}
 * method. You may name a wildcard by prefixing it with "{someWildcardName}".
 * For example a named wildcard may look as follows: "{arg1}*" or "{arg2}**" or
 * "{arg3}?".
 */
public interface RestEndpoint extends RestRequestConsumer, HttpMethodAccessor {

	/**
	 * Retrieves the resource Locator-Pattern this endpoint has been registered
	 * to. The Locator-Pattern may contain wildcards as known from file-systems
	 * as follows: A single asterisk ("*") matches zero or more characters
	 * within a locator name. A double asterisk ("**") matches zero or more
	 * characters across directory levels. A question mark ("?") matches exactly
	 * one character within a locator name. The single asterisk ("*"), the
	 * double asterisk ("**") and the question mark ("?") we refer to as
	 * wildcard: You get an array with the substitutes of the wildcards using
	 * the {@link RestRequestEvent}'s method
	 * {@link RestRequestEvent#getWildcardReplacements()} inside the
	 * {@link #onRequest(RestRequestEvent, org.refcodes.web.HttpServerResponse)}
	 * method. You may name a wildcard by prefixing it with
	 * "{someWildcardName}". For example a named wildcard may look as follows:
	 * "{arg1}*" or "{arg2}**" or "{arg3}?". The {@link RestRequestEvent} lets
	 * you access the wildcard substitutes either by index or by name.
	 * {@inheritDoc}
	 */
	String getLocatorPathPattern();

	/**
	 * Retrieves the resource locator (regular expression) pattern this endpoint
	 * has been registered to. The {@link RestRequestEvent} lets you access the
	 * matching regular expression groups either by index or by name.
	 * {@inheritDoc}
	 */
	Pattern getLocatorRegExp();

}
