// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.net.InetSocketAddress;

import org.refcodes.mixin.WildcardSubstitutes;
import org.refcodes.observer.ActionEvent;
import org.refcodes.web.BasicAuthCredentials;
import org.refcodes.web.BasicAuthRequest;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.LocalAddressAccessor;
import org.refcodes.web.RealmAccessor;
import org.refcodes.web.RemoteAddressAccessor;
import org.refcodes.web.Url;

/**
 * The {@link BasicAuthEvent} describes a {@link BasicAuthRequest} being the
 * request as consumed by a {@link RestEndpoint}. Usually you will use
 * {@link RestfulServer#onRequest(HttpMethod, String, RestRequestConsumer)} for
 * registering a {@link RestRequestConsumer} to the {@link RestfulServer} (
 * {@link RestfulHttpServer}).
 */
public class BasicAuthEvent extends BasicAuthRequest implements ActionEvent<HttpMethod, RestfulServer>, WildcardSubstitutes, RealmAccessor, LocalAddressAccessor, RemoteAddressAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final InetSocketAddress _localAddress;
	private final InetSocketAddress _remoteAddress;
	private final RestfulServer _restfulServer;
	private final WildcardSubstitutes _wildcardSubstitutes;
	private final String _realm;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new basic auth event.
	 *
	 * @param aLocalAddress The local address where the event is being received.
	 * @param aRemoteAddress The remote address from which the request
	 *        originates.
	 * @param aHttpMethod The {@link HttpMethod} with which the request has been
	 *        sent.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aWildcardSubstitutes The text being substituted by the wildcard of
	 *        the {@link RestEndpoint}'s Locator-Pattern.
	 * @param aCredentials The credentials retrieved from the request.
	 * @param aRealm The realm to which this request is being addressed.
	 * @param aRestfulServer The system firing the event.
	 */
	public BasicAuthEvent( InetSocketAddress aLocalAddress, InetSocketAddress aRemoteAddress, HttpMethod aHttpMethod, Url aUrl, WildcardSubstitutes aWildcardSubstitutes, BasicAuthCredentials aCredentials, String aRealm, RestfulServer aRestfulServer ) {
		super( aHttpMethod, aUrl, aCredentials );
		_restfulServer = aRestfulServer;
		_localAddress = aLocalAddress;
		_remoteAddress = aRemoteAddress;
		_wildcardSubstitutes = aWildcardSubstitutes;
		_realm = aRealm;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpMethod getAction() {
		return getHttpMethod();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getWildcardReplacements() {
		return _wildcardSubstitutes.getWildcardReplacements();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getWildcardReplacementAt( int aIndex ) {
		return _wildcardSubstitutes.getWildcardReplacementAt( aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getWildcardReplacement( String aWildcardName ) {
		return _wildcardSubstitutes.getWildcardReplacement( aWildcardName );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getWildcardReplacements( String... aWildcardNames ) {
		return _wildcardSubstitutes.getWildcardReplacements( aWildcardNames );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getWildcardReplacementsAt( int... aIndexes ) {
		return _wildcardSubstitutes.getWildcardReplacementsAt( aIndexes );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InetSocketAddress getRemoteAddress() {
		return _remoteAddress;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InetSocketAddress getLocalAddress() {
		return _localAddress;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getRealm() {
		return _realm;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestfulServer getSource() {
		return _restfulServer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getWildcardNames() {
		return _wildcardSubstitutes.getWildcardNames();
	}
}
