// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.io.IOException;
import java.util.Iterator;
import java.util.regex.Pattern;

import org.refcodes.data.Scheme;
import org.refcodes.security.KeyStoreDescriptor;
import org.refcodes.web.BasicAuthObserver;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpServerContext;
import org.refcodes.web.HttpsConnectionRequestObserver;
import org.refcodes.web.MediaType;
import org.refcodes.web.MediaTypeFactory;
import org.refcodes.web.PostHttpServerInterceptor;
import org.refcodes.web.PreHttpServerInterceptor;

/**
 * Abstract class for easily decorating a {@link RestfulHttpServer}.
 *
 * @param <B> the generic type
 */
public abstract class AbstractRestfulHttpServerDecorator<B extends RestfulHttpServer> implements RestfulHttpServer {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected RestfulHttpServer _server;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the given {@link RestfulHttpServer} with additional
	 * functionality.
	 * 
	 * @param aServer The {@link RestfulHttpServer} to be decorated.
	 */
	public AbstractRestfulHttpServerDecorator( RestfulHttpServer aServer ) {
		_server = aServer;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPort() {
		return _server.getPort();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPort( int aPort ) {
		_server.setPort( aPort );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Scheme getScheme() {
		return _server.getScheme();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setScheme( Scheme aScheme ) {
		_server.setScheme( aScheme );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addMediaTypeFactory( MediaTypeFactory aMediaTypeFactory ) {
		return _server.addMediaTypeFactory( aMediaTypeFactory );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_server.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void closeIn( int aCloseMillis ) {
		_server.closeIn( aCloseMillis );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void closeQuietly() {
		_server.closeQuietly();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void closeUnchecked() {
		_server.closeUnchecked();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getBaseLocator() {
		return _server.getBaseLocator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MediaType[] getFactoryMediaTypes() {
		return _server.getFactoryMediaTypes();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getRealm() {
		return _server.getRealm();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasMediaTypeFactory( MediaType aMediaType ) {
		return _server.hasMediaTypeFactory( aMediaType );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasObserver( RestEndpoint aObserver ) {
		return _server.hasObserver( aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B onBasicAuthRequest( BasicAuthObserver aObserver ) {
		_server.onBasicAuthRequest( aObserver );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B onConnectionRequest( HttpsConnectionRequestObserver aObserver ) {
		_server.onConnectionRequest( aObserver );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestEndpointBuilder onDelete( String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		return _server.onDelete( aLocatorPathPattern, aRequestConsumer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestEndpointBuilder onGet( String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		return _server.onGet( aLocatorPathPattern, aRequestConsumer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestEndpointBuilder onPost( String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		return _server.onPost( aLocatorPathPattern, aRequestConsumer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestEndpointBuilder onPut( String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		return _server.onPut( aLocatorPathPattern, aRequestConsumer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestEndpointBuilder onRequest( HttpMethod aHttpMethod, String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		return _server.onRequest( aHttpMethod, aLocatorPathPattern, aRequestConsumer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestEndpointBuilder onRequest( String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		return _server.onRequest( aLocatorPathPattern, aRequestConsumer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestEndpointBuilder onDelete( Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		return _server.onDelete( aLocatorRegExp, aRequestConsumer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestEndpointBuilder onGet( Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		return _server.onGet( aLocatorRegExp, aRequestConsumer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestEndpointBuilder onPost( Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		return _server.onPost( aLocatorRegExp, aRequestConsumer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestEndpointBuilder onPut( Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		return _server.onPut( aLocatorRegExp, aRequestConsumer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestEndpointBuilder onRequest( HttpMethod aHttpMethod, Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		return _server.onRequest( aHttpMethod, aLocatorRegExp, aRequestConsumer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestEndpointBuilder onRequest( Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		return _server.onRequest( aLocatorRegExp, aRequestConsumer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean onRequest( RestEndpoint aRestEndpoint ) {
		return _server.onRequest( aRestEndpoint );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open( int aPort ) throws IOException {
		_server.open( aPort );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open( int aPort, int aMaxConnections ) throws IOException {
		_server.open( aPort, aMaxConnections );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open( String aProtocol, KeyStoreDescriptor aStoreDescriptor, int aPort ) throws IOException {
		_server.open( aProtocol, aStoreDescriptor, aPort );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open( String aProtocol, KeyStoreDescriptor aStoreDescriptor, int aPort, int aMaxConnections ) throws IOException {
		_server.open( aProtocol, aStoreDescriptor, aPort, aMaxConnections );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void openUnchecked( HttpServerContext aConnection ) {
		_server.openUnchecked( aConnection );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBaseLocator( String aBaseLocator ) {
		_server.setBaseLocator( aBaseLocator );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRealm( String aRealm ) {
		_server.setRealm( aRealm );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean subscribeObserver( RestEndpoint aObserver ) {
		final boolean isSubscribed = _server.subscribeObserver( aObserver );
		return isSubscribed;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MediaTypeFactory toMediaTypeFactory( MediaType aMediaType ) {
		return _server.toMediaTypeFactory( aMediaType );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean unsubscribeObserver( RestEndpoint aObserver ) {
		return _server.unsubscribeObserver( aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<RestEndpoint> observers() {
		return _server.observers();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withBaseLocator( String aBaseLocator ) {
		_server.setBaseLocator( aBaseLocator );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withClose() throws IOException {
		_server.close();
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withCloseIn( int aCloseMillis ) {
		_server.closeIn( aCloseMillis );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withCloseQuietly() {
		_server.closeQuietly();
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withCloseUnchecked() {
		_server.closeUnchecked();
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withOpen( HttpServerContext aConnection ) throws IOException {
		_server.open( aConnection );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withOpenUnchecked( HttpServerContext aConnection ) {
		_server.openUnchecked( aConnection );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withRealm( String aRealm ) {
		_server.setRealm( aRealm );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getMaxConnections() {
		return _server.getMaxConnections();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public KeyStoreDescriptor getKeyStoreDescriptor() {
		return _server.getKeyStoreDescriptor();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setKeyStoreDescriptor( KeyStoreDescriptor aKeyStoreDescriptor ) {
		_server.setKeyStoreDescriptor( aKeyStoreDescriptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMaxConnections( int aMaxConnections ) {
		_server.setMaxConnections( aMaxConnections );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setProtocol( String aProtocol ) {
		_server.setProtocol( aProtocol );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toProtocol() {
		return _server.toProtocol();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasPreHttpInterceptor( PreHttpServerInterceptor aPreInterceptor ) {
		return _server.hasPreHttpInterceptor( aPreInterceptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addPreHttpInterceptor( PreHttpServerInterceptor aPreInterceptor ) {
		return _server.addPreHttpInterceptor( aPreInterceptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean removePreHttpInterceptor( PreHttpServerInterceptor aPreInterceptor ) {
		return _server.removePreHttpInterceptor( aPreInterceptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasPostHttpInterceptor( PostHttpServerInterceptor aPostInterceptor ) {
		return _server.hasPostHttpInterceptor( aPostInterceptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addPostHttpInterceptor( PostHttpServerInterceptor aPostInterceptor ) {
		return _server.addPostHttpInterceptor( aPostInterceptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean removePostHttpInterceptor( PostHttpServerInterceptor aPostInterceptor ) {
		return _server.removePostHttpInterceptor( aPostInterceptor );
	}
}
