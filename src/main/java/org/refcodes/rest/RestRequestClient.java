// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.net.MalformedURLException;

import org.refcodes.data.Scheme;
import org.refcodes.web.BaseUrlAccessor;
import org.refcodes.web.FormFields;
import org.refcodes.web.HttpClientRequest;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpResponseException;
import org.refcodes.web.HttpStatusCode;
import org.refcodes.web.RequestHeaderFields;
import org.refcodes.web.Url;

/**
 * Helper interface to keep the huge amount of convenience methods under
 * control.
 */
public interface RestRequestClient extends RestRequestHandler {

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath );
		return buildRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath, aQueryFields );
		return buildRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath, aQueryFields );
		return buildRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} at this request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath, aQueryFields );
		return buildRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath );
		return buildRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath );
		return buildRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath ) {
		final Url theUrl = new Url( aScheme, aHost, aPath );
		return buildRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields ) {
		final Url theUrl = new Url( aScheme, aHost, aPath, aQueryFields );
		return buildRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) {
		final Url theUrl = new Url( aScheme, aHost, aPath, aQueryFields );
		return buildRequest( aHttpMethod, theUrl, null, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} at this request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		final Url theUrl = new Url( aScheme, aHost, aPath, aQueryFields );
		return buildRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, Object aRequest ) {
		final Url theUrl = new Url( aScheme, aHost, aPath );
		return buildRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		final Url theUrl = new Url( aScheme, aHost, aPath );
		return buildRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aUrl ) throws MalformedURLException {
		Url theUrl = new Url( aUrl );
		// In case of relative path, use base URL (if applicable) |-->
		if ( theUrl.getHost() == null && theUrl.getScheme() == null && this instanceof BaseUrlAccessor ) {
			final BaseUrlAccessor theBaseUrlAccessor = (BaseUrlAccessor) this;
			final Url theBaseUrl = theBaseUrlAccessor.getBaseUrl();
			if ( theBaseUrl != null ) {
				theUrl = new Url( theBaseUrlAccessor.getBaseUrl(), aUrl );
			}
		}
		// <--| In case of relative path, use base URL (if applicable)
		return buildRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields ) throws MalformedURLException {
		final Url theUrl = new Url( aLocator, aQueryFields );
		return buildRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The local targetd locator.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, Object aRequest ) throws MalformedURLException {
		final Url theUrl = new Url( aLocator, aQueryFields );
		return buildRequest( aHttpMethod, theUrl, null, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} at this request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws MalformedURLException {
		final Url theUrl = new Url( aLocator, aQueryFields );
		return buildRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted locator.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aUrl, Object aRequest ) throws MalformedURLException {
		final Url theUrl = new Url( aUrl );
		return buildRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) throws MalformedURLException {
		final Url theUrl = new Url( aUrl );
		return buildRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath );
		return buildRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath, aQueryFields );
		return buildRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath, aQueryFields );
		return buildRequest( aHttpMethod, theUrl, null, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} at this request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath, aQueryFields );
		return buildRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, Object aRequest ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath );
		return buildRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath );
		return buildRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath );
		return buildRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath, aQueryFields );
		return buildRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath, aQueryFields );
		return buildRequest( aHttpMethod, theUrl, null, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} at this request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath, aQueryFields );
		return buildRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, Object aRequest ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath );
		return buildRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath );
		return buildRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Url aUrl ) {
		return buildRequest( aHttpMethod, aUrl, (RequestHeaderFields) null, null );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestRequestBuilder} at this request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields ) {
		return buildRequest( aHttpMethod, aUrl, aHeaderFields, null );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} at this request.
	 */
	default RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return buildRequest( aHttpMethod, aUrl, aHeaderFields, aRequest, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Prepares a request builder with the possible attributes and returns the
	 * according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aRedirectCount The number of redirect ping-pong cycles to follow
	 *        before when issuing the HTTP-Request.
	 * 
	 * @return The {@link RestRequestBuilder} at this request.
	 */
	RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, int aRedirectCount );

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestResponse doRequest( HttpClientRequest aHttpClientRequest ) throws HttpResponseException {
		return doRequest( aHttpClientRequest.getHttpMethod(), aHttpClientRequest.getUrl(), aHttpClientRequest.getHeaderFields(), aHttpClientRequest.getRequest() );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath ) throws HttpResponseException {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath );
		return doRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath, aQueryFields );
		return doRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath, aQueryFields );
		return doRequest( aHttpMethod, theUrl, null, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath, aQueryFields );
		return doRequest( aHttpMethod, theUrl, aHeaderFields, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} for this request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath, aQueryFields );
		return doRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest ) throws HttpResponseException {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath );
		return doRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath );
		return doRequest( aHttpMethod, theUrl, aHeaderFields, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath );
		return doRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath ) throws HttpResponseException {
		final Url theUrl = new Url( aScheme, aHost, aPath );
		return doRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		final Url theUrl = new Url( aScheme, aHost, aPath, aQueryFields );
		return doRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		final Url theUrl = new Url( aScheme, aHost, aPath, aQueryFields );
		return doRequest( aHttpMethod, theUrl, null, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return doRequest( aHttpMethod, aScheme, aHost, aPath, aQueryFields, aHeaderFields, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} for this request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		final Url theUrl = new Url( aScheme, aHost, aPath, aQueryFields );
		return doRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, Object aRequest ) throws HttpResponseException {
		final Url theUrl = new Url( aScheme, aHost, aPath );
		return doRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		final Url theUrl = new Url( aScheme, aHost, aPath );
		return doRequest( aHttpMethod, theUrl, aHeaderFields, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		final Url theUrl = new Url( aScheme, aHost, aPath );
		return doRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aUrl ) throws HttpResponseException, MalformedURLException {
		final Url theUrl = new Url( aUrl );
		return doRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields ) throws HttpResponseException, MalformedURLException {
		final Url theUrl = new Url( aLocator, aQueryFields );
		return doRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		final Url theUrl = new Url( aLocator, aQueryFields );
		return doRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException, MalformedURLException {
		final Url theUrl = new Url( aLocator, aQueryFields );
		return doRequest( aHttpMethod, theUrl, aHeaderFields, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} for this request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		final Url theUrl = new Url( aLocator, aQueryFields );
		return doRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aUrl, Object aRequest ) throws HttpResponseException, MalformedURLException {
		final Url theUrl = new Url( aUrl );
		return doRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aUrl, RequestHeaderFields aHeaderFields ) throws HttpResponseException, MalformedURLException {
		final Url theUrl = new Url( aUrl );
		return doRequest( aHttpMethod, theUrl, aHeaderFields, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return doRequest( aHttpMethod, aUrl, (FormFields) null, aHeaderFields, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath ) throws HttpResponseException {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath );
		return doRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath, aQueryFields );
		return doRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath, aQueryFields );
		return doRequest( aHttpMethod, theUrl, null, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath, aQueryFields );
		return doRequest( aHttpMethod, theUrl, aHeaderFields, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} for this request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath, aQueryFields );
		return doRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, Object aRequest ) throws HttpResponseException {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath );
		return doRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath );
		return doRequest( aHttpMethod, theUrl, aHeaderFields, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath );
		return doRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath ) throws HttpResponseException {
		final Url theUrl = new Url( aProtocol, aHost, aPath );
		return doRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		final Url theUrl = new Url( aProtocol, aHost, aPath, aQueryFields );
		return doRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		final Url theUrl = new Url( aProtocol, aHost, aPath, aQueryFields );
		return doRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		final Url theUrl = new Url( aProtocol, aHost, aPath, aQueryFields );
		return doRequest( aHttpMethod, theUrl, aHeaderFields, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} for this request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		final Url theUrl = new Url( aProtocol, aHost, aPath, aQueryFields );
		return doRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, Object aRequest ) throws HttpResponseException {
		final Url theUrl = new Url( aProtocol, aHost, aPath );
		return doRequest( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		final Url theUrl = new Url( aProtocol, aHost, aPath );
		return doRequest( aHttpMethod, theUrl, aHeaderFields, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		final Url theUrl = new Url( aProtocol, aHost, aPath );
		return doRequest( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * 
	 * @return The {@link RestResponse} for this request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Url aUrl ) throws HttpResponseException {
		return doRequest( aHttpMethod, aUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} for this request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Url aUrl, Object aRequest ) throws HttpResponseException {
		return doRequest( aHttpMethod, aUrl, null, aRequest );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} for this request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return doRequest( aHttpMethod, aUrl, aHeaderFields, (Object) null );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} for this request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doRequest( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return doRequest( aHttpMethod, aUrl, aHeaderFields, aRequest, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aRedirectDepth The redirect depth provides the count of
	 *        HTTP-Request and HTTP-Response cycles where the response
	 *        represents a redirect as of
	 *        {@link HttpStatusCode#isRedirectStatus()}. A value of -1
	 *        represents the default behavior, e.g. using
	 *        {@link java.net.HttpURLConnection}'s redirection means.
	 * 
	 * @return The {@link RestResponse} for this request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	RestResponse doRequest( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, int aRedirectDepth ) throws HttpResponseException;

	/**
	 * Sends a request with the possible attributes and returns the according
	 * request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aRequest The {@link RestRequest} encapsulating all necessary
	 *        attributes to issue the request.
	 * 
	 * @return The {@link RestResponse} for this request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponse doRequest( RestRequest aRequest ) throws HttpResponseException, MalformedURLException {
		return doRequest( aRequest.getHttpMethod(), aRequest.getUrl(), aRequest.getHeaderFields(), aRequest.getRequest() );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, null, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, null, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( aHttpMethod, aScheme, aHost, aPath, aQueryFields, null, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aScheme, aHost, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aScheme, aHost, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aScheme, aHost, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aScheme, aHost, aPath );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aScheme, aHost, aPath );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aScheme, aHost, aPath );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aScheme, aHost, aPath );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		final Url theUrl = new Url( aLocator, aQueryFields );
		return onResponse( aHttpMethod, theUrl, null, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		final Url theUrl = new Url( aLocator, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		final Url theUrl = new Url( aLocator, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		final Url theUrl = new Url( aLocator, aQueryFields );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aUrl, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		final Url theUrl = new Url( aUrl );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aUrl, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		final Url theUrl = new Url( aUrl );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aUrl, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return onResponse( aHttpMethod, aUrl, aHeaderFields, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aUrl, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return onResponse( aHttpMethod, aUrl, (RequestHeaderFields) null, (Object) null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, null, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, null, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, RestResponseConsumer aResponseConsumer ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Url aUrl, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( aHttpMethod, aUrl, (RequestHeaderFields) null, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aRedirectDepth The redirect depth provides the count of
	 *        HTTP-Request and HTTP-Response cycles where the response
	 *        represents a redirect as of
	 *        {@link HttpStatusCode#isRedirectStatus()}. A value of -1
	 *        represents the default behavior, e.g. using
	 *        {@link java.net.HttpURLConnection}'s redirection means.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	RestResponseHandler onResponse( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, int aRedirectDepth, RestResponseConsumer aResponseConsumer );

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( aHttpMethod, aUrl, aHeaderFields, aRequest, HttpClientRequest.DEFAULT_REDIRECT_DEPTH, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	default RestResponseHandler onResponse( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return onResponse( aHttpMethod, aUrl, aHeaderFields, null, aResponseConsumer );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, null, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, null, null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath ) {
		final Url theUrl = new Url( aScheme, aHost, aPort, aPath );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) {
		return onResponse( aHttpMethod, aScheme, aHost, aPath, aQueryFields, null, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		final Url theUrl = new Url( aScheme, aHost, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		final Url theUrl = new Url( aScheme, aHost, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields ) {
		final Url theUrl = new Url( aScheme, aHost, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, Object aRequest ) {
		final Url theUrl = new Url( aScheme, aHost, aPath );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		final Url theUrl = new Url( aScheme, aHost, aPath );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields ) {
		final Url theUrl = new Url( aScheme, aHost, aPath );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath ) {
		final Url theUrl = new Url( aScheme, aHost, aPath );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, Object aRequest ) throws MalformedURLException {
		final Url theUrl = new Url( aLocator, aQueryFields );
		return onResponse( aHttpMethod, theUrl, null, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws MalformedURLException {
		final Url theUrl = new Url( aLocator, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws MalformedURLException {
		final Url theUrl = new Url( aLocator, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponseResult} which is used by the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields ) throws MalformedURLException {
		final Url theUrl = new Url( aLocator, aQueryFields );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aUrl, Object aRequest ) throws MalformedURLException {
		final Url theUrl = new Url( aUrl );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) throws MalformedURLException {
		final Url theUrl = new Url( aUrl );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aUrl, RequestHeaderFields aHeaderFields ) throws MalformedURLException {
		return onResponse( aHttpMethod, aUrl, aHeaderFields );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * 
	 * @return The {@link RestResponseResult} which is used by the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aUrl ) throws MalformedURLException {
		return onResponse( aHttpMethod, aUrl, (RequestHeaderFields) null, (Object) null );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, null, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, (Object) null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, null, null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, Object aRequest ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath ) {
		final Url theUrl = new Url( aProtocol, aHost, aPort, aPath );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath, aQueryFields );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, Object aRequest ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath );
		return onResponse( aHttpMethod, theUrl, aHeaderFields, null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath ) {
		final Url theUrl = new Url( aProtocol, aHost, aPath );
		return onResponse( aHttpMethod, theUrl, (RequestHeaderFields) null, null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Url aUrl, Object aRequest ) {
		return onResponse( aHttpMethod, aUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aRedirectDepth The redirect depth provides the count of
	 *        HTTP-Request and HTTP-Response cycles where the response
	 *        represents a redirect as of
	 *        {@link HttpStatusCode#isRedirectStatus()}. A value of -1
	 *        represents the default behavior, e.g. using
	 *        {@link java.net.HttpURLConnection}'s redirection means.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	RestResponseResult onResponse( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, int aRedirectDepth );

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return onResponse( aHttpMethod, aUrl, aHeaderFields, aRequest, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}

	/**
	 * Creates a request with the possible attributes and returns the according
	 * {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	default RestResponseResult onResponse( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields ) {
		return onResponse( aHttpMethod, aUrl, aHeaderFields, null, HttpClientRequest.DEFAULT_REDIRECT_DEPTH );
	}
}
