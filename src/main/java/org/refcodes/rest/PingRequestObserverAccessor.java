// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

/**
 * Provides an accessor for a ping {@link RestRequestConsumer} property.
 */
public interface PingRequestObserverAccessor {

	/**
	 * Retrieves the ping {@link RestRequestConsumer} from the ping
	 * {@link RestRequestConsumer} property.
	 * 
	 * @return The ping {@link RestRequestConsumer} stored by the ping
	 *         {@link RestRequestConsumer} property.
	 */
	RestRequestConsumer getPingRequestObserver();

	/**
	 * Provides a mutator for a ping {@link RestRequestConsumer} property.
	 */
	public interface PingRequestObserverMutator {

		/**
		 * Sets the ping {@link RestRequestConsumer} for the ping
		 * {@link RestRequestConsumer} property.
		 * 
		 * @param aPingRequestObserver The ping {@link RestRequestConsumer} to
		 *        be stored by the ping {@link RestRequestConsumer} property.
		 */
		void setPingRequestObserver( RestRequestConsumer aPingRequestObserver );
	}

	/**
	 * Provides a builder method for a ping {@link RestRequestConsumer} property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface PingRequestObserverBuilder<B extends PingRequestObserverBuilder<B>> {

		/**
		 * Sets the ping {@link RestRequestConsumer} for the ping
		 * {@link RestRequestConsumer} property.
		 * 
		 * @param aRequestConsumer The ping {@link RestRequestConsumer} to be
		 *        stored by the ping {@link RestRequestConsumer} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withPingRequestObserver( RestRequestConsumer aRequestConsumer );
	}

	/**
	 * Provides a ping {@link RestRequestConsumer} property.
	 */
	public interface PingRequestObserverProperty extends PingRequestObserverAccessor, PingRequestObserverMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link RestRequestConsumer} (setter) as of
		 * {@link #setPingRequestObserver(RestRequestConsumer)} and returns the
		 * very same value (getter).
		 * 
		 * @param aRequestConsumer The {@link RestRequestConsumer} to set (via
		 *        {@link #setPingRequestObserver(RestRequestConsumer)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default RestRequestConsumer letPingRequestObserver( RestRequestConsumer aRequestConsumer ) {
			setPingRequestObserver( aRequestConsumer );
			return aRequestConsumer;
		}
	}
}
