// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.security.TrustStoreDescriptorAccessor;
import org.refcodes.web.Url;

/**
 * This context describes all information required to register a server
 * ("service") at a service discovery registry.
 * 
 * @param <DESC> The {@link HttpServerDescriptor} which describes a server to be
 *        registered at a discovery registry so clients can resolve the server's
 *        URL.
 */
public interface HttpRegistryContext<DESC extends HttpServerDescriptor> extends HttpRegistryUrlAccessor, TrustStoreDescriptorAccessor, HttpServerDescriptorAccessor<DESC>, PingRequestObserverAccessor {

	/**
	 * The {@link HttpRegistryContextBuilder} interface extends the
	 * {@link HttpRegistryContext} with builder functionality as of the builder
	 * pattern.
	 *
	 * @param <DESC> The {@link HttpServerDescriptor} which describes a server
	 *        to be registered at a discovery registry so clients can resolve
	 *        the server's URL.
	 */
	public interface HttpRegistryContextBuilder<DESC extends HttpServerDescriptor> extends HttpRegistryUrlProperty, HttpRegistryUrlBuilder<HttpRegistryContextBuilder<DESC>>, TrustStoreDescriptorProperty, TrustStoreDescriptorBuilder<HttpRegistryContextBuilder<DESC>>, HttpServerDescriptorProperty<DESC>, HttpServerDescriptorBuilder<DESC, HttpRegistryContextBuilder<DESC>>, PingRequestObserverProperty, PingRequestObserverBuilder<HttpRegistryContextBuilder<DESC>> {

		/**
		 * {@inheritDoc}
		 */
		@Override
		default HttpRegistryContextBuilder<DESC> withHttpRegistryUrl( Url aUrl ) {
			setHttpRegistryUrl( aUrl );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default HttpRegistryContextBuilder<DESC> withHttpServerDescriptor( DESC aServerDescriptor ) {
			setHttpServerDescriptor( aServerDescriptor );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default HttpRegistryContextBuilder<DESC> withPingRequestObserver( RestRequestConsumer aRequestConsumer ) {
			setPingRequestObserver( aRequestConsumer );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default HttpRegistryContextBuilder<DESC> withTrustStoreDescriptor( TrustStoreDescriptor aStoreDescriptor ) {
			setTrustStoreDescriptor( aStoreDescriptor );
			return this;
		}
	}
}