// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.net.MalformedURLException;

import org.refcodes.data.Scheme;
import org.refcodes.web.FormFields;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpResponseException;
import org.refcodes.web.RequestHeaderFields;
import org.refcodes.web.Url;

/**
 * Helper class to get the syntactic sugar (from a maintenance point of view)
 * under control. You should actually statically import the
 * {@link HttpRestClientSugar}.
 */
public class RestPutClientSugar extends RestPostClientSugar {

	/**
	 * See also {@link RestfulHttpClient#buildPut(Scheme, String, int, String)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( Scheme aScheme, String aHost, int aPort, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aScheme, aHost, aPort, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(Scheme, String, int, String, FormFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aScheme, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(Scheme, String, int, String, FormFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aScheme, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(Scheme, String, int, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(Scheme, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(Scheme, String, int, String, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aScheme, aHost, aPort, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(Scheme, String, int, String, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aScheme, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(Scheme, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aScheme, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildPut(Scheme, String, String)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( Scheme aScheme, String aHost, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aScheme, aHost, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(Scheme, String, String, FormFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aScheme, aHost, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(Scheme, String, String, FormFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aScheme, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(Scheme, String, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aScheme, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(Scheme, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aScheme, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(Scheme, String, String, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( Scheme aScheme, String aHost, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aScheme, aHost, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(Scheme, String, String, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aScheme, aHost, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(Scheme, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aScheme, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildPut(String)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildPut( String aUrl ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildPut( aUrl );
	}

	/**
	 * See also {@link RestfulHttpClient#buildPut(String, FormFields)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildPut( String aLocator, FormFields aQueryFields ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildPut( aLocator, aQueryFields );
	}

	/**
	 * See also {@link RestfulHttpClient#buildPut(String, FormFields, Object)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildPut( String aLocator, FormFields aQueryFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildPut( aLocator, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildPut( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildPut( aLocator, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildPut( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildPut( aLocator, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildPut(String, Object)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildPut( String aUrl, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildPut( aUrl, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildPut(String, RequestHeaderFields)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildPut( String aUrl, RequestHeaderFields aHeaderFields ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildPut( aUrl, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildPut( String aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildPut( aUrl, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildPut(String, String, int, String)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( String aProtocol, String aHost, int aPort, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aProtocol, aHost, aPort, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(String, String, int, String, FormFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aProtocol, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(String, String, int, String, FormFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aProtocol, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(String, String, int, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(String, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(String, String, int, String, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( String aProtocol, String aHost, int aPort, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aProtocol, aHost, aPort, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(String, String, int, String, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aProtocol, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(String, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aProtocol, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildPut(String, String, String)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( String aProtocol, String aHost, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aProtocol, aHost, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(String, String, String, FormFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( String aProtocol, String aHost, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aProtocol, aHost, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(String, String, String, FormFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aProtocol, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(String, String, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aProtocol, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(String, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(String, String, String, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( String aProtocol, String aHost, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aProtocol, aHost, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(String, String, String, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aProtocol, aHost, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildPut(String, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aProtocol, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildPut(Url)}.
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( Url aUrl ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aUrl );
	}

	/**
	 * See also {@link RestfulHttpClient#buildPut(Url, RequestHeaderFields)}.
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildPut( Url aUrl, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildPut( aUrl, aHeaderFields );
	}

	/**
	 * See also {@link RestfulHttpClient#doPut(Scheme, String, int, String)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( Scheme aScheme, String aHost, int aPort, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aScheme, aHost, aPort, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(Scheme, String, int, String, FormFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aScheme, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(Scheme, String, int, String, FormFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aScheme, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(Scheme, String, int, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(Scheme, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(Scheme, String, int, String, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aScheme, aHost, aPort, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(Scheme, String, int, String, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aScheme, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(Scheme, String, int, String, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aScheme, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doPut(Scheme, String, String)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( Scheme aScheme, String aHost, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aScheme, aHost, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(Scheme, String, String, FormFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aScheme, aHost, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(Scheme, String, String, FormFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aScheme, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(Scheme, String, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aScheme, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(Scheme, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aScheme, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doPut(Scheme, String, String, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( Scheme aScheme, String aHost, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aScheme, aHost, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(Scheme, String, String, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aScheme, aHost, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(Scheme, String, String, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aScheme, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doPut(String)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doPut( String aUrl ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doPut( aUrl );
	}

	/**
	 * See also {@link RestfulHttpClient#doPut(String, FormFields)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doPut( String aLocator, FormFields aQueryFields ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doPut( aLocator, aQueryFields );
	}

	/**
	 * See also {@link RestfulHttpClient#doPut(String, FormFields, Object)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doPut( String aLocator, FormFields aQueryFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doPut( aLocator, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doPut( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doPut( aLocator, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doPut( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doPut( aLocator, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doPut(String, Object)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doPut( String aUrl, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doPut( aUrl, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doPut(String, RequestHeaderFields)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doPut( String aUrl, RequestHeaderFields aHeaderFields ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doPut( aUrl, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(String, RequestHeaderFields, Object)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doPut( String aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doPut( aUrl, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doPut(String, String, int, String)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( String aProtocol, String aHost, int aPort, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aProtocol, aHost, aPort, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(String, String, int, String, FormFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aProtocol, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(String, String, int, String, FormFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aProtocol, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(String, String, int, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(String, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(String, String, int, String, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( String aProtocol, String aHost, int aPort, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aProtocol, aHost, aPort, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(String, String, int, String, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aProtocol, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(String, String, int, String, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aProtocol, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doPut(String, String, String)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( String aProtocol, String aHost, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aProtocol, aHost, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(String, String, String, FormFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( String aProtocol, String aHost, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aProtocol, aHost, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(String, String, String, FormFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aProtocol, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(String, String, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aProtocol, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(String, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doPut(String, String, String, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( String aProtocol, String aHost, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aProtocol, aHost, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(String, String, String, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aProtocol, aHost, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doPut(String, String, String, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doPut( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doPut( aProtocol, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doPut(Url, Object)}.
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doPut( Url aUrl, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doPut( aUrl, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, int, String, FormFields, Object, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPort, aPath, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, int, String, FormFields, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, int, String, FormFields, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, int, String, FormFields, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPort, aPath, aQueryFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, int, String, Object, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPort, aPath, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, int, String, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPort, aPath, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, int, String, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPort, aPath, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, int, String, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Scheme aScheme, String aHost, int aPort, String aPath, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPort, aPath, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, String, FormFields, Object, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPath, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, String, FormFields, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPath, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, String, FormFields, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPath, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, String, FormFields, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPath, aQueryFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, String, Object, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Scheme aScheme, String aHost, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPath, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, String, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPath, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, String, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPath, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, String, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Scheme aScheme, String aHost, String aPath, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPath, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, FormFields, Object, RestResponseConsumer)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onPut( String aLocator, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onPut( aLocator, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, FormFields, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onPut( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onPut( aLocator, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, FormFields, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onPut( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onPut( aLocator, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, FormFields, RestResponseConsumer)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onPut( String aLocator, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onPut( aLocator, aQueryFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, Object, RestResponseConsumer)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onPut( String aUrl, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onPut( aUrl, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onPut( String aUrl, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onPut( aUrl, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onPut( String aUrl, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onPut( aUrl, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also {@link RestfulHttpClient#onPut(String, RestResponseConsumer)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onPut( String aUrl, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onPut( aUrl, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, int, String, FormFields, Object, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPort, aPath, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, int, String, FormFields, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, int, String, FormFields, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, int, String, FormFields, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPort, aPath, aQueryFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, int, String, Object, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( String aProtocol, String aHost, int aPort, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPort, aPath, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, int, String, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPort, aPath, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, int, String, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPort, aPath, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, int, String, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( String aProtocol, String aHost, int aPort, String aPath, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPort, aPath, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, String, FormFields, Object, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPath, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, String, FormFields, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, String, FormFields, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, String, FormFields, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPath, aQueryFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, String, Object, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( String aProtocol, String aHost, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPath, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, String, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPath, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, String, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPath, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, String, RestResponseConsumer)}}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( String aProtocol, String aHost, String aPath, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPath, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Url, Object, RestResponseConsumer)}.
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Url aUrl, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aUrl, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, String, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aUrl, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Url, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onPut( Url aUrl, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onPut( aUrl, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, int, String, FormFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, int, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, int, String, FormFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, int, String, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPort, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, int, String, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, int, String, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onPut(Scheme, String, int, String)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Scheme aScheme, String aHost, int aPort, String aPath ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPort, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, String, FormFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, String, FormFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPath, aQueryFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onPut(Scheme, String, String, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Scheme aScheme, String aHost, String aPath, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, String, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(Scheme, String, String, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPath, aHeaderFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onPut(Scheme, String, String)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Scheme aScheme, String aHost, String aPath ) {
		return HttpRestClientSingleton.getInstance().onPut( aScheme, aHost, aPath );
	}

	/**
	 * See also {@link RestfulHttpClient#onPut(String, FormFields, Object)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onPut( String aLocator, FormFields aQueryFields, Object aRequest ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onPut( aLocator, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onPut( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onPut( aLocator, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onPut( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onPut( aLocator, aQueryFields, aHeaderFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onPut(String, FormFields)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onPut( String aLocator, FormFields aQueryFields ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onPut( aLocator, aQueryFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onPut(String, Object)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onPut( String aUrl, Object aRequest ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onPut( aUrl, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, RequestHeaderFields, Object)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onPut( String aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onPut( aUrl, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#onPut(String, RequestHeaderFields)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onPut( String aUrl, RequestHeaderFields aHeaderFields ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onPut( aUrl, aHeaderFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onPut(String)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onPut( String aUrl ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onPut( aUrl );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, int, String, FormFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, int, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, int, String, FormFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, int, String, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( String aProtocol, String aHost, int aPort, String aPath, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPort, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, int, String, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, int, String, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onPut(String, String, int, String)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( String aProtocol, String aHost, int aPort, String aPath ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPort, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, String, FormFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, String, FormFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( String aProtocol, String aHost, String aPath, FormFields aQueryFields ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPath, aQueryFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onPut(String, String, String, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( String aProtocol, String aHost, String aPath, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, String, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, String, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPath, aHeaderFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onPut(String, String, String)}}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( String aProtocol, String aHost, String aPath ) {
		return HttpRestClientSingleton.getInstance().onPut( aProtocol, aHost, aPath );
	}

	/**
	 * See also {@link RestfulHttpClient#onPut(Url, Object)}.
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Url aUrl, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onPut( aUrl, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onPut(String, String, String, RequestHeaderFields, Object)}.
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#PUT} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onPut( aUrl, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#onPut(Url, RequestHeaderFields)}.
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onPut( Url aUrl, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onPut( aUrl, aHeaderFields );
	}
}
