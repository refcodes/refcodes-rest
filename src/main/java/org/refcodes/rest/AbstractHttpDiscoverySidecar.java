// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.component.InitializeException;
import org.refcodes.component.LifecycleMachine.ManualLifecycleMachine;
import org.refcodes.component.LifecycleStatus;
import org.refcodes.component.PauseException;
import org.refcodes.component.ResumeException;
import org.refcodes.component.StartException;
import org.refcodes.component.StopException;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.web.LoadBalancingStrategy;
import org.refcodes.web.Url;

/**
 * Abstract class for easily decorating a {@link HttpRegistrySidecar}.
 *
 * @param <B> the generic type
 */
public abstract class AbstractHttpDiscoverySidecar<B extends HttpDiscoverySidecar<B>> implements HttpDiscoverySidecar<B> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private LoadBalancingStrategy _strategy = LoadBalancingStrategy.RANDOM;
	private Url _serviceDiscoveryUrl;
	private TrustStoreDescriptor _storeDescriptor;
	protected ManualLifecycleMachine _lifeCycleAutomaton = new ManualLifecycleMachine();

	// /////////////////////////////////////////////////////////////////////////
	// LIFECYCLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isRunning() {
		return _lifeCycleAutomaton.isRunning();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isInitialized() {
		return _lifeCycleAutomaton.isInitialized();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LifecycleStatus getLifecycleStatus() {
		return _lifeCycleAutomaton.getLifecycleStatus();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void pause() throws PauseException {
		_lifeCycleAutomaton.pause();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop() throws StopException {
		_lifeCycleAutomaton.stop();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() throws StartException {
		_lifeCycleAutomaton.start();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void resume() throws ResumeException {
		_lifeCycleAutomaton.resume();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize() throws InitializeException {
		_lifeCycleAutomaton.initialize();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStoppable() {
		return _lifeCycleAutomaton.isStoppable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isPausable() {
		return _lifeCycleAutomaton.isPausable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStartable() {
		return _lifeCycleAutomaton.isStartable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isResumable() {
		return _lifeCycleAutomaton.isResumable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStopped() {
		return _lifeCycleAutomaton.isStopped();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isPaused() {
		return _lifeCycleAutomaton.isPaused();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isInitalizable() {
		return _lifeCycleAutomaton.isInitalizable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		_lifeCycleAutomaton.destroy();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isDestroyable() {
		return _lifeCycleAutomaton.isDestroyable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isDestroyed() {
		return _lifeCycleAutomaton.isDestroyed();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoadBalancingStrategy getLoadBalancingStrategy() {
		return _strategy;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpDiscoveryUrl( Url aUrl ) {
		_serviceDiscoveryUrl = aUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setLoadBalancingStrategy( LoadBalancingStrategy aStrategy ) {
		_strategy = aStrategy;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TrustStoreDescriptor getTrustStoreDescriptor() {
		return _storeDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTrustStoreDescriptor( TrustStoreDescriptor aTrustStoreDescriptor ) {
		_storeDescriptor = aTrustStoreDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getHttpDiscoveryUrl() {
		return _serviceDiscoveryUrl;
	}

	/////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Resolves the property from the provided value and the provided property
	 * and the and sets the property in case the provided value is not null.
	 * 
	 * @param aStrategy The value to be used when not null.
	 * @param aProperty The property to be used when the value is null and which
	 *        is to be set when the value is not null.
	 * 
	 * @return The value when not null, else the value of the provided property.
	 */
	protected static LoadBalancingStrategy toLoadBalancingStrategy( LoadBalancingStrategy aStrategy, LoadBalancingStrategyProperty aProperty ) {
		if ( aStrategy != null ) {
			aProperty.setLoadBalancingStrategy( aStrategy );
		}
		else {
			aStrategy = aProperty.getLoadBalancingStrategy();
		}
		return aStrategy;
	}

	/**
	 * Resolves the property from the provided value and the provided property
	 * and the and sets the property in case the provided value is not null.
	 * 
	 * @param aStoreDescriptor The value to be used when not null.
	 * @param aProperty The property to be used when the value is null and which
	 *        is to be set when the value is not null.
	 * 
	 * @return The value when not null, else the value of the provided property.
	 */
	protected static TrustStoreDescriptor toTrustStoreDescriptor( TrustStoreDescriptor aStoreDescriptor, TrustStoreDescriptorProperty aProperty ) {
		if ( aStoreDescriptor != null ) {
			aProperty.setTrustStoreDescriptor( aStoreDescriptor );
		}
		else {
			aStoreDescriptor = aProperty.getTrustStoreDescriptor();
		}
		return aStoreDescriptor;
	}

	/**
	 * Resolves the property from the provided value and the provided property
	 * and the and sets the property in case the provided value is not null.
	 * 
	 * @param aDiscoveryUrl The value to be used when not null.
	 * @param aProperty The property to be used when the value is null and which
	 *        is to be set when the value is not null.
	 * 
	 * @return The value when not null, else the value of the provided property.
	 */
	protected static Url toHttpDiscoveryUrl( Url aDiscoveryUrl, HttpDiscoveryUrlProperty aProperty ) {
		if ( aDiscoveryUrl != null ) {
			aProperty.setHttpDiscoveryUrl( aDiscoveryUrl );
		}
		else {
			aDiscoveryUrl = aProperty.getHttpDiscoveryUrl();
		}
		return aDiscoveryUrl;
	}
}
