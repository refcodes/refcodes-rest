// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.web.Url;

/**
 * Provides an accessor for a service registry registry {@link Url} property.
 */
public interface HttpRegistryUrlAccessor {

	/**
	 * Retrieves the URL from the URL property locating the service registry
	 * registry.
	 * 
	 * @return The URL stored by the URL property.
	 */
	Url getHttpRegistryUrl();

	/**
	 * Provides a mutator for a service registry registry URL property.
	 */
	public interface HttpRegistryUrlMutator {
		/**
		 * Sets the URL for the URL property locating the service registry
		 * registry.
		 * 
		 * @param aRegistryUrl The service registry registry URL to be stored by
		 *        the URL property.
		 */
		void setHttpRegistryUrl( Url aRegistryUrl );
	}

	/**
	 * Provides a builder method for a service registry registry URL property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface HttpRegistryUrlBuilder<B extends HttpRegistryUrlBuilder<B>> {

		/**
		 * Sets the URL for the URL property locating the service registry
		 * registry.
		 * 
		 * @param aUrl The service registry registry URL to be stored by the URL
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withHttpRegistryUrl( Url aUrl );
	}

	/**
	 * Provides a service registry registry URL property.
	 */
	public interface HttpRegistryUrlProperty extends HttpRegistryUrlAccessor, HttpRegistryUrlMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Url} (setter) as
		 * of {@link #setHttpRegistryUrl(Url)} and returns the very same value
		 * (getter).
		 * 
		 * @param aUrl The {@link Url} to set (via
		 *        {@link #setHttpRegistryUrl(Url)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Url letHttpRegistryUrl( Url aUrl ) {
			setHttpRegistryUrl( aUrl );
			return aUrl;
		}
	}
}
