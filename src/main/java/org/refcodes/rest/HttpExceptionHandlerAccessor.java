// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

/**
 * Provides access to a {@link HttpExceptionHandler} property.
 */
public interface HttpExceptionHandlerAccessor {

	/**
	 * Retrieves the {@link HttpExceptionHandler} from the
	 * {@link HttpExceptionHandler} property.
	 * 
	 * @return The {@link HttpExceptionHandler} stored by the
	 *         {@link HttpExceptionHandler} property.
	 */
	HttpExceptionHandler getHttpExceptionHandler();

	/**
	 * Extends the {@link HttpExceptionHandlerAccessor} with a setter method.
	 */
	public interface HttpExceptionHandlerMutator {

		/**
		 * Sets the {@link HttpExceptionHandler} for the
		 * {@link HttpExceptionHandler} property.
		 * 
		 * @param aHttpExceptionHandler The {@link HttpExceptionHandler} to be
		 *        stored by the {@link HttpExceptionHandler} property.
		 */
		void setHttpExceptionHandler( HttpExceptionHandler aHttpExceptionHandler );

		/**
		 * Sets the {@link HttpExceptionHandler} for the
		 * {@link HttpExceptionHandler} property.
		 * 
		 * @param aHttpExceptionHandler The {@link HttpExceptionHandler} to be
		 *        stored by the {@link HttpExceptionHandler} property.
		 */
		default void onHttpException( HttpExceptionHandler aHttpExceptionHandler ) {
			setHttpExceptionHandler( aHttpExceptionHandler );
		}
	}

	/**
	 * Provides a builder method for a {@link HttpExceptionHandler} property
	 * returning the builder for applying multiple build operations.
	 *
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface HttpExceptionHandlerBuilder<B extends HttpExceptionHandlerBuilder<B>> {

		/**
		 * Sets the {@link HttpExceptionHandler} for the
		 * {@link HttpExceptionHandler} property.
		 * 
		 * @param aHttpExceptionHandler The {@link HttpExceptionHandler} to be
		 *        stored by the {@link HttpExceptionHandler} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withHttpExceptionHandler( HttpExceptionHandler aHttpExceptionHandler );

		/**
		 * Sets the {@link HttpExceptionHandler} for the
		 * {@link HttpExceptionHandler} property.
		 * 
		 * @param aHttpExceptionHandler The {@link HttpExceptionHandler} to be
		 *        stored by the {@link HttpExceptionHandler} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		default B withOnHttpException( HttpExceptionHandler aHttpExceptionHandler ) {
			return withHttpExceptionHandler( aHttpExceptionHandler );
		}
	}

	/**
	 * Extends the {@link HttpExceptionHandlerAccessor} with a setter method.
	 */
	public interface HttpExceptionHandlerProperty extends HttpExceptionHandlerAccessor, HttpExceptionHandlerMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link HttpExceptionHandler} (setter) as of
		 * {@link #setHttpExceptionHandler(HttpExceptionHandler)} and returns
		 * the very same value (getter).
		 * 
		 * @param aHttpExceptionHandler The {@link HttpExceptionHandler} to set
		 *        (via {@link #setHttpExceptionHandler(HttpExceptionHandler)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default HttpExceptionHandler letHttpExceptionHandler( HttpExceptionHandler aHttpExceptionHandler ) {
			setHttpExceptionHandler( aHttpExceptionHandler );
			return aHttpExceptionHandler;
		}
	}
}
