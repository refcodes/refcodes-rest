package org.refcodes.rest;

import org.refcodes.web.BasicAuthCredentials;
import org.refcodes.web.OauthToken;

/**
 * Implements the {@link RestfulClient} interface to be used as loopback device
 * e.g. for testing purposes such as testing your {@link RestResponseConsumer}
 * implementations. Register your custom {@link RestRequestHandler} with
 * {@link #onRestRequest(RestRequestHandler)} to simulate REST responses on the
 * {@link LoopbackRestClient}.
 */
public class LoopbackRestClient extends AbstractRestfulClient {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Registers a {@link RestRequestHandler} to simulate a REST response: The
	 * {@link RestRequestHandler#doRequest(org.refcodes.web.HttpClientRequest)}
	 * method is invoked with a prepared {@link RestResponseHandler} instance
	 * representing the request being sent and produces an according
	 * {@link RestResponseEvent} to be passed back to the issuer of the request.
	 * 
	 * @param aHandler The {@link RestRequestHandler} for handling the HTTP
	 *        request and producing the {@link RestResponseEvent}.
	 */
	@Override
	public void onRestRequest( RestRequestHandler aHandler ) {
		super.onRestRequest( aHandler );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoopbackRestClient withBasicAuthCredentials( BasicAuthCredentials aBasicAuthCredentials ) {
		setBasicAuthCredentials( aBasicAuthCredentials );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoopbackRestClient withBasicAuthCredentials( String aUserName, String aSecret ) {
		setBasicAuthCredentials( aUserName, aSecret );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoopbackRestClient withOAuthToken( OauthToken aOauthToken ) {
		setOauthToken( aOauthToken );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoopbackRestClient withUserAgent( String aUserAgent ) {
		setUserAgent( aUserAgent );
		return this;
	}
}
