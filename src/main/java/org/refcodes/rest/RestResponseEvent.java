// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.io.InputStream;
import java.net.InetSocketAddress;

import org.refcodes.observer.ActionEvent;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpStatusCode;
import org.refcodes.web.ResponseHeaderFields;
import org.refcodes.web.Url;

/**
 * Defines a {@link RestResponseEvent} being the response as consumed by a
 * {@link RestResponseHandler}'s {@link RestResponseConsumer}. Usually you will
 * use
 * {@link RestfulClient#onResponse(HttpMethod, String, RestResponseConsumer)} or
 * the like for registering a {@link RestResponseConsumer} to the
 * {@link RestfulClient} ({@link RestfulHttpClient}).
 */
public class RestResponseEvent extends RestResponse implements ActionEvent<HttpStatusCode, RestfulServer> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link RestResponseEvent} with all required attributes.
	 * 
	 * @param aRestResponse The response with all the required attributes to be
	 *        used for the event.
	 * @param aRestClient The system firing the event.
	 */
	public RestResponseEvent( RestResponse aRestResponse, RestfulClient aRestClient ) {
		this( aRestResponse.getUrl(), aRestResponse.getLocalAddress(), aRestResponse.getRemoteAddress(), aRestResponse.getHttpStatusCode(), aRestResponse.getHeaderFields(), aRestResponse.getHttpInputStream(), aRestClient );
	}

	/**
	 * Constructs a {@link RestResponseEvent} with all required attributes.
	 * 
	 * @param aUrl The URL from which the response originates.
	 * @param aLocalAddress The local address where the event is being received.
	 * @param aRemoteAddress The remote address from which the request
	 *        originates.
	 * @param aHttpStatusCode The {@link HttpStatusCode} of the response.
	 * @param aHeaderFields The {@link ResponseHeaderFields} sent by the
	 *        response.
	 * @param aHttpInputStream The {@link InputStream} representing the
	 *        request's HTTP body.
	 * @param aRestClient The system firing the event.
	 */
	public RestResponseEvent( Url aUrl, InetSocketAddress aLocalAddress, InetSocketAddress aRemoteAddress, HttpStatusCode aHttpStatusCode, ResponseHeaderFields aHeaderFields, InputStream aHttpInputStream, RestfulClient aRestClient ) {
		super( aUrl, aLocalAddress, aRemoteAddress, aHttpStatusCode, aHeaderFields, aHttpInputStream, aRestClient );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestfulServer getSource() {
		return (RestfulServer) _mediaTypeFactoryLookup;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpStatusCode getAction() {
		return getHttpStatusCode();
	}
}
