// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.util.regex.Pattern;

import org.refcodes.exception.BugException;
import org.refcodes.observer.Observable;
import org.refcodes.observer.Observers;
import org.refcodes.web.BaseLocatorAccessor.BaseLocatorBuilder;
import org.refcodes.web.BaseLocatorAccessor.BaseLocatorProperty;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.MediaTypeFactoryLookup.MutableMediaTypeFactoryLookup;
import org.refcodes.web.RealmAccessor.RealmBuilder;
import org.refcodes.web.RealmAccessor.RealmProperty;

/**
 * The {@link RestfulServer} acts as the target for clients issuing REST
 * requests. {@link RestEndpointBuilder} instances, most easily being created
 * with the {@link #onRequest(HttpMethod, String, RestRequestConsumer)} method
 * (or the like), are registered as listeners to the {@link RestfulServer} (
 * {@link RestfulHttpServer}). The {@link RestfulServer}
 * ({@link RestfulHttpServer}) fires {@link RestRequestEvent} events to the
 * {@link RestRequestConsumer}s of an {@link RestEndpoint} dedicated to an
 * according locator (pattern) for a specific {@link HttpMethod}.
 */
public interface RestfulServer extends Observable<RestEndpoint>, Observers<RestEndpoint, RestfulServer>, MutableMediaTypeFactoryLookup, RealmProperty, RealmBuilder<RestfulServer>, BaseLocatorProperty, BaseLocatorBuilder<RestfulServer> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulServer withObserversActive( boolean isActive ) {
		setObserversActive( isActive );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulServer withEnableObservers() {
		setObserversActive( true );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulServer withDisableObservers() {
		setObserversActive( false );
		return this;
	}

	/**
	 * For the sake of unified naming, this method just delegates to
	 * {@link #subscribeObserver(Object)}.
	 *
	 * @param aRestEndpoint the rest endpoint
	 * 
	 * @return true, if successful
	 */
	default boolean onRequest( RestEndpoint aRestEndpoint ) {
		return subscribeObserver( aRestEndpoint );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulServer withBaseLocator( String aBaseLocator ) {
		setBaseLocator( aBaseLocator );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulServer withRealm( String aRealm ) {
		setRealm( aRealm );
		return this;
	}

	/**
	 * Registers a preconfigured {@link RestEndpoint}, triggerd by any HTTP
	 * method, and returns its instance. To unsubscribe via
	 * {@link #unsubscribeObserver(Object)}, use the returned instance.
	 * Attention: Invoke {@link RestEndpointBuilder#open()} to activate this
	 * endpoint!
	 * 
	 * @param aLocatorPathPattern The local locator (regular expression) pattern
	 *        to which this {@link RestEndpoint} is bound.
	 * @param aRequestConsumer The listener processing a request targeted at
	 *        this {@link RestEndpoint}.
	 * 
	 * @return The preconfigured {@link RestEndpoint} which acts as handle to
	 *         unsubscribe the {@link RestEndpoint} via
	 *         {@link #unsubscribeObserver(Object)}.
	 */
	default RestEndpointBuilder onRequest( String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		return onRequest( null, aLocatorPathPattern, aRequestConsumer );
	}

	/**
	 * Registers a preconfigured {@link RestEndpoint}, triggerd by any HTTP
	 * method, and returns its instance. To unsubscribe via
	 * {@link #unsubscribeObserver(Object)}, use the returned instance.
	 * Attention: Invoke {@link RestEndpointBuilder#open()} to activate this
	 * endpoint!
	 * 
	 * @param aLocatorRegExp The local locator (regular expression) pattern to
	 *        which this {@link RestEndpoint} is bound.
	 * @param aRequestConsumer The listener processing a request targeted at
	 *        this {@link RestEndpoint}.
	 * 
	 * @return The preconfigured {@link RestEndpoint} which acts as handle to
	 *         unsubscribe the {@link RestEndpoint} via
	 *         {@link #unsubscribeObserver(Object)}.
	 */
	default RestEndpointBuilder onRequest( Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		return onRequest( null, aLocatorRegExp, aRequestConsumer );
	}

	/**
	 * Registers a preconfigured {@link RestEndpoint} with the least required
	 * attributes and returns its instance. To unsubscribe via
	 * {@link #unsubscribeObserver(Object)}, use the returned instance.
	 * Attention: Invoke {@link RestEndpointBuilder#open()} to activate this
	 * endpoint!
	 * 
	 * @param aHttpMethod The HTTP-Method to which this {@link RestEndpoint} is
	 *        bound. A value of <code>null</code> means that any HTTP-Method may
	 *        trigger this {@link RestEndpoint}.
	 * @param aLocatorPathPattern The local Locator-Pattern to which this
	 *        {@link RestEndpoint} is bound. A value of <code>null</code> means
	 *        that any locator may trigger this {@link RestEndpoint}.
	 * @param aRequestConsumer The listener processing a request targeted at
	 *        this {@link RestEndpoint}.
	 * 
	 * @return The preconfigured {@link RestEndpoint} which acts as handle to
	 *         unsubscribe the {@link RestEndpoint} via
	 *         {@link #unsubscribeObserver(Object)}.
	 */
	default RestEndpointBuilder onRequest( HttpMethod aHttpMethod, String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		final RestEndpointBuilder theEndpoint = new RestEndpointBuilder( aHttpMethod, aLocatorPathPattern, aRequestConsumer );
		if ( !subscribeObserver( theEndpoint ) ) {
			throw new BugException( "We encountered a bug! As we created the endpoint within this method, it cannot have been added already!" );
		}
		return theEndpoint;
	}

	/**
	 * Registers a preconfigured {@link RestEndpoint} with the least required
	 * attributes and returns its instance. To unsubscribe via
	 * {@link #unsubscribeObserver(Object)}, use the returned instance.
	 * Attention: Invoke {@link RestEndpointBuilder#open()} to activate this
	 * endpoint!
	 * 
	 * @param aHttpMethod The HTTP-Method to which this {@link RestEndpoint} is
	 *        bound. A value of <code>null</code> means that any HTTP-Method may
	 *        trigger this {@link RestEndpoint}.
	 * @param aLocatorRegExp The local locator (regular expression) pattern to
	 *        which this {@link RestEndpoint} is bound. A value of
	 *        <code>null</code> means that any locator may trigger this
	 *        {@link RestEndpoint}.
	 * @param aRequestConsumer The listener processing a request targeted at
	 *        this {@link RestEndpoint}.
	 * 
	 * @return The preconfigured {@link RestEndpoint} which acts as handle to
	 *         unsubscribe the {@link RestEndpoint} via
	 *         {@link #unsubscribeObserver(Object)}.
	 */
	default RestEndpointBuilder onRequest( HttpMethod aHttpMethod, Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		final RestEndpointBuilder theEndpoint = new RestEndpointBuilder( aHttpMethod, aLocatorRegExp, aRequestConsumer );
		if ( !subscribeObserver( theEndpoint ) ) {
			throw new BugException( "We encountered a bug! As we created the endpoint within this method, it cannot have been added already!" );
		}
		return theEndpoint;
	}

	/**
	 * Even more convenient: Everything done here can also be done using
	 * {@link #onRequest(HttpMethod, String, RestRequestConsumer)}: Registers a
	 * {@link RestRequestConsumer} with the given Locator-Pattern to incoming
	 * GET methods. Attention: Invoke {@link RestEndpointBuilder#open()} to
	 * activate this endpoint!
	 * 
	 * @param aLocatorPathPattern The local Locator-Pattern to which this
	 *        {@link RestEndpoint} is bound.
	 * @param aRequestConsumer The listener processing a request targeted at
	 *        this {@link RestEndpoint}.
	 * 
	 * @return The preconfigured {@link RestEndpoint} which acts as handle to
	 *         unsubscribe the {@link RestEndpoint} via
	 *         {@link #unsubscribeObserver(Object)}.
	 */
	default RestEndpointBuilder onGet( String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		return onRequest( HttpMethod.GET, aLocatorPathPattern, aRequestConsumer );
	}

	/**
	 * Even more convenient: Everything done here can also be done using
	 * {@link #onRequest(HttpMethod, String, RestRequestConsumer)}: Registers a
	 * {@link RestRequestConsumer} with the given Locator-Pattern to incoming
	 * GET methods. Attention: Invoke {@link RestEndpointBuilder#open()} to
	 * activate this endpoint!
	 * 
	 * @param aLocatorRegExp The local locator (regular expression) pattern to
	 *        which this {@link RestEndpoint} is bound.
	 * @param aRequestConsumer The listener processing a request targeted at
	 *        this {@link RestEndpoint}.
	 * 
	 * @return The preconfigured {@link RestEndpoint} which acts as handle to
	 *         unsubscribe the {@link RestEndpoint} via
	 *         {@link #unsubscribeObserver(Object)}.
	 */
	default RestEndpointBuilder onGet( Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		return onRequest( HttpMethod.GET, aLocatorRegExp, aRequestConsumer );
	}

	/**
	 * Even more convenient: Everything done here can also be done using
	 * {@link #onRequest(HttpMethod, String, RestRequestConsumer)}: Registers a
	 * {@link RestRequestConsumer} with the given Locator-Pattern to incoming
	 * PUT methods. Attention: Invoke {@link RestEndpointBuilder#open()} to
	 * activate this endpoint!
	 * 
	 * @param aLocatorPathPattern The local Locator-Pattern to which this
	 *        {@link RestEndpoint} is bound.
	 * @param aRequestConsumer The listener processing a request targeted at
	 *        this {@link RestEndpoint}.
	 * 
	 * @return The preconfigured {@link RestEndpoint} which acts as handle to
	 *         unsubscribe the {@link RestEndpoint} via
	 *         {@link #unsubscribeObserver(Object)}.
	 */
	default RestEndpointBuilder onPut( String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		return onRequest( HttpMethod.PUT, aLocatorPathPattern, aRequestConsumer );
	}

	/**
	 * Even more convenient: Everything done here can also be done using
	 * {@link #onRequest(HttpMethod, String, RestRequestConsumer)}: Registers a
	 * {@link RestRequestConsumer} with the given Locator-Pattern to incoming
	 * PUT methods. Attention: Invoke {@link RestEndpointBuilder#open()} to
	 * activate this endpoint!
	 * 
	 * @param aLocatorRegExp The local locator (regular expression) pattern to
	 *        which this {@link RestEndpoint} is bound.
	 * @param aRequestConsumer The listener processing a request targeted at
	 *        this {@link RestEndpoint}.
	 * 
	 * @return The preconfigured {@link RestEndpoint} which acts as handle to
	 *         unsubscribe the {@link RestEndpoint} via
	 *         {@link #unsubscribeObserver(Object)}.
	 */
	default RestEndpointBuilder onPut( Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		return onRequest( HttpMethod.PUT, aLocatorRegExp, aRequestConsumer );
	}

	/**
	 * Even more convenient: Everything done here can also be done using
	 * {@link #onRequest(HttpMethod, String, RestRequestConsumer)}: Registers a
	 * {@link RestRequestConsumer} with the given Locator-Pattern to incoming
	 * POST methods. Attention: Invoke {@link RestEndpointBuilder#open()} to
	 * activate this endpoint!
	 * 
	 * @param aLocatorPathPattern The local Locator-Pattern to which this
	 *        {@link RestEndpoint} is bound.
	 * @param aRequestConsumer The listener processing a request targeted at
	 *        this {@link RestEndpoint}.
	 * 
	 * @return The preconfigured {@link RestEndpoint} which acts as handle to
	 *         unsubscribe the {@link RestEndpoint} via
	 *         {@link #unsubscribeObserver(Object)}.
	 */
	default RestEndpointBuilder onPost( String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		return onRequest( HttpMethod.POST, aLocatorPathPattern, aRequestConsumer );
	}

	/**
	 * Even more convenient: Everything done here can also be done using
	 * {@link #onRequest(HttpMethod, String, RestRequestConsumer)}: Registers a
	 * {@link RestRequestConsumer} with the given Locator-Pattern to incoming
	 * POST methods. Attention: Invoke {@link RestEndpointBuilder#open()} to
	 * activate this endpoint!
	 * 
	 * @param aLocatorRegExp The local locator (regular expression) pattern to
	 *        which this {@link RestEndpoint} is bound.
	 * @param aRequestConsumer The listener processing a request targeted at
	 *        this {@link RestEndpoint}.
	 * 
	 * @return The preconfigured {@link RestEndpoint} which acts as handle to
	 *         unsubscribe the {@link RestEndpoint} via
	 *         {@link #unsubscribeObserver(Object)}.
	 */
	default RestEndpointBuilder onPost( Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		return onRequest( HttpMethod.POST, aLocatorRegExp, aRequestConsumer );
	}

	/**
	 * Even more convenient: Everything done here can also be done using
	 * {@link #onRequest(HttpMethod, String, RestRequestConsumer)}: Registers a
	 * {@link RestRequestConsumer} with the given Locator-Pattern to incoming
	 * DELETE methods. Attention: Invoke {@link RestEndpointBuilder#open()} to
	 * activate this endpoint!
	 * 
	 * @param aLocatorPathPattern The local Locator-Pattern to which this
	 *        {@link RestEndpoint} is bound.
	 * @param aRequestConsumer The listener processing a request targeted at
	 *        this {@link RestEndpoint}.
	 * 
	 * @return The preconfigured {@link RestEndpoint} which acts as handle to
	 *         unsubscribe the {@link RestEndpoint} via
	 *         {@link #unsubscribeObserver(Object)}.
	 */
	default RestEndpointBuilder onDelete( String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		return onRequest( HttpMethod.DELETE, aLocatorPathPattern, aRequestConsumer );
	}

	/**
	 * Even more convenient: Everything done here can also be done using
	 * {@link #onRequest(HttpMethod, String, RestRequestConsumer)}: Registers a
	 * {@link RestRequestConsumer} with the given Locator-Pattern to incoming
	 * DELETE methods. Attention: Invoke {@link RestEndpointBuilder#open()} to
	 * activate this endpoint!
	 * 
	 * @param aLocatorRegExp The local locator (regular expression) pattern to
	 *        which this {@link RestEndpoint} is bound.
	 * @param aRequestConsumer The listener processing a request targeted at
	 *        this {@link RestEndpoint}.
	 * 
	 * @return The preconfigured {@link RestEndpoint} which acts as handle to
	 *         unsubscribe the {@link RestEndpoint} via
	 *         {@link #unsubscribeObserver(Object)}.
	 */
	default RestEndpointBuilder onDelete( Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		return onRequest( HttpMethod.DELETE, aLocatorRegExp, aRequestConsumer );
	}
}
