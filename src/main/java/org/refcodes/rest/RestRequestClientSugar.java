// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.net.MalformedURLException;

import org.refcodes.data.Scheme;
import org.refcodes.web.FormFields;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpResponseException;
import org.refcodes.web.RequestHeaderFields;
import org.refcodes.web.Url;

/**
 * Helper class to get the syntactic sugar (from a maintenance point of view)
 * under control. You should actually statically import the
 * {@link HttpRestClientSugar}.
 */
public class RestRequestClientSugar {

	/**
	 * See also {@link RestfulHttpClient#buildRequest(HttpMethod, String)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aScheme, aHost, aPort, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, Scheme, String, int, String, FormFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aScheme, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, Scheme, String, int, String, FormFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aScheme, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, Scheme, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, Scheme, String, int, String, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aScheme, aHost, aPort, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, Scheme, String, int, String, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aScheme, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildRequest(HttpMethod, String)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aScheme, aHost, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, Scheme, String, String, FormFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aScheme, aHost, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, Scheme, String, String, FormFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aScheme, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, Scheme, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aScheme, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, Scheme, String, String, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aScheme, aHost, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, Scheme, String, String, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aScheme, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildRequest(HttpMethod, String)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aUrl ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aUrl );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, String, FormFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aLocator, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, String, FormFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aLocator, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aLocator, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, String, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aUrl, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aUrl, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, String, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aUrl, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildRequest(HttpMethod, String)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aProtocol, aHost, aPort, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, String, String, int, String, FormFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aProtocol, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, String, String, int, String, FormFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aProtocol, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, String, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, String, String, int, String, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aProtocol, aHost, aPort, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, String, String, int, String, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aProtocol, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildRequest(HttpMethod, String)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aProtocol, aHost, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, String, String, String, FormFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aProtocol, aHost, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, String, String, String, FormFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aProtocol, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, String, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, String, String, String, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aProtocol, aHost, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, String, String, String, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aProtocol, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildRequest(HttpMethod, String)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Url aUrl ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aUrl );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildRequest(HttpMethod, Url, RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildRequest( aHttpMethod, aUrl, aHeaderFields );
	}

	/**
	 * See also {@link RestfulHttpClient#doRequest(HttpMethod, String)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aScheme, aHost, aPort, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, Scheme, String, int, String, FormFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aScheme, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, Scheme, String, int, String, FormFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aScheme, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, Scheme, String, int, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, Scheme, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, Scheme, String, int, String, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aScheme, aHost, aPort, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, Scheme, String, int, String, RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aScheme, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, Scheme, String, int, String, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aScheme, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doRequest(HttpMethod, String)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aScheme, aHost, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, Scheme, String, String, FormFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aScheme, aHost, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, Scheme, String, String, FormFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aScheme, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, Scheme, String, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aScheme, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, Scheme, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aScheme, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, Scheme, String, String, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aScheme, aHost, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, Scheme, String, String, RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aScheme, aHost, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, Scheme, String, String, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aScheme, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doRequest(HttpMethod, String)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aUrl ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aUrl );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, FormFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aLocator, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, FormFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aLocator, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aLocator, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aLocator, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doRequest(HttpMethod, String, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aUrl, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aUrl, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aUrl, RequestHeaderFields aHeaderFields ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aUrl, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aUrl, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doRequest(HttpMethod, String)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aProtocol, aHost, aPort, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, String, int, String, FormFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aProtocol, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, String, int, String, FormFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aProtocol, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, String, int, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, String, int, String, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aProtocol, aHost, aPort, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, String, int, String, RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aProtocol, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, String, int, String, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aProtocol, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doRequest(HttpMethod, String)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aProtocol, aHost, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, String, String, FormFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aProtocol, aHost, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, String, String, FormFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aProtocol, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, String, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aProtocol, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, String, String, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aProtocol, aHost, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, String, String, RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aProtocol, aHost, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doRequest(HttpMethod, String, String, String, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aProtocol, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doRequest(HttpMethod, Url)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doRequest( HttpMethod aHttpMethod, Url aUrl ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doRequest( aHttpMethod, aUrl );
	}

	/**
	 * See also {@link RestfulHttpClient#doRequest(RestRequest)}.
	 *
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doRequest( RestRequest aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doRequest( aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, int, String, FormFields, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPort, aPath, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, int, String, FormFields, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, int, String, FormFields,RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, int, String, FormFields, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPort, aPath, aQueryFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, int, String, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPort, aPath, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, int, String, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPort, aPath, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, int, String, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPort, aPath, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, int, String, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPort, aPath, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, String, FormFields, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPath, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, String, FormFields, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPath, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, String, FormFields,RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPath, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, String, FormFields, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPath, aQueryFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, String, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPath, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, String, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPath, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, String, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPath, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, String, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPath, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, FormFields, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aLocator, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, FormFields, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aLocator, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, FormFields,RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aLocator, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, FormFields, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aLocator, aQueryFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aUrl, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aUrl, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, RequestHeaderFields, Object, RestResponseConsumer)}
	 * *.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aUrl, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aUrl, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aUrl, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aUrl, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aUrl, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aUrl, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, int, String, FormFields, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPort, aPath, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, int, String, FormFields, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, int, String, FormFields,RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, int, String, FormFields, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPort, aPath, aQueryFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, int, String, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPort, aPath, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, int, String, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPort, aPath, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, int, String, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPort, aPath, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, int, String, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPort, aPath, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, String, FormFields, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPath, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, String, FormFields, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, String, FormFields,RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, String, FormFields, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPath, aQueryFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, String, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPath, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, String, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPath, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, String, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPath, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, String, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPath, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Url, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Url aUrl, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aUrl, (RequestHeaderFields) null, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Url, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aUrl, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Url, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by the request.
	 */
	public static RestResponseHandler onResponse( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aUrl, aHeaderFields, null, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, int, String, FormFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, int, String, FormFields,RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, int, String, FormFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, int, String, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPort, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, int, String, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, int, String, RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, int, String)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, int aPort, String aPath ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPort, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, String, FormFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, String, FormFields,RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, String, FormFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, FormFields aQueryFields ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, String, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, String, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, String, RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Scheme, String, String)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Scheme aScheme, String aHost, String aPath ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aScheme, aHost, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, FormFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, Object aRequest ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aLocator, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aLocator, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, FormFields,RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aLocator, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, FormFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aLocator, FormFields aQueryFields ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aLocator, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aUrl, Object aRequest ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aUrl, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, RequestHeaderFields, Object)}
	 * *.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aUrl, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aUrl, RequestHeaderFields aHeaderFields ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aUrl, aHeaderFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onResponse(HttpMethod, String)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The targeted URL locating the addressed resource.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aUrl ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aUrl );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, int, String, FormFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, int, String, FormFields,RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, int, String, FormFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, int, String, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPort, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, int, String, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, int, String, RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, int, String)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, int aPort, String aPath ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPort, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, String, FormFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, String, FormFields,RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, String, FormFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, FormFields aQueryFields ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, String, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, String, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, String, RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, String, String, String)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aProtocol The protocol
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, String aProtocol, String aHost, String aPath ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aProtocol, aHost, aPath );
	}

	/**
	 * See also {@link RestfulHttpClient#onResponse(HttpMethod, Url, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Url aUrl, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aUrl, (RequestHeaderFields) null, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Url, RequestHeaderFields, Object)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#POST} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aUrl, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onResponse(HttpMethod, Url, RequestHeaderFields)}.
	 *
	 * @param aHttpMethod The HTTP-Method for the request.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by the request.
	 */
	public static RestResponseResult onResponse( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onResponse( aHttpMethod, aUrl, (RequestHeaderFields) aHeaderFields );
	}
}
