package org.refcodes.rest;

import org.refcodes.web.HttpResponseException;

/**
 * The {@link RestResponseConsumer} can be coded using the <code>lambda</code>
 * syntax and processes a response from a server. The {@link RestResponseEvent}
 * describes the context of the response e.g. retrieve the server's response via
 * {@link RestResponseEvent#getResponse(Class)} or the response's HTTP
 * Status-Code via {@link RestResponseEvent#getHttpStatusCode()}.
 */
@FunctionalInterface
public interface RestResponseConsumer {

	/**
	 * The invoker provides a response context being a {@link RestResponseEvent}
	 * describing the response being processed upon by your
	 * <code>lambda</code>'s code. The method works synchronously and waits
	 * (blocks the caller's thread) till it finishes execution.
	 * 
	 * @param aResponse The response of type {@link RestResponseEvent}
	 *        describing the response context. Use
	 *        {@link RestResponseEvent#getResponse(Class)} to retrieve the
	 *        server's response body or
	 *        {@link RestResponseEvent#getHttpStatusCode()} to retrieve the
	 *        respone's HTTP Status-Code.
	 * 
	 * @throws HttpResponseException thrown by a HTTP-Response handling system
	 *         in case of some unexpected response.
	 */
	void onResponse( RestResponseEvent aResponse ) throws HttpResponseException;
}
