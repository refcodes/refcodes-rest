// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.net.MalformedURLException;

import org.refcodes.data.Scheme;
import org.refcodes.web.FormFields;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpResponseException;
import org.refcodes.web.RequestHeaderFields;
import org.refcodes.web.Url;

/**
 * Helper class to get the syntactic sugar (from a maintenance point of view)
 * under control. You should actually statically import the
 * {@link HttpRestClientSugar}.
 */
public class RestGetClientSugar extends RestRequestClientSugar {

	/**
	 * See also {@link RestfulHttpClient#buildGet(Scheme, String, int, String)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( Scheme aScheme, String aHost, int aPort, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aScheme, aHost, aPort, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(Scheme, String, int, String, FormFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aScheme, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(Scheme, String, int, String, FormFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aScheme, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(Scheme, String, int, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(Scheme, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(Scheme, String, int, String, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aScheme, aHost, aPort, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(Scheme, String, int, String, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aScheme, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(Scheme, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aScheme, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildGet(Scheme, String, String)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( Scheme aScheme, String aHost, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aScheme, aHost, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(Scheme, String, String, FormFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aScheme, aHost, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(Scheme, String, String, FormFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aScheme, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(Scheme, String, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aScheme, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(Scheme, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aScheme, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(Scheme, String, String, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( Scheme aScheme, String aHost, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aScheme, aHost, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(Scheme, String, String, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aScheme, aHost, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(Scheme, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aScheme, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildGet(String)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildGet( String aUrl ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildGet( aUrl );
	}

	/**
	 * See also {@link RestfulHttpClient#buildGet(String, FormFields)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildGet( String aLocator, FormFields aQueryFields ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildGet( aLocator, aQueryFields );
	}

	/**
	 * See also {@link RestfulHttpClient#buildGet(String, FormFields, Object)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildGet( String aLocator, FormFields aQueryFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildGet( aLocator, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildGet( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildGet( aLocator, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildGet( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildGet( aLocator, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildGet(String, Object)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildGet( String aUrl, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildGet( aUrl, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildGet(String, RequestHeaderFields)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildGet( String aUrl, RequestHeaderFields aHeaderFields ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildGet( aUrl, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestRequestBuilder buildGet( String aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().buildGet( aUrl, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildGet(String, String, int, String)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( String aProtocol, String aHost, int aPort, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aProtocol, aHost, aPort, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(String, String, int, String, FormFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aProtocol, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(String, String, int, String, FormFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aProtocol, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(String, String, int, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(String, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(String, String, int, String, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( String aProtocol, String aHost, int aPort, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aProtocol, aHost, aPort, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(String, String, int, String, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aProtocol, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(String, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aProtocol, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildGet(String, String, String)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( String aProtocol, String aHost, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aProtocol, aHost, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(String, String, String, FormFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( String aProtocol, String aHost, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aProtocol, aHost, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(String, String, String, FormFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aProtocol, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(String, String, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aProtocol, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(String, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(String, String, String, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( String aProtocol, String aHost, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aProtocol, aHost, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(String, String, String, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aProtocol, aHost, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#buildGet(String, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aProtocol, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#buildGet(Url)}.
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( Url aUrl ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aUrl );
	}

	/**
	 * See also {@link RestfulHttpClient#buildGet(Url, RequestHeaderFields)}.
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestRequestBuilder buildGet( Url aUrl, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().buildGet( aUrl, aHeaderFields );
	}

	/**
	 * See also {@link RestfulHttpClient#doGet(Scheme, String, int, String)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( Scheme aScheme, String aHost, int aPort, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aScheme, aHost, aPort, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(Scheme, String, int, String, FormFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aScheme, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(Scheme, String, int, String, FormFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aScheme, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(Scheme, String, int, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(Scheme, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(Scheme, String, int, String, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aScheme, aHost, aPort, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(Scheme, String, int, String, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aScheme, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(Scheme, String, int, String, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aScheme, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doGet(Scheme, String, String)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( Scheme aScheme, String aHost, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aScheme, aHost, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(Scheme, String, String, FormFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aScheme, aHost, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(Scheme, String, String, FormFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aScheme, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(Scheme, String, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aScheme, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(Scheme, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aScheme, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doGet(Scheme, String, String, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( Scheme aScheme, String aHost, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aScheme, aHost, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(Scheme, String, String, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aScheme, aHost, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(Scheme, String, String, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aScheme, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doGet(String)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doGet( String aUrl ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doGet( aUrl );
	}

	/**
	 * See also {@link RestfulHttpClient#doGet(String, FormFields)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doGet( String aLocator, FormFields aQueryFields ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doGet( aLocator, aQueryFields );
	}

	/**
	 * See also {@link RestfulHttpClient#doGet(String, FormFields, Object)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doGet( String aLocator, FormFields aQueryFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doGet( aLocator, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doGet( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doGet( aLocator, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doGet( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doGet( aLocator, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doGet(String, Object)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doGet( String aUrl, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doGet( aUrl, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doGet(String, RequestHeaderFields)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doGet( String aUrl, RequestHeaderFields aHeaderFields ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doGet( aUrl, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(String, RequestHeaderFields, Object)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doGet( String aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doGet( aUrl, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doGet(String, String, int, String)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( String aProtocol, String aHost, int aPort, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aProtocol, aHost, aPort, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(String, String, int, String, FormFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aProtocol, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(String, String, int, String, FormFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aProtocol, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(String, String, int, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(String, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(String, String, int, String, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( String aProtocol, String aHost, int aPort, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aProtocol, aHost, aPort, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(String, String, int, String, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aProtocol, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(String, String, int, String, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aProtocol, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doGet(String, String, String)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( String aProtocol, String aHost, String aPath ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aProtocol, aHost, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(String, String, String, FormFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( String aProtocol, String aHost, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aProtocol, aHost, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(String, String, String, FormFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aProtocol, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(String, String, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aProtocol, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(String, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doGet(String, String, String, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( String aProtocol, String aHost, String aPath, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aProtocol, aHost, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(String, String, String, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aProtocol, aHost, aPath, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#doGet(String, String, String, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	public static RestResponse doGet( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return HttpRestClientSingleton.getInstance().doGet( aProtocol, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#doGet(Url, Object)}.
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponse doGet( Url aUrl, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return HttpRestClientSingleton.getInstance().doGet( aUrl, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, int, String, FormFields, Object, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPort, aPath, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, int, String, FormFields, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, int, String, FormFields, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, int, String, FormFields, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPort, aPath, aQueryFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, int, String, Object, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPort, aPath, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, int, String, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPort, aPath, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, int, String, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPort, aPath, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, int, String, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Scheme aScheme, String aHost, int aPort, String aPath, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPort, aPath, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, String, FormFields, Object, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPath, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, String, FormFields, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPath, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, String, FormFields, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPath, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, String, FormFields, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPath, aQueryFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, String, Object, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Scheme aScheme, String aHost, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPath, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, String, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPath, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, String, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPath, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, String, RestResponseConsumer)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Scheme aScheme, String aHost, String aPath, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPath, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, FormFields, Object, RestResponseConsumer)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onGet( String aLocator, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onGet( aLocator, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, FormFields, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onGet( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onGet( aLocator, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, FormFields, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onGet( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onGet( aLocator, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, FormFields, RestResponseConsumer)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onGet( String aLocator, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onGet( aLocator, aQueryFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, Object, RestResponseConsumer)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onGet( String aUrl, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onGet( aUrl, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onGet( String aUrl, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onGet( aUrl, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onGet( String aUrl, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onGet( aUrl, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also {@link RestfulHttpClient#onGet(String, RestResponseConsumer)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseHandler onGet( String aUrl, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onGet( aUrl, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, int, String, FormFields, Object, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPort, aPath, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, int, String, FormFields, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, int, String, FormFields, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, int, String, FormFields, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPort, aPath, aQueryFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, int, String, Object, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( String aProtocol, String aHost, int aPort, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPort, aPath, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, int, String, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPort, aPath, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, int, String, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPort, aPath, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, int, String, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( String aProtocol, String aHost, int aPort, String aPath, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPort, aPath, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, String, FormFields, Object, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPath, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, String, FormFields, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, String, FormFields, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, String, FormFields, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPath, aQueryFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, String, Object, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( String aProtocol, String aHost, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPath, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, String, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPath, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, String, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPath, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, String, RestResponseConsumer)}}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( String aProtocol, String aHost, String aPath, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPath, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Url, Object, RestResponseConsumer)}.
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Url aUrl, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aUrl, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, String, RequestHeaderFields, Object, RestResponseConsumer)}.
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aUrl, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Url, RequestHeaderFields, RestResponseConsumer)}.
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 * 
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	public static RestResponseHandler onGet( Url aUrl, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return HttpRestClientSingleton.getInstance().onGet( aUrl, aHeaderFields, aResponseConsumer );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, int, String, FormFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, int, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, int, String, FormFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, int, String, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPort, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, int, String, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, int, String, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onGet(Scheme, String, int, String)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Scheme aScheme, String aHost, int aPort, String aPath ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPort, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, String, FormFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, String, FormFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPath, aQueryFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onGet(Scheme, String, String, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Scheme aScheme, String aHost, String aPath, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, String, RequestHeaderFields, Object)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(Scheme, String, String, RequestHeaderFields)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPath, aHeaderFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onGet(Scheme, String, String)}.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Scheme aScheme, String aHost, String aPath ) {
		return HttpRestClientSingleton.getInstance().onGet( aScheme, aHost, aPath );
	}

	/**
	 * See also {@link RestfulHttpClient#onGet(String, FormFields, Object)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onGet( String aLocator, FormFields aQueryFields, Object aRequest ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onGet( aLocator, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onGet( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onGet( aLocator, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onGet( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onGet( aLocator, aQueryFields, aHeaderFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onGet(String, FormFields)}.
	 *
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onGet( String aLocator, FormFields aQueryFields ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onGet( aLocator, aQueryFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onGet(String, Object)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onGet( String aUrl, Object aRequest ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onGet( aUrl, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, RequestHeaderFields, Object)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onGet( String aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onGet( aUrl, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#onGet(String, RequestHeaderFields)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onGet( String aUrl, RequestHeaderFields aHeaderFields ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onGet( aUrl, aHeaderFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onGet(String)}.
	 *
	 * @param aUrl The targeted URL locating the addressed resource.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public static RestResponseResult onGet( String aUrl ) throws MalformedURLException {
		return HttpRestClientSingleton.getInstance().onGet( aUrl );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, int, String, FormFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, int, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, int, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, int, String, FormFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, int, String, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( String aProtocol, String aHost, int aPort, String aPath, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPort, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, int, String, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, int, String, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onGet(String, String, int, String)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( String aProtocol, String aHost, int aPort, String aPath ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPort, aPath );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, String, FormFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, String, FormFields, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, String, FormFields, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, String, FormFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( String aProtocol, String aHost, String aPath, FormFields aQueryFields ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPath, aQueryFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onGet(String, String, String, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( String aProtocol, String aHost, String aPath, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPath, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, String, RequestHeaderFields, Object)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, String, RequestHeaderFields)}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPath, aHeaderFields );
	}

	/**
	 * See also {@link RestfulHttpClient#onGet(String, String, String)}}.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( String aProtocol, String aHost, String aPath ) {
		return HttpRestClientSingleton.getInstance().onGet( aProtocol, aHost, aPath );
	}

	/**
	 * See also {@link RestfulHttpClient#onGet(Url, Object)}.
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Url aUrl, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onGet( aUrl, aRequest );
	}

	/**
	 * See also
	 * {@link RestfulHttpClient#onGet(String, String, String, RequestHeaderFields, Object)}.
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#GET} request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return HttpRestClientSingleton.getInstance().onGet( aUrl, aHeaderFields, aRequest );
	}

	/**
	 * See also {@link RestfulHttpClient#onGet(Url, RequestHeaderFields)}.
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * 
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	public static RestResponseResult onGet( Url aUrl, RequestHeaderFields aHeaderFields ) {
		return HttpRestClientSingleton.getInstance().onGet( aUrl, aHeaderFields );
	}
}
