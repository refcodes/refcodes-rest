package org.refcodes.rest;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.refcodes.component.ConnectionStatus;
import org.refcodes.component.ConnectionStatusAccessor;
import org.refcodes.component.LinkComponent.LinkComponentBuilder;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpMethodAccessor.HttpMethodBuilder;
import org.refcodes.web.HttpMethodAccessor.HttpMethodProperty;
import org.refcodes.web.HttpServerResponse;
import org.refcodes.web.HttpStatusException;

/**
 * An {@link RestEndpointBuilder} extends an {@link RestEndpoint} with builder
 * functionality and adds <code>lambda</code> support for handling the requests
 * addressed to this {@link RestEndpoint}. The <code>lambda</code> defined as
 * {@link RestRequestConsumer} acts as the single listener to this
 * {@link RestEndpoint} responsible for handling the requests for which this
 * {@link RestEndpoint} is responsible. The locator for which an
 * {@link RestEndpointBuilder} is responsible for is defined by the
 * {@link RestEndpointBuilder}'s Locator-Pattern: A single asterisk ("*")
 * matches zero or more characters within a locator name. A double asterisk
 * ("**") matches zero or more characters across directory levels. A question
 * mark ("?") matches exactly one character within a locator name. The single
 * asterisk ("*"), the double asterisk ("**") and the question mark ("?") we
 * refer to as wildcard: You get an array with the substitutes of the wildcards
 * using the method {@link RestRequestEvent#getWildcardReplacements()} . You may
 * name a wildcard by prefixing it with "${someWildcardName}". For example a
 * named wildcard may look as follows: "${arg1}=*" or "${arg2}=**" or
 * "${arg3}=?". You can get the text substituting a named wildcard using the
 * method {@link RestRequestEvent#getWildcardReplacement(String)}. For ease of
 * use, a named wildcard with single asterisk ("*") such as "${arg1}=*" can be
 * abbreviated as "${arg1}".
 */
public class RestEndpointBuilder implements RestEndpoint, HttpMethodProperty, HttpMethodBuilder<RestEndpointBuilder>, LinkComponentBuilder<RestEndpointBuilder>, ConnectionStatusAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( RestEndpointBuilder.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected RestRequestConsumer _requestObserver = null;
	protected HttpMethod _httpMethod = HttpMethod.GET;
	protected String _locatorPathPattern = null;
	protected Pattern _locatorRegExp = null;
	protected ConnectionStatus _connectionStatus = ConnectionStatus.NONE;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a plain {@link RestEndpointBuilder}, make sure to provide the
	 * least required attributes as demonstrated by the constructor
	 * {@link #RestEndpointBuilder(HttpMethod, String, RestRequestConsumer)}.
	 */
	public RestEndpointBuilder() {}

	/**
	 * Constructs an {@link RestEndpointBuilder} with the least required
	 * attributes.
	 * 
	 * @param aHttpMethod The HTTP-Method to which this
	 *        {@link RestEndpointBuilder} is bound.
	 * @param aLocatorPathPattern The local host's locator pattern to which this
	 *        {@link RestEndpointBuilder} is bound. See
	 *        {@link #setLocatorPathPattern(String)} on the syntax of the
	 *        pattern.
	 * @param aRequestConsumer The listener processing requests targeted at this
	 *        {@link RestEndpointBuilder}.
	 */
	public RestEndpointBuilder( HttpMethod aHttpMethod, String aLocatorPathPattern, RestRequestConsumer aRequestConsumer ) {
		_httpMethod = aHttpMethod;
		_locatorPathPattern = aLocatorPathPattern;
		_requestObserver = aRequestConsumer;
	}

	/**
	 * Constructs an {@link RestEndpointBuilder} with the least required
	 * attributes.
	 * 
	 * @param aHttpMethod The HTTP-Method to which this
	 *        {@link RestEndpointBuilder} is bound.
	 * @param aLocatorRegExp The local host's locator (regular expression)
	 *        pattern to which this {@link RestEndpointBuilder} is bound. See
	 *        {@link #setLocatorPathPattern(String)} on the syntax of the
	 *        pattern.
	 * @param aRequestConsumer The listener processing requests targeted at this
	 *        {@link RestEndpointBuilder}.
	 */
	public RestEndpointBuilder( HttpMethod aHttpMethod, Pattern aLocatorRegExp, RestRequestConsumer aRequestConsumer ) {
		_httpMethod = aHttpMethod;
		_locatorRegExp = aLocatorRegExp;
		_requestObserver = aRequestConsumer;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestEndpointBuilder withHttpMethod( HttpMethod aHttpMethod ) {
		setHttpMethod( aHttpMethod );
		return this;
	}

	/**
	 * Sets the resource locator pattern as of the Builder-Pattern. The locator
	 * may contain wildcards as known from file-systems as follows: A single
	 * asterisk ("*") matches zero or more characters within a locator name. A
	 * double asterisk ("**") matches zero or more characters across directory
	 * levels. A question mark ("?") matches exactly one character within a
	 * locator name. You may name a wildcard by prefixing it with
	 * "${someWildcardName}". For example a named wildcard may look as follows:
	 * "${arg1}=*" or "${arg2}=**" or "${arg3}=?". You can get the text
	 * substituting a named wildcard using the method
	 * {@link RestRequestEvent#getWildcardReplacement(String)}. For ease of use,
	 * a named wildcard with single asterisk ("*") such as "${arg1}=*" can be
	 * abbreviated as "${arg1}".
	 *
	 * @param aLocatorPathPattern the locator pattern
	 * 
	 * @return The {@link RestEndpoint} builder to continue configuration (as of
	 *         the Builder-Pattern).
	 */
	public RestEndpointBuilder withLocatorPathPattern( String aLocatorPathPattern ) {
		setLocatorPathPattern( aLocatorPathPattern );
		return this;
	}

	/**
	 * Sets the resource locator (regular expression) pattern as of the
	 * Builder-Pattern.
	 *
	 * @param aLocatorRegExp the locator (regular expression) pattern
	 * 
	 * @return The {@link RestEndpoint} builder to continue configuration (as of
	 *         the Builder-Pattern).
	 */
	public RestEndpointBuilder withLocatorRegExp( Pattern aLocatorRegExp ) {
		setLocatorRegExp( aLocatorRegExp );
		return this;
	}

	/**
	 * Builder method for setting the {@link RestRequestConsumer}.
	 * 
	 * @param aLambda The (user defined) {@link RestRequestConsumer} to handle
	 *        requests, feel free to code it as <code>lambda</code> expression
	 * 
	 * @return The {@link RestEndpointBuilder} for the sake of a fluent API.
	 */
	public RestEndpointBuilder withRequestObserver( RestRequestConsumer aLambda ) {
		setRequestObserver( aLambda );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestEndpointBuilder withOpen() throws IOException {
		open();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestEndpointBuilder withClose() throws IOException {
		close();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestEndpointBuilder withCloseQuietly() {
		closeQuietly();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestEndpointBuilder withCloseIn( int aCloseMillis ) {
		closeIn( aCloseMillis );
		return this;
	}

	/**
	 * Sets the resource locator pattern. The locator may contain wildcards as
	 * known from file-systems as follows: A single asterisk ("*") matches zero
	 * or more characters within a locator name. A double asterisk ("**")
	 * matches zero or more characters across directory levels. A question mark
	 * ("?") matches exactly one character within a locator name. You may name a
	 * wildcard by prefixing it with "${someWildcardName}". For example a named
	 * wildcard may look as follows: "${arg1}=*" or "${arg2}=**" or "${arg3}=?".
	 * You can get the text substituting a named wildcard using the method
	 * {@link RestRequestEvent#getWildcardReplacement(String)}. For ease of use,
	 * a named wildcard with single asterisk ("*") such as "${arg1}=*" can be
	 * abbreviated as "${arg1}".
	 *
	 * @param aLocatorPathPattern the new locator pattern
	 */
	public void setLocatorPathPattern( String aLocatorPathPattern ) {
		_locatorPathPattern = aLocatorPathPattern;
		_locatorRegExp = null;
	}

	/**
	 * Sets the resource locator (regular expression) pattern.
	 *
	 * @param aLocatorRegExp the new locator (regular expression) pattern
	 */
	public void setLocatorRegExp( Pattern aLocatorRegExp ) {
		_locatorRegExp = aLocatorRegExp;
		_locatorPathPattern = null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpMethod( HttpMethod aHttpMethod ) {
		_httpMethod = aHttpMethod;
	}

	/**
	 * Sets the {@link RestRequestConsumer} to which any requests are delegated
	 * upon invocation of the
	 * {@link #onRequest(RestRequestEvent, org.refcodes.web.HttpServerResponse)}
	 * method.
	 * 
	 * @param aLambda The (user defined) {@link RestRequestConsumer} to handle
	 *        requests, feel free to code it as <code>lambda</code> expression!
	 */
	public void setRequestObserver( RestRequestConsumer aLambda ) {
		_requestObserver = aLambda;
	}

	/**
	 * Retrieves the {@link RestRequestConsumer} to which any requests are
	 * delegated upon invocation of the
	 * {@link #onRequest(RestRequestEvent, org.refcodes.web.HttpServerResponse)}
	 * method.
	 * 
	 * @return The (user defined) {@link RestRequestConsumer} to handle
	 *         requests.
	 */
	public RestRequestConsumer getRequestObserver() {
		return _requestObserver;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onRequest( RestRequestEvent aRequest, HttpServerResponse aResponse ) throws HttpStatusException {
		if ( _connectionStatus == ConnectionStatus.OPENED ) {
			_requestObserver.onRequest( aRequest, aResponse );
		}
		else {
			LOGGER.log( Level.WARNING, "Ignoring request <" + aRequest + "> as this rest endpoint is in status <" + _connectionStatus + ">, you may not have opened it?" );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpMethod getHttpMethod() {
		return _httpMethod;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLocatorPathPattern() {
		return _locatorPathPattern;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Pattern getLocatorRegExp() {
		return _locatorRegExp;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open() throws IOException {
		_connectionStatus = ConnectionStatus.OPENED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_connectionStatus = ConnectionStatus.CLOSED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConnectionStatus getConnectionStatus() {
		return _connectionStatus;
	}
}
