// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.io.InputStream;
import java.net.InetSocketAddress;

import org.refcodes.mixin.Dumpable;
import org.refcodes.mixin.WildcardSubstitutes;
import org.refcodes.observer.ActionEvent;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpServerRequest;
import org.refcodes.web.LocalAddressAccessor;
import org.refcodes.web.RealmAccessor;
import org.refcodes.web.RemoteAddressAccessor;
import org.refcodes.web.RequestHeaderFields;
import org.refcodes.web.Url;

/**
 * Defines a {@link RestRequestEvent} being the request as consumed by a
 * {@link RestEndpoint}. Usually you will use
 * {@link RestfulServer#onRequest(HttpMethod, String, RestRequestConsumer)} for
 * registering a {@link RestRequestConsumer} to the {@link RestfulServer} (
 * {@link RestfulHttpServer}).
 */
public class RestRequestEvent extends HttpServerRequest implements ActionEvent<HttpMethod, RestfulServer>, WildcardSubstitutes, RemoteAddressAccessor, LocalAddressAccessor, RealmAccessor, Dumpable {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final InetSocketAddress _remoteAddress;
	private final InetSocketAddress _localAddress;
	private final WildcardSubstitutes _wildcardSubstitutes;
	private final String _realm;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link RestRequestEvent} with all required attributes.
	 * 
	 * @param aLocalAddress The local address where the event is being received.
	 * @param aRemoteAddress The remote address from which the request
	 *        originates.
	 * @param aHttpMethod The {@link HttpMethod} with which the request has been
	 *        sent.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aWildcardSubstitutes The text being substituted by the wildcard of
	 *        the {@link RestEndpoint}'s Locator-Pattern.
	 * @param aHeaderFields The {@link RequestHeaderFields} sent by the request.
	 * @param aHttpInputStream The {@link InputStream} representing the
	 *        request's HTTP body.
	 * @param aRestfulServer The system firing the event.
	 */
	public RestRequestEvent( InetSocketAddress aLocalAddress, InetSocketAddress aRemoteAddress, HttpMethod aHttpMethod, Url aUrl, WildcardSubstitutes aWildcardSubstitutes, RequestHeaderFields aHeaderFields, InputStream aHttpInputStream, RestfulServer aRestfulServer ) {
		super( aHttpMethod, aUrl, aHeaderFields, aHttpInputStream, aRestfulServer );
		_wildcardSubstitutes = aWildcardSubstitutes;
		_remoteAddress = aRemoteAddress;
		_localAddress = aLocalAddress;
		_realm = aRestfulServer.getRealm();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpMethod getAction() {
		return getHttpMethod();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestfulServer getSource() {
		return (RestfulServer) _mediaTypeFactoryLookup;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getWildcardReplacements() {
		return _wildcardSubstitutes.getWildcardReplacements();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getWildcardReplacementAt( int aIndex ) {
		return _wildcardSubstitutes.getWildcardReplacementAt( aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getWildcardReplacement( String aWildcardName ) {
		return _wildcardSubstitutes.getWildcardReplacement( aWildcardName );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getWildcardReplacements( String... aWildcardNames ) {
		return _wildcardSubstitutes.getWildcardReplacements( aWildcardNames );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getWildcardReplacementsAt( int... aIndexes ) {
		return _wildcardSubstitutes.getWildcardReplacementsAt( aIndexes );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InetSocketAddress getRemoteAddress() {
		return _remoteAddress;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InetSocketAddress getLocalAddress() {
		return _localAddress;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getRealm() {
		return _realm;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getWildcardNames() {
		return _wildcardSubstitutes.getWildcardNames();
	}
}
