// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.net.MalformedURLException;

import org.refcodes.data.Scheme;
import org.refcodes.web.FormFields;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpResponseException;
import org.refcodes.web.RequestHeaderFields;
import org.refcodes.web.Url;

/**
 * Helper interface to keep the huge amount of convenience methods under
 * control.
 */
public interface RestDeleteClient extends RestRequestClient {

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( Scheme aScheme, String aHost, int aPort, String aPath ) {
		return buildRequest( HttpMethod.DELETE, aScheme, aHost, aPort, aPath );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		return buildRequest( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) {
		return buildRequest( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return buildRequest( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return buildRequest( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest ) {
		return buildRequest( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) {
		return buildRequest( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return buildRequest( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( Scheme aScheme, String aHost, String aPath ) {
		return buildRequest( HttpMethod.DELETE, aScheme, aHost, aPath );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields ) {
		return buildRequest( HttpMethod.DELETE, aScheme, aHost, aPath, aQueryFields );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) {
		return buildRequest( HttpMethod.DELETE, aScheme, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return buildRequest( HttpMethod.DELETE, aScheme, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return buildRequest( HttpMethod.DELETE, aScheme, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( Scheme aScheme, String aHost, String aPath, Object aRequest ) {
		return buildRequest( HttpMethod.DELETE, aScheme, aHost, aPath, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields ) {
		return buildRequest( HttpMethod.DELETE, aScheme, aHost, aPath, aHeaderFields );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return buildRequest( HttpMethod.DELETE, aScheme, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aUrl The targeted URL locating the addressed resource..
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestRequestBuilder buildDelete( String aUrl ) throws MalformedURLException {
		return buildRequest( HttpMethod.DELETE, aUrl );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestRequestBuilder buildDelete( String aLocator, FormFields aQueryFields ) throws MalformedURLException {
		return buildRequest( HttpMethod.DELETE, aLocator, aQueryFields );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestRequestBuilder buildDelete( String aLocator, FormFields aQueryFields, Object aRequest ) throws MalformedURLException {
		return buildRequest( HttpMethod.DELETE, aLocator, aQueryFields, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment..
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestRequestBuilder buildDelete( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws MalformedURLException {
		return buildRequest( HttpMethod.DELETE, aLocator, aQueryFields, aHeaderFields );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment..
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestRequestBuilder buildDelete( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws MalformedURLException {
		return buildRequest( HttpMethod.DELETE, aLocator, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aUrl The targeted URL locating the addressed resource..
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestRequestBuilder buildDelete( String aUrl, Object aRequest ) throws MalformedURLException {
		return buildRequest( HttpMethod.DELETE, aUrl, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 *
	 * @param aUrl The targeted URL locating the addressed resource..
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestRequestBuilder buildDelete( String aUrl, RequestHeaderFields aHeaderFields ) throws MalformedURLException {
		return buildRequest( HttpMethod.DELETE, aUrl, aHeaderFields );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aUrl The targeted URL locating the addressed resource..
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestRequestBuilder buildDelete( String aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) throws MalformedURLException {
		return buildRequest( HttpMethod.DELETE, aUrl, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) including the specific
	 *        part (such as "//" for "http://") to be used for the destination
	 *        URL: For HTTP, provide "http://", for HTTPS, provide "https://".
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( String aProtocol, String aHost, int aPort, String aPath ) {
		return buildRequest( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		return buildRequest( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) {
		return buildRequest( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return buildRequest( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return buildRequest( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( String aProtocol, String aHost, int aPort, String aPath, Object aRequest ) {
		return buildRequest( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) {
		return buildRequest( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return buildRequest( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( String aProtocol, String aHost, String aPath ) {
		return buildRequest( HttpMethod.DELETE, aProtocol, aHost, aPath );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( String aProtocol, String aHost, String aPath, FormFields aQueryFields ) {
		return buildRequest( HttpMethod.DELETE, aProtocol, aHost, aPath, aQueryFields );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) {
		return buildRequest( HttpMethod.DELETE, aProtocol, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return buildRequest( HttpMethod.DELETE, aProtocol, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return buildRequest( HttpMethod.DELETE, aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( String aProtocol, String aHost, String aPath, Object aRequest ) {
		return buildRequest( HttpMethod.DELETE, aProtocol, aHost, aPath, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 *
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields ) {
		return buildRequest( HttpMethod.DELETE, aProtocol, aHost, aPath, aHeaderFields );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return buildRequest( HttpMethod.DELETE, aProtocol, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( Url aUrl ) {
		return buildRequest( HttpMethod.DELETE, aUrl );
	}

	/**
	 * Prepares a DELETE request builder with the common attributes and returns
	 * the according request's {@link RestRequestBuilder} instance. Invoking
	 * {@link RestRequestBuilder#toRestResponse} starts the request and
	 * synchronously returns the response.
	 * 
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestRequestBuilder} which is used to prepare and
	 *         synchronously issue the request.
	 */
	default RestRequestBuilder buildDelete( Url aUrl, RequestHeaderFields aHeaderFields ) {
		return buildRequest( HttpMethod.DELETE, aUrl, aHeaderFields );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Scheme aScheme, String aHost, int aPort, String aPath ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aScheme, aHost, aPort, aPath );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 * 
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Scheme aScheme, String aHost, String aPath ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aScheme, aHost, aPath );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aScheme, aHost, aPath, aQueryFields );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aScheme, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aScheme, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aScheme, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Scheme aScheme, String aHost, String aPath, Object aRequest ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aScheme, aHost, aPath, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aScheme, aHost, aPath, aHeaderFields );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aScheme, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aUrl The targeted URL locating the addressed resource..
	 * 
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponse doDelete( String aUrl ) throws HttpResponseException, MalformedURLException {
		return doRequest( HttpMethod.DELETE, aUrl );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment..
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponse doDelete( String aLocator, FormFields aQueryFields ) throws HttpResponseException, MalformedURLException {
		return doRequest( HttpMethod.DELETE, aLocator, aQueryFields );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment..
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponse doDelete( String aLocator, FormFields aQueryFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return doRequest( HttpMethod.DELETE, aLocator, aQueryFields, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment..
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponse doDelete( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException, MalformedURLException {
		return doRequest( HttpMethod.DELETE, aLocator, aQueryFields, aHeaderFields );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment..
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponse doDelete( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return doRequest( HttpMethod.DELETE, aLocator, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aUrl The targeted URL locating the addressed resource..
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponse doDelete( String aUrl, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return doRequest( HttpMethod.DELETE, aUrl, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aUrl The targeted URL locating the addressed resource..
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponse doDelete( String aUrl, RequestHeaderFields aHeaderFields ) throws HttpResponseException, MalformedURLException {
		return doRequest( HttpMethod.DELETE, aUrl, aHeaderFields );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aUrl The targeted URL locating the addressed resource..
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponse doDelete( String aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException, MalformedURLException {
		return doRequest( HttpMethod.DELETE, aUrl, aHeaderFields, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( String aProtocol, String aHost, int aPort, String aPath ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( String aProtocol, String aHost, int aPort, String aPath, Object aRequest ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( String aProtocol, String aHost, String aPath ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aProtocol, aHost, aPath );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( String aProtocol, String aHost, String aPath, FormFields aQueryFields ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aProtocol, aHost, aPath, aQueryFields );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aProtocol, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aProtocol, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( String aProtocol, String aHost, String aPath, Object aRequest ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aProtocol, aHost, aPath, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aProtocol, aHost, aPath, aHeaderFields );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aProtocol, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Url aUrl ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aUrl );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Url aUrl, Object aRequest ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aUrl, aRequest );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Url aUrl, RequestHeaderFields aHeaderFields ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aUrl, aHeaderFields );
	}

	/**
	 * Sends a DELETE request with the common attributes and returns the
	 * according request's {@link RestResponse} instance synchronously.
	 * 
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponse} as of your request.
	 *
	 * @throws HttpResponseException thrown in case of some unexpected response.
	 */
	default RestResponse doDelete( Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) throws HttpResponseException {
		return doRequest( HttpMethod.DELETE, aUrl, aHeaderFields, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aQueryFields, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aHeaderFields, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Scheme aScheme, String aHost, int aPort, String aPath, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPath, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPath, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPath, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPath, aQueryFields, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Scheme aScheme, String aHost, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPath, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPath, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPath, aHeaderFields, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Scheme aScheme, String aHost, String aPath, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPath, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment..
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseHandler onDelete( String aLocator, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return onResponse( HttpMethod.DELETE, aLocator, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment..
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseHandler onDelete( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return onResponse( HttpMethod.DELETE, aLocator, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment..
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseHandler onDelete( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return onResponse( HttpMethod.DELETE, aLocator, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment..
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseHandler onDelete( String aLocator, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return onResponse( HttpMethod.DELETE, aLocator, aQueryFields, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aUrl The targeted URL locating the addressed resource..
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseHandler onDelete( String aUrl, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return onResponse( HttpMethod.DELETE, aUrl, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aUrl The targeted URL locating the addressed resource..
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseHandler onDelete( String aUrl, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return onResponse( HttpMethod.DELETE, aUrl, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aUrl The targeted URL locating the addressed resource..
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseHandler onDelete( String aUrl, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return onResponse( HttpMethod.DELETE, aUrl, aHeaderFields, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aUrl The targeted URL locating the addressed resource..
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseHandler onDelete( String aUrl, RestResponseConsumer aResponseConsumer ) throws MalformedURLException {
		return onResponse( HttpMethod.DELETE, aUrl, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aQueryFields, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( String aProtocol, String aHost, int aPort, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aHeaderFields, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( String aProtocol, String aHost, int aPort, String aPath, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPath, aQueryFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPath, aQueryFields, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( String aProtocol, String aHost, String aPath, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPath, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPath, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPath, aHeaderFields, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( String aProtocol, String aHost, String aPath, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPath, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Url aUrl, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aUrl, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 * 
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aUrl, aHeaderFields, aRequest, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseHandler} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseHandler#open()} on the returned
	 * {@link RestResponseHandler} as the {@link RestResponseHandler} still may
	 * be modified after invoking this method!
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aResponseConsumer The listener processing a response targeted at
	 *        this {@link RestResponseHandler}.
	 *
	 * @return The {@link RestResponseHandler} which is used by your request.
	 */
	default RestResponseHandler onDelete( Url aUrl, RequestHeaderFields aHeaderFields, RestResponseConsumer aResponseConsumer ) {
		return onResponse( HttpMethod.DELETE, aUrl, aHeaderFields, aResponseConsumer );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Scheme aScheme, String aHost, int aPort, String aPath, Object aRequest ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Scheme aScheme, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Scheme aScheme, String aHost, int aPort, String aPath ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPort, aPath );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPath, aQueryFields );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Scheme aScheme, String aHost, String aPath, Object aRequest ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPath, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Scheme aScheme, String aHost, String aPath, RequestHeaderFields aHeaderFields ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPath, aHeaderFields );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Scheme aScheme, String aHost, String aPath ) {
		return onResponse( HttpMethod.DELETE, aScheme, aHost, aPath );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment..
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseResult onDelete( String aLocator, FormFields aQueryFields, Object aRequest ) throws MalformedURLException {
		return onResponse( HttpMethod.DELETE, aLocator, aQueryFields, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment..
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseResult onDelete( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) throws MalformedURLException {
		return onResponse( HttpMethod.DELETE, aLocator, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment..
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseResult onDelete( String aLocator, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) throws MalformedURLException {
		return onResponse( HttpMethod.DELETE, aLocator, aQueryFields, aHeaderFields );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aLocator The targeted locator, usually the part of the URL till
	 *        (excluding) the query fields and the fragment..
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseResult onDelete( String aLocator, FormFields aQueryFields ) throws MalformedURLException {
		return onResponse( HttpMethod.DELETE, aLocator, aQueryFields );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aUrl The targeted URL locating the addressed resource..
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseResult onDelete( String aUrl, Object aRequest ) throws MalformedURLException {
		return onResponse( HttpMethod.DELETE, aUrl, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aUrl The targeted URL locating the addressed resource..
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseResult onDelete( String aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) throws MalformedURLException {
		return onResponse( HttpMethod.DELETE, aUrl, aHeaderFields, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aUrl The targeted URL locating the addressed resource..
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseResult onDelete( String aUrl, RequestHeaderFields aHeaderFields ) throws MalformedURLException {
		return onResponse( HttpMethod.DELETE, aUrl, aHeaderFields );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aUrl The targeted URL locating the addressed resource..
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 *
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default RestResponseResult onDelete( String aUrl ) throws MalformedURLException {
		return onResponse( HttpMethod.DELETE, aUrl );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, Object aRequest ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aQueryFields, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( String aProtocol, String aHost, int aPort, String aPath, Object aRequest ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aHeaderFields, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( String aProtocol, String aHost, int aPort, String aPath, RequestHeaderFields aHeaderFields ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath, aHeaderFields );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( String aProtocol, String aHost, int aPort, String aPath ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPort, aPath );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( String aProtocol, String aHost, String aPath, FormFields aQueryFields, Object aRequest ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPath, aQueryFields, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPath, aQueryFields, aHeaderFields, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( String aProtocol, String aHost, String aPath, FormFields aQueryFields, RequestHeaderFields aHeaderFields ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPath, aQueryFields, aHeaderFields );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( String aProtocol, String aHost, String aPath, FormFields aQueryFields ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPath, aQueryFields );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( String aProtocol, String aHost, String aPath, Object aRequest ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPath, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPath, aHeaderFields, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( String aProtocol, String aHost, String aPath, RequestHeaderFields aHeaderFields ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPath, aHeaderFields );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( String aProtocol, String aHost, String aPath ) {
		return onResponse( HttpMethod.DELETE, aProtocol, aHost, aPath );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Url aUrl, Object aRequest ) {
		return onResponse( HttpMethod.DELETE, aUrl, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 * 
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 * @param aRequest The body to be sent with the request (e.g. when doing a
	 *        {@link HttpMethod#DELETE} request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest ) {
		return onResponse( HttpMethod.DELETE, aUrl, aHeaderFields, aRequest );
	}

	/**
	 * Creates a DELETE request with the common attributes and returns the
	 * according {@link RestResponseResult} instance used for the request. The
	 * HTTP-Request is actually sent not earlier than you calling
	 * {@link RestResponseResult#open()} on the returned
	 * {@link RestResponseResult} as the {@link RestResponseResult} still may be
	 * modified after invoking this method!
	 *
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The HTTP-Header's fields to be used for the request.
	 *
	 * @return The {@link RestResponseResult} which is used by your request.
	 */
	default RestResponseResult onDelete( Url aUrl, RequestHeaderFields aHeaderFields ) {
		return onResponse( HttpMethod.DELETE, aUrl, aHeaderFields );
	}
}
