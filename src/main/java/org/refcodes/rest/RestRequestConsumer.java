package org.refcodes.rest;

import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpServerResponse;
import org.refcodes.web.HttpStatusException;

/**
 * The {@link RestRequestConsumer} can be coded using the <code>lambda</code>
 * syntax and processes a request for a given locator and for a given
 * {@link HttpMethod}. A {@link RestRequestConsumer} is invoked with a request
 * of type {@link RestRequestEvent} and a response being of type
 * {@link HttpServerResponse}. The {@link RestRequestEvent} describes the
 * context of the request whereas the {@link HttpServerResponse} is used by the
 * <code>lambda</code> expression to prepare the response e.g. via
 * {@link HttpServerResponse#setResponse(Object)}.
 */
@FunctionalInterface
public interface RestRequestConsumer {

	/**
	 * The invoker provides a request context being a {@link RestRequestEvent})
	 * describing the request and a response being a {@link HttpServerResponse}
	 * to be processed upon by your <code>lambda</code>'s code. The method works
	 * synchronously and waits (blocks the caller's thread) till it finishes
	 * execution.
	 * 
	 * @param aRequest The request of type {@link RestRequestEvent} describing
	 *        the request context. Use
	 *        {@link RestRequestEvent#getRequest(Class)} to retrieve the
	 *        caller's request body or
	 *        {@link RestRequestEvent#getHeaderFields()} to retrieve the
	 *        request's cookies and other Header-Fields.
	 * @param aResponse The response of type {@link HttpServerResponse} do be
	 *        processed upon by the method's implementation. Use
	 *        {@link HttpServerResponse#setResponse(Object)} to provide a
	 *        response for the client. Use
	 *        {@link HttpServerResponse#getHeaderFields()} to set cookies or
	 *        modify Header-Fields. Throw one of the {@link HttpStatusException}
	 *        sub-types to signal an according erroneous HTTP state.
	 * 
	 * @throws HttpStatusException to be thrown in case something went wrong.
	 */
	void onRequest( RestRequestEvent aRequest, HttpServerResponse aResponse ) throws HttpStatusException;
}
