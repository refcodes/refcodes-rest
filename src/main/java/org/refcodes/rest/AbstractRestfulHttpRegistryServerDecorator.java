// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.component.InitializeException;
import org.refcodes.component.LifecycleMachine.ManualLifecycleMachine;
import org.refcodes.component.LifecycleStatus;
import org.refcodes.component.PauseException;
import org.refcodes.component.ResumeException;
import org.refcodes.component.StartException;
import org.refcodes.component.StopException;
import org.refcodes.data.Scheme;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.web.MediaType;
import org.refcodes.web.Url;

/**
 * Abstract class for easily decorating a {@link RestfulHttpRegistryServer}.
 *
 * @param <DESC> the generic type
 * @param <B> the generic type
 */
public abstract class AbstractRestfulHttpRegistryServerDecorator<DESC extends HttpServerDescriptor, B extends RestfulHttpRegistryServer<DESC, B>> extends AbstractRestfulHttpServerDecorator<B> implements RestfulHttpRegistryServer<DESC, B> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( AbstractRestfulHttpRegistryServerDecorator.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected ManualLifecycleMachine _lifeCycleAutomaton = new ManualLifecycleMachine();
	private Url _serviceRegistryUrl;
	private DESC _serverDescriptor;
	private TrustStoreDescriptor _storeDescriptor;
	private String _alias;
	private String _instanceId;
	private String _host;
	private String _virtualHost;
	private int[] _ipAddress;
	private String _pingPath;
	protected RestRequestConsumer _pingRequestObserver = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the given {@link RestfulHttpServer} with discovery
	 * functionality.
	 * 
	 * @param aServer The {@link RestfulHttpServer} to be decorated.
	 */
	public AbstractRestfulHttpRegistryServerDecorator( RestfulHttpServer aServer ) {
		super( aServer );
	}

	// /////////////////////////////////////////////////////////////////////////
	// LIFECYCLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isRunning() {
		return _lifeCycleAutomaton.isRunning();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isInitialized() {
		return _lifeCycleAutomaton.isInitialized();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LifecycleStatus getLifecycleStatus() {
		return _lifeCycleAutomaton.getLifecycleStatus();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void pause() throws PauseException {
		_lifeCycleAutomaton.pause();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop() throws StopException {
		_lifeCycleAutomaton.stop();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() throws StartException {
		_lifeCycleAutomaton.start();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void resume() throws ResumeException {
		_lifeCycleAutomaton.resume();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize() throws InitializeException {
		_lifeCycleAutomaton.initialize();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStoppable() {
		return _lifeCycleAutomaton.isStoppable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isPausable() {
		return _lifeCycleAutomaton.isPausable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStartable() {
		return _lifeCycleAutomaton.isStartable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isResumable() {
		return _lifeCycleAutomaton.isResumable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStopped() {
		return _lifeCycleAutomaton.isStopped();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isPaused() {
		return _lifeCycleAutomaton.isPaused();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isInitalizable() {
		return _lifeCycleAutomaton.isInitalizable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		_lifeCycleAutomaton.destroy();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isDestroyable() {
		return _lifeCycleAutomaton.isDestroyable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isDestroyed() {
		return _lifeCycleAutomaton.isDestroyed();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setInstanceId( String aInstanceId ) {
		_instanceId = aInstanceId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getInstanceId() {
		return _instanceId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHost( String aHost ) {
		_host = aHost;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getHost() {
		return _host;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getVirtualHost() {
		return _virtualHost;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setVirtualHost( String aVirtualHost ) {
		_virtualHost = aVirtualHost;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int[] getIpAddress() {
		return _ipAddress;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setIpAddress( int[] aIpAddress ) {
		_ipAddress = aIpAddress;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TrustStoreDescriptor getTrustStoreDescriptor() {
		return _storeDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTrustStoreDescriptor( TrustStoreDescriptor aTrustStoreDescriptor ) {
		_storeDescriptor = aTrustStoreDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DESC getHttpServerDescriptor() {
		return _serverDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpServerDescriptor( DESC aServerDescriptor ) {
		_serverDescriptor = aServerDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getHttpRegistryUrl() {
		return _serviceRegistryUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpRegistryUrl( Url aRegistryUrl ) {
		_serviceRegistryUrl = aRegistryUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAlias() {
		return _alias;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setAlias( String aAlias ) {
		_alias = aAlias;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getPingPath() {
		return _pingPath;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPingPath( String aPingPath ) {
		_pingPath = aPingPath;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onPingRequest( RestRequestConsumer aRequestConsumer ) {
		_pingRequestObserver = aRequestConsumer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestRequestConsumer getPingRequestObserver() {
		return _pingRequestObserver;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Resolves the property from the provided value and the this instance's
	 * property and the and sets the property in case the provided value is not
	 * null.
	 * 
	 * @param aHost The value to be used when not null.
	 * 
	 * @return The value when not null, else the value of this instance's
	 *         property.
	 * 
	 * @throws UnknownHostException thrown in case the local machine's host
	 *         cannot be resolved when the provided host is null.
	 */
	protected String toHost( String aHost ) throws UnknownHostException {
		return AbstractHttpRegistrySidecar.toHost( aHost, this );
	}

	/**
	 * Resolves the property from the provided value and the this instance's
	 * property and the and sets the property in case the provided value is not
	 * null.
	 * 
	 * @param aVirtualHost The value to be used when not null.
	 * 
	 * @return The value when not null, else the value of this instance's
	 *         property.
	 */
	protected String toVirtualHost( String aVirtualHost ) {
		return AbstractHttpRegistrySidecar.toVirtualHost( aVirtualHost, this );
	}

	/**
	 * Resolves the property from the provided value and the this instance's
	 * property and the and sets the property in case the provided value is not
	 * null.
	 * 
	 * @param aIpAddress The value to be used when not null.
	 * 
	 * @return The value when not null, else the value of this instance's
	 *         property.
	 * 
	 * @throws IOException thrown in case the local machine's IP-Address cannot
	 *         be resolved when the provided IP-Address is null.
	 */
	protected int[] toIpAddress( int[] aIpAddress ) throws IOException {
		return AbstractHttpRegistrySidecar.toIpAddress( aIpAddress, this );
	}

	/**
	 * Resolves the property from the provided value and the this instance's
	 * property and the and sets the property in case the provided value is not
	 * null.
	 * 
	 * @param aAlias The value to be used when not null.
	 * 
	 * @return The value when not null, else the value of this instance's
	 *         property.
	 */
	protected String toAlias( String aAlias ) {
		return AbstractHttpRegistrySidecar.toAlias( aAlias, this );
	}

	/**
	 * Resolves the property from the provided value and the this instance's
	 * property and the and sets the property in case the provided value is not
	 * null.
	 * 
	 * @param aScheme The value to be used when not null.
	 * 
	 * @return The value when not null, else the value of this instance's
	 *         property.
	 */
	protected Scheme toScheme( Scheme aScheme ) {
		return AbstractHttpRegistrySidecar.toScheme( aScheme, this );
	}

	/**
	 * Resolves the property from the provided value and the this instance's
	 * property and the and sets the property in case the provided value is not
	 * null.
	 * 
	 * @param aPort The value to be used when not null.
	 * 
	 * @return The value when not null, else the value of this instance's
	 *         property.
	 */
	protected int toPort( int aPort ) {
		return AbstractHttpRegistrySidecar.toPort( aPort, this );
	}

	/**
	 * Resolves the property from the provided value and the this instance's
	 * property and the and sets the property in case the provided value is not
	 * null.
	 * 
	 * @param aInstanceId The value to be used when not null.
	 * @param aHost The value to be used when instance TID is null.
	 * 
	 * @return The value when not null, else the value of this instance's
	 *         property.
	 */
	protected String toInstanceId( String aInstanceId, String aHost ) {
		return AbstractHttpRegistrySidecar.toInstanceId( aInstanceId, aHost, this );
	}

	/**
	 * Resolves the property from the provided value and the this instance's
	 * property and the and sets the property in case the provided value is not
	 * null.
	 * 
	 * @param aPingPath The value to be used when not null.
	 * @param aDefaultPingPath The value to be used when the ping path is null.
	 * 
	 * @return The value when not null, else the value of this instance's
	 *         property.
	 */
	protected String toPingPath( String aPingPath, String aDefaultPingPath ) {
		return AbstractHttpRegistrySidecar.toPingPath( aPingPath, aDefaultPingPath, this );
	}

	/**
	 * Resolves the property from the provided value and the this instance's
	 * property and the and sets the property in case the provided value is not
	 * null.
	 * 
	 * @param aStoreDescriptor The value to be used when not null.
	 * 
	 * @return The value when not null, else the value of this instance's
	 *         property.
	 */
	protected TrustStoreDescriptor toTrustStoreDescriptor( TrustStoreDescriptor aStoreDescriptor ) {
		return AbstractHttpRegistrySidecar.toTrustStoreDescriptor( aStoreDescriptor, this );
	}

	/**
	 * Resolves the property from the provided value and the this instance's
	 * property and the and sets the property in case the provided value is not
	 * null.
	 * 
	 * @param aRegistryUrl The value to be used when not null.
	 * 
	 * @return The value when not null, else the value of this instance's
	 *         property.
	 */
	protected Url toHttpRegistryUrl( Url aRegistryUrl ) {
		return AbstractHttpRegistrySidecar.toHttpRegistryUrl( aRegistryUrl, this );
	}

	/**
	 * Resolves the property from the provided value and the this instance's
	 * property and the and sets the property in case the provided value is not
	 * null.
	 * 
	 * @param aServerDescriptor The value to be used when not null.
	 * 
	 * @return The value when not null, else the value of this instance's
	 *         property.
	 */
	protected DESC toHttpServerDescriptor( DESC aServerDescriptor ) {
		return AbstractHttpRegistrySidecar.toHttpServerDescriptor( aServerDescriptor, this );
	}

	/**
	 * Resolves the property from the provided value and the this instance's
	 * property and the and sets the property in case the provided value is not
	 * null.
	 * 
	 * @param aPingRequestObserver The value to be used when not null.
	 * 
	 * @return The value when not null, else the value of this instance's
	 *         property.
	 */
	protected RestRequestConsumer toPingObserver( RestRequestConsumer aPingRequestObserver ) {
		if ( aPingRequestObserver == null ) {
			aPingRequestObserver = _pingRequestObserver;
			if ( aPingRequestObserver == null ) {
				aPingRequestObserver = ( aReq, aResp ) -> { aResp.getHeaderFields().putContentType( MediaType.TEXT_PLAIN ); aResp.setResponse( "Pong!" ); LOGGER.log( Level.INFO, "Received a PING request, no PING handler defined, using default handler!" ); };
				_pingRequestObserver = aPingRequestObserver;
			}
		}
		return aPingRequestObserver;
	}
}
