// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.component.ConnectionStatus;
import org.refcodes.controlflow.RetryCounter;
import org.refcodes.data.IoRetryCount;
import org.refcodes.data.IoTimeout;
import org.refcodes.data.RecoverySleepTime;
import org.refcodes.web.BasicAuthCredentials;
import org.refcodes.web.FormFields;
import org.refcodes.web.GrantType;
import org.refcodes.web.HttpBodyMap;
import org.refcodes.web.HttpStatusException;
import org.refcodes.web.MediaType;
import org.refcodes.web.OauthField;
import org.refcodes.web.OauthToken;
import org.refcodes.web.Url;

/**
 * Self refreshing implementation of the {@link OauthToken}. In case a refresh
 * token (as of {@link #getRefreshToken()} and has been provided, then the
 * access token (as of {@link #getAccessToken()} is refreshed within the
 * "expires in" time (as of {@link #getExpiresIn()}. The refresh daemon
 * terminates and this instance is disposed when the provided
 * {@link RestfulHttpClient}'s {@link RestfulHttpClient#close()} method is
 * called or the {@link #dispose()} method is invoked.
 */
public class OauthTokenHandler extends OauthToken {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( OauthTokenHandler.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final double DELAY_COEFFICIENT = 0.99;
	private static final int DEFAULT_EXPIRES_IN_SECONDS = 60 * 3;
	private static final int TOKEN_REFRESH_RETRIES = IoRetryCount.MIN.getValue();
	private static final long TOKEN_REFRESH_RETRY_WAIT_TIME_MILLIS = IoTimeout.MIN.getTimeMillis();
	private static final long SERVER_RECOVERY_WAIT_TIME_MILLIS = RecoverySleepTime.NORM.getTimeMillis();

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Timer _timer = null;
	private String _url;
	private RestfulHttpClient _restClient;
	private RetryCounter _tokenRefreshRetryCounter;
	private long _serverRecoveryWaitTimeMillis;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( Url aUrl, String aClientId, String aClientSecret, String aUserName, String aUserPassword ) throws HttpStatusException, IOException {
		this( aUrl.toHttpUrl(), new HttpRestClient().withOpen(), aClientId, aClientSecret, aUserName, aUserPassword, (GrantType) null );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 */
	public OauthTokenHandler( Url aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword ) throws HttpStatusException, MalformedURLException {
		this( aUrl.toHttpUrl(), aHttpRestClient, aClientId, aClientSecret, aUserName, aUserPassword, (GrantType) null );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * 
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 */
	public OauthTokenHandler( Url aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword, String aGrantType ) throws HttpStatusException, MalformedURLException {
		this( aUrl.toHttpUrl(), aHttpRestClient, aClientId, aClientSecret, aUserName, aUserPassword, aGrantType );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 */
	public OauthTokenHandler( Url aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword, GrantType aGrantType ) throws HttpStatusException, MalformedURLException {
		this( aUrl.toHttpUrl(), aHttpRestClient, aClientId, aClientSecret, aUserName, aUserPassword, aGrantType );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( String aUrl, String aClientId, String aClientSecret, String aUserName, String aUserPassword ) throws HttpStatusException, IOException {
		this( aUrl, new HttpRestClient().withOpen(), aClientId, aClientSecret, aUserName, aUserPassword, (GrantType) null );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( String aUrl, String aClientId, String aClientSecret, String aUserName, String aUserPassword, String aGrantType ) throws HttpStatusException, IOException {
		this( aUrl, new HttpRestClient().withOpen(), aClientId, aClientSecret, aUserName, aUserPassword, GrantType.fromName( aGrantType ) );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * 
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( String aUrl, String aClientId, String aClientSecret, String aUserName, String aUserPassword, GrantType aGrantType ) throws HttpStatusException, IOException {
		this( aUrl, new HttpRestClient().withOpen(), aClientId, aClientSecret, aUserName, aUserPassword, aGrantType );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 */
	public OauthTokenHandler( String aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword ) throws HttpStatusException, MalformedURLException {
		this( aUrl, aHttpRestClient, aClientId, aClientSecret, aUserName, aUserPassword, (GrantType) null );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aClientId The client's TID for "password" grant type.
	 * 
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 */
	public OauthTokenHandler( String aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword, String aGrantType ) throws HttpStatusException, MalformedURLException {
		this( aUrl, aHttpRestClient, toOauthToken( aUrl, aHttpRestClient, aClientId, aClientSecret, aUserName, aUserPassword, null, aGrantType ) );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 */
	public OauthTokenHandler( String aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword, GrantType aGrantType ) throws HttpStatusException, MalformedURLException {
		this( aUrl, aHttpRestClient, toOauthToken( aUrl, aHttpRestClient, aClientId, aClientSecret, aUserName, aUserPassword, aGrantType, null ) );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters. For the refresh token parameters, please refer to
	 * "https://www.oauth.com/oauth2-servers/access-tokens/refreshing-access-tokens".
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aAccessToken The access token to be stored (as of
	 *        {@link #getAccessToken()}).
	 * @param aRefreshToken The refresh token to be used (as of
	 *        {@link #getRefreshToken()}).
	 * @param aTokenType The token type to be stored (as of
	 *        {@link #getTokenType()}).
	 * @param aExpiresIn The "expires in" time to be stored (as of
	 *        {@link #getExpiresIn()}).
	 * @param aScope The scope to be stored (as of {@link #getScope()}).
	 * 
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( String aUrl, String aAccessToken, String aRefreshToken, String aTokenType, Integer aExpiresIn, String aScope ) throws IOException {
		super( aAccessToken, aRefreshToken, aTokenType, aExpiresIn, aScope );
		initialize( aUrl, new HttpRestClient().withOpen(), new RetryCounter( TOKEN_REFRESH_RETRIES, TOKEN_REFRESH_RETRY_WAIT_TIME_MILLIS ), SERVER_RECOVERY_WAIT_TIME_MILLIS );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aAccessToken The access token to be stored (as of
	 *        {@link #getAccessToken()}).
	 * @param aRefreshToken The refresh token to be used (as of
	 *        {@link #getRefreshToken()}).
	 * @param aTokenType The token type to be stored (as of
	 *        {@link #getTokenType()}).
	 * @param aExpiresIn The "expires in" time to be stored (as of
	 *        {@link #getExpiresIn()}).
	 * @param aScope The scope to be stored (as of {@link #getScope()}).
	 * 
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( Url aUrl, String aAccessToken, String aRefreshToken, String aTokenType, Integer aExpiresIn, String aScope ) throws IOException {
		super( aAccessToken, aRefreshToken, aTokenType, aExpiresIn, aScope );
		initialize( aUrl.toHttpUrl(), new HttpRestClient().withOpen(), new RetryCounter( TOKEN_REFRESH_RETRIES, TOKEN_REFRESH_RETRY_WAIT_TIME_MILLIS ), SERVER_RECOVERY_WAIT_TIME_MILLIS );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aOauthToken The {@link HttpBodyMap} containing the OAuth token
	 *        with the required information.
	 * 
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( Url aUrl, HttpBodyMap aOauthToken ) throws IOException {
		super( aOauthToken );
		initialize( aUrl.toHttpUrl(), new HttpRestClient().withOpen(), new RetryCounter( TOKEN_REFRESH_RETRIES, TOKEN_REFRESH_RETRY_WAIT_TIME_MILLIS ), SERVER_RECOVERY_WAIT_TIME_MILLIS );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aOauthToken The {@link OauthToken} containing the required
	 *        information.
	 * 
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( Url aUrl, OauthToken aOauthToken ) throws IOException {
		super( aOauthToken );
		initialize( aUrl.toHttpUrl(), new HttpRestClient().withOpen(), new RetryCounter( TOKEN_REFRESH_RETRIES, TOKEN_REFRESH_RETRY_WAIT_TIME_MILLIS ), SERVER_RECOVERY_WAIT_TIME_MILLIS );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters. For the refresh token parameters, please refer to
	 * "https://www.oauth.com/oauth2-servers/access-tokens/refreshing-access-tokens".
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aOauthToken The {@link OauthToken} containing the required
	 *        information.
	 * 
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( String aUrl, OauthToken aOauthToken ) throws IOException {
		super( aOauthToken );
		initialize( aUrl, new HttpRestClient().withOpen(), new RetryCounter( TOKEN_REFRESH_RETRIES, TOKEN_REFRESH_RETRY_WAIT_TIME_MILLIS ), SERVER_RECOVERY_WAIT_TIME_MILLIS );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aAccessToken The access token to be stored (as of
	 *        {@link #getAccessToken()}).
	 * @param aRefreshToken The refresh token to be used (as of
	 *        {@link #getRefreshToken()}).
	 * @param aTokenType The token type to be stored (as of
	 *        {@link #getTokenType()}).
	 * @param aExpiresIn The "expires in" time to be stored (as of
	 *        {@link #getExpiresIn()}).
	 * @param aScope The scope to be stored (as of {@link #getScope()}).
	 */
	public OauthTokenHandler( Url aUrl, RestfulHttpClient aHttpRestClient, String aAccessToken, String aRefreshToken, String aTokenType, Integer aExpiresIn, String aScope ) {
		super( aAccessToken, aRefreshToken, aTokenType, aExpiresIn, aScope );
		initialize( aUrl.toHttpUrl(), aHttpRestClient, new RetryCounter( TOKEN_REFRESH_RETRIES, TOKEN_REFRESH_RETRY_WAIT_TIME_MILLIS ), SERVER_RECOVERY_WAIT_TIME_MILLIS );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aOauthToken The {@link HttpBodyMap} containing the OAuth token
	 *        with the required information.
	 */
	public OauthTokenHandler( Url aUrl, RestfulHttpClient aHttpRestClient, HttpBodyMap aOauthToken ) {
		super( aOauthToken );
		initialize( aUrl.toHttpUrl(), aHttpRestClient, new RetryCounter( TOKEN_REFRESH_RETRIES, TOKEN_REFRESH_RETRY_WAIT_TIME_MILLIS ), SERVER_RECOVERY_WAIT_TIME_MILLIS );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aOauthToken The {@link OauthToken} containing the required
	 *        information.
	 */
	public OauthTokenHandler( String aUrl, RestfulHttpClient aHttpRestClient, OauthToken aOauthToken ) {
		super( aOauthToken );
		initialize( aUrl, aHttpRestClient, new RetryCounter( TOKEN_REFRESH_RETRIES, TOKEN_REFRESH_RETRY_WAIT_TIME_MILLIS ), SERVER_RECOVERY_WAIT_TIME_MILLIS );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aOauthToken The {@link OauthToken} containing the required
	 *        information.
	 */
	public OauthTokenHandler( Url aUrl, RestfulHttpClient aHttpRestClient, OauthToken aOauthToken ) {
		super( aOauthToken );
		initialize( aUrl.toHttpUrl(), aHttpRestClient, new RetryCounter( TOKEN_REFRESH_RETRIES, TOKEN_REFRESH_RETRY_WAIT_TIME_MILLIS ), SERVER_RECOVERY_WAIT_TIME_MILLIS );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters. For the refresh token parameters, please refer to
	 * "https://www.oauth.com/oauth2-servers/access-tokens/refreshing-access-tokens".
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aAccessToken The access token to be stored (as of
	 *        {@link #getAccessToken()}).
	 * @param aRefreshToken The refresh token to be used (as of
	 *        {@link #getRefreshToken()}).
	 * @param aTokenType The token type to be stored (as of
	 *        {@link #getTokenType()}).
	 * @param aExpiresIn The "expires in" time to be stored (as of
	 *        {@link #getExpiresIn()}).
	 * @param aScope The scope to be stored (as of {@link #getScope()}).
	 */
	public OauthTokenHandler( String aUrl, RestfulHttpClient aHttpRestClient, String aAccessToken, String aRefreshToken, String aTokenType, Integer aExpiresIn, String aScope ) {
		super( aAccessToken, aRefreshToken, aTokenType, aExpiresIn, aScope );
		initialize( aUrl, aHttpRestClient, new RetryCounter( TOKEN_REFRESH_RETRIES, TOKEN_REFRESH_RETRY_WAIT_TIME_MILLIS ), SERVER_RECOVERY_WAIT_TIME_MILLIS );
	}

	// -------------------------------------------------------------------------

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( Url aUrl, String aClientId, String aClientSecret, String aUserName, String aUserPassword, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, IOException {
		this( aUrl.toHttpUrl(), new HttpRestClient().withOpen(), aClientId, aClientSecret, aUserName, aUserPassword, (GrantType) null, new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 */
	public OauthTokenHandler( Url aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, MalformedURLException {
		this( aUrl.toHttpUrl(), aHttpRestClient, aClientId, aClientSecret, aUserName, aUserPassword, (GrantType) null, new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * 
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use.
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 */
	public OauthTokenHandler( Url aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword, String aGrantType, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, MalformedURLException {
		this( aUrl.toHttpUrl(), aHttpRestClient, aClientId, aClientSecret, aUserName, aUserPassword, aGrantType, new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use.
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 */
	public OauthTokenHandler( Url aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword, GrantType aGrantType, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, MalformedURLException {
		this( aUrl.toHttpUrl(), aHttpRestClient, aClientId, aClientSecret, aUserName, aUserPassword, aGrantType, new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( String aUrl, String aClientId, String aClientSecret, String aUserName, String aUserPassword, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, IOException {
		this( aUrl, new HttpRestClient().withOpen(), aClientId, aClientSecret, aUserName, aUserPassword, (GrantType) null, new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use.
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( String aUrl, String aClientId, String aClientSecret, String aUserName, String aUserPassword, String aGrantType, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, IOException {
		this( aUrl, new HttpRestClient().withOpen(), aClientId, aClientSecret, aUserName, aUserPassword, GrantType.fromName( aGrantType ), new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * 
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use.
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( String aUrl, String aClientId, String aClientSecret, String aUserName, String aUserPassword, GrantType aGrantType, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, IOException {
		this( aUrl, new HttpRestClient().withOpen(), aClientId, aClientSecret, aUserName, aUserPassword, aGrantType, new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 */
	public OauthTokenHandler( String aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, MalformedURLException {
		this( aUrl, aHttpRestClient, aClientId, aClientSecret, aUserName, aUserPassword, (GrantType) null, new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aClientId The client's TID for "password" grant type.
	 * 
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use.
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 */
	public OauthTokenHandler( String aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword, String aGrantType, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, MalformedURLException {
		this( aUrl, aHttpRestClient, toOauthToken( aUrl, aHttpRestClient, aClientId, aClientSecret, aUserName, aUserPassword, null, aGrantType ), new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use.
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 */
	public OauthTokenHandler( String aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword, GrantType aGrantType, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, MalformedURLException {
		this( aUrl, aHttpRestClient, toOauthToken( aUrl, aHttpRestClient, aClientId, aClientSecret, aUserName, aUserPassword, aGrantType, null ), new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters. For the refresh token parameters, please refer to
	 * "https://www.oauth.com/oauth2-servers/access-tokens/refreshing-access-tokens".
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aAccessToken The access token to be stored (as of
	 *        {@link #getAccessToken()}).
	 * @param aRefreshToken The refresh token to be used (as of
	 *        {@link #getRefreshToken()}).
	 * @param aTokenType The token type to be stored (as of
	 *        {@link #getTokenType()}).
	 * @param aExpiresIn The "expires in" time to be stored (as of
	 *        {@link #getExpiresIn()}).
	 * @param aScope The scope to be stored (as of {@link #getScope()}).
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( String aUrl, String aAccessToken, String aRefreshToken, String aTokenType, Integer aExpiresIn, String aScope, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) throws IOException {
		super( aAccessToken, aRefreshToken, aTokenType, aExpiresIn, aScope );
		initialize( aUrl, new HttpRestClient().withOpen(), new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aAccessToken The access token to be stored (as of
	 *        {@link #getAccessToken()}).
	 * @param aRefreshToken The refresh token to be used (as of
	 *        {@link #getRefreshToken()}).
	 * @param aTokenType The token type to be stored (as of
	 *        {@link #getTokenType()}).
	 * @param aExpiresIn The "expires in" time to be stored (as of
	 *        {@link #getExpiresIn()}).
	 * @param aScope The scope to be stored (as of {@link #getScope()}).
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( Url aUrl, String aAccessToken, String aRefreshToken, String aTokenType, Integer aExpiresIn, String aScope, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) throws IOException {
		super( aAccessToken, aRefreshToken, aTokenType, aExpiresIn, aScope );
		initialize( aUrl.toHttpUrl(), new HttpRestClient().withOpen(), new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aOauthToken The {@link HttpBodyMap} containing the OAuth token
	 *        with the required information.
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( Url aUrl, HttpBodyMap aOauthToken, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) throws IOException {
		super( aOauthToken );
		initialize( aUrl.toHttpUrl(), new HttpRestClient().withOpen(), new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aOauthToken The {@link OauthToken} containing the required
	 *        information.
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( Url aUrl, OauthToken aOauthToken, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) throws IOException {
		super( aOauthToken );
		initialize( aUrl.toHttpUrl(), new HttpRestClient().withOpen(), new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters. For the refresh token parameters, please refer to
	 * "https://www.oauth.com/oauth2-servers/access-tokens/refreshing-access-tokens".
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aOauthToken The {@link OauthToken} containing the required
	 *        information.
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( String aUrl, OauthToken aOauthToken, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) throws IOException {
		super( aOauthToken );
		initialize( aUrl, new HttpRestClient().withOpen(), new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aAccessToken The access token to be stored (as of
	 *        {@link #getAccessToken()}).
	 * @param aRefreshToken The refresh token to be used (as of
	 *        {@link #getRefreshToken()}).
	 * @param aTokenType The token type to be stored (as of
	 *        {@link #getTokenType()}).
	 * @param aExpiresIn The "expires in" time to be stored (as of
	 *        {@link #getExpiresIn()}).
	 * @param aScope The scope to be stored (as of {@link #getScope()}).
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 */
	public OauthTokenHandler( Url aUrl, RestfulHttpClient aHttpRestClient, String aAccessToken, String aRefreshToken, String aTokenType, Integer aExpiresIn, String aScope, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) {
		super( aAccessToken, aRefreshToken, aTokenType, aExpiresIn, aScope );
		initialize( aUrl.toHttpUrl(), aHttpRestClient, new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aOauthToken The {@link HttpBodyMap} containing the OAuth token
	 *        with the required information.
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 */
	public OauthTokenHandler( Url aUrl, RestfulHttpClient aHttpRestClient, HttpBodyMap aOauthToken, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) {
		super( aOauthToken );
		initialize( aUrl.toHttpUrl(), aHttpRestClient, new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aOauthToken The {@link OauthToken} containing the required
	 *        information.
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 */
	public OauthTokenHandler( String aUrl, RestfulHttpClient aHttpRestClient, OauthToken aOauthToken, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) {
		super( aOauthToken );
		initialize( aUrl, aHttpRestClient, new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aOauthToken The {@link OauthToken} containing the required
	 *        information.
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 */
	public OauthTokenHandler( Url aUrl, RestfulHttpClient aHttpRestClient, OauthToken aOauthToken, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) {
		super( aOauthToken );
		initialize( aUrl.toHttpUrl(), aHttpRestClient, new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters. For the refresh token parameters, please refer to
	 * "https://www.oauth.com/oauth2-servers/access-tokens/refreshing-access-tokens".
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aAccessToken The access token to be stored (as of
	 *        {@link #getAccessToken()}).
	 * @param aRefreshToken The refresh token to be used (as of
	 *        {@link #getRefreshToken()}).
	 * @param aTokenType The token type to be stored (as of
	 *        {@link #getTokenType()}).
	 * @param aExpiresIn The "expires in" time to be stored (as of
	 *        {@link #getExpiresIn()}).
	 * @param aScope The scope to be stored (as of {@link #getScope()}).
	 * @param aTokenRefreshRetries The number of retries when refreshing a token
	 *        fails.
	 * @param aTokenRefreshRetryWaitTimeMillis The time to wait between each
	 *        retry when refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 */
	public OauthTokenHandler( String aUrl, RestfulHttpClient aHttpRestClient, String aAccessToken, String aRefreshToken, String aTokenType, Integer aExpiresIn, String aScope, int aTokenRefreshRetries, long aTokenRefreshRetryWaitTimeMillis, long aServerRecoveryWaitTimeMillis ) {
		super( aAccessToken, aRefreshToken, aTokenType, aExpiresIn, aScope );
		initialize( aUrl, aHttpRestClient, new RetryCounter( aTokenRefreshRetries, aTokenRefreshRetryWaitTimeMillis ), aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( Url aUrl, String aClientId, String aClientSecret, String aUserName, String aUserPassword, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, IOException {
		this( aUrl.toHttpUrl(), new HttpRestClient().withOpen(), aClientId, aClientSecret, aUserName, aUserPassword, (GrantType) null, aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 */
	public OauthTokenHandler( Url aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, MalformedURLException {
		this( aUrl.toHttpUrl(), aHttpRestClient, aClientId, aClientSecret, aUserName, aUserPassword, (GrantType) null, aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * 
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use.
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 */
	public OauthTokenHandler( Url aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword, String aGrantType, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, MalformedURLException {
		this( aUrl.toHttpUrl(), aHttpRestClient, aClientId, aClientSecret, aUserName, aUserPassword, aGrantType, aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use.
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 */
	public OauthTokenHandler( Url aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword, GrantType aGrantType, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, MalformedURLException {
		this( aUrl.toHttpUrl(), aHttpRestClient, aClientId, aClientSecret, aUserName, aUserPassword, aGrantType, aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( String aUrl, String aClientId, String aClientSecret, String aUserName, String aUserPassword, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, IOException {
		this( aUrl, new HttpRestClient().withOpen(), aClientId, aClientSecret, aUserName, aUserPassword, (GrantType) null, aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use.
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( String aUrl, String aClientId, String aClientSecret, String aUserName, String aUserPassword, String aGrantType, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, IOException {
		this( aUrl, new HttpRestClient().withOpen(), aClientId, aClientSecret, aUserName, aUserPassword, GrantType.fromName( aGrantType ), aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * 
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use.
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( String aUrl, String aClientId, String aClientSecret, String aUserName, String aUserPassword, GrantType aGrantType, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, IOException {
		this( aUrl, new HttpRestClient().withOpen(), aClientId, aClientSecret, aUserName, aUserPassword, aGrantType, aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 */
	public OauthTokenHandler( String aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, MalformedURLException {
		this( aUrl, aHttpRestClient, aClientId, aClientSecret, aUserName, aUserPassword, (GrantType) null, aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aClientId The client's TID for "password" grant type.
	 * 
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use.
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 */
	public OauthTokenHandler( String aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword, String aGrantType, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, MalformedURLException {
		this( aUrl, aHttpRestClient, toOauthToken( aUrl, aHttpRestClient, aClientId, aClientSecret, aUserName, aUserPassword, null, aGrantType ), aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters by using the user's name and password for a "password" grant
	 * type authentication to retrieve a {@link OauthToken}.
	 *
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aClientId The client's TID for "password" grant type.
	 * @param aClientSecret The client's secret for "password" grant type.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use.
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 */
	public OauthTokenHandler( String aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword, GrantType aGrantType, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) throws HttpStatusException, MalformedURLException {
		this( aUrl, aHttpRestClient, toOauthToken( aUrl, aHttpRestClient, aClientId, aClientSecret, aUserName, aUserPassword, aGrantType, null ), aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters. For the refresh token parameters, please refer to
	 * "https://www.oauth.com/oauth2-servers/access-tokens/refreshing-access-tokens".
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aAccessToken The access token to be stored (as of
	 *        {@link #getAccessToken()}).
	 * @param aRefreshToken The refresh token to be used (as of
	 *        {@link #getRefreshToken()}).
	 * @param aTokenType The token type to be stored (as of
	 *        {@link #getTokenType()}).
	 * @param aExpiresIn The "expires in" time to be stored (as of
	 *        {@link #getExpiresIn()}).
	 * @param aScope The scope to be stored (as of {@link #getScope()}).
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( String aUrl, String aAccessToken, String aRefreshToken, String aTokenType, Integer aExpiresIn, String aScope, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) throws IOException {
		super( aAccessToken, aRefreshToken, aTokenType, aExpiresIn, aScope );
		initialize( aUrl, new HttpRestClient().withOpen(), aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aAccessToken The access token to be stored (as of
	 *        {@link #getAccessToken()}).
	 * @param aRefreshToken The refresh token to be used (as of
	 *        {@link #getRefreshToken()}).
	 * @param aTokenType The token type to be stored (as of
	 *        {@link #getTokenType()}).
	 * @param aExpiresIn The "expires in" time to be stored (as of
	 *        {@link #getExpiresIn()}).
	 * @param aScope The scope to be stored (as of {@link #getScope()}).
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( Url aUrl, String aAccessToken, String aRefreshToken, String aTokenType, Integer aExpiresIn, String aScope, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) throws IOException {
		super( aAccessToken, aRefreshToken, aTokenType, aExpiresIn, aScope );
		initialize( aUrl.toHttpUrl(), new HttpRestClient().withOpen(), aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aOauthToken The {@link HttpBodyMap} containing the OAuth token
	 *        with the required information.
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( Url aUrl, HttpBodyMap aOauthToken, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) throws IOException {
		super( aOauthToken );
		initialize( aUrl.toHttpUrl(), new HttpRestClient().withOpen(), aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aOauthToken The {@link OauthToken} containing the required
	 *        information.
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( Url aUrl, OauthToken aOauthToken, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) throws IOException {
		super( aOauthToken );
		initialize( aUrl.toHttpUrl(), new HttpRestClient().withOpen(), aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters. For the refresh token parameters, please refer to
	 * "https://www.oauth.com/oauth2-servers/access-tokens/refreshing-access-tokens".
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aOauthToken The {@link OauthToken} containing the required
	 *        information.
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 * 
	 * @throws IOException in case connecting the the token endpoint failed due
	 *         to I/O problems or malformed ULR issues.
	 */
	public OauthTokenHandler( String aUrl, OauthToken aOauthToken, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) throws IOException {
		super( aOauthToken );
		initialize( aUrl, new HttpRestClient().withOpen(), aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aAccessToken The access token to be stored (as of
	 *        {@link #getAccessToken()}).
	 * @param aRefreshToken The refresh token to be used (as of
	 *        {@link #getRefreshToken()}).
	 * @param aTokenType The token type to be stored (as of
	 *        {@link #getTokenType()}).
	 * @param aExpiresIn The "expires in" time to be stored (as of
	 *        {@link #getExpiresIn()}).
	 * @param aScope The scope to be stored (as of {@link #getScope()}).
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 */
	public OauthTokenHandler( Url aUrl, RestfulHttpClient aHttpRestClient, String aAccessToken, String aRefreshToken, String aTokenType, Integer aExpiresIn, String aScope, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) {
		super( aAccessToken, aRefreshToken, aTokenType, aExpiresIn, aScope );
		initialize( aUrl.toHttpUrl(), aHttpRestClient, aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aOauthToken The {@link HttpBodyMap} containing the OAuth token
	 *        with the required information.
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 */
	public OauthTokenHandler( Url aUrl, RestfulHttpClient aHttpRestClient, HttpBodyMap aOauthToken, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) {
		super( aOauthToken );
		initialize( aUrl.toHttpUrl(), aHttpRestClient, aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The URL to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aOauthToken The {@link OauthToken} containing the required
	 *        information.
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 */
	public OauthTokenHandler( String aUrl, RestfulHttpClient aHttpRestClient, OauthToken aOauthToken, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) {
		super( aOauthToken );
		initialize( aUrl, aHttpRestClient, aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aOauthToken The {@link OauthToken} containing the required
	 *        information.
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 */
	public OauthTokenHandler( Url aUrl, RestfulHttpClient aHttpRestClient, OauthToken aOauthToken, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) {
		super( aOauthToken );
		initialize( aUrl.toHttpUrl(), aHttpRestClient, aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	/**
	 * Constructs an instance of the {@link OauthTokenHandler} with the given
	 * parameters. For the refresh token parameters, please refer to
	 * "https://www.oauth.com/oauth2-servers/access-tokens/refreshing-access-tokens".
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aAccessToken The access token to be stored (as of
	 *        {@link #getAccessToken()}).
	 * @param aRefreshToken The refresh token to be used (as of
	 *        {@link #getRefreshToken()}).
	 * @param aTokenType The token type to be stored (as of
	 *        {@link #getTokenType()}).
	 * @param aExpiresIn The "expires in" time to be stored (as of
	 *        {@link #getExpiresIn()}).
	 * @param aScope The scope to be stored (as of {@link #getScope()}).
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 */
	public OauthTokenHandler( String aUrl, RestfulHttpClient aHttpRestClient, String aAccessToken, String aRefreshToken, String aTokenType, Integer aExpiresIn, String aScope, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) {
		super( aAccessToken, aRefreshToken, aTokenType, aExpiresIn, aScope );
		initialize( aUrl, aHttpRestClient, aTokenRefreshRetryCounter, aServerRecoveryWaitTimeMillis );
	}

	// -------------------------------------------------------------------------

	// -------------------------------------------------------------------------

	/**
	 * Called by the according the CTORs.
	 * 
	 * @param aUrl The {@link Url} to be used when refreshing the access token.
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use when
	 *        refreshing the token.
	 * @param aTokenRefreshRetryCounter The {@link RetryCounter} taking care of
	 *        the number of retries and the time to wait between each retry when
	 *        refreshing a token fails.
	 * @param aServerRecoveryWaitTimeMillis The time to wait in case the count
	 *        of the token refresh retries is exceeded.
	 */
	private void initialize( String aUrl, RestfulHttpClient aHttpRestClient, RetryCounter aTokenRefreshRetryCounter, long aServerRecoveryWaitTimeMillis ) {
		if ( aHttpRestClient != null && !aHttpRestClient.isOpened() ) {
			LOGGER.log( Level.WARNING, "The provided <" + aHttpRestClient.getClass().getSimpleName() + "> being in status <" + aHttpRestClient.getConnectionStatus() + "> must be in status <" + ConnectionStatus.OPENED + "> when provided for the token refresh loop to get set up!" );
		}
		if ( _refreshToken != null ) {
			Integer theExpiresIn = _expiresIn;
			if ( theExpiresIn == null || theExpiresIn == -1 ) {
				theExpiresIn = DEFAULT_EXPIRES_IN_SECONDS;
			}
			_url = aUrl;
			_restClient = aHttpRestClient;
			_timer = new Timer( true );
			_timer.schedule( new RefreshTask(), toDelayMillis( theExpiresIn ) );
		}
		_tokenRefreshRetryCounter = aTokenRefreshRetryCounter != null ? aTokenRefreshRetryCounter : new RetryCounter( TOKEN_REFRESH_RETRIES, TOKEN_REFRESH_RETRY_WAIT_TIME_MILLIS );
		_serverRecoveryWaitTimeMillis = aServerRecoveryWaitTimeMillis > 0 ? aServerRecoveryWaitTimeMillis : SERVER_RECOVERY_WAIT_TIME_MILLIS;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void dispose() {
		if ( _timer != null ) {
			_timer.cancel();
		}
		super.dispose();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * We wait for about {@link #DELAY_COEFFICIENT} of the expiration time to be
	 * inside the refresh time-slot.
	 * 
	 * @param aExpiresInSecs The expiration time in seconds.
	 * 
	 * @return The calculated expiration time in milliseconds.
	 */
	private long toDelayMillis( Integer aExpiresInSecs ) {
		final Double theTimeout = ( (double) aExpiresInSecs ) * ( (double) 1000 ) * DELAY_COEFFICIENT;
		final long theDelay = theTimeout.longValue();
		return theDelay;
	}

	/**
	 * Retrieves an {@link OauthToken} by applying the grant type "password" to
	 * the given URL.
	 * 
	 * @param aUrl The URL from which to get the {@link OauthToken}
	 * @param aHttpRestClient The {@link RestfulHttpClient} to use for querying
	 *        the {@link OauthToken}.
	 * @param aUserName The user's name for "password" grant type.
	 * @param aUserPassword The user's password for "password" grant type.
	 * @param aGrantType The {@link GrantType} to use or null for using trying
	 *        to determine the {@link GrantType} automatically.
	 * @param aCustomGrant The custom grant in case there is none such grant in
	 *        the {@link GrantType} enumeration.
	 * 
	 * @return The {@link OauthToken}.
	 * 
	 * @throws MalformedURLException thrown in case of a malformed URL.
	 * @throws HttpStatusException thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	private static OauthToken toOauthToken( String aUrl, RestfulHttpClient aHttpRestClient, String aClientId, String aClientSecret, String aUserName, String aUserPassword, GrantType aGrantType, String aCustomGrant ) throws MalformedURLException, HttpStatusException {
		final RestRequestBuilder theRequestBuilder = aHttpRestClient.buildPost( aUrl );
		theRequestBuilder.getHeaderFields().putAcceptTypes( MediaType.APPLICATION_JSON );
		final String theGrantType = aGrantType != null ? aGrantType.getValue() : aCustomGrant;
		// |--> Client-ID + Client-Secret = Form-Fields:
		if ( aClientId != null && aClientId.length() != 0 ) {
			final FormFields theFormFields = new FormFields();
			theFormFields.put( OauthField.CLIENT_ID.getName(), aClientId );
			if ( aClientSecret != null && aClientSecret.length() != 0 ) {
				theFormFields.put( OauthField.CLIENT_SECRET.getName(), aClientSecret );
			}
			if ( aUserName != null && aUserName.length() != 0 ) {
				theFormFields.put( OauthField.USER_NAME.getName(), aUserName );
			}
			if ( aUserPassword != null && aUserPassword.length() != 0 ) {
				theFormFields.put( OauthField.PASSWORD.getName(), aUserPassword );
			}
			theFormFields.put( OauthField.GRANT_TYPE.getName(), theGrantType != null ? theGrantType : GrantType.PASSWORD.getValue() );
			theRequestBuilder.setRequest( theFormFields );
		}
		// Client-ID + Client-Secret: Form-Fields <--|
		// |--> Otherwise = Basic-Auth in the HTTP-Header:
		else {
			if ( theGrantType != null ) {
				final FormFields theFormFields = new FormFields();
				theFormFields.put( theGrantType );
				theRequestBuilder.setRequest( theFormFields );
			}
			final BasicAuthCredentials theCredentials = new BasicAuthCredentials( aUserName, aUserPassword );
			theRequestBuilder.getHeaderFields().putBasicAuthCredentials( theCredentials );
		}
		// Otherwise = Basic-Auth in the HTTP-Header <--|
		final RestResponse theResponse = theRequestBuilder.toRestResponse();
		if ( theResponse.getHttpStatusCode().isErrorStatus() ) {
			throw theResponse.getHttpStatusCode().toHttpStatusException( theResponse.getHttpBody() );
		}
		final OauthToken theToken = new OauthToken( theResponse.getResponse() );
		return theToken;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@link TimerTask} for refreshing the access token.
	 */
	private class RefreshTask extends TimerTask {

		@Override
		public void run() {
			if ( !_isDisposed ) {
				synchronized ( OauthTokenHandler.this ) {
					// Client not opened:
					if ( !_restClient.isOpened() ) {
						LOGGER.log( Level.WARNING, "The rest client, being in status <" + _restClient.getConnectionStatus() + ">, must be in status <" + ConnectionStatus.OPENED + "> (waiting for <" + _serverRecoveryWaitTimeMillis + "> milliseconds before next retry)!" );
						_timer.schedule( new RefreshTask(), _serverRecoveryWaitTimeMillis ); // Give the application a chance to open the connection!
					}
					// No refresh token available:
					else if ( _refreshToken == null ) {
						LOGGER.log( Level.WARNING, "No refresh token available, aborting refresh timer!" );
						if ( _timer != null ) {
							_timer.cancel(); // Will never get new token again: Stop daemon!
						}
						return;
					}
					// Get the token:
					else {
						Exception theException = null;
						final String theUrl = _url;
						_tokenRefreshRetryCounter.restart();
						while ( _tokenRefreshRetryCounter.nextRetry() ) {
							try {
								final RestRequestBuilder theBuilder = _restClient.buildPost( theUrl );
								theBuilder.getHeaderFields().putAcceptTypes( MediaType.APPLICATION_JSON );
								theBuilder.getHeaderFields().putContentType( MediaType.APPLICATION_X_WWW_FORM_URLENCODED );
								theBuilder.setRedirectDepth( 3 );
								final FormFields theFormFields = new FormFields();
								theFormFields.put( OauthField.GRANT_TYPE.getName(), GrantType.REFRESH_TOKEN.getValue() );
								theFormFields.put( OauthField.REFRESH_TOKEN.getName(), getRefreshToken() );
								theBuilder.setRequest( theFormFields );
								if ( getScope() != null ) {
									theFormFields.put( OauthField.SCOPE.getName(), getScope() );
								}
								final RestResponse theResponse = theBuilder.toRestResponse();
								// Success:
								if ( theResponse.getHttpStatusCode().isSuccessStatus() ) {
									final HttpBodyMap theBody = theResponse.getResponse();
									// Copy token properties:
									updateToken( theBody );
									// Set scheduler:
									Integer theExpiresInSeconds = _expiresIn;
									if ( theExpiresInSeconds == null || theExpiresInSeconds == -1 ) {
										theExpiresInSeconds = DEFAULT_EXPIRES_IN_SECONDS;
									}
									_timer.schedule( new RefreshTask(), toDelayMillis( theExpiresInSeconds ) );
									return;
								}
								// Client error:
								else if ( theResponse.getHttpStatusCode().isClientErrorStatus() ) {
									LOGGER.log( Level.WARNING, "Client can not refresh tokens, aborting refresh timer as of client error <" + theResponse.getHttpStatusCode().getStatusCode() + "> (" + theResponse.getHttpStatusCode() + "): " + theResponse.getHttpBody() );
									if ( _timer != null ) {
										_timer.cancel(); // It is us doing it wrong, we will never get new token again: Stop daemon!
									}
									return;
								}
							}
							catch ( Exception e ) {
								if ( theException == null ) {
									theException = e;
								}
							}
						}
						// Some server issue? No token retrieved:
						LOGGER.log( Level.WARNING, "Unable to refresh token within <" + _tokenRefreshRetryCounter.getRetryNumber() + "> tries and a delay of <" + _tokenRefreshRetryCounter.getTotalRetryDelayMillis() + "> milliseconds altogether (waiting for <" + _serverRecoveryWaitTimeMillis + "> milliseconds before next retry)" + ( theException != null ? " as of: " + theException.getLocalizedMessage() : "!" ), theException );
						_timer.schedule( new RefreshTask(), _serverRecoveryWaitTimeMillis ); // Give the server a chance to recover!
					}
				}
			}
		}
	}
}
