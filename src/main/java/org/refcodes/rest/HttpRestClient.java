// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "HTTP://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("HTTP://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("HTTP://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.component.ConnectionStatus;
import org.refcodes.data.Delimiter;
import org.refcodes.data.Port;
import org.refcodes.data.Scheme;
import org.refcodes.io.ReplayInputStream;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.textual.VerboseTextBuilder;
import org.refcodes.web.BadRequestException;
import org.refcodes.web.BasicAuthCredentials;
import org.refcodes.web.FormMediaTypeFactory;
import org.refcodes.web.HeaderField;
import org.refcodes.web.HtmlMediaTypeFactory;
import org.refcodes.web.HttpClientContext;
import org.refcodes.web.HttpClientRequest;
import org.refcodes.web.HttpClientResponse;
import org.refcodes.web.HttpResponseException;
import org.refcodes.web.HttpStatusCode;
import org.refcodes.web.JsonMediaTypeFactory;
import org.refcodes.web.MediaTypeFactory;
import org.refcodes.web.OauthToken;
import org.refcodes.web.PostHttpClientInterceptor;
import org.refcodes.web.PreHttpClientInterceptor;
import org.refcodes.web.ResponseHeaderFields;
import org.refcodes.web.TextMediaTypeFactory;
import org.refcodes.web.Url;
import org.refcodes.web.UrlBuilder;
import org.refcodes.web.XmlMediaTypeFactory;

/**
 * The {@link HttpRestClient} implements the {@link RestfulHttpClient}
 * interface. The {@link HttpRestClient} is being initialized with some common
 * {@link MediaTypeFactory} instances (as implemented by the
 * {@link AbstractRestfulClient}). At the time of writing this document the
 * {@link MediaTypeFactory} instances being preconfigured are:
 * <ul>
 * <li>{@link JsonMediaTypeFactory}</li>
 * <li>{@link XmlMediaTypeFactory}</li>
 * <li>{@link TextMediaTypeFactory}</li>
 * <li>{@link FormMediaTypeFactory}</li>
 * <li>{@link HtmlMediaTypeFactory}</li>
 * </ul>
 * The {@link HttpRestClient} supports HTTP as well as HTTPS protocols as being
 * based on the {@link HttpURLConnection}. For configuring HTTPS capabilities,
 * refer to the methods such as {@link #open(Url, TrustStoreDescriptor)} or
 * {@link #open(Url, TrustStoreDescriptor)}.
 */
public class HttpRestClient extends AbstractRestfulClient implements RestfulHttpClient {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( HttpRestClient.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int PIPE_STREAM_BUFFER = 1024;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Url _baseUrl = null;
	private ConnectionStatus _connectionStatus = ConnectionStatus.NONE;
	private TrustStoreDescriptor _storeDescriptor;
	private final List<PreHttpClientInterceptor> _preHttpInterceptos = new ArrayList<>();
	private final List<PostHttpClientInterceptor> _postHttpInterceptos = new ArrayList<>();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link HttpRestClient}. Use {@link #open()} or similar to
	 * activate it.
	 */
	public HttpRestClient() {
		onRestRequest( new HttpRestRequestHandler() );
	}

	/**
	 * Constructs a {@link HttpRestClient}. Use {@link #open()} or similar to
	 * activate it.
	 *
	 * @param aExecutorService An executor service to be used when creating
	 *        {@link Thread}s.
	 */
	public HttpRestClient( ExecutorService aExecutorService ) {
		super( aExecutorService );
		onRestRequest( new HttpRestRequestHandler() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_connectionStatus = ConnectionStatus.CLOSED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getBaseUrl() {
		return _baseUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConnectionStatus getConnectionStatus() {
		return _connectionStatus;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TrustStoreDescriptor getTrustStoreDescriptor() {
		return _storeDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open( Url aBaseUrl, TrustStoreDescriptor aStoreDescriptor ) throws IOException {
		if ( aStoreDescriptor == null ) {
			aStoreDescriptor = getTrustStoreDescriptor();
		}
		else {
			_storeDescriptor = aStoreDescriptor;
		}
		if ( aBaseUrl == null ) {
			aBaseUrl = getBaseUrl();
		}
		else {
			_baseUrl = aBaseUrl;
		}

		if ( _connectionStatus == ConnectionStatus.OPENED ) {
			throw new IOException( "The HTTP rest client is already open, close first before opening again!" );
		}
		_connectionStatus = ConnectionStatus.OPENED;
		if ( aStoreDescriptor != null ) {
			SystemProperty.TRUST_STORE_FILE.setValue( aStoreDescriptor.getStoreFile().getAbsolutePath() );
			SystemProperty.TRUST_STORE_PASSWORD.setValue( aStoreDescriptor.getStorePassword() != null ? aStoreDescriptor.getStorePassword() : "" );
			SystemProperty.TRUST_STORE_TYPE.setValue( aStoreDescriptor.getStoreType().name() );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBaseUrl( Url aBaseUrl ) {
		if ( aBaseUrl != null ) {
			if ( aBaseUrl.getScheme() != Scheme.HTTP && aBaseUrl.getScheme() != Scheme.HTTPS ) {
				throw new IllegalArgumentException( "Cannot use the protocol <" + aBaseUrl.getScheme() + "> to do HTTP requests (" + aBaseUrl.toHttpUrl() + "). You must provide a base URL for protocols <" + Scheme.HTTP.getName() + "> or <" + Scheme.HTTPS.getName() + ">." );
			}
			if ( aBaseUrl.getQueryFields() != null && aBaseUrl.getQueryFields().size() != 0 ) {
				throw new IllegalArgumentException( "Cannot use a query <" + new VerboseTextBuilder().withElements( aBaseUrl.getQueryFields() ) + "> as bayse path. You must provide a base URL without a query." );
			}
		}
		_baseUrl = aBaseUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBaseUrl( URL aBaseUrl ) {
		setBaseUrl( new Url( aBaseUrl ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTrustStoreDescriptor( TrustStoreDescriptor aTrustStoreDescriptor ) {
		_storeDescriptor = aTrustStoreDescriptor;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INTERCEPTOS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasPreHttpInterceptor( PreHttpClientInterceptor aPreInterceptor ) {
		return _preHttpInterceptos.contains( aPreInterceptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addPreHttpInterceptor( PreHttpClientInterceptor aPreInterceptor ) {
		if ( !_preHttpInterceptos.contains( aPreInterceptor ) ) {
			return _preHttpInterceptos.add( aPreInterceptor );
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean removePreHttpInterceptor( PreHttpClientInterceptor aPreInterceptor ) {
		return _preHttpInterceptos.remove( aPreInterceptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasPostHttpInterceptor( PostHttpClientInterceptor aPostInterceptor ) {
		return _postHttpInterceptos.contains( aPostInterceptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addPostHttpInterceptor( PostHttpClientInterceptor aPostInterceptor ) {
		if ( !_postHttpInterceptos.contains( aPostInterceptor ) ) {
			return _postHttpInterceptos.add( aPostInterceptor );
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean removePostHttpInterceptor( PostHttpClientInterceptor aPostInterceptor ) {
		return _postHttpInterceptos.remove( aPostInterceptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withOpen() throws IOException {
		open();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withOpen( HttpClientContext aCtx ) throws IOException {
		open( aCtx );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withOpen( TrustStoreDescriptor aStoreDescriptor ) throws IOException {
		open( aStoreDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withOpen( Url aBaseUrl ) throws IOException {
		open( aBaseUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withOpen( Url aBaseUrl, TrustStoreDescriptor aStoreDescriptor ) throws IOException {
		open( aBaseUrl, aStoreDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withBaseUrl( String aBaseUrl ) throws MalformedURLException {
		setBaseUrl( aBaseUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withBaseUrl( Url aBaseUrl ) {
		setBaseUrl( aBaseUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withBaseUrl( URL aBaseURL ) {
		setBaseUrl( aBaseURL );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withBasicAuthCredentials( BasicAuthCredentials aBasicAuthCredentials ) {
		setBasicAuthCredentials( aBasicAuthCredentials );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withBasicAuthCredentials( String aUserName, String aSecret ) {
		setBasicAuthCredentials( aUserName, aSecret );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withOAuthToken( OauthToken aOauthToken ) {
		setOauthToken( aOauthToken );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withTrustStoreDescriptor( TrustStoreDescriptor aStoreDescriptor ) {
		setTrustStoreDescriptor( aStoreDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withUserAgent( String aUserAgent ) {
		setUserAgent( aUserAgent );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withBaseUrl( String aProtocol, String aHost ) throws MalformedURLException {
		setBaseUrl( aProtocol, aHost );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withBaseUrl( Scheme aScheme, String aHost ) throws MalformedURLException {
		setBaseUrl( aScheme, aHost );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withBaseUrl( String aProtocol, String aHost, String aPath ) throws MalformedURLException {
		setBaseUrl( aProtocol, aHost, aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withBaseUrl( Scheme aScheme, String aHost, String aPath ) throws MalformedURLException {
		setBaseUrl( aScheme, aHost, aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withBaseUrl( String aProtocol, String aHost, int aPort ) throws MalformedURLException {
		setBaseUrl( aProtocol, aHost, aPort );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withBaseUrl( Scheme aScheme, String aHost, int aPort ) throws MalformedURLException {
		setBaseUrl( aScheme, aHost, aPort );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withBaseUrl( String aProtocol, String aHost, int aPort, String aPath ) throws MalformedURLException {
		setBaseUrl( aProtocol, aHost, aPort, aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withBaseUrl( Scheme aScheme, String aHost, int aPort, String aPath ) throws MalformedURLException {
		setBaseUrl( aScheme, aHost, aPort, aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpRestClient withOpenUnchecked() {
		openUnchecked();
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Pipes the {@link InputStream} to the {@link OutputStream}.
	 *
	 * @param aInputStream The {@link InputStream} from which to pipe.
	 * @param aOutoutStream The {@link OutputStream} to pipe to.
	 * 
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	protected static void pipe( InputStream aInputStream, OutputStream aOutoutStream ) throws IOException {
		int eBytes;
		final byte[] theBuffer = new byte[PIPE_STREAM_BUFFER];
		while ( ( eBytes = aInputStream.read( theBuffer ) ) > -1 ) {
			aOutoutStream.write( theBuffer, 0, eBytes );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void preIntercept( HttpClientRequest aRequest, HttpClientResponse aResponse ) {
		for ( PreHttpClientInterceptor eInterceptor : _preHttpInterceptos ) {
			eInterceptor.preIntercept( aRequest, aResponse );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void postIntercept( HttpClientRequest aRequest, HttpClientResponse aResponse ) {
		for ( PostHttpClientInterceptor eInterceptor : _postHttpInterceptos ) {
			eInterceptor.postIntercept( aRequest, aResponse );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Encapsulates the {@link HttpURLConnection} for retrieving the actual HTTP
	 * {@link InputStream} ({@link HttpURLConnection#getInputStream()}) or in
	 * case of an exception doing so encapsulates the exception's message in an
	 * {@link InputStream}.
	 */
	private static class HttpConnectionInputStream extends InputStream {

		private InputStream _inputStream;

		/**
		 * Instantiates a new HTTP connection input stream.
		 *
		 * @param aHttpURLConnection the HTTP URL connection
		 */
		public HttpConnectionInputStream( HttpURLConnection aHttpURLConnection ) {
			try {
				_inputStream = aHttpURLConnection.getInputStream();
			}
			catch ( IOException e1 ) {
				_inputStream = aHttpURLConnection.getErrorStream();
				if ( _inputStream == null ) {
					final String theMessage = e1.getMessage();
					_inputStream = new ByteArrayInputStream( theMessage.getBytes( StandardCharsets.UTF_8 ) );
				}
			}
		}

		/**
		 * Availability.
		 *
		 * @return the int
		 * 
		 * @throws IOException Signals that an I/O exception has occurred.
		 */
		@Override
		public int available() throws IOException {
			return _inputStream.available();
		}

		/**
		 * Close.
		 *
		 * @throws IOException Signals that an I/O exception has occurred.
		 */
		@Override
		public void close() throws IOException {
			_inputStream.close();
		}

		/**
		 * Equals.
		 *
		 * @param obj the obj
		 * 
		 * @return true, if successful
		 */
		@Override
		public boolean equals( Object obj ) {
			return _inputStream.equals( obj );
		}

		/**
		 * Hash code.
		 *
		 * @return the int
		 */
		@Override
		public int hashCode() {
			return _inputStream.hashCode();
		}

		/**
		 * Mark.
		 *
		 * @param readlimit the readlimit
		 */
		@Override
		public void mark( int readlimit ) {
			_inputStream.mark( readlimit );
		}

		/**
		 * Mark supported.
		 *
		 * @return true, if successful
		 */
		@Override
		public boolean markSupported() {
			return _inputStream.markSupported();
		}

		/**
		 * Read.
		 *
		 * @return the int
		 * 
		 * @throws IOException Signals that an I/O exception has occurred.
		 */
		@Override
		public int read() throws IOException {
			return _inputStream.read();
		}

		/**
		 * Read.
		 *
		 * @param b the b
		 * 
		 * @return the int
		 * 
		 * @throws IOException Signals that an I/O exception has occurred.
		 */
		@Override
		public int read( byte[] b ) throws IOException {
			return _inputStream.read( b );
		}

		/**
		 * Read.
		 *
		 * @param b the b
		 * @param off the off
		 * @param len the len
		 * 
		 * @return the int
		 * 
		 * @throws IOException Signals that an I/O exception has occurred.
		 */
		@Override
		public int read( byte[] b, int off, int len ) throws IOException {
			return _inputStream.read( b, off, len );
		}

		/**
		 * Reset.
		 *
		 * @throws IOException Signals that an I/O exception has occurred.
		 */
		@Override
		public void reset() throws IOException {
			_inputStream.reset();
		}

		/**
		 * Skip.
		 *
		 * @param n the n
		 * 
		 * @return the long
		 * 
		 * @throws IOException Signals that an I/O exception has occurred.
		 */
		@Override
		public long skip( long n ) throws IOException {
			return _inputStream.skip( n );
		}

		/**
		 * To string.
		 *
		 * @return the string
		 */
		@Override
		public String toString() {
			return _inputStream.toString();
		}
	}

	private class HttpRestRequestHandler implements RestRequestHandler {

		/**
		 * Handler for the REST requests issued by the {@link HttpRestClient}.
		 *
		 * @param aClientRequest The {@link HttpClientRequest} to handle.
		 * 
		 * @return The according {@link RestResponse}.
		 * 
		 * @throws HttpResponseException thrown by a HTTP-Response handling
		 *         system in case of some unexpected response.
		 */
		@SuppressWarnings("resource") // We need the "theHttpInputStream" stay open for the client (business) code read the response from the stream! 
		@Override
		public RestResponse doRequest( HttpClientRequest aClientRequest ) throws HttpResponseException {
			if ( _connectionStatus == ConnectionStatus.CLOSED ) {
				throw new IllegalStateException( " Expected a connection status <" + ConnectionStatus.OPENED.name() + ">, unable to produce a HTTP request while the connection is in status <" + _connectionStatus.name() + ">, did you forget to call #open(...)?" );
			}
			Url theRequestUrl = aClientRequest.getUrl();
			try {
				HttpURLConnection theHttpConnection = null;
				InetSocketAddress theLocalAddress = null;
				InetSocketAddress theRemoteAddress = null;
				HttpStatusCode theStatusCode = null;
				final ResponseHeaderFields theResponseHeaderFields = new ResponseHeaderFields();
				final HttpClientResponse theClientResponse = new HttpClientResponse( theRequestUrl, theResponseHeaderFields, HttpRestClient.this );
				preIntercept( aClientRequest, theClientResponse );

				// |--> Redirect;
				int theRedirectDepth = aClientRequest.getRedirectDepth();
				if ( theRedirectDepth < 0 ) {
					HttpURLConnection.setFollowRedirects( true ); // Automatic
					theRedirectDepth = 0; // Redirect is automatic
				}
				else {
					HttpURLConnection.setFollowRedirects( false );
				}
				// Redirect <--|

				boolean isRedirectCycle = false;
				// |--> Redirect loop
				while ( theRedirectDepth >= 0 ) {
					// |--> Create arguments for listener invocation:
					URL theUrl = null;
					try {
						theUrl = theRequestUrl.toURL();
					}
					catch ( MalformedURLException e ) {
						if ( _baseUrl == null ) {
							throw new HttpResponseException( "Unable to create a valid URL from the locator <{1}> with a <null> base URL!", HttpStatusCode.INTERNAL_CLIENT_ERROR, theRequestUrl, e );
						}
						else {
							try {
								final String theExternalForm;
								// |--> Redirect:
								if ( isRedirectCycle ) {
									theExternalForm = new UrlBuilder( _baseUrl ).withPath( null ).toHttpUrl();
								}
								// Redirect <--|
								else {
									theExternalForm = _baseUrl.toHttpUrl();
								}
								final String theRelativeUrl = theRequestUrl.toHttpUrl();
								String theSeparator = "";
								if ( ( theExternalForm == null || !theExternalForm.endsWith( "" + Delimiter.PATH.getChar() ) ) && ( theRelativeUrl == null || !theRelativeUrl.startsWith( "" + Delimiter.PATH.getChar() ) ) ) {
									theSeparator = "" + Delimiter.PATH.getChar();
								}
								theUrl = new URL( theExternalForm + theSeparator + theRelativeUrl );
							}
							catch ( MalformedURLException e2 ) {
								throw new HttpResponseException( "Unable to create a valid URL from the base URL <" + _baseUrl.toHttpUrl() + "> and the request URL <{1}>!", HttpStatusCode.INTERNAL_CLIENT_ERROR, theRequestUrl, e2 );
							}
						}
					}

					int thePort = theUrl.getPort();
					if ( thePort == -1 ) {
						final String theProtocol = theUrl.getProtocol();
						if ( Scheme.HTTP.getName().equalsIgnoreCase( theProtocol ) ) {
							thePort = Port.HTTP.getPort();
						}
						if ( Scheme.HTTPS.getName().equalsIgnoreCase( theProtocol ) ) {
							thePort = Port.HTTPS.getPort();
						}
					}

					theRemoteAddress = new InetSocketAddress( theUrl.getHost(), thePort );
					// ---------------------------------------------------------
					// The port definitely is wrong as outgoing HTTP requests
					// may use any port available, though usually not the port
					// being targeted at! Unfortunately there is no way to
					// determine the originating InetSocketAddress from an
					// HttpURLConnection |-->
					theLocalAddress = InetSocketAddress.createUnresolved( InetAddress.getLocalHost().getHostName(), thePort );
					// <--| Unfortunately there is no way to determine the
					// originating InetSocketAddress from an HttpURLConnection
					// ---------------------------------------------------------
					// Create arguments for listener invocation <--|

					// |--> Do the HTTP request:
					theHttpConnection = (HttpURLConnection) theUrl.openConnection();
					theHttpConnection.setRequestMethod( aClientRequest.getHttpMethod().name() );
					final List<String> theAcceptTypes = aClientRequest.getHeaderFields().get( HeaderField.ACCEPT );
					if ( theAcceptTypes == null || theAcceptTypes.size() == 0 ) {
						aClientRequest.getHeaderFields().putAcceptTypes( getFactoryMediaTypes() );
					}
					theHttpConnection.setRequestMethod( aClientRequest.getHttpMethod().name() );

					// -------------------------------------------------------------
					// As "HttpURLConnection#getRequestProperties()" is
					// immutable, we cannot go for this one:
					// -------------------------------------------------------------
					// theRestCaller.getHeaderFields().toHeaderFields(
					// theHttpConnection.getRequestProperties() );
					// -------------------------------------------------------------
					for ( String eKey : aClientRequest.getHeaderFields().keySet() ) {
						theHttpConnection.setRequestProperty( eKey, aClientRequest.getHeaderFields().toField( eKey ) );
					}

					// |--> Process the request:
					final Object theRequest = aClientRequest.getRequest();
					// |--> Input-Stream:
					if ( ( theRequest instanceof InputStream ) ) {
						final InputStream theInputStream = (InputStream) theRequest;
						theHttpConnection.setDoOutput( true );
						pipe( theInputStream, theHttpConnection.getOutputStream() );
						theHttpConnection.getOutputStream().flush();
					}
					// Input-Stream <--|
					// Object |-->
					else {
						final String theBody = aClientRequest.toHttpBody();
						if ( theBody != null ) {
							theHttpConnection.setDoOutput( true );
							theHttpConnection.getOutputStream().write( theBody.getBytes() );
							theHttpConnection.getOutputStream().flush();
						}
					}

					if ( !aClientRequest.getHttpMethod().name().equalsIgnoreCase( theHttpConnection.getRequestMethod() ) ) {
						LOGGER.log( Level.WARNING, "You issued a request with HTTP-Method <" + aClientRequest.getHttpMethod().name() + "> which is not applicable for sending a HTTP body, the HTTP-Method has been changed to <" + theHttpConnection.getRequestMethod() + ">." );
					}
					// Object <--|
					// Process the request <--|

					// |--> Process the response:
					final Map<String, List<String>> theHeaderFields = theHttpConnection.getHeaderFields();
					theResponseHeaderFields.putAll( theHeaderFields );

					theStatusCode = HttpStatusCode.toHttpStatusCode( theHttpConnection.getResponseCode() );
					// Process the response <--|
					// Do the HTTP request <--|

					// |--> Redirect
					if ( theStatusCode.isRedirectStatus() && theRedirectDepth > 0 ) {
						isRedirectCycle = true;
						final String theLocation = theResponseHeaderFields.getLocation();
						final UrlBuilder theUrlBuilder = new UrlBuilder( theRequestUrl );
						if ( theLocation != null ) {
							if ( isAbsoluteUrl( theLocation ) ) {
								// |--> Merge absolute location to request:
								final UrlBuilder theTmpUrl = new UrlBuilder( theLocation );
								final Scheme theTmpScheme = theTmpUrl.getScheme();
								if ( theTmpScheme != null ) {
									theUrlBuilder.setScheme( theTmpScheme );
								}
								final String theTmpHost = theTmpUrl.getHost();
								if ( theTmpHost != null ) {
									theUrlBuilder.setHost( theTmpHost );
								}
								final int[] theTmpIp = theTmpUrl.getIpAddress();
								if ( theTmpIp != null && theTmpIp.length > 0 ) {
									theUrlBuilder.setIpAddress( theTmpIp );
								}
								final String theTmpPath = theTmpUrl.getPath();
								if ( theTmpPath != null ) {
									theUrlBuilder.setPath( theTmpPath );
								}
								final int theTmpPort = theTmpUrl.getPort();
								if ( theTmpPort != -1 ) {
									theUrlBuilder.setPort( theTmpPort );
									// Merge absolute location to request <--|
								}
							}
							else {
								// |--> Set relative location to request:
								theUrlBuilder.setPath( theLocation );
								// Set relative location to request <--|
							}
							theRequestUrl = theUrlBuilder;
							theRedirectDepth--;
						}
						else {
							theRedirectDepth = -1;
						}
					}
					else {
						theRedirectDepth = -1;
					}
					// Redirect <--|
				}
				// Redirect loop <--|

				// |--> Invoke the caller:
				InputStream theHttpInputStream = new HttpConnectionInputStream( theHttpConnection );
				if ( !theHttpInputStream.markSupported() ) {
					theHttpInputStream = new ReplayInputStream( theHttpInputStream ); // We need the "theHttpInputStream" stay open for the client (business) code read the response from the stream
				}
				final RestResponse theRestResponse = new RestResponse( theRequestUrl, theLocalAddress, theRemoteAddress, theStatusCode, theResponseHeaderFields, theHttpInputStream, HttpRestClient.this );
				// Invoke the caller <--|
				postIntercept( aClientRequest, theRestResponse );
				return theRestResponse;
				// Invoke the caller <--|
			}
			catch ( IOException e ) {
				throw new HttpResponseException( toMessage( aClientRequest ), HttpStatusCode.INTERNAL_CLIENT_ERROR, theRequestUrl, e );
			}
			catch ( BadRequestException e ) {
				throw new HttpResponseException( toMessage( aClientRequest ), e.getStatusCode(), e );
			}
		}

		private boolean isAbsoluteUrl( String aLocation ) {
			final String theLocation = aLocation.toLowerCase();
			if ( theLocation.startsWith( Scheme.HTTP.toProtocol().toLowerCase() ) ) {
				return true;
			}
			if ( theLocation.startsWith( Scheme.HTTPS.toProtocol().toLowerCase() ) ) {
				return true;
			}
			return false;
		}

		private String toMessage( HttpClientRequest aClientRequest ) {
			return "Error while processing URL <" + aClientRequest.getUrl().toHttpUrl() + "> using HTTP-Method <" + aClientRequest.getHttpMethod().toString() + ">!";
		}
	}
}
