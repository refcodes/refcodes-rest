// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.component.InitializeException;
import org.refcodes.component.PauseException;
import org.refcodes.component.ResumeException;
import org.refcodes.component.StartException;
import org.refcodes.component.StopException;
import org.refcodes.data.Scheme;
import org.refcodes.rest.PingRequestObserverAccessor.PingRequestObserverBuilder;
import org.refcodes.web.Url;

/**
 * The {@link RestfulHttpRegistryServer} provides additional functionality for
 * registering at and signing off from a service discovery service in order to
 * resolve URLs to or from other services. This type is intended to be used by
 * different separate hierarchy branches by providing of the generic type
 * &lt;B&gt;, ensuring a coherent type hierarchy for each branch.
 *
 * @param <DESC> The type of the server discovery descriptor (the object
 *        describing your service and locating the service registry).
 * @param <B> In order to implement the builder pattern with a coherent type
 *        hierarchy.
 */
public interface RestfulHttpRegistryServer<DESC extends HttpServerDescriptor, B extends RestfulHttpRegistryServer<DESC, B>> extends HttpRegistry<DESC, B>, RestfulHttpServer, PingRequestObserver, PingRequestObserverAccessor, PingRequestObserverBuilder<B> {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withPort( int aPort ) {
		setPort( aPort );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withScheme( Scheme aScheme ) {
		setScheme( aScheme );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withProtocol( String aProtocol ) {
		setProtocol( aProtocol );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withPingRequestObserver( RestRequestConsumer aRequestConsumer ) {
		onPingRequest( aRequestConsumer );
		return (B) this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// LIERFCYCLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Initializes the {@link RestfulHttpRegistryServer} by registering it at
	 * the service registry with a status such as "starting" or "initializing"
	 * or "not-ready-yet". {@inheritDoc}
	 * 
	 * @param aAlias The name ("alias") which identifies the server in the
	 *        registry.
	 * @param aScheme The {@link Scheme} to which this server is being attached
	 *        (HTTP or HTTPS).
	 * @param aHost The host name to be used to address this server. If omitted,
	 *        then the system's host name should be used.
	 * @param aVirtualHost The virtual host name to be used for resolving.
	 * @param aIpAddress The IP-Address identifying the host.
	 * @param aPort The port of your service being registered.
	 * @param aPingPath The path to use as health-check end-point by this
	 *        server.
	 * @param aPingRequestObserver The {@link RestRequestConsumer} hooking into
	 *        a ping request.
	 * @param aRegistryUrl The registry server where to register.
	 * 
	 * @throws InitializeException thrown in case initializing a component
	 *         caused problems. Usually a method similar to "initialize()"
	 *         throws such an exception.
	 */
	default void initialize( String aAlias, Scheme aScheme, String aHost, String aVirtualHost, int[] aIpAddress, int aPort, String aPingPath, RestRequestConsumer aPingRequestObserver, Url aRegistryUrl ) throws InitializeException {
		initialize( aAlias, null, aScheme, aHost, aVirtualHost, aIpAddress, aPort, aPingPath, aPingRequestObserver, aRegistryUrl );
	}

	/**
	 * Initializes the {@link RestfulHttpRegistryServer} by registering it at
	 * the service registry with a status such as "starting" or "initializing"
	 * or "not-ready-yet". {@inheritDoc}
	 * 
	 * @param aAlias The name ("alias") which identifies the server in the
	 *        registry.
	 * @param aInstanceId The TID for the instance when being registered at the
	 *        service registry. If omitted, then the host name is used.
	 * @param aScheme The {@link Scheme} to which this server is being attached
	 *        (HTTP or HTTPS).
	 * @param aHost The host name to be used to address this server. If omitted,
	 *        then the system's host name should be used.
	 * @param aVirtualHost The virtual host name to be used for resolving.
	 * @param aIpAddress The IP-Address identifying the host.
	 * @param aPort The port of your service being registered.
	 * @param aPingPath The path to use as health-check end-point by this
	 *        server.
	 * @param aPingRequestObserver The {@link RestRequestConsumer} hooking into
	 *        a ping request.
	 * @param aRegistryUrl The registry server where to register.
	 * 
	 * @throws InitializeException thrown in case initializing a component
	 *         caused problems. Usually a method similar to "initialize()"
	 *         throws such an exception.
	 */
	void initialize( String aAlias, String aInstanceId, Scheme aScheme, String aHost, String aVirtualHost, int[] aIpAddress, int aPort, String aPingPath, RestRequestConsumer aPingRequestObserver, Url aRegistryUrl ) throws InitializeException;

	/**
	 * Some "up-and-running" status is communicated to the registry server. The
	 * registered {@link RestEndpoint} instances (observers) (e.g. via
	 * {@link #onRequest(org.refcodes.web.HttpMethod, String, RestRequestConsumer)},
	 * {@link #onGet(String, RestRequestConsumer)}
	 * {@link #onPut(String, RestRequestConsumer)},
	 * {@link #onPut(String, RestRequestConsumer)} or
	 * {@link #onDelete(String, RestRequestConsumer)}) are enabled by calling
	 * {@link #enableObservers()}. If necessary, the connection is opened via
	 * {@link #open()} or the like. May also start a "ping" or "heartbeat"
	 * daemon. {@inheritDoc}
	 */
	@Override
	void start() throws StartException;

	/**
	 * Some "deactivated" status is communicated to the registry server. The
	 * registered {@link RestEndpoint} instances (observers) (e.g. via
	 * {@link RestEndpointBuilder} being registered e.g. via
	 * {@link #onRequest(org.refcodes.web.HttpMethod, String, RestRequestConsumer)},
	 * {@link #onGet(String, RestRequestConsumer)}
	 * {@link #onPut(String, RestRequestConsumer)},
	 * {@link #onPut(String, RestRequestConsumer)} or
	 * {@link #onDelete(String, RestRequestConsumer)} is closed by calling its
	 * {@link RestEndpointBuilder#close()}) are disabled by calling
	 * {@link #disableObservers()}. {@inheritDoc}
	 */
	@Override
	void pause() throws PauseException;

	/**
	 * Some "up-and-running (again)" status is communicated to the registry
	 * server. The registered {@link RestEndpoint} instances (observers) (e.g.
	 * via {@link RestEndpointBuilder} being registered e.g. via
	 * {@link #onRequest(org.refcodes.web.HttpMethod, String, RestRequestConsumer)},
	 * {@link #onGet(String, RestRequestConsumer)}
	 * {@link #onPut(String, RestRequestConsumer)},
	 * {@link #onPut(String, RestRequestConsumer)} or
	 * {@link #onDelete(String, RestRequestConsumer)} is closed by calling its
	 * {@link RestEndpointBuilder#close()}) are enabled (again) by calling
	 * {@link #enableObservers()}. {@inheritDoc}
	 */
	@Override
	void resume() throws ResumeException;

	/**
	 * Some "out-of-order" status is communicated to the registry server. The
	 * registered {@link RestEndpoint} instances (observers) (e.g. via
	 * {@link #onRequest(org.refcodes.web.HttpMethod, String, RestRequestConsumer)},
	 * {@link #onGet(String, RestRequestConsumer)}
	 * {@link #onPut(String, RestRequestConsumer)},
	 * {@link #onPut(String, RestRequestConsumer)} or
	 * {@link #onDelete(String, RestRequestConsumer)}) are disabled by calling
	 * {@link #disableObservers()}. Also stops a "ping" or "heartbeat" daemon.
	 * {@inheritDoc}
	 */
	@Override
	void stop() throws StopException;

	/**
	 * This server is taken (removed) from the registry server. The registered
	 * {@link RestEndpoint} instances (observers) (e.g. via
	 * {@link #onRequest(org.refcodes.web.HttpMethod, String, RestRequestConsumer)},
	 * {@link #onGet(String, RestRequestConsumer)}
	 * {@link #onPut(String, RestRequestConsumer)},
	 * {@link #onPut(String, RestRequestConsumer)} or
	 * {@link #onDelete(String, RestRequestConsumer)}) are disabled by calling
	 * {@link #disableObservers()}. Also stops a "ping" or "heartbeat" daemon.
	 * Finally the connection is closed via {@link #close()}. {@inheritDoc}
	 */
	@Override
	void destroy();

}