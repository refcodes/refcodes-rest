// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.security.TrustStoreDescriptorAccessor.TrustStoreDescriptorBuilder;
import org.refcodes.security.TrustStoreDescriptorAccessor.TrustStoreDescriptorProperty;

/**
 * The {@link HttpDiscoverySidecar} describes the functionality required in
 * order to discover a service at a service discovery and discovery service.
 * This type is intended to be used by different separate hierarchy branches by
 * providing the generic type &lt;B&gt;, ensuring a coherent type hierarchy for
 * each branch.
 *
 * @param <B> In order to implement the builder pattern with a coherent type
 *        hierarchy.
 */
public interface HttpDiscoverySidecar<B extends HttpDiscoverySidecar<B>> extends HttpDiscovery<B>, TrustStoreDescriptorProperty, TrustStoreDescriptorBuilder<B> {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withTrustStoreDescriptor( TrustStoreDescriptor aStoreDescriptor ) {
		setTrustStoreDescriptor( aStoreDescriptor );
		return (B) this;
	}
}
