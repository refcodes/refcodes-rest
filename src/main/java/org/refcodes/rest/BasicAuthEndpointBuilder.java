package org.refcodes.rest;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.component.ConnectionStatus;
import org.refcodes.component.ConnectionStatusAccessor;
import org.refcodes.component.LinkComponent.LinkComponentBuilder;
import org.refcodes.web.BasicAuthCredentials;
import org.refcodes.web.BasicAuthObserver;
import org.refcodes.web.BasicAuthResponse;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpMethodAccessor.HttpMethodBuilder;
import org.refcodes.web.HttpMethodAccessor.HttpMethodProperty;

/**
 * A {@link BasicAuthEndpointBuilder} extends an {@link BasicAuthEndpoint} with
 * builder functionality and adds <code>lambda</code> support for handling the
 * requests addressed to this {@link BasicAuthEndpoint}. The <code>lambda</code>
 * defined as {@link BasicAuthObserver} acts as the single listener to this
 * {@link BasicAuthEndpoint} responsible for handling the requests for which
 * this {@link BasicAuthEndpoint} is responsible. The locator for which an
 * {@link BasicAuthEndpointBuilder} is responsible for is defined by the
 * {@link BasicAuthEndpointBuilder}'s Locator-Pattern: A single asterisk ("*")
 * matches zero or more characters within a locator name. A double asterisk
 * ("**") matches zero or more characters across directory levels. A question
 * mark ("?") matches exactly one character within a locator name. The single
 * asterisk ("*"), the double asterisk ("**") and the question mark ("?") we
 * refer to as wildcard: You get an array with the substitutes of the wildcards
 * using the method {@link RestRequestEvent#getWildcardReplacements()} . You may
 * name a wildcard by prefixing it with "${someWildcardName}". For example a
 * named wildcard may look as follows: "${arg1}=*" or "${arg2}=**" or
 * "${arg3}=?". You can get the text substituting a named wildcard using the
 * method {@link RestRequestEvent#getWildcardReplacement(String)}. For ease of
 * use, a named wildcard with single asterisk ("*") such as "${arg1}=*" can be
 * abbreviated as "${arg1}".
 */
public class BasicAuthEndpointBuilder implements BasicAuthEndpoint, HttpMethodProperty, HttpMethodBuilder<BasicAuthEndpointBuilder>, LinkComponentBuilder<BasicAuthEndpointBuilder>, ConnectionStatusAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( BasicAuthEndpointBuilder.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected BasicAuthObserver _requestObserver = null;
	protected HttpMethod _httpMethod = HttpMethod.GET;
	protected String _locatorPattern = null;
	protected ConnectionStatus _connectionStatus = ConnectionStatus.NONE;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a plain {@link BasicAuthEndpointBuilder}, make sure to provide
	 * the least required attributes as demonstrated by the constructor
	 * {@link #BasicAuthEndpointBuilder(HttpMethod, String, BasicAuthObserver)}.
	 */
	public BasicAuthEndpointBuilder() {}

	/**
	 * Constructs an {@link BasicAuthEndpointBuilder} with the least required
	 * attributes.
	 * 
	 * @param aHttpMethod The HTTP-Method to which this
	 *        {@link BasicAuthEndpointBuilder} is bound.
	 * @param aLocatorPathPattern The local host's locator patter to which this
	 *        {@link BasicAuthEndpointBuilder} is bound. See
	 *        {@link #setLocatorPathPattern(String)} on the syntax of the
	 *        pattern.
	 * @param aRequestConsumer The listener processing requests targeted at this
	 *        {@link BasicAuthEndpointBuilder}.
	 */
	public BasicAuthEndpointBuilder( HttpMethod aHttpMethod, String aLocatorPathPattern, BasicAuthObserver aRequestConsumer ) {
		_httpMethod = aHttpMethod;
		_locatorPattern = aLocatorPathPattern;
		_requestObserver = aRequestConsumer;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BasicAuthEndpointBuilder withHttpMethod( HttpMethod aHttpMethod ) {
		setHttpMethod( aHttpMethod );
		return this;
	}

	/**
	 * Sets the resource locator as of the Builder-Pattern. The locator may
	 * contain wildcards as known from file-systems as follows: A single
	 * asterisk ("*") matches zero or more characters within a locator name. A
	 * double asterisk ("**") matches zero or more characters across directory
	 * levels. A question mark ("?") matches exactly one character within a
	 * locator name. You may name a wildcard by prefixing it with
	 * "${someWildcardName}". For example a named wildcard may look as follows:
	 * "${arg1}=*" or "${arg2}=**" or "${arg3}=?". You can get the text
	 * substituting a named wildcard using the method
	 * {@link RestRequestEvent#getWildcardReplacement(String)}. For ease of use,
	 * a named wildcard with single asterisk ("*") such as "${arg1}=*" can be
	 * abbreviated as "${arg1}".
	 *
	 * @param aLocatorPathPattern the locator pattern
	 * 
	 * @return The {@link BasicAuthEndpoint} builder to continue configuration
	 *         (as of the Builder-Pattern).
	 */
	public BasicAuthEndpointBuilder withLocatorPathPattern( String aLocatorPathPattern ) {
		setLocatorPathPattern( aLocatorPathPattern );
		return this;
	}

	/**
	 * Builder method for setting the {@link BasicAuthObserver}.
	 * 
	 * @param aLambda The (user defined) {@link BasicAuthObserver} to handle
	 *        requests, feel free to code it as <code>lambda</code> expression
	 * 
	 * @return The {@link BasicAuthEndpointBuilder} for the sake of a fluent
	 *         API.
	 */
	public BasicAuthEndpointBuilder withRequestObserver( BasicAuthObserver aLambda ) {
		setRequestObserver( aLambda );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BasicAuthEndpointBuilder withOpen() throws IOException {
		open();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BasicAuthEndpointBuilder withClose() throws IOException {
		close();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BasicAuthEndpointBuilder withCloseQuietly() {
		closeQuietly();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BasicAuthEndpointBuilder withCloseIn( int aCloseMillis ) {
		closeIn( aCloseMillis );
		return this;
	}

	/**
	 * Sets the resource locator. The locator may contain wildcards as known
	 * from file-systems as follows: A single asterisk ("*") matches zero or
	 * more characters within a locator name. A double asterisk ("**") matches
	 * zero or more characters across directory levels. A question mark ("?")
	 * matches exactly one character within a locator name. You may name a
	 * wildcard by prefixing it with "${someWildcardName}". For example a named
	 * wildcard may look as follows: "${arg1}=*" or "${arg2}=**" or "${arg3}=?".
	 * You can get the text substituting a named wildcard using the method
	 * {@link RestRequestEvent#getWildcardReplacement(String)}. For ease of use,
	 * a named wildcard with single asterisk ("*") such as "${arg1}=*" can be
	 * abbreviated as "${arg1}".
	 *
	 * @param aLocatorPathPattern the new locator pattern
	 */
	public void setLocatorPathPattern( String aLocatorPathPattern ) {
		_locatorPattern = aLocatorPathPattern;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpMethod( HttpMethod aHttpMethod ) {
		_httpMethod = aHttpMethod;
	}

	/**
	 * Sets the {@link BasicAuthObserver} to which any requests are delegated
	 * upon invocation of the
	 * {@link #onBasicAuthRequest(java.net.InetSocketAddress, java.net.InetSocketAddress, HttpMethod, String, org.refcodes.web.BasicAuthCredentials, String)}
	 * method.
	 * 
	 * @param aLambda The (user defined) {@link BasicAuthObserver} to handle
	 *        requests, feel free to code it as <code>lambda</code> expression!
	 */
	public void setRequestObserver( BasicAuthObserver aLambda ) {
		_requestObserver = aLambda;
	}

	/**
	 * Retrieves the {@link BasicAuthObserver} to which any requests are
	 * delegated upon invocation of the
	 * {@link #onBasicAuthRequest(java.net.InetSocketAddress, java.net.InetSocketAddress, HttpMethod, String, org.refcodes.web.BasicAuthCredentials, String)}
	 * method.
	 * 
	 * @return The (user defined) {@link BasicAuthObserver} to handle requests.
	 */
	public BasicAuthObserver getRequestObserver() {
		return _requestObserver;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BasicAuthResponse onBasicAuthRequest( InetSocketAddress aLocalAddress, InetSocketAddress aRemoteAddress, HttpMethod aHttpMethod, String aLocator, BasicAuthCredentials aCredentials, String aRealm ) {
		if ( _connectionStatus == ConnectionStatus.OPENED ) {
			return _requestObserver.onBasicAuthRequest( aLocalAddress, aRemoteAddress, aHttpMethod, aLocator, aCredentials, aRealm );
		}
		else {
			LOGGER.log( Level.WARNING, "Ignoring Basic-Auth <" + aHttpMethod + "> request for locator <" + aLocator + "> as this rest endpoint is in status <" + _connectionStatus + ">, you may not have opened it?" );
		}
		return BasicAuthResponse.BASIC_AUTH_SKIP;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpMethod getHttpMethod() {
		return _httpMethod;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLocatorPathPattern() {
		return _locatorPattern;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open() throws IOException {
		_connectionStatus = ConnectionStatus.OPENED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_connectionStatus = ConnectionStatus.CLOSED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConnectionStatus getConnectionStatus() {
		return _connectionStatus;
	}
}
