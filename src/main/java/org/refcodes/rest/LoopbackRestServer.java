// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;

import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpServerResponse;
import org.refcodes.web.HttpStatusException;
import org.refcodes.web.RequestHeaderFields;
import org.refcodes.web.Url;

/**
 * Implements a {@link RestfulServer} to be used as loopback device e.g. for
 * testing purposes such as testing your {@link RestRequestConsumer}
 * implementations. Use the
 * {@link #onHttpRequest(InetSocketAddress, InetSocketAddress, HttpMethod, Url, RequestHeaderFields, InputStream, HttpServerResponse)}
 * method to simulate REST requests on the {@link LoopbackRestServer}. An
 * registered {@link RestEndpoint} instances being targeted at will be invoked
 * accordingly.
 */
public class LoopbackRestServer extends AbstractRestfulServer {

	/**
	 * Invoke this method to simulate a REST request to be handled by the
	 * according registered {@link RestEndpoint} instances:.
	 *
	 * @param aLocalAddress the local address
	 * @param aClientAddress the client address
	 * @param aHttpMethod The method for the simulated REST request to be used
	 * @param aUrl The query string part of the request.
	 * @param aRequestHeaderFields The simulated REST request's Header-Fields to
	 *        be used
	 * @param aHttpBody The raw body for the simulated REST request to be used.
	 * @param aHttpServerResponse The {@link HttpServerResponse} to be filled
	 *        with the complete result of processing the request.
	 * 
	 * @throws HttpStatusException thrown in case of HTTP status code related
	 *         exceptions (e.g. as of a HTTP response was of an erroneous
	 *         status).
	 */
	public void onHttpRequest( InetSocketAddress aLocalAddress, InetSocketAddress aClientAddress, HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aRequestHeaderFields, String aHttpBody, HttpServerResponse aHttpServerResponse ) throws HttpStatusException {
		super.onHttpRequest( aLocalAddress, aClientAddress, aHttpMethod, aUrl, aRequestHeaderFields == null ? new RequestHeaderFields() : aRequestHeaderFields, new ByteArrayInputStream( aHttpBody.getBytes( StandardCharsets.UTF_8 ) ), aHttpServerResponse );
	}

	/**
	 * Invoke this method to simulate a REST request to be handled by the
	 * according registered {@link RestEndpoint} instances:.
	 *
	 * @param aLocalAddress the local address
	 * @param aClientAddress the client address
	 * @param aHttpMethod The method for the simulated REST request to be used
	 * @param aUrl The query string part of the request.
	 * @param aRequestHeaderFields The simulated REST request's Header-Fields to
	 *        be used
	 * @param aHttpInputStream The HTTP {@link InputStream} representing the
	 *        body for the simulated REST request to be used.
	 * @param aHttpServerResponse The {@link HttpServerResponse} to be filled
	 *        with the complete result of processing the request.
	 * 
	 * @throws HttpStatusException thrown in case of HTTP status code related
	 *         exceptions (e.g. as of a HTTP response was of an erroneous
	 *         status).
	 */
	@Override
	public void onHttpRequest( InetSocketAddress aLocalAddress, InetSocketAddress aClientAddress, HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aRequestHeaderFields, InputStream aHttpInputStream, HttpServerResponse aHttpServerResponse ) throws HttpStatusException {
		super.onHttpRequest( aLocalAddress, aClientAddress, aHttpMethod, aUrl, aRequestHeaderFields == null ? new RequestHeaderFields() : aRequestHeaderFields, aHttpInputStream, aHttpServerResponse );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoopbackRestServer withBaseLocator( String aBaseLocator ) {
		setBaseLocator( aBaseLocator );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoopbackRestServer withRealm( String aRealm ) {
		setRealm( aRealm );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoopbackRestServer withObserversActive( boolean isActive ) {
		setObserversActive( isActive );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoopbackRestServer withEnableObservers() {
		enableObservers();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoopbackRestServer withDisableObservers() {
		disableObservers();
		return this;
	}
}