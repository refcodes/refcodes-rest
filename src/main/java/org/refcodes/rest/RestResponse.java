// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.io.InputStream;
import java.net.InetSocketAddress;

import org.refcodes.mixin.Dumpable;
import org.refcodes.web.HttpClientResponse;
import org.refcodes.web.HttpStatusCode;
import org.refcodes.web.LocalAddressAccessor;
import org.refcodes.web.MediaTypeFactoryLookup;
import org.refcodes.web.RemoteAddressAccessor;
import org.refcodes.web.ResponseHeaderFields;
import org.refcodes.web.Url;

/**
 * Defines a {@link RestResponse} being the base definition of a response as
 * returned as of a request issued by a {@link RestfulClient} (
 * {@link RestfulHttpClient}).
 */
public class RestResponse extends HttpClientResponse implements RemoteAddressAccessor, LocalAddressAccessor, Dumpable {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final InetSocketAddress _remoteAddress;
	private final InetSocketAddress _localAddress;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link RestResponseEvent} with all required attributes.
	 * 
	 * @param aUrl The URL from which the response originates.
	 * @param aLocalAddress The local address where the event is being received.
	 * @param aRemoteAddress The remote address from which the request
	 *        originates.
	 * @param aHttpStatusCode The {@link HttpStatusCode} of the response.
	 * @param aHeaderFields The {@link ResponseHeaderFields} sent by the
	 *        response.
	 * @param aHttpInputStream The {@link InputStream} representing the
	 *        request's HTTP body.
	 * @param aMediaTypeFactoryLookup The system firing the event.
	 */
	public RestResponse( Url aUrl, InetSocketAddress aLocalAddress, InetSocketAddress aRemoteAddress, HttpStatusCode aHttpStatusCode, ResponseHeaderFields aHeaderFields, InputStream aHttpInputStream, MediaTypeFactoryLookup aMediaTypeFactoryLookup ) {
		super( aUrl, aHttpStatusCode, aHeaderFields, aHttpInputStream, aMediaTypeFactoryLookup );
		_remoteAddress = aRemoteAddress;
		_localAddress = aLocalAddress;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InetSocketAddress getRemoteAddress() {
		return _remoteAddress;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InetSocketAddress getLocalAddress() {
		return _localAddress;
	}
}
