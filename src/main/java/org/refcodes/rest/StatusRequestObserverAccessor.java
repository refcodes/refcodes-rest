// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

/**
 * Provides an accessor for a status {@link RestRequestConsumer} property.
 */
public interface StatusRequestObserverAccessor {

	/**
	 * Retrieves the status {@link RestRequestConsumer} from the status
	 * {@link RestRequestConsumer} property.
	 * 
	 * @return The status {@link RestRequestConsumer} stored by the status
	 *         {@link RestRequestConsumer} property.
	 */
	RestRequestConsumer getStatusRequestConsumer();

	/**
	 * Provides a mutator for a status {@link RestRequestConsumer} property.
	 */
	public interface StatusRequestObserverMutator {

		/**
		 * Sets the status {@link RestRequestConsumer} for the status
		 * {@link RestRequestConsumer} property.
		 * 
		 * @param aStatusRequestObserver The status {@link RestRequestConsumer}
		 *        to be stored by the status {@link RestRequestConsumer}
		 *        property.
		 */
		void setStatusRequestObserver( RestRequestConsumer aStatusRequestObserver );
	}

	/**
	 * Provides a builder method for a status {@link RestRequestConsumer}
	 * property returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface StatusRequestObserverBuilder<B extends StatusRequestObserverBuilder<B>> {

		/**
		 * Sets the status {@link RestRequestConsumer} for the status
		 * {@link RestRequestConsumer} property.
		 * 
		 * @param aRequestConsumer The status {@link RestRequestConsumer} to be
		 *        stored by the status {@link RestRequestConsumer} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withStatusRequestConsumer( RestRequestConsumer aRequestConsumer );
	}

	/**
	 * Provides a status {@link RestRequestConsumer} property.
	 */
	public interface StatusRequestObserverProperty extends StatusRequestObserverAccessor, StatusRequestObserverMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link RestRequestConsumer} (setter) as of
		 * {@link #setStatusRequestObserver(RestRequestConsumer)} and returns
		 * the very same value (getter).
		 * 
		 * @param aRequestConsumer The {@link RestRequestConsumer} to set (via
		 *        {@link #setStatusRequestObserver(RestRequestConsumer)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default RestRequestConsumer letStatusRequestObserver( RestRequestConsumer aRequestConsumer ) {
			setStatusRequestObserver( aRequestConsumer );
			return aRequestConsumer;
		}
	}
}
