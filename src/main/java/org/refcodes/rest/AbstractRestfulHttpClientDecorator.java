// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.refcodes.component.ConnectionStatus;
import org.refcodes.data.Scheme;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpResponseException;
import org.refcodes.web.MediaType;
import org.refcodes.web.MediaTypeFactory;
import org.refcodes.web.OauthToken;
import org.refcodes.web.PostHttpClientInterceptor;
import org.refcodes.web.PreHttpClientInterceptor;
import org.refcodes.web.RequestHeaderFields;
import org.refcodes.web.Url;

/**
 * Abstract class for easily decorating a {@link RestfulHttpClient}.
 *
 * @param <B> the generic type
 */
public abstract class AbstractRestfulHttpClientDecorator<B extends RestfulHttpClient> implements RestfulHttpClient {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected RestfulHttpClient _client;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the given {@link RestfulHttpClient}.
	 * 
	 * @param aClient The {@link RestfulHttpClient} to be decorated.
	 */
	public AbstractRestfulHttpClientDecorator( RestfulHttpClient aClient ) {
		_client = aClient;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OauthToken getOauthToken() {
		return _client.getOauthToken();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setOauthToken( OauthToken aOauthToken ) {
		_client.setOauthToken( aOauthToken );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConnectionStatus getConnectionStatus() {
		return _client.getConnectionStatus();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addMediaTypeFactory( MediaTypeFactory aMediaTypeFactory ) {
		return _client.addMediaTypeFactory( aMediaTypeFactory );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_client.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void closeIn( int aCloseMillis ) {
		_client.closeIn( aCloseMillis );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void closeQuietly() {
		_client.closeQuietly();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void closeUnchecked() {
		_client.closeUnchecked();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getBaseUrl() {
		return _client.getBaseUrl();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MediaType[] getFactoryMediaTypes() {
		return _client.getFactoryMediaTypes();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TrustStoreDescriptor getTrustStoreDescriptor() {
		return _client.getTrustStoreDescriptor();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUserAgent() {
		return _client.getUserAgent();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasMediaTypeFactory( MediaType aMediaType ) {
		return _client.hasMediaTypeFactory( aMediaType );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open() throws IOException {
		_client.open();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open( Url aBaseUrl, TrustStoreDescriptor aStoreDescriptor ) throws IOException {
		_client.open( getBaseUrl(), aStoreDescriptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void openUnchecked() {
		_client.openUnchecked();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBaseUrl( Scheme aProtocol, String aHost ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBaseUrl( Scheme aProtocol, String aHost, int aPort ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost, aPort );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBaseUrl( Scheme aProtocol, String aHost, int aPort, String aPath ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost, aPort, aPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBaseUrl( Scheme aProtocol, String aHost, String aPath ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost, aPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBaseUrl( String aBaseUrl ) throws MalformedURLException {
		_client.setBaseUrl( aBaseUrl );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBaseUrl( String aProtocol, String aHost ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBaseUrl( String aProtocol, String aHost, int aPort ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost, aPort );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBaseUrl( String aProtocol, String aHost, int aPort, String aPath ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost, aPort, aPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBaseUrl( String aProtocol, String aHost, String aPath ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost, aPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBaseUrl( Url aBaseUrl ) {
		_client.setBaseUrl( aBaseUrl );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBaseUrl( URL aBaseUrl ) {
		_client.setBaseUrl( aBaseUrl );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTrustStoreDescriptor( TrustStoreDescriptor aTrustStoreDescriptor ) {
		_client.setTrustStoreDescriptor( aTrustStoreDescriptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setUserAgent( String aUserAgent ) {
		_client.setUserAgent( aUserAgent );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MediaTypeFactory toMediaTypeFactory( MediaType aMediaType ) {
		return _client.toMediaTypeFactory( aMediaType );
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withBaseUrl( Scheme aProtocol, String aHost ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withBaseUrl( Scheme aProtocol, String aHost, int aPort ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost, aPort );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withBaseUrl( Scheme aProtocol, String aHost, int aPort, String aPath ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost, aPort, aPath );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withBaseUrl( Scheme aProtocol, String aHost, String aPath ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost, aPath );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withBaseUrl( String aBaseUrl ) throws MalformedURLException {
		_client.setBaseUrl( aBaseUrl );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withBaseUrl( String aProtocol, String aHost ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withBaseUrl( String aProtocol, String aHost, int aPort ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost, aPort );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withBaseUrl( String aProtocol, String aHost, int aPort, String aPath ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost, aPort, aPath );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withBaseUrl( String aProtocol, String aHost, String aPath ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost, aPath );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withBaseUrl( Url aBaseUrl ) {
		_client.setBaseUrl( aBaseUrl );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withBaseUrl( URL aBaseURL ) {
		_client.setBaseUrl( aBaseURL );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withUserAgent( String aUserAgent ) {
		_client.setUserAgent( aUserAgent );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, int aRedirectDepth ) {
		return _client.buildRequest( aHttpMethod, toUrl( aUrl ), aHeaderFields, aRequest, aRedirectDepth );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponse doRequest( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, int aRedirectDepth ) throws HttpResponseException {
		return _client.doRequest( aHttpMethod, toUrl( aUrl ), aHeaderFields, aRequest, aRedirectDepth );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseHandler onResponse( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, int aRedirectDepth, RestResponseConsumer aResponseConsumer ) {
		return _client.onResponse( aHttpMethod, toUrl( aUrl ), aHeaderFields, aRequest, aRedirectDepth, aResponseConsumer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseResult onResponse( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, int aRedirectDepth ) {
		return _client.onResponse( aHttpMethod, aUrl, aHeaderFields, aRequest, aRedirectDepth );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasPreHttpInterceptor( PreHttpClientInterceptor aPreInterceptor ) {
		return _client.hasPreHttpInterceptor( aPreInterceptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addPreHttpInterceptor( PreHttpClientInterceptor aPreInterceptor ) {
		return _client.addPreHttpInterceptor( aPreInterceptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean removePreHttpInterceptor( PreHttpClientInterceptor aPreInterceptor ) {
		return _client.removePreHttpInterceptor( aPreInterceptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasPostHttpInterceptor( PostHttpClientInterceptor aPostInterceptor ) {
		return _client.hasPostHttpInterceptor( aPostInterceptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addPostHttpInterceptor( PostHttpClientInterceptor aPostInterceptor ) {
		return _client.addPostHttpInterceptor( aPostInterceptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean removePostHttpInterceptor( PostHttpClientInterceptor aPostInterceptor ) {
		return _client.removePostHttpInterceptor( aPostInterceptor );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Hook for sub-classes to modify the request URL.
	 * 
	 * @param aUrl The {@link Url} for the request.
	 * 
	 * @return The tinkered {@link Url}, by default it returns the provided
	 *         {@link Url} unmodified.
	 */
	protected Url toUrl( Url aUrl ) {
		return aUrl;
	}
}
