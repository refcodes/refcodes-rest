// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.rest;

import org.refcodes.controlflow.ThreadingModel;

/**
 * The singleton of the {@link HttpRestServer} for easy
 * {@link RestfulHttpServer} access. Used by the {@link HttpRestServerSugar}'s
 * syntactic sugar.
 */
public class HttpRestServerSingleton extends HttpRestServer {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static HttpRestServerSingleton _httpRestServerSingleton;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new http rest server singleton.
	 */
	protected HttpRestServerSingleton() {
		super( ThreadingModel.MULTI );
	}

	/**
	 * Returns the singleton's instance as fabricated by this
	 * {@link HttpRestServerSingleton}.
	 *
	 * @return The {@link HttpRestServer} singleton's instance.
	 */
	public static RestfulHttpServer getInstance() {
		if ( _httpRestServerSingleton == null ) {
			synchronized ( HttpRestServerSingleton.class ) {
				_httpRestServerSingleton = new HttpRestServerSingleton();
			}
		}
		return _httpRestServerSingleton;
	}
}
