// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.web.HttpRequest;
import org.refcodes.web.HttpResponse;

/**
 * Defines how errors affect the HTTP-Body whilst processing a
 * {@link HttpRequest} or a {@link HttpResponse} along with the according
 * sub-classes. Usually the following fields are written into the HTTP-Body (if
 * possible): "timestamp", "status", "error": "Bad Request", "exception" and
 * "message". Usually the causing exception's message is used for the "message"
 * attribute: Make sure you do not reveal any sensible information in the
 * exception's message.
 */
public enum HttpExceptionHandling {

	/**
	 * Do not modify the so far processed HTTP-Response except update the actual
	 * HTTP-Status-Code. Useful when you manually prepared the HTTP-Response
	 * yourself.
	 */
	KEEP,

	/**
	 * Keep the so far processed HTTP-Response as is except update the actual
	 * HTTP-Status-Code, add only non-existing fields if possible with the error
	 * description fields. Useful when you manually prepared the HTTP-Response
	 * yourself.
	 */
	MERGE,

	/**
	 * Keep the so far processed HTTP-Response as is except the HTTP-Status-Code
	 * and also update all error description fields. Useful when you want to
	 * preserve already set fields.
	 */
	UPDATE,

	/**
	 * Create a new HTTP-Response with the according HTTP-Status-Code and the
	 * error description fields.
	 */
	REPLACE,

	/**
	 * Create a new HTTP-Response with the according HTTP-Status-Code and and an
	 * empty body.
	 */
	EMPTY

}
