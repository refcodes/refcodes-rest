// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.rest;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import org.refcodes.controlflow.ControlFlowUtility;
import org.refcodes.data.Text;
import org.refcodes.rest.RestResponseHandler.RestResponseHandlerDaemon;
import org.refcodes.rest.RestResponseResult.RestResponseResultDaemon;
import org.refcodes.textual.VerboseTextBuilder;
import org.refcodes.web.AuthType;
import org.refcodes.web.BasicAuthCredentials;
import org.refcodes.web.FormMediaTypeFactory;
import org.refcodes.web.HeaderFields;
import org.refcodes.web.HtmlMediaTypeFactory;
import org.refcodes.web.HttpClientRequest;
import org.refcodes.web.HttpClientResponse;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpResponseException;
import org.refcodes.web.JsonMediaTypeFactory;
import org.refcodes.web.MediaType;
import org.refcodes.web.MediaTypeFactory;
import org.refcodes.web.OauthToken;
import org.refcodes.web.RequestHeaderFields;
import org.refcodes.web.TextMediaTypeFactory;
import org.refcodes.web.Url;
import org.refcodes.web.XmlMediaTypeFactory;

/**
 * Abstract base implementation of the {@link RestfulClient} interface being the
 * foundation for various {@link RestfulClient} implementations such as
 * {@link HttpRestClient} or {@link LoopbackRestClient}. The
 * {@link AbstractRestfulClient} is preconfigured with the following
 * {@link MediaTypeFactory} instances:
 * <ul>
 * <li>{@link JsonMediaTypeFactory}</li>
 * <li>{@link XmlMediaTypeFactory}</li>
 * <li>{@link TextMediaTypeFactory}</li>
 * <li>{@link FormMediaTypeFactory}</li>
 * <li>{@link HtmlMediaTypeFactory}</li>
 * </ul>
 * In your sub-classes, overwrite the method {@link #initMedaTypeFactories()},
 * therein calling {@link #addMediaTypeFactory(MediaTypeFactory)} to add (by
 * also invoking super's {@link #initMedaTypeFactories()}) or to set your own
 * (without invoking super's {@link #initMedaTypeFactories()})
 * {@link MediaTypeFactory} instances.
 */
public abstract class AbstractRestfulClient implements RestfulClient {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Map<MediaType, MediaTypeFactory> _mediaTypeFacotries = new LinkedHashMap<>();
	private ExecutorService _executorService;
	private String _userAgent = getClass().getSimpleName() + "@" + Text.REFCODES_ORG.getText();
	private RestRequestHandler _requestHandler = null;
	private BasicAuthCredentials _basicAuthCredentials;
	private OauthToken _oauthToken = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new abstract rest client.
	 *
	 * @param aExecutorService the executor service
	 */
	public AbstractRestfulClient( ExecutorService aExecutorService ) {
		_executorService = ( aExecutorService != null ) ? aExecutorService : ControlFlowUtility.createCachedExecutorService( true );
		initMedaTypeFactories();
	}

	/**
	 * Instantiates a new abstract rest client.
	 */
	public AbstractRestfulClient() {
		this( ControlFlowUtility.createCachedExecutorService( true ) );
	}

	/**
	 * Adds the default {@link MediaTypeFactory} instances. Can be overridden.
	 */
	protected void initMedaTypeFactories() {
		addMediaTypeFactory( new JsonMediaTypeFactory() );
		addMediaTypeFactory( new XmlMediaTypeFactory() );
		addMediaTypeFactory( new FormMediaTypeFactory() );
		addMediaTypeFactory( new TextMediaTypeFactory() );
		addMediaTypeFactory( new HtmlMediaTypeFactory() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BasicAuthCredentials getBasicAuthCredentials() {
		return _basicAuthCredentials;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBasicAuthCredentials( BasicAuthCredentials aBasicAuthCredentials ) {
		_basicAuthCredentials = aBasicAuthCredentials;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setOauthToken( OauthToken aOauthToken ) {
		_oauthToken = aOauthToken;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OauthToken getOauthToken() {
		return _oauthToken;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponse doRequest( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, int aRedirectDepth ) throws HttpResponseException {
		final RestRequestHandler theHandler = _requestHandler;
		if ( theHandler == null ) {
			throw new IllegalStateException( "Unable to process your request with HTTP-Method <" + aHttpMethod + "> for locator <" + aUrl.toLocator() + "> and query fields <" + new VerboseTextBuilder().withElements( aUrl.getQueryFields() ).toString() + "> as no <" + RestRequestHandler.class.getSimpleName() + "> has been registered via <doRestRequest(...)>, aborting!" );
		}
		aHeaderFields = preProcessHeaderFields( aHeaderFields );
		return theHandler.doRequest( new HttpClientRequest( aHttpMethod, aUrl, aHeaderFields, aRequest, aRedirectDepth, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseHandler onResponse( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, int aRedirectDepth, RestResponseConsumer aResponseConsumer ) {
		aHeaderFields = preProcessHeaderFields( aHeaderFields );
		final RestResponseHandler theResponseHandler = new RestResponseHandler( aHttpMethod, aUrl, aHeaderFields, aRequest, aRedirectDepth, aResponseConsumer, this );
		_executorService.execute( new RestResponseHandlerDaemon( theResponseHandler, _requestHandler, this ) );
		return theResponseHandler;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestResponseResult onResponse( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, int aRedirectDepth ) {
		aHeaderFields = preProcessHeaderFields( aHeaderFields );
		final RestResponseResult theResponseResult = new RestResponseResult( aHttpMethod, aUrl, aHeaderFields, aRequest, aRedirectDepth, this );
		_executorService.execute( new RestResponseResultDaemon( theResponseResult, _requestHandler, this ) );
		return theResponseResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestRequestBuilder buildRequest( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, int aRedirectDepth ) {
		aHeaderFields = preProcessHeaderFields( aHeaderFields );
		return new RestRequestBuilder( aHttpMethod, aUrl, aHeaderFields, aRequest, aRedirectDepth, this );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean addMediaTypeFactory( MediaTypeFactory aMediaTypeFactory ) {
		boolean hasAddedAny = false;
		for ( MediaType eMediaType : aMediaTypeFactory.getMediaTypes() ) {
			if ( !_mediaTypeFacotries.containsKey( eMediaType ) ) {
				_mediaTypeFacotries.put( eMediaType, aMediaTypeFactory );
				hasAddedAny = true;
			}
		}
		return hasAddedAny;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MediaTypeFactory toMediaTypeFactory( MediaType aMediaType ) {
		return _mediaTypeFacotries.get( aMediaType );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MediaType[] getFactoryMediaTypes() {
		return _mediaTypeFacotries.keySet().toArray( new MediaType[_mediaTypeFacotries.size()] );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUserAgent() {
		return _userAgent;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setUserAgent( String aUserAgent ) {
		_userAgent = aUserAgent;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INTERCEPTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Invoked to pre-process a {@link HttpClientRequest} alongside a
	 * {@link HttpClientResponse}.
	 *
	 * @param aRequest The {@link HttpClientRequest} to pre-process.
	 * @param aResponse The {@link HttpClientResponse} to post-process.
	 */
	protected void preIntercept( HttpClientRequest aRequest, HttpClientResponse aResponse ) {}

	/**
	 * Invoked to post-process a {@link HttpClientRequest} alongside a
	 * {@link HttpClientResponse}.
	 *
	 * @param aRequest The {@link HttpClientRequest} to post-process.
	 * @param aResponse The {@link HttpClientResponse} to post-process.
	 */
	protected void postIntercept( HttpClientRequest aRequest, HttpClientResponse aResponse ) {}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Sets the hook receiving a prepared {@link RestResponseHandler} instance
	 * to be used to do the technical request with the technology chosen by the
	 * implementing sub-class.
	 * 
	 * @param aHandler The {@link RestRequestHandler} for handling the HTTP
	 *        request.
	 */
	protected void onRestRequest( RestRequestHandler aHandler ) {
		_requestHandler = aHandler;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Pre-processes the given header fields before the HTTP-Request is issued.
	 * By default the User-Agent is set and (if present) the OAuth token.
	 *
	 * @param aHeaderFields The {@link HeaderFields} to be pre-prcessed.
	 * 
	 * @return the request header fields
	 */
	protected RequestHeaderFields preProcessHeaderFields( RequestHeaderFields aHeaderFields ) {
		if ( _userAgent != null && ( aHeaderFields == null || aHeaderFields.getUserAgent() == null ) ) {
			aHeaderFields = aHeaderFields != null ? aHeaderFields : new RequestHeaderFields();
			aHeaderFields.putUserAgent( _userAgent );
		}
		if ( _oauthToken != null && ( aHeaderFields == null || aHeaderFields.getAuthorization() == null ) ) {
			aHeaderFields = aHeaderFields != null ? aHeaderFields : new RequestHeaderFields();
			aHeaderFields.putAuthorization( AuthType.BEARER, _oauthToken.getAccessToken() );
		}
		if ( _basicAuthCredentials != null && ( aHeaderFields == null || aHeaderFields.getBasicAuthCredentials() == null ) ) {
			aHeaderFields = aHeaderFields != null ? aHeaderFields : new RequestHeaderFields();
			aHeaderFields.putBasicAuthCredentials( _basicAuthCredentials );
		}
		return aHeaderFields;
	}
}
