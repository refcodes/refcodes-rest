// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

/**
 * Provides access to a {@link HttpExceptionHandling} property.
 */
public interface HttpExceptionHandlingAccessor {

	/**
	 * Retrieves the {@link HttpExceptionHandling} from the
	 * {@link HttpExceptionHandling} property.
	 * 
	 * @return The {@link HttpExceptionHandling} stored by the
	 *         {@link HttpExceptionHandling} property.
	 */
	HttpExceptionHandling getHttpExceptionHandling();

	/**
	 * Extends the {@link HttpExceptionHandlingAccessor} with a setter method.
	 */
	public interface HttpExceptionHandlingMutator {

		/**
		 * Sets the {@link HttpExceptionHandling} for the
		 * {@link HttpExceptionHandling} property.
		 * 
		 * @param aHttpExceptionHandling The {@link HttpExceptionHandling} to be
		 *        stored by the {@link HttpExceptionHandling} property.
		 */
		void setHttpExceptionHandling( HttpExceptionHandling aHttpExceptionHandling );
	}

	/**
	 * Provides a builder method for a {@link HttpExceptionHandling} property
	 * returning the builder for applying multiple build operations.
	 *
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface HttpExceptionHandlingBuilder<B extends HttpExceptionHandlingBuilder<B>> {

		/**
		 * Sets the {@link HttpExceptionHandling} for the
		 * {@link HttpExceptionHandling} property.
		 * 
		 * @param aHttpExceptionHandling The {@link HttpExceptionHandling} to be
		 *        stored by the {@link HttpExceptionHandling} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withHttpExceptionHandling( HttpExceptionHandling aHttpExceptionHandling );
	}

	/**
	 * Extends the {@link HttpExceptionHandlingAccessor} with a setter method.
	 */
	public interface HttpExceptionHandlingProperty extends HttpExceptionHandlingAccessor, HttpExceptionHandlingMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link HttpExceptionHandling} (setter) as of
		 * {@link #setHttpExceptionHandling(HttpExceptionHandling)} and returns
		 * the very same value (getter).
		 * 
		 * @param aHttpExceptionHandling The {@link HttpExceptionHandling} to
		 *        set (via
		 *        {@link #setHttpExceptionHandling(HttpExceptionHandling)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default HttpExceptionHandling letHttpExceptionHandling( HttpExceptionHandling aHttpExceptionHandling ) {
			setHttpExceptionHandling( aHttpExceptionHandling );
			return aHttpExceptionHandling;
		}
	}
}
