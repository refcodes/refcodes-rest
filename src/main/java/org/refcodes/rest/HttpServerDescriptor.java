// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.mixin.AliasAccessor;
import org.refcodes.mixin.PortAccessor;
import org.refcodes.net.IpAddressAccessor;
import org.refcodes.web.HostAccessor;
import org.refcodes.web.Url;
import org.refcodes.web.VirtualHostAccessor.VirtualHostBuilder;
import org.refcodes.web.VirtualHostAccessor.VirtualHostProperty;

/**
 * The {@link HttpServerDescriptor} describes a server to be registered at a
 * discovery registry so clients can resolve the server's URL.
 */
public interface HttpServerDescriptor extends AliasAccessor, PingUrlAccessor, PortAccessor, HostAccessor, IpAddressAccessor {

	/**
	 * The {@link HttpServerDescriptorBuilder} interface extends the
	 * {@link HttpServerDescriptor} interface with builder functionality as of
	 * the builder pattern.
	 *
	 * @param <B> the generic type
	 */
	public static interface HttpServerDescriptorBuilder<B extends HttpServerDescriptorBuilder<B>> extends HttpServerDescriptor, IpAddressProperty, IpAddressBuilder<B>, HostProperty, HostBuilder<B>, VirtualHostProperty, VirtualHostBuilder<B>, PortProperty, PortBuilder<B>, PingUrlProperty, PingUrlBuilder<B>, AliasProperty, AliasBuilder<B> {

		/**
		 * {@inheritDoc}
		 */
		@SuppressWarnings("unchecked")
		@Override
		default B withAlias( String aAlias ) {
			setAlias( aAlias );
			return (B) this;
		}

		/**
		 * {@inheritDoc}
		 */
		@SuppressWarnings("unchecked")
		@Override
		default B withIpAddress( int[] aIpAddress ) {
			setIpAddress( aIpAddress );
			return (B) this;
		}

		/**
		 * {@inheritDoc}
		 */
		@SuppressWarnings("unchecked")
		@Override
		default B withPingUrl( Url aUrl ) {
			setPingUrl( aUrl );
			return (B) this;
		}

		/**
		 * {@inheritDoc}
		 */
		@SuppressWarnings("unchecked")
		@Override
		default B withCidrNotation( String aCidrNotation ) {
			fromCidrNotation( aCidrNotation );
			return (B) this;
		}

		/**
		 * {@inheritDoc}
		 */
		@SuppressWarnings("unchecked")
		@Override
		default B withPort( int aPort ) {
			setPort( aPort );
			return (B) this;
		}

		/**
		 * {@inheritDoc}
		 */
		@SuppressWarnings("unchecked")
		@Override
		default B withHost( String aHost ) {
			setHost( aHost );
			return (B) this;
		}

		/**
		 * {@inheritDoc}
		 */
		@SuppressWarnings("unchecked")
		@Override
		default B withVirtualHost( String aVirtualHost ) {
			setVirtualHost( aVirtualHost );
			return (B) this;
		}
	}
}
