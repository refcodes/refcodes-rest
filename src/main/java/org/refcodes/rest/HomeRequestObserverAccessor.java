// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

/**
 * Provides an accessor for a home {@link RestRequestConsumer} property.
 */
public interface HomeRequestObserverAccessor {

	/**
	 * Retrieves the home {@link RestRequestConsumer} from the home
	 * {@link RestRequestConsumer} property.
	 * 
	 * @return The home {@link RestRequestConsumer} stored by the home
	 *         {@link RestRequestConsumer} property.
	 */
	RestRequestConsumer getHomeRequestConsumer();

	/**
	 * Provides a mutator for a home {@link RestRequestConsumer} property.
	 */
	public interface HomeRequestObserverMutator {

		/**
		 * Sets the home {@link RestRequestConsumer} for the home
		 * {@link RestRequestConsumer} property.
		 * 
		 * @param aHomeRequestObserver The home {@link RestRequestConsumer} to
		 *        be stored by the home {@link RestRequestConsumer} property.
		 */
		void setHomeRequestObserver( RestRequestConsumer aHomeRequestObserver );
	}

	/**
	 * Provides a builder method for a home {@link RestRequestConsumer} property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface HomeRequestObserverBuilder<B extends HomeRequestObserverBuilder<B>> {

		/**
		 * Sets the home {@link RestRequestConsumer} for the home
		 * {@link RestRequestConsumer} property.
		 * 
		 * @param aRequestConsumer The home {@link RestRequestConsumer} to be
		 *        stored by the home {@link RestRequestConsumer} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withHomeRequestConsumer( RestRequestConsumer aRequestConsumer );
	}

	/**
	 * Provides a home {@link RestRequestConsumer} property.
	 */
	public interface HomeRequestObserverProperty extends HomeRequestObserverAccessor, HomeRequestObserverMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link RestRequestConsumer} (setter) as of
		 * {@link #setHomeRequestObserver(RestRequestConsumer)} and returns the
		 * very same value (getter).
		 * 
		 * @param aHomeRequestObserver The {@link RestRequestConsumer} to set
		 *        (via {@link #setHomeRequestObserver(RestRequestConsumer)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default RestRequestConsumer letHomeRequestObserver( RestRequestConsumer aHomeRequestObserver ) {
			setHomeRequestObserver( aHomeRequestObserver );
			return aHomeRequestObserver;
		}
	}
}
