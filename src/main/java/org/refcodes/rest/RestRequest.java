// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.io.InputStream;

import org.refcodes.mixin.Dumpable;
import org.refcodes.textual.VerboseTextBuilder;
import org.refcodes.web.HeaderFields;
import org.refcodes.web.HeaderFieldsAccessor.HeaderFieldsProperty;
import org.refcodes.web.HttpClientRequest;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpResponseException;
import org.refcodes.web.HttpStatusCode;
import org.refcodes.web.RequestHeaderFields;
import org.refcodes.web.Url;

/**
 * A {@link RestRequest} describes a REST request and the {@link RestResponse}
 * providing the response. The {@link RestRequest} describes the
 * {@link HttpMethod} to be used, the targeted URL, the {@link HeaderFields} as
 * well as the Query-Fields and the request {@link Object} or
 * {@link InputStream}.
 */
public class RestRequest extends HttpClientRequest implements HeaderFieldsProperty<RequestHeaderFields>, Dumpable {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private RestfulClient _restClient;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new rest request builder impl.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields the Header-Fields
	 * @param aRequest the request
	 * @param aRestClient the rest client
	 */
	protected RestRequest( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, RestfulClient aRestClient ) {
		this( aHttpMethod, aUrl instanceof org.refcodes.web.UrlBuilder ? aUrl : new org.refcodes.web.UrlBuilder( aUrl ), aHeaderFields, aRequest, HttpClientRequest.DEFAULT_REDIRECT_DEPTH, aRestClient );
	}

	/**
	 * Instantiates a new rest request builder impl.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRestClient the rest client
	 */
	protected RestRequest( HttpMethod aHttpMethod, Url aUrl, RestfulClient aRestClient ) {
		this( aHttpMethod, aUrl, null, null, aRestClient );
	}

	/**
	 * Instantiates a new rest request builder impl.
	 *
	 * @param aRestClient the rest client
	 */
	protected RestRequest( RestfulClient aRestClient ) {
		this( null, null, null, null, aRestClient );
	}

	/**
	 * Instantiates a new rest request builder impl.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest the request
	 * @param aRestClient the rest client
	 */
	protected RestRequest( HttpMethod aHttpMethod, Url aUrl, Object aRequest, RestfulClient aRestClient ) {
		this( aHttpMethod, aUrl, null, aRequest, aRestClient );
	}

	/**
	 * Instantiates a new rest request builder impl.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRedirectDepth The redirect depth provides the count of
	 *        HTTP-Request and HTTP-Response cycles where the response
	 *        represents a redirect as of
	 *        {@link HttpStatusCode#isRedirectStatus()}. A value of -1
	 *        represents the default behavior, e.g. using
	 *        {@link java.net.HttpURLConnection}'s redirection means.
	 * @param aRestClient the rest client
	 */
	protected RestRequest( HttpMethod aHttpMethod, Url aUrl, int aRedirectDepth, RestfulClient aRestClient ) {
		this( aHttpMethod, aUrl, null, null, aRedirectDepth, aRestClient );
	}

	/**
	 * Instantiates a new rest request builder impl.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aRequest the request
	 * @param aRedirectDepth The redirect depth provides the count of
	 *        HTTP-Request and HTTP-Response cycles where the response
	 *        represents a redirect as of
	 *        {@link HttpStatusCode#isRedirectStatus()}. A value of -1
	 *        represents the default behavior, e.g. using
	 *        {@link java.net.HttpURLConnection}'s redirection means.
	 * @param aRestClient the rest client
	 */
	protected RestRequest( HttpMethod aHttpMethod, Url aUrl, Object aRequest, int aRedirectDepth, RestfulClient aRestClient ) {
		this( aHttpMethod, aUrl, null, aRequest, aRedirectDepth, aRestClient );
	}

	/**
	 * Instantiates a new rest request builder impl.
	 *
	 * @param aHttpMethod the http method
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields the Header-Fields
	 * @param aRequest the request
	 * @param aRedirectDepth The redirect depth provides the count of
	 *        HTTP-Request and HTTP-Response cycles where the response
	 *        represents a redirect as of
	 *        {@link HttpStatusCode#isRedirectStatus()}. A value of -1
	 *        represents the default behavior, e.g. using
	 *        {@link java.net.HttpURLConnection}'s redirection means.
	 * @param aRestClient the rest client
	 */
	protected RestRequest( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields, Object aRequest, int aRedirectDepth, RestfulClient aRestClient ) {
		super( aHttpMethod, aUrl instanceof UrlBuilder ? aUrl : new org.refcodes.web.UrlBuilder( aUrl ), aHeaderFields, aRequest, aRedirectDepth, aRestClient );
		_restClient = aRestClient;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * This is a convenience method for easily instantiating the according
	 * builder.
	 * 
	 * @param aRestClient the rest client
	 * 
	 * @return an instance (using a public implementation) of this builder
	 */
	public RestRequest build( RestfulClient aRestClient ) {
		return new RestRequest( aRestClient );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.refcodes.web.UrlBuilder getUrl() {
		return (org.refcodes.web.UrlBuilder) _url;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpMethod getHttpMethod() {
		return _httpMethod;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + _httpMethod + ": " + _url.toHttpUrl() + "?" + new VerboseTextBuilder().withElements( _url.getQueryFields() ).toString() + ")@" + hashCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHeaderFields( RequestHeaderFields aHeaderFields ) {
		_headerFields = aHeaderFields;
	}

	/**
	 * Sends the request and returns synchronously the according
	 * {@link RestResponse}.
	 *
	 * @return the rest response
	 * 
	 * @throws HttpResponseException the http response exception
	 */
	public RestResponse toRestResponse() throws HttpResponseException {
		return _restClient.doRequest( getHttpMethod(), getUrl(), getHeaderFields(), getRequest(), getRedirectDepth() );
	}
}
