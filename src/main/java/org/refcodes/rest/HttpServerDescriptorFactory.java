// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.data.Scheme;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.web.Url;

/**
 * The {@link HttpServerDescriptorFactory} provides factory functionality for
 * creating {@link HttpServerDescriptor} instances.
 * 
 * @param <DESC> The type of the server discovery descriptor (the object
 *        describing your service and locating the service registry).
 */
public interface HttpServerDescriptorFactory<DESC extends HttpServerDescriptor> {

	// /////////////////////////////////////////////////////////////////////////
	// TWEAKS:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * Prepares the {@link HttpServerDescriptor} by creating it from this
	 * instance's state and the provided arguments. The provided arguments can
	 * modify the instance's state. The {@link HttpServerDescriptor} as finally
	 * used is returned. You may modify this context and use it after
	 * modification to initialize the according server via
	 * {@link HttpRegistrySidecar#initialize(HttpServerDescriptor, Url)} or
	 * {@link HttpRegistrySidecar#initialize(HttpServerDescriptor, Url, TrustStoreDescriptor)}.
	 *
	 * @return The {@link HttpServerDescriptor} as would be used when
	 *         initializing the according server via
	 *         {@link HttpRegistrySidecar#initialize()}
	 */
	default DESC toHttpServerDescriptor() {
		return toHttpServerDescriptor( null, null, null, null, null, null, -1, null );
	};

	/**
	 * Prepares the {@link HttpServerDescriptor} by creating it from this
	 * instance's state and the provided arguments. The provided arguments can
	 * modify the instance's state. The {@link HttpServerDescriptor} as finally
	 * used is returned. You may modify this context and use it after
	 * modification to initialize the according server via
	 * {@link HttpRegistrySidecar#initialize(HttpServerDescriptor, Url)} or
	 * {@link HttpRegistrySidecar#initialize(HttpServerDescriptor, Url, TrustStoreDescriptor)}.
	 * 
	 * @param aAlias The name ("alias") which identifies the server in the
	 *        registry.
	 * @param aInstanceId The TID for the instance when being registered at the
	 *        service registry. If omitted, then the host name is used.
	 * @param aScheme The {@link Scheme} to which this server is being attached
	 *        (HTTP or HTTPS).
	 * @param aPort The port of your service being registered.
	 *
	 * @return The {@link HttpServerDescriptor} as would be used when
	 *         initializing the according server via
	 *         {@link HttpRegistrySidecar#initialize()}
	 */
	default DESC toHttpServerDescriptor( String aAlias, String aInstanceId, Scheme aScheme, int aPort ) {
		return toHttpServerDescriptor( aAlias, aInstanceId, aScheme, null, null, null, aPort, null );
	}

	/**
	 * Prepares the {@link HttpServerDescriptor} by creating it from this
	 * instance's state and the provided arguments. The provided arguments can
	 * modify the instance's state. The {@link HttpServerDescriptor} as finally
	 * used is returned. You may modify this context and use it after
	 * modification to initialize the according server via
	 * {@link HttpRegistrySidecar#initialize(HttpServerDescriptor, Url)} or
	 * {@link HttpRegistrySidecar#initialize(HttpServerDescriptor, Url, TrustStoreDescriptor)}.
	 * 
	 * @param aAlias The name ("alias") which identifies the server in the
	 *        registry.
	 * @param aScheme The {@link Scheme} to which this server is being attached
	 *        (HTTP or HTTPS).
	 * @param aHost The host name to be used to address this server. If omitted,
	 *        then the system's host name should be used.
	 * @param aPort The port of your service being registered. Make sure, you do
	 *        not
	 *
	 * @return The {@link HttpServerDescriptor} as would be used when
	 *         initializing the according server via
	 *         {@link HttpRegistrySidecar#initialize()}
	 */
	default DESC toHttpServerDescriptor( String aAlias, Scheme aScheme, String aHost, int aPort ) {
		return toHttpServerDescriptor( aAlias, null, aScheme, aHost, null, null, aPort, null );
	}

	/**
	 * Prepares the {@link HttpServerDescriptor} by creating it from this
	 * instance's state and the provided arguments. The provided arguments can
	 * modify the instance's state. The {@link HttpServerDescriptor} as finally
	 * used is returned. You may modify this context and use it after
	 * modification to initialize the according server via
	 * {@link HttpRegistrySidecar#initialize(HttpServerDescriptor, Url)} or
	 * {@link HttpRegistrySidecar#initialize(HttpServerDescriptor, Url, TrustStoreDescriptor)}.
	 * {@inheritDoc}
	 * 
	 * @param aAlias The name ("alias") which identifies the server in the
	 *        registry.
	 * @param aScheme The {@link Scheme} to which this server is being attached
	 *        (HTTP or HTTPS).
	 * @param aHost The host name to be used to address this server. If omitted,
	 *        then the system's host name should be used.
	 * @param aVirtualHost The virtual host name to be used for resolving.
	 * @param aIpAddress The IP-Address identifying the host.
	 * @param aPort The port of your service being registered. Make sure, you do
	 *        not
	 * @param aPingPath The path to use as health-check end-point by this
	 *        server.
	 *
	 * @return The {@link HttpServerDescriptor} as would be used when
	 *         initializing the according server via
	 *         {@link HttpRegistrySidecar#initialize()}
	 */
	default DESC toHttpServerDescriptor( String aAlias, Scheme aScheme, String aHost, String aVirtualHost, int[] aIpAddress, int aPort, String aPingPath ) {
		return toHttpServerDescriptor( aAlias, null, aScheme, aHost, aVirtualHost, aIpAddress, aPort, aPingPath );
	}

	/**
	 * Prepares the {@link HttpServerDescriptor} by creating it from this
	 * instance's state and the provided arguments. The provided arguments can
	 * modify the instance's state. The {@link HttpServerDescriptor} as finally
	 * used is returned. You may modify this context and use it after
	 * modification to initialize the according server via
	 * {@link HttpRegistrySidecar#initialize(HttpServerDescriptor, Url)} or
	 * {@link HttpRegistrySidecar#initialize(HttpServerDescriptor, Url, TrustStoreDescriptor)}.
	 * {@inheritDoc}
	 * 
	 * @param aAlias The name ("alias") which identifies the server in the
	 *        registry.
	 * @param aInstanceId The TID for the instance when being registered at the
	 *        service registry. If omitted, then the host name is used.
	 * @param aScheme The {@link Scheme} to which this server is being attached
	 *        (HTTP or HTTPS).
	 * @param aHost The host name to be used to address this server. If omitted,
	 *        then the system's host name should be used.
	 * @param aVirtualHost The virtual host name to be used for resolving.
	 * @param aIpAddress The IP-Address identifying the host.
	 * @param aPort The port of your service being registered. Make sure, you do
	 *        not
	 * @param aPingPath The path to use as health-check end-point by this
	 *        server.
	 * 
	 * @return The {@link HttpServerDescriptor} as would be used when
	 *         initializing the according server via
	 *         {@link HttpRegistrySidecar#initialize()}
	 */
	DESC toHttpServerDescriptor( String aAlias, String aInstanceId, Scheme aScheme, String aHost, String aVirtualHost, int[] aIpAddress, int aPort, String aPingPath );

}