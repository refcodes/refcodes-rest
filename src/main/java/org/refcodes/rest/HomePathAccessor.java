// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

/**
 * Provides an accessor for a home path property.
 */
public interface HomePathAccessor {

	/**
	 * Retrieves the home path from the home path property.
	 * 
	 * @return The home path stored by the home path property.
	 */
	String getHomePath();

	/**
	 * Provides a mutator for a home path property.
	 */
	public interface HomePathMutator {

		/**
		 * Sets the home path for the home path property.
		 * 
		 * @param aHomePath The home path to be stored by the home path
		 *        property.
		 */
		void setHomePath( String aHomePath );
	}

	/**
	 * Provides a mutator for an home path property.
	 * 
	 * @param <B> The builder which implements the {@link HomePathBuilder}.
	 */
	public interface HomePathBuilder<B extends HomePathBuilder<?>> {

		/**
		 * Sets the home path to use and returns this builder as of the builder
		 * pattern.
		 * 
		 * @param aHomePath The home path to be stored by the home path
		 *        property.
		 * 
		 * @return This {@link HomePathBuilder} instance to continue
		 *         configuration.
		 */
		B withHomePath( String aHomePath );
	}

	/**
	 * Provides a home path property.
	 */
	public interface HomePathProperty extends HomePathAccessor, HomePathMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setHomePath(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aHomePath The {@link String} to set (via
		 *        {@link #setHomePath(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letHomePath( String aHomePath ) {
			setHomePath( aHomePath );
			return aHomePath;
		}
	}
}
