// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.web.Url;

/**
 * Provides an accessor for a service discovery discovery {@link Url} property.
 */
public interface HttpDiscoveryUrlAccessor {

	/**
	 * Retrieves the URL from the URL property locating the service discovery
	 * discovery.
	 * 
	 * @return The URL stored by the URL property.
	 */
	Url getHttpDiscoveryUrl();

	/**
	 * Provides a mutator for a service discovery discovery URL property.
	 */
	public interface HttpDiscoveryUrlMutator {
		/**
		 * Sets the URL for the URL property locating the service discovery
		 * discovery.
		 * 
		 * @param aUrl The service discovery discovery URL to be stored by the
		 *        URL property.
		 */
		void setHttpDiscoveryUrl( Url aUrl );
	}

	/**
	 * Provides a builder method for a service discovery discovery URL property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface HttpDiscoveryUrlBuilder<B extends HttpDiscoveryUrlBuilder<B>> {

		/**
		 * Sets the URL for the URL property locating the service discovery
		 * discovery.
		 * 
		 * @param aUrl The service discovery discovery URL to be stored by the
		 *        URL property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withHttpDiscoveryUrl( Url aUrl );
	}

	/**
	 * Provides a service discovery discovery URL property.
	 */
	public interface HttpDiscoveryUrlProperty extends HttpDiscoveryUrlAccessor, HttpDiscoveryUrlMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Url} (setter) as
		 * of {@link #setHttpDiscoveryUrl(Url)} and returns the very same value
		 * (getter).
		 * 
		 * @param aUrl The {@link Url} to set (via
		 *        {@link #setHttpDiscoveryUrl(Url)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Url letHttpDiscoveryUrl( Url aUrl ) {
			setHttpDiscoveryUrl( aUrl );
			return aUrl;
		}
	}
}
