// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.component.InitializeException;
import org.refcodes.component.LifecycleMachine.ManualLifecycleMachine;
import org.refcodes.component.LifecycleStatus;
import org.refcodes.component.PauseException;
import org.refcodes.component.ResumeException;
import org.refcodes.component.StartException;
import org.refcodes.component.StopException;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.web.LoadBalancingStrategy;
import org.refcodes.web.Url;

/**
 * Abstract class for easily decorating a {@link RestfulHttpDiscoveryClient}.
 *
 * @param <B> the generic type
 */
public abstract class AbstractRestfulHttpDiscoveryClientDecorator<B extends RestfulHttpDiscoveryClient<B>> extends AbstractRestfulHttpClientDecorator<B> implements RestfulHttpDiscoveryClient<B> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected ManualLifecycleMachine _lifeCycleAutomaton = new ManualLifecycleMachine();
	private LoadBalancingStrategy _strategy = LoadBalancingStrategy.RANDOM;
	private Url _serviceDiscoveryUrl;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the given {@link RestfulHttpClient} with discovery
	 * functionality.
	 * 
	 * @param aClient The {@link RestfulHttpClient} to be decorated.
	 */
	public AbstractRestfulHttpDiscoveryClientDecorator( RestfulHttpClient aClient ) {
		super( aClient );
	}

	// /////////////////////////////////////////////////////////////////////////
	// LIFECYCLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isRunning() {
		return _lifeCycleAutomaton.isRunning();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isInitialized() {
		return _lifeCycleAutomaton.isInitialized();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LifecycleStatus getLifecycleStatus() {
		return _lifeCycleAutomaton.getLifecycleStatus();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void pause() throws PauseException {
		_lifeCycleAutomaton.pause();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop() throws StopException {
		_lifeCycleAutomaton.stop();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() throws StartException {
		_lifeCycleAutomaton.start();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void resume() throws ResumeException {
		_lifeCycleAutomaton.resume();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize() throws InitializeException {
		_lifeCycleAutomaton.initialize();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStoppable() {
		return _lifeCycleAutomaton.isStoppable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isPausable() {
		return _lifeCycleAutomaton.isPausable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStartable() {
		return _lifeCycleAutomaton.isStartable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isResumable() {
		return _lifeCycleAutomaton.isResumable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStopped() {
		return _lifeCycleAutomaton.isStopped();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isPaused() {
		return _lifeCycleAutomaton.isPaused();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isInitalizable() {
		return _lifeCycleAutomaton.isInitalizable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		_lifeCycleAutomaton.destroy();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isDestroyable() {
		return _lifeCycleAutomaton.isDestroyable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isDestroyed() {
		return _lifeCycleAutomaton.isDestroyed();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public abstract Url toUrl( Url aUrl );

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setLoadBalancingStrategy( LoadBalancingStrategy aStrategy ) {
		_strategy = aStrategy;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoadBalancingStrategy getLoadBalancingStrategy() {
		return _strategy;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getHttpDiscoveryUrl() {
		return _serviceDiscoveryUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpDiscoveryUrl( Url aUrl ) {
		_serviceDiscoveryUrl = aUrl;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * To trust store descriptor.
	 *
	 * @param aStoreDescriptor the store descriptor
	 * 
	 * @return the trust store descriptor
	 */
	protected TrustStoreDescriptor toTrustStoreDescriptor( TrustStoreDescriptor aStoreDescriptor ) {
		if ( aStoreDescriptor != null ) {
			setTrustStoreDescriptor( aStoreDescriptor );
		}
		else {
			aStoreDescriptor = getTrustStoreDescriptor();
		}
		return aStoreDescriptor;
	}

	/**
	 * To http discovery url.
	 *
	 * @param aDiscoveryUrl the discovery url
	 * 
	 * @return the url
	 */
	protected Url toHttpDiscoveryUrl( Url aDiscoveryUrl ) {
		if ( aDiscoveryUrl != null ) {
			setHttpDiscoveryUrl( aDiscoveryUrl );
		}
		else {
			aDiscoveryUrl = getHttpDiscoveryUrl();
		}
		return aDiscoveryUrl;
	}

	/**
	 * To load balancer strategy.
	 *
	 * @param aStrategy the strategy
	 * 
	 * @return the load balancing strategy
	 */
	protected LoadBalancingStrategy toLoadBalancerStrategy( LoadBalancingStrategy aStrategy ) {
		if ( aStrategy != null ) {
			setLoadBalancingStrategy( aStrategy );
		}
		else {
			aStrategy = getLoadBalancingStrategy();
		}
		return aStrategy;
	}
}
