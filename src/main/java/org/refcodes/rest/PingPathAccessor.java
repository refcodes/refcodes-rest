// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

/**
 * Provides an accessor for a ping path property.
 */
public interface PingPathAccessor {

	/**
	 * Retrieves the ping path from the ping path property.
	 * 
	 * @return The ping path stored by the ping path property.
	 */
	String getPingPath();

	/**
	 * Provides a mutator for a ping path property.
	 */
	public interface PingPathMutator {

		/**
		 * Sets the ping path for the ping path property.
		 * 
		 * @param aPingPath The ping path to be stored by the ping path
		 *        property.
		 */
		void setPingPath( String aPingPath );
	}

	/**
	 * Provides a mutator for an ping path property.
	 * 
	 * @param <B> The builder which implements the {@link PingPathBuilder}.
	 */
	public interface PingPathBuilder<B extends PingPathBuilder<?>> {

		/**
		 * Sets the ping path to use and returns this builder as of the builder
		 * pattern.
		 * 
		 * @param aPingPath The ping path to be stored by the ping path
		 *        property.
		 * 
		 * @return This {@link PingPathBuilder} instance to continue
		 *         configuration.
		 */
		B withPingPath( String aPingPath );
	}

	/**
	 * Provides a ping path property.
	 */
	public interface PingPathProperty extends PingPathAccessor, PingPathMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setPingPath(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aPingPath The {@link String} to set (via
		 *        {@link #setPingPath(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letPingPath( String aPingPath ) {
			setPingPath( aPingPath );
			return aPingPath;
		}
	}
}
