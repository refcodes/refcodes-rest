// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import java.net.MalformedURLException;
import java.net.URL;

import org.refcodes.component.Configurable;
import org.refcodes.component.InitializeException;
import org.refcodes.component.LifecycleComponent.LifecycleAutomaton;
import org.refcodes.component.PauseException;
import org.refcodes.component.ResumeException;
import org.refcodes.component.StartException;
import org.refcodes.component.StopException;
import org.refcodes.data.Scheme;
import org.refcodes.rest.HttpDiscoveryUrlAccessor.HttpDiscoveryUrlBuilder;
import org.refcodes.rest.HttpDiscoveryUrlAccessor.HttpDiscoveryUrlProperty;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.web.FormFields;
import org.refcodes.web.LoadBalancingStrategy;
import org.refcodes.web.LoadBalancingStrategyAccessor.LoadBalancingStrategyBuilder;
import org.refcodes.web.LoadBalancingStrategyAccessor.LoadBalancingStrategyProperty;
import org.refcodes.web.Url;

/**
 * The {@link HttpDiscovery} describes the functionality required in order to
 * discover a service at a service discovery and discovery service. This type is
 * intended to be used by different separate hierarchy branches by providing the
 * generic type &lt;B&gt;, ensuring a coherent type hierarchy for each branch.
 *
 * @param <B> In order to implement the builder pattern with a coherent type
 *        hierarchy.
 */
public interface HttpDiscovery<B extends HttpDiscovery<B>> extends LoadBalancingStrategyProperty, LoadBalancingStrategyBuilder<B>, Configurable<HttpDiscoveryContext>, LifecycleAutomaton, HttpDiscoveryUrlProperty, HttpDiscoveryUrlBuilder<B> {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withLoadBalancingStrategy( LoadBalancingStrategy aStrategy ) {
		setLoadBalancingStrategy( aStrategy );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withHttpDiscoveryUrl( Url aUrl ) {
		setHttpDiscoveryUrl( aUrl );
		return (B) this;
	}

	/**
	 * Resolves the server's alias from the provided {@link Url} by an actual
	 * host and returns the accordingly resolved {@link Url}.
	 * 
	 * @param aUrl The {@link Url} for the request.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	Url toUrl( Url aUrl );

	/**
	 * Resolves the server's alias from the provided {@link String} by an actual
	 * host and returns the accordingly resolved {@link Url}.
	 * 
	 * @param aUrl The URL {@link String} to be parsed. The URL consists of the
	 *        scheme (protocol), the identify and the secret (optional), the
	 *        host as well as an optional port and the (optional) path.
	 * 
	 * @return The tinkered {@link Url}.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default Url toUrl( String aUrl ) throws MalformedURLException {
		return toUrl( new Url( aUrl ) );
	}

	/**
	 * Resolves the server's alias from the provided {@link String} by an actual
	 * host and returns the accordingly resolved {@link Url}.
	 * 
	 * @param aUrl The URL {@link String} to be parsed. The URL consists of the
	 *        scheme (protocol), the identify and the secret (optional), the
	 *        host as well as an optional port and the (optional) path.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The tinkered {@link Url}.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default Url toUrl( String aUrl, FormFields aQueryFields ) throws MalformedURLException {
		return toUrl( new Url( aUrl, aQueryFields ) );
	}

	/**
	 * Resolves the server's alias from the provided {@link String} by an actual
	 * host and returns the accordingly resolved {@link Url}.
	 * 
	 * @param aUrl The URL {@link String} to be parsed. The URL consists of the
	 *        scheme (protocol), the identify and the secret (optional), the
	 *        host as well as an optional port and the (optional) path.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aFragment The fragment to be set.
	 * 
	 * @return The tinkered {@link Url}.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	default Url toUrl( String aUrl, FormFields aQueryFields, String aFragment ) throws MalformedURLException {
		return toUrl( new Url( aUrl, aQueryFields, aFragment ) );
	}

	/**
	 * Constructs an {@link Url} from the provided {@link URL} instance.
	 * 
	 * @param aURL The {@link URL} to be used.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( URL aURL ) {
		return toUrl( new Url( aURL ) );
	}

	/**
	 * Constructs an {@link Url} with the common attributes.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( Scheme aScheme, String aHost ) {
		return toUrl( new Url( aScheme, aHost ) );
	}

	/**
	 * Constructs an {@link Url} with the common attributes.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( Scheme aScheme, String aHost, int aPort ) {
		return toUrl( new Url( aScheme, aHost, aPort ) );
	}

	/**
	 * Constructs an {@link Url} with the common attributes.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( Scheme aScheme, String aHost, int aPort, String aPath ) {
		return toUrl( new Url( aScheme, aHost, aPort, aPath ) );
	}

	/**
	 * Constructs an {@link Url} with the common attributes.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		return toUrl( new Url( aScheme, aHost, aPort, aPath, aQueryFields ) );
	}

	/**
	 * Constructs an {@link Url} with the common attributes.
	 *
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aFragment The fragment to be set.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, String aFragment ) {
		return toUrl( new Url( aScheme, aHost, aPort, aPath, aQueryFields, aFragment ) );
	}

	/**
	 * Constructs an {@link Url} with the common attributes.
	 * 
	 * @param aProtocol The protocol {@link String} (e.g. "http://" or
	 *        "https://") to be used for the destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( String aProtocol, String aHost ) {
		return toUrl( new Url( aProtocol, aHost ) );
	}

	/**
	 * Constructs an {@link Url} with the common attributes.
	 * 
	 * @param aProtocol The protocol {@link String} (e.g. "http://" or
	 *        "https://") to be used for the destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( String aProtocol, String aHost, int aPort ) {
		return toUrl( new Url( aProtocol, aHost, aPort ) );
	}

	/**
	 * Constructs an {@link Url} with the common attributes.
	 * 
	 * @param aProtocol The protocol {@link String} (e.g. "http://" or
	 *        "https://") to be used for the destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( String aProtocol, String aHost, int aPort, String aPath ) {
		return toUrl( new Url( aProtocol, aHost, aPort, aPath ) );
	}

	/**
	 * Constructs an {@link Url} with the common attributes.
	 * 
	 * @param aProtocol The protocol {@link String} (e.g. "http://" or
	 *        "https://") to be used for the destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		return toUrl( new Url( aProtocol, aHost, aPort, aPath, aQueryFields ) );
	}

	/**
	 * Constructs an {@link Url} with the common attributes.
	 *
	 * @param aProtocol The protocol {@link String} (e.g. "http://" or
	 *        "https://") to be used for the destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aFragment The fragment to be set.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, String aFragment ) {
		return toUrl( new Url( aProtocol, aHost, aPort, aPath, aQueryFields, aFragment ) );
	}

	/**
	 * Constructs an {@link Url} with the common attributes.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( Scheme aScheme, String aHost, String aPath ) {
		return toUrl( new Url( aScheme, aHost, aPath ) );
	}

	/**
	 * Constructs an {@link Url} with the common attributes.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields ) {
		return toUrl( new Url( aScheme, aHost, aPath, aQueryFields ) );
	}

	/**
	 * Constructs an {@link Url} with the common attributes.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aFragment The fragment to be set.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, String aFragment ) {
		return toUrl( new Url( aScheme, aHost, aPath, aQueryFields, aFragment ) );
	}

	/**
	 * Constructs an {@link Url} with the common attributes.
	 * 
	 * @param aProtocol The protocol {@link String} (e.g. "http://" or
	 *        "https://") to be used for the destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( String aProtocol, String aHost, String aPath ) {
		return toUrl( new Url( aProtocol, aHost, aPath ) );
	}

	/**
	 * Constructs an {@link Url} with the common attributes.
	 * 
	 * @param aProtocol The protocol {@link String} (e.g. "http://" or
	 *        "https://") to be used for the destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( String aProtocol, String aHost, String aPath, FormFields aQueryFields ) {
		return toUrl( new Url( aProtocol, aHost, aPath, aQueryFields ) );
	}

	/**
	 * Constructs an {@link Url} with the common attributes.
	 * 
	 * @param aProtocol The protocol {@link String} (e.g. "http://" or
	 *        "https://") to be used for the destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aFragment The fragment to be set.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( String aProtocol, String aHost, String aPath, FormFields aQueryFields, String aFragment ) {
		return toUrl( new Url( aProtocol, aHost, aPath, aQueryFields, aFragment ) );
	}

	/**
	 * Constructs a new {@link Url} from the given {@link Url} instances by
	 * adding the other {@link Url}'s data to the first {@link Url}'s data. E.g.
	 * a path from the other {@link Url} is append to the first {@link Url}'s
	 * path, the query parameters are added or overwritten accordingly and so
	 * on.
	 * 
	 * @param aUrl The {@link Url} which is to be enriched.
	 * @param aOtherUrl The {@link Url} enriching the given {@link Url} by an
	 *        actual host and returns the accordingly resolved {@link Url}.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( Url aUrl, Url aOtherUrl ) {
		return toUrl( new Url( aUrl, aOtherUrl ) );
	}

	/**
	 * Some {@link Url} algebra: Adds the provided path to the given {@link Url}
	 * by prepending it to the {@link Url}'s path.
	 * 
	 * @param aUrl The {@link Url} to which to add the path.
	 * @param aPaths The paths to be added to the given {@link Url} by an actual
	 *        host and returns the accordingly resolved {@link Url}.
	 * 
	 * @return The tinkered {@link Url}.
	 */
	default Url toUrl( Url aUrl, String... aPaths ) {
		return toUrl( new Url( aUrl, aPaths ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// LIFECYCLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Initially fetches the list of available services and their accordingly
	 * associated {@link Url} from the discovery service. Sets up the client
	 * according to the current client's state when invoking
	 * {@link #initialize(Url, LoadBalancingStrategy, TrustStoreDescriptor)}.
	 * {@inheritDoc}
	 */
	@Override
	default void initialize() throws InitializeException {
		initialize( null, null, null );
	}

	/**
	 * Initially fetches the list of available services and their accordingly
	 * associated {@link Url} from the discovery service. Sets up the client
	 * according to the current client's state and the provided parameters when
	 * invoking
	 * {@link #initialize(Url, LoadBalancingStrategy, TrustStoreDescriptor)}.
	 * 
	 * @param aDiscoveryUrl The {@link Url} pointing to the discovery service.
	 * 
	 * @throws InitializeException Thrown in case initializing fails.
	 */
	default void initialize( Url aDiscoveryUrl ) throws InitializeException {
		initialize( aDiscoveryUrl, null, null );
	}

	/**
	 * Initially fetches the list of available services and their accordingly
	 * associated {@link Url} from the discovery service. Sets up the client
	 * according to the provided context when invoking
	 * {@link #initialize(Url, LoadBalancingStrategy, TrustStoreDescriptor)}.
	 * {@inheritDoc}
	 * 
	 * @param aDiscoveryContext The {@link HttpDiscoveryContext} describing the
	 *        information for connecting to the discovery service.
	 */
	@Override
	default void initialize( HttpDiscoveryContext aDiscoveryContext ) throws InitializeException {
		initialize( aDiscoveryContext.getHttpDiscoveryUrl(), aDiscoveryContext.getLoadBalancingStrategy(), aDiscoveryContext.getTrustStoreDescriptor() );
	}

	/**
	 * Initially fetches the list of available services and their accordingly
	 * associated {@link Url} from the discovery service. Sets up the client
	 * according to the current client's state and the provided parameters when
	 * invoking
	 * {@link #initialize(Url, LoadBalancingStrategy, TrustStoreDescriptor)}.
	 * {@inheritDoc}
	 * 
	 * @param aDiscoveryUrl The {@link Url} pointing to the discovery service
	 * @param aStoreDescriptor The descriptor describing the truststore to be
	 *        used by this client.
	 */
	default void initialize( Url aDiscoveryUrl, TrustStoreDescriptor aStoreDescriptor ) throws InitializeException {
		initialize( aDiscoveryUrl, null, aStoreDescriptor );
	}

	/**
	 * Initially fetches the list of available services and their accordingly
	 * associated {@link Url} from the discovery service. Sets up the client
	 * according to the current client's state and the provided parameters.
	 * {@inheritDoc}
	 * 
	 * @param aDiscoveryUrl The {@link Url} pointing to the discovery service
	 * @param aStrategy The load balancing strategy to use when resolving
	 *        targeted {@link Url} of issued requests.
	 */
	default void initialize( Url aDiscoveryUrl, LoadBalancingStrategy aStrategy ) throws InitializeException {
		initialize( aDiscoveryUrl, aStrategy, null );
	}

	/**
	 * Initially fetches the list of available services and their accordingly
	 * associated {@link Url} from the discovery service. Sets up the client
	 * according to the current client's state and the provided parameters.
	 * {@inheritDoc}
	 * 
	 * @param aDiscoveryUrl The {@link Url} pointing to the discovery service
	 * @param aStrategy The load balancing strategy to use when resolving
	 *        targeted {@link Url} of issued requests.
	 * @param aStoreDescriptor The descriptor describing the truststore to be
	 *        used by this client.
	 */
	void initialize( Url aDiscoveryUrl, LoadBalancingStrategy aStrategy, TrustStoreDescriptor aStoreDescriptor ) throws InitializeException;

	/**
	 * Starts resolving of the host part of an {@link Url} from the list of
	 * available services and their accordingly associated {@link Url} as of the
	 * discovery service. Takes care to update the list of available services
	 * and their accordingly associated {@link Url} from the discovery service.
	 * This may be achieved by starting a scheduler. If necessary, the
	 * connection is opened. {@inheritDoc}
	 */
	@Override
	void start() throws StartException;

	/**
	 * Pauses the resolving of the host part of an {@link Url} from the list of
	 * available services and their accordingly associated {@link Url} as of the
	 * discovery service. {@inheritDoc}
	 */
	@Override
	void pause() throws PauseException;

	/**
	 * Resumes the resolving of the host part of an {@link Url} from the list of
	 * available services and their accordingly associated {@link Url} as of the
	 * discovery service. {@inheritDoc}
	 */
	@Override
	void resume() throws ResumeException;

	/**
	 * Stops resolving of the host part of an {@link Url} from the list of
	 * available services and their accordingly associated {@link Url} as of the
	 * discovery service. Does not update the list of available services and
	 * their accordingly associated {@link Url} from the discovery service. This
	 * may be achieved by canceling a scheduler. {@inheritDoc}
	 */
	@Override
	void stop() throws StopException;

	/**
	 * Stops resolving of the host part of an {@link Url} from the list of
	 * available services and their accordingly associated {@link Url} as of the
	 * discovery service. Does not update the list of available services and
	 * their accordingly associated {@link Url} from the discovery service. This
	 * may be achieved by canceling a scheduler. Finally the connection is
	 * closed. {@inheritDoc}
	 */
	@Override
	void destroy();

}
