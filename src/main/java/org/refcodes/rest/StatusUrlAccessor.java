// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import org.refcodes.web.Url;

/**
 * Provides an accessor for a status {@link Url} property.
 */
public interface StatusUrlAccessor {

	/**
	 * Retrieves the status {@link Url} from the status {@link Url} property.
	 * 
	 * @return The status {@link Url} stored by the status {@link Url} property.
	 */
	Url getStatusUrl();

	/**
	 * Provides a mutator for a status {@link Url} property.
	 */
	public interface StatusUrlMutator {

		/**
		 * Sets the status {@link Url} for the status {@link Url} property.
		 * 
		 * @param aStatusUrl The status {@link Url} to be stored by the status
		 *        {@link Url} property.
		 */
		void setStatusUrl( Url aStatusUrl );
	}

	/**
	 * Provides a mutator for an status {@link Url} property.
	 * 
	 * @param <B> The builder which implements the {@link StatusUrlBuilder}.
	 */
	public interface StatusUrlBuilder<B extends StatusUrlBuilder<?>> {

		/**
		 * Sets the status {@link Url} to use and returns this builder as of the
		 * builder pattern.
		 * 
		 * @param aStatusUrl The status {@link Url} to be stored by the status
		 *        {@link Url} property.
		 * 
		 * @return This {@link StatusUrlBuilder} instance to continue
		 *         configuration.
		 */
		B withStatusUrl( Url aStatusUrl );
	}

	/**
	 * Provides a status {@link Url} property.
	 */
	public interface StatusUrlProperty extends StatusUrlAccessor, StatusUrlMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Url} (setter) as
		 * of {@link #setStatusUrl(Url)} and returns the very same value
		 * (getter).
		 * 
		 * @param aUrl The {@link Url} to set (via {@link #setStatusUrl(Url)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Url letStatusUrl( Url aUrl ) {
			setStatusUrl( aUrl );
			return aUrl;
		}
	}
}
