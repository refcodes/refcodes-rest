// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

/**
 * Provides an accessor for a status path property.
 */
public interface StatusPathAccessor {

	/**
	 * Retrieves the status path from the status path property.
	 * 
	 * @return The status path stored by the status path property.
	 */
	String getStatusPath();

	/**
	 * Provides a mutator for a status path property.
	 */
	public interface StatusPathMutator {

		/**
		 * Sets the status path for the status path property.
		 * 
		 * @param aStatusPath The status path to be stored by the status path
		 *        property.
		 */
		void setStatusPath( String aStatusPath );
	}

	/**
	 * Provides a mutator for an status path property.
	 * 
	 * @param <B> The builder which implements the {@link StatusPathBuilder}.
	 */
	public interface StatusPathBuilder<B extends StatusPathBuilder<?>> {

		/**
		 * Sets the status path to use and returns this builder as of the
		 * builder pattern.
		 * 
		 * @param aStatusPath The status path to be stored by the status path
		 *        property.
		 * 
		 * @return This {@link StatusPathBuilder} instance to continue
		 *         configuration.
		 */
		B withStatusPath( String aStatusPath );
	}

	/**
	 * Provides a status path property.
	 */
	public interface StatusPathProperty extends StatusPathAccessor, StatusPathMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setStatusPath(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aStatusPath The {@link String} to set (via
		 *        {@link #setStatusPath(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letStatusPath( String aStatusPath ) {
			setStatusPath( aStatusPath );
			return aStatusPath;
		}
	}
}
