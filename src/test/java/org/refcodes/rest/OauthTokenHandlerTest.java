// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.refcodes.net.PortManagerSingleton;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.web.FormFields;
import org.refcodes.web.GrantType;
import org.refcodes.web.HttpBodyMap;
import org.refcodes.web.HttpStatusCode;
import org.refcodes.web.HttpStatusException;
import org.refcodes.web.InternalServerErrorException;
import org.refcodes.web.OauthField;
import org.refcodes.web.TokenType;

public class OauthTokenHandlerTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testOauthTokenHandler1() throws IOException, InterruptedException {
		final boolean[] theStages = { false, false, false, false };
		final int thePort = PortManagerSingleton.getInstance().bindAnyPort();
		final String theOauthEndpoint = "/tokens";
		final String theUrl = "http://localhost:" + thePort + theOauthEndpoint;
		final String theClientId = "clientId001";
		final String theClientSecret = "clientSecret2049";
		final String theUserName = "userName5161";
		final String thePasswort = "password1234567890";
		final GrantType theGrantType = GrantType.PASSWORD;
		final String theAccessToken1 = "1_access0815_1";
		final String theRefreshToken1 = "1_refresh0815_1";
		final GrantType theGrantType1 = GrantType.REFRESH_TOKEN;
		final int theExpiresSeconds1 = 1;
		final String theAccessToken2 = "2_access0815_2";
		final String theRefreshToken2 = "2_refresh0815_2";
		final int theExpiresSeconds2 = 3;
		final GrantType theGrantType2 = GrantType.REFRESH_TOKEN;
		final String theAccessToken3 = "3_access0815_3";
		final String theRefreshToken3 = "3_refresh0815_3";
		final int theExpiresSeconds3 = 5;
		final GrantType theGrantType3 = GrantType.REFRESH_TOKEN;
		final String theAccessToken4 = "4_access0815_4";
		final String theRefreshToken4 = "4_refresh0815_4";
		final int theExpiresSeconds4 = -1;
		final GrantType theGrantType4 = GrantType.REFRESH_TOKEN;
		// Our tiny KeyCloak |-->
		final RestfulHttpServer theRestServer = new HttpRestServer();
		final long[] theStartTime = new long[1];
		theRestServer.onPost( theOauthEndpoint, ( req, res ) -> {
			final FormFields theRequestBody = req.getRequest( FormFields.class );
			final String eClientId = theRequestBody.getFirst( OauthField.CLIENT_ID.getName() );
			final String eClientSecret = theRequestBody.getFirst( OauthField.CLIENT_SECRET.getName() );
			final String eUserName = theRequestBody.getFirst( OauthField.USER_NAME.getName() );
			final String ePassword = theRequestBody.getFirst( OauthField.PASSWORD.getName() );
			final String eGrantType = theRequestBody.getFirst( OauthField.GRANT_TYPE.getName() );
			final String eRefreshToken = theRequestBody.getFirst( OauthField.REFRESH_TOKEN.getName() );
			final float theDelay = ( System.currentTimeMillis() - theStartTime[0] ) / 1000F;
			if ( theClientId.equals( eClientId ) && theClientSecret.equals( eClientSecret ) && theUserName.equals( eUserName ) && thePasswort.equals( ePassword ) && theGrantType.getValue().equals( eGrantType ) ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "0) Processing <" + eGrantType + "> after <" + theDelay + "> seconds (~" + Math.round( theDelay ) + " seconds) ..." );
				}
				assertFalse( theStages[0] );
				assertFalse( theStages[1] );
				assertFalse( theStages[2] );
				assertFalse( theStages[3] );
				assertEquals( 0, Math.round( theDelay ) );
				final HttpBodyMap theResponseBody = new HttpBodyMap();
				theResponseBody.put( OauthField.ACCESS_TOKEN.getPath(), theAccessToken1 );
				theResponseBody.put( OauthField.REFRESH_TOKEN.getPath(), theRefreshToken1 );
				theResponseBody.put( OauthField.TOKEN_TYPE.getPath(), TokenType.BEARER.getName() );
				theResponseBody.put( OauthField.EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds1 ) );
				theResponseBody.put( OauthField.REFRESH_EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds1 ) );
				theResponseBody.put( OauthField.GRANT_TYPE.getPath(), eGrantType );
				res.setResponse( theResponseBody );
				res.setHttpStatusCode( HttpStatusCode.OK );
				theStages[0] = true;
				theStartTime[0] = System.currentTimeMillis();
			}
			else if ( theRefreshToken1.equals( eRefreshToken ) && theGrantType1.getValue().equalsIgnoreCase( eGrantType ) ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "1) Processing <" + eGrantType + "> after <" + theDelay + "> seconds (~" + Math.round( theDelay ) + " seconds) ..." );
				}
				assertTrue( theStages[0] );
				assertFalse( theStages[1] );
				assertFalse( theStages[2] );
				assertFalse( theStages[3] );
				assertEquals( theExpiresSeconds1, Math.round( theDelay ) );
				final HttpBodyMap theResponseBody = new HttpBodyMap();
				theResponseBody.put( OauthField.ACCESS_TOKEN.getPath(), theAccessToken2 );
				theResponseBody.put( OauthField.REFRESH_TOKEN.getPath(), theRefreshToken2 );
				theResponseBody.put( OauthField.TOKEN_TYPE.getPath(), TokenType.BEARER.getName() );
				theResponseBody.put( OauthField.EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds2 ) );
				theResponseBody.put( OauthField.REFRESH_EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds2 ) );
				theResponseBody.put( OauthField.GRANT_TYPE.getPath(), eGrantType );
				res.setResponse( theResponseBody );
				res.setHttpStatusCode( HttpStatusCode.OK );
				theStages[1] = true;
				theStartTime[0] = System.currentTimeMillis();
			}
			else if ( theRefreshToken2.equals( eRefreshToken ) && theGrantType2.getValue().equalsIgnoreCase( eGrantType ) ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "2) Processing <" + eGrantType + "> after <" + theDelay + "> seconds (~" + Math.round( theDelay ) + " seconds) ..." );
				}
				assertTrue( theStages[0] );
				assertTrue( theStages[1] );
				assertFalse( theStages[2] );
				assertFalse( theStages[3] );
				assertEquals( theExpiresSeconds2, Math.round( theDelay ) );
				final HttpBodyMap theResponseBody = new HttpBodyMap();
				theResponseBody.put( OauthField.ACCESS_TOKEN.getPath(), theAccessToken3 );
				theResponseBody.put( OauthField.REFRESH_TOKEN.getPath(), theRefreshToken3 );
				theResponseBody.put( OauthField.TOKEN_TYPE.getPath(), TokenType.BEARER.getName() );
				theResponseBody.put( OauthField.EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds3 ) );
				theResponseBody.put( OauthField.REFRESH_EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds3 ) );
				theResponseBody.put( OauthField.GRANT_TYPE.getPath(), eGrantType );
				res.setResponse( theResponseBody );
				res.setHttpStatusCode( HttpStatusCode.OK );
				theStages[2] = true;
				theStartTime[0] = System.currentTimeMillis();
			}
			else if ( theRefreshToken3.equals( eRefreshToken ) && theGrantType3.getValue().equalsIgnoreCase( eGrantType ) ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "3) Processing <" + eGrantType + "> after <" + theDelay + "> seconds (~" + Math.round( theDelay ) + " seconds) ..." );
				}
				assertTrue( theStages[0] );
				assertTrue( theStages[1] );
				assertTrue( theStages[2] );
				assertFalse( theStages[3] );
				assertEquals( theExpiresSeconds3, Math.round( theDelay ) );
				final HttpBodyMap theResponseBody = new HttpBodyMap();
				theResponseBody.put( OauthField.ACCESS_TOKEN.getPath(), theAccessToken4 );
				theResponseBody.put( OauthField.REFRESH_TOKEN.getPath(), theRefreshToken4 );
				theResponseBody.put( OauthField.TOKEN_TYPE.getPath(), TokenType.BEARER.getName() );
				theResponseBody.put( OauthField.EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds4 ) );
				theResponseBody.put( OauthField.REFRESH_EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds4 ) );
				theResponseBody.put( OauthField.GRANT_TYPE.getPath(), eGrantType );
				res.setResponse( theResponseBody );
				res.setHttpStatusCode( HttpStatusCode.OK );
				theStages[3] = true;
				theStartTime[0] = System.currentTimeMillis();
			}
			else if ( theRefreshToken4.equals( eRefreshToken ) && theGrantType4.getValue().equalsIgnoreCase( eGrantType ) ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "4) Processing <" + eGrantType + "> after <" + theDelay + "> seconds (~" + Math.round( theDelay ) + " seconds) ..." );
				}
				fail( "Some timeout schedule may be wrong, the test should already have ended!" );
			}
		} ).open();
		theRestServer.open( thePort );
		// Our tiny KeyCloak <--|
		theStartTime[0] = System.currentTimeMillis();
		try {
			final OauthTokenHandler theTokenHandler = new OauthTokenHandler( theUrl, theClientId, theClientSecret, theUserName, thePasswort, theGrantType );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theTokenHandler.toString() );
			}
			Thread.sleep( ( theExpiresSeconds1 + theExpiresSeconds2 + theExpiresSeconds3 ) * 1000 );
			assertTrue( theStages[0] );
			assertTrue( theStages[1] );
			assertTrue( theStages[2] );
			assertTrue( theStages[3] );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theTokenHandler.toString() );
			}
		}
		catch ( HttpStatusException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( e.toMessage() );
				e.printStackTrace();
			}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testOauthTokenHandler2() throws IOException, InterruptedException {
		final long[] theStartTime = new long[1];
		final boolean[] theStages = { false, false, false, false };
		final int thePort = PortManagerSingleton.getInstance().bindAnyPort();
		final String theOauthEndpoint = "/tokens";
		final String theUrl = "http://localhost:" + thePort + theOauthEndpoint;
		final String theClientId = "clientId001";
		final String theClientSecret = "clientSecret2049";
		final String theUserName = "userName5161";
		final String thePasswort = "password1234567890";
		final GrantType theGrantType = GrantType.PASSWORD;
		final String theAccessToken1 = "1_access0815_1";
		final String theRefreshToken1 = "1_refresh0815_1";
		final GrantType theGrantType1 = GrantType.REFRESH_TOKEN;
		final int theExpiresSeconds1 = 2;
		final String theAccessToken2 = "2_access0815_2";
		final String theRefreshToken2 = "2_refresh0815_2";
		final int theExpiresSeconds2 = 4;
		final GrantType theGrantType2 = GrantType.REFRESH_TOKEN;
		final String theAccessToken3 = "3_access0815_3";
		final String theRefreshToken3 = "3_refresh0815_3";
		final int theExpiresSeconds3 = 6;
		final GrantType theGrantType3 = GrantType.REFRESH_TOKEN;
		final String theAccessToken4 = "4_access0815_4";
		final String theRefreshToken4 = "4_refresh0815_4";
		final int theExpiresSeconds4 = 1;
		final GrantType theGrantType4 = GrantType.REFRESH_TOKEN;
		// Our tiny KeyCloak |-->
		final RestfulHttpServer theRestServer = new HttpRestServer();
		theRestServer.onPost( theOauthEndpoint, ( req, res ) -> {
			final FormFields theRequestBody = req.getRequest( FormFields.class );
			final String eClientId = theRequestBody.getFirst( OauthField.CLIENT_ID.getName() );
			final String eClientSecret = theRequestBody.getFirst( OauthField.CLIENT_SECRET.getName() );
			final String eUserName = theRequestBody.getFirst( OauthField.USER_NAME.getName() );
			final String ePassword = theRequestBody.getFirst( OauthField.PASSWORD.getName() );
			final String eGrantType = theRequestBody.getFirst( OauthField.GRANT_TYPE.getName() );
			final String eRefreshToken = theRequestBody.getFirst( OauthField.REFRESH_TOKEN.getName() );
			final float theDelay = ( System.currentTimeMillis() - theStartTime[0] ) / 1000F;
			if ( theClientId.equals( eClientId ) && theClientSecret.equals( eClientSecret ) && theUserName.equals( eUserName ) && thePasswort.equals( ePassword ) && theGrantType.getValue().equals( eGrantType ) ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "0) Processing <" + eGrantType + "> after <" + theDelay + "> seconds (~" + Math.round( theDelay ) + " seconds) ..." );
				}
				assertFalse( theStages[0] );
				assertFalse( theStages[1] );
				assertFalse( theStages[2] );
				assertFalse( theStages[3] );
				assertEquals( 0, Math.round( theDelay ) );
				final HttpBodyMap theResponseBody = new HttpBodyMap();
				theResponseBody.put( OauthField.ACCESS_TOKEN.getPath(), theAccessToken1 );
				theResponseBody.put( OauthField.REFRESH_TOKEN.getPath(), theRefreshToken1 );
				theResponseBody.put( OauthField.TOKEN_TYPE.getPath(), TokenType.BEARER.getName() );
				theResponseBody.put( OauthField.EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds1 ) );
				theResponseBody.put( OauthField.REFRESH_EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds1 ) );
				theResponseBody.put( OauthField.GRANT_TYPE.getPath(), eGrantType );
				res.setResponse( theResponseBody );
				res.setHttpStatusCode( HttpStatusCode.OK );
				theStages[0] = true;
				theStartTime[0] = System.currentTimeMillis();
			}
			else if ( theRefreshToken1.equals( eRefreshToken ) && theGrantType1.getValue().equalsIgnoreCase( eGrantType ) ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "1) Processing <" + eGrantType + "> after <" + theDelay + "> seconds (~" + Math.round( theDelay ) + " seconds) ..." );
				}
				assertTrue( theStages[0] );
				assertFalse( theStages[1] );
				assertFalse( theStages[2] );
				assertFalse( theStages[3] );
				assertEquals( theExpiresSeconds1, Math.round( theDelay ) );
				final HttpBodyMap theResponseBody = new HttpBodyMap();
				theResponseBody.put( OauthField.ACCESS_TOKEN.getPath(), theAccessToken2 );
				theResponseBody.put( OauthField.REFRESH_TOKEN.getPath(), theRefreshToken2 );
				theResponseBody.put( OauthField.TOKEN_TYPE.getPath(), TokenType.BEARER.getName() );
				theResponseBody.put( OauthField.EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds2 ) );
				theResponseBody.put( OauthField.REFRESH_EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds2 ) );
				theResponseBody.put( OauthField.GRANT_TYPE.getPath(), eGrantType );
				res.setResponse( theResponseBody );
				res.setHttpStatusCode( HttpStatusCode.OK );
				theStages[1] = true;
				theStartTime[0] = System.currentTimeMillis();
			}
			else if ( theRefreshToken2.equals( eRefreshToken ) && theGrantType2.getValue().equalsIgnoreCase( eGrantType ) ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "2) Processing <" + eGrantType + "> after <" + theDelay + "> seconds (~" + Math.round( theDelay ) + " seconds) ..." );
				}
				assertTrue( theStages[0] );
				assertTrue( theStages[1] );
				assertFalse( theStages[2] );
				assertFalse( theStages[3] );
				assertEquals( theExpiresSeconds2, Math.round( theDelay ) );
				final HttpBodyMap theResponseBody = new HttpBodyMap();
				theResponseBody.put( OauthField.ACCESS_TOKEN.getPath(), theAccessToken3 );
				theResponseBody.put( OauthField.REFRESH_TOKEN.getPath(), theRefreshToken3 );
				theResponseBody.put( OauthField.TOKEN_TYPE.getPath(), TokenType.BEARER.getName() );
				theResponseBody.put( OauthField.EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds3 ) );
				theResponseBody.put( OauthField.REFRESH_EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds3 ) );
				theResponseBody.put( OauthField.GRANT_TYPE.getPath(), eGrantType );
				res.setResponse( theResponseBody );
				res.setHttpStatusCode( HttpStatusCode.OK );
				theStages[2] = true;
				theStartTime[0] = System.currentTimeMillis();
			}
			else if ( theRefreshToken3.equals( eRefreshToken ) && theGrantType3.getValue().equalsIgnoreCase( eGrantType ) ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "3) Processing <" + eGrantType + "> after <" + theDelay + "> seconds (~" + Math.round( theDelay ) + " seconds) ..." );
				}
				assertTrue( theStages[0] );
				assertTrue( theStages[1] );
				assertTrue( theStages[2] );
				assertFalse( theStages[3] );
				assertEquals( theExpiresSeconds3, Math.round( theDelay ) );
				final HttpBodyMap theResponseBody = new HttpBodyMap();
				theResponseBody.put( OauthField.ACCESS_TOKEN.getPath(), theAccessToken4 );
				theResponseBody.put( OauthField.REFRESH_TOKEN.getPath(), theRefreshToken4 );
				theResponseBody.put( OauthField.TOKEN_TYPE.getPath(), TokenType.BEARER.getName() );
				theResponseBody.put( OauthField.EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds4 ) );
				theResponseBody.put( OauthField.REFRESH_EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds4 ) );
				theResponseBody.put( OauthField.GRANT_TYPE.getPath(), eGrantType );
				res.setResponse( theResponseBody );
				res.setHttpStatusCode( HttpStatusCode.OK );
				theStages[3] = true;
				theStartTime[0] = System.currentTimeMillis();
			}
			else if ( theRefreshToken4.equals( eRefreshToken ) && theGrantType4.getValue().equalsIgnoreCase( eGrantType ) ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "4) Processing <" + eGrantType + "> after <" + theDelay + "> seconds (~" + Math.round( theDelay ) + " seconds) ..." );
				}
				assertEquals( theExpiresSeconds4, Math.round( theDelay ) );
				throw HttpStatusCode.UNAUTHORIZED.toHttpStatusException( "No more covered access!" );
			}
		} ).open();
		theRestServer.open( thePort );
		// Our tiny KeyCloak <--|
		theStartTime[0] = System.currentTimeMillis();
		try {
			final OauthTokenHandler theTokenHandler = new OauthTokenHandler( theUrl, theClientId, theClientSecret, theUserName, thePasswort, theGrantType );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theTokenHandler.toString() );
			}
			Thread.sleep( theExpiresSeconds1 * 1000 );
			assertTrue( theStages[0] );
			assertTrue( theTokenHandler.isValid() );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theTokenHandler.toString() );
			}
			Thread.sleep( theExpiresSeconds2 * 1000 );
			assertTrue( theStages[1] );
			assertTrue( theTokenHandler.isValid() );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theTokenHandler.toString() );
			}
			Thread.sleep( theExpiresSeconds3 * 1000 );
			assertTrue( theStages[2] );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theTokenHandler.toString() );
			}
			assertTrue( theTokenHandler.isValid() );
			Thread.sleep( theExpiresSeconds4 * 1000 );
			assertTrue( theStages[3] );
			assertFalse( theTokenHandler.isValid() );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theTokenHandler.toString() );
			}
		}
		catch ( HttpStatusException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( e.toMessage() );
				e.printStackTrace();
			}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testOauthTokenHandler3() throws IOException, InterruptedException {
		final long FAILURE_DELAY_TIME_SECONDS = 5;
		final long[] theStartTime = new long[1];
		final boolean[] theStages = { false, false, false, false };
		final boolean[] hasFailed = { true, false, false, false };
		final int thePort = PortManagerSingleton.getInstance().bindAnyPort();
		final String theOauthEndpoint = "/tokens";
		final String theUrl = "http://localhost:" + thePort + theOauthEndpoint;
		final String theClientId = "clientId001";
		final String theClientSecret = "clientSecret2049";
		final String theUserName = "userName5161";
		final String thePasswort = "password1234567890";
		final GrantType theGrantType = GrantType.PASSWORD;
		final String theAccessToken1 = "1_access0815_1";
		final String theRefreshToken1 = "1_refresh0815_1";
		final GrantType theGrantType1 = GrantType.REFRESH_TOKEN;
		final int theExpiresSeconds1 = 1;
		final String theAccessToken2 = "2_access0815_2";
		final String theRefreshToken2 = "2_refresh0815_2";
		final int theExpiresSeconds2 = 3;
		final GrantType theGrantType2 = GrantType.REFRESH_TOKEN;
		final String theAccessToken3 = "3_access0815_3";
		final String theRefreshToken3 = "3_refresh0815_3";
		final int theExpiresSeconds3 = 5;
		final GrantType theGrantType3 = GrantType.REFRESH_TOKEN;
		final String theAccessToken4 = "4_access0815_4";
		final String theRefreshToken4 = "4_refresh0815_4";
		final int theExpiresSeconds4 = -1;
		final GrantType theGrantType4 = GrantType.REFRESH_TOKEN;
		// Our tiny KeyCloak |-->
		final RestfulHttpServer theRestServer = new HttpRestServer();
		theRestServer.onPost( theOauthEndpoint, ( req, res ) -> {
			final FormFields theRequestBody = req.getRequest( FormFields.class );
			final String eClientId = theRequestBody.getFirst( OauthField.CLIENT_ID.getName() );
			final String eClientSecret = theRequestBody.getFirst( OauthField.CLIENT_SECRET.getName() );
			final String eUserName = theRequestBody.getFirst( OauthField.USER_NAME.getName() );
			final String ePassword = theRequestBody.getFirst( OauthField.PASSWORD.getName() );
			final String eGrantType = theRequestBody.getFirst( OauthField.GRANT_TYPE.getName() );
			final String eRefreshToken = theRequestBody.getFirst( OauthField.REFRESH_TOKEN.getName() );
			final float theDelay = ( System.currentTimeMillis() - theStartTime[0] ) / 1000F;
			if ( theClientId.equals( eClientId ) && theClientSecret.equals( eClientSecret ) && theUserName.equals( eUserName ) && thePasswort.equals( ePassword ) && theGrantType.getValue().equals( eGrantType ) ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "0) Processing <" + eGrantType + "> after <" + theDelay + "> seconds (~" + Math.round( theDelay ) + " seconds) ..." );
				}
				if ( !hasFailed[0] ) {
					hasFailed[0] = true;
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "0) Failing 1x ..." );
					}
					throw new InternalServerErrorException( "0) Hick-up!" );
				}
				assertFalse( theStages[0] );
				assertFalse( theStages[1] );
				assertFalse( theStages[2] );
				assertFalse( theStages[3] );
				assertEquals( 0, Math.round( theDelay ) );
				final HttpBodyMap theResponseBody = new HttpBodyMap();
				theResponseBody.put( OauthField.ACCESS_TOKEN.getPath(), theAccessToken1 );
				theResponseBody.put( OauthField.REFRESH_TOKEN.getPath(), theRefreshToken1 );
				theResponseBody.put( OauthField.TOKEN_TYPE.getPath(), TokenType.BEARER.getName() );
				theResponseBody.put( OauthField.EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds1 ) );
				theResponseBody.put( OauthField.REFRESH_EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds1 ) );
				theResponseBody.put( OauthField.GRANT_TYPE.getPath(), eGrantType );
				res.setResponse( theResponseBody );
				res.setHttpStatusCode( HttpStatusCode.OK );
				theStages[0] = true;
				theStartTime[0] = System.currentTimeMillis();
			}
			else if ( theRefreshToken1.equals( eRefreshToken ) && theGrantType1.getValue().equalsIgnoreCase( eGrantType ) ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "1) Processing <" + eGrantType + "> after <" + theDelay + "> seconds (~" + Math.round( theDelay ) + " seconds) ..." );
				}
				if ( !hasFailed[1] ) {
					hasFailed[1] = true;
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "1) Failing 1x ..." );
					}
					throw new InternalServerErrorException( "1) Hick-up!" );
				}
				assertTrue( theStages[0] );
				assertFalse( theStages[1] );
				assertFalse( theStages[2] );
				assertFalse( theStages[3] );
				assertEquals( theExpiresSeconds1 + FAILURE_DELAY_TIME_SECONDS, Math.round( theDelay ) );
				final HttpBodyMap theResponseBody = new HttpBodyMap();
				theResponseBody.put( OauthField.ACCESS_TOKEN.getPath(), theAccessToken2 );
				theResponseBody.put( OauthField.REFRESH_TOKEN.getPath(), theRefreshToken2 );
				theResponseBody.put( OauthField.TOKEN_TYPE.getPath(), TokenType.BEARER.getName() );
				theResponseBody.put( OauthField.EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds2 ) );
				theResponseBody.put( OauthField.REFRESH_EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds2 ) );
				theResponseBody.put( OauthField.GRANT_TYPE.getPath(), eGrantType );
				res.setResponse( theResponseBody );
				res.setHttpStatusCode( HttpStatusCode.OK );
				theStages[1] = true;
				theStartTime[0] = System.currentTimeMillis();
			}
			else if ( theRefreshToken2.equals( eRefreshToken ) && theGrantType2.getValue().equalsIgnoreCase( eGrantType ) ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "2) Processing <" + eGrantType + "> after <" + theDelay + "> seconds (~" + Math.round( theDelay ) + " seconds) ..." );
				}
				if ( !hasFailed[2] ) {
					hasFailed[2] = true;
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "2) Failing 1x ..." );
					}
					throw new InternalServerErrorException( "2) Hick-up!" );
				}
				assertTrue( theStages[0] );
				assertTrue( theStages[1] );
				assertFalse( theStages[2] );
				assertFalse( theStages[3] );
				assertEquals( theExpiresSeconds2 + FAILURE_DELAY_TIME_SECONDS, Math.round( theDelay ) );
				final HttpBodyMap theResponseBody = new HttpBodyMap();
				theResponseBody.put( OauthField.ACCESS_TOKEN.getPath(), theAccessToken3 );
				theResponseBody.put( OauthField.REFRESH_TOKEN.getPath(), theRefreshToken3 );
				theResponseBody.put( OauthField.TOKEN_TYPE.getPath(), TokenType.BEARER.getName() );
				theResponseBody.put( OauthField.EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds3 ) );
				theResponseBody.put( OauthField.REFRESH_EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds3 ) );
				theResponseBody.put( OauthField.GRANT_TYPE.getPath(), eGrantType );
				res.setResponse( theResponseBody );
				res.setHttpStatusCode( HttpStatusCode.OK );
				theStages[2] = true;
				theStartTime[0] = System.currentTimeMillis();
			}
			else if ( theRefreshToken3.equals( eRefreshToken ) && theGrantType3.getValue().equalsIgnoreCase( eGrantType ) ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "3) Processing <" + eGrantType + "> after <" + theDelay + "> seconds (~" + Math.round( theDelay ) + " seconds) ..." );
				}
				if ( !hasFailed[3] ) {
					hasFailed[3] = true;
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "3) Failing 1x ..." );
					}
					throw new InternalServerErrorException( "3) Hick-up!" );
				}
				assertTrue( theStages[0] );
				assertTrue( theStages[1] );
				assertTrue( theStages[2] );
				assertFalse( theStages[3] );
				assertEquals( theExpiresSeconds3 + FAILURE_DELAY_TIME_SECONDS, Math.round( theDelay ) );
				final HttpBodyMap theResponseBody = new HttpBodyMap();
				theResponseBody.put( OauthField.ACCESS_TOKEN.getPath(), theAccessToken4 );
				theResponseBody.put( OauthField.REFRESH_TOKEN.getPath(), theRefreshToken4 );
				theResponseBody.put( OauthField.TOKEN_TYPE.getPath(), TokenType.BEARER.getName() );
				theResponseBody.put( OauthField.EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds4 ) );
				theResponseBody.put( OauthField.REFRESH_EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds4 ) );
				theResponseBody.put( OauthField.GRANT_TYPE.getPath(), eGrantType );
				res.setResponse( theResponseBody );
				res.setHttpStatusCode( HttpStatusCode.OK );
				theStages[3] = true;
				theStartTime[0] = System.currentTimeMillis();
			}
			else if ( theRefreshToken4.equals( eRefreshToken ) && theGrantType4.getValue().equalsIgnoreCase( eGrantType ) ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "4) Processing <" + eGrantType + "> after <" + theDelay + "> seconds (~" + Math.round( theDelay ) + " seconds) ..." );
				}
				fail( "Some timeout schedule may be wrong, the test should already have ended!" );
			}
		} ).open();
		theRestServer.open( thePort );
		// Our tiny KeyCloak <--|
		theStartTime[0] = System.currentTimeMillis();
		try {
			final OauthTokenHandler theTokenHandler = new OauthTokenHandler( theUrl, theClientId, theClientSecret, theUserName, thePasswort, theGrantType );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theTokenHandler.toString() );
			}
			Thread.sleep( ( theExpiresSeconds1 + FAILURE_DELAY_TIME_SECONDS + theExpiresSeconds2 + FAILURE_DELAY_TIME_SECONDS + theExpiresSeconds3 + FAILURE_DELAY_TIME_SECONDS ) * 1000 );
			assertTrue( theStages[0] );
			assertTrue( theStages[1] );
			assertTrue( theStages[2] );
			assertTrue( theStages[3] );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theTokenHandler.toString() );
			}
		}
		catch ( HttpStatusException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( e.toMessage() );
				e.printStackTrace();
			}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testOauthTokenHandler4() throws IOException, InterruptedException {
		final int[] theFailLoop = { 0 };
		final long[] theExpectedDelays = { 0, 1, 1, 1, 5, 1 };
		final int thePort = PortManagerSingleton.getInstance().bindAnyPort();
		final String theOauthEndpoint = "/tokens";
		final String theUrl = "http://localhost:" + thePort + theOauthEndpoint;
		final String theClientId = "clientId001";
		final String theClientSecret = "clientSecret2049";
		final String theUserName = "userName5161";
		final String thePasswort = "password1234567890";
		final GrantType theGrantType = GrantType.PASSWORD;
		final String theAccessToken1 = "1_access0815_1";
		final String theRefreshToken1 = "1_refresh0815_1";
		final int theExpiresSeconds1 = 1;
		// Our tiny KeyCloak |-->
		final RestfulHttpServer theRestServer = new HttpRestServer();
		final long[] theStartTime = new long[1];
		theRestServer.onPost( theOauthEndpoint, ( req, res ) -> {
			final FormFields theRequestBody = req.getRequest( FormFields.class );
			final String eClientId = theRequestBody.getFirst( OauthField.CLIENT_ID.getName() );
			final String eClientSecret = theRequestBody.getFirst( OauthField.CLIENT_SECRET.getName() );
			final String eUserName = theRequestBody.getFirst( OauthField.USER_NAME.getName() );
			final String ePassword = theRequestBody.getFirst( OauthField.PASSWORD.getName() );
			final String eGrantType = theRequestBody.getFirst( OauthField.GRANT_TYPE.getName() );
			final float theDelay = ( System.currentTimeMillis() - theStartTime[0] ) / 1000F;
			theStartTime[0] = System.currentTimeMillis();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( ( theFailLoop[0] < theExpectedDelays.length ? theFailLoop[0] : "?" ) + ") Processing <" + eGrantType + "> after <" + theDelay + "> seconds (~" + Math.round( theDelay ) + " seconds) ..." );
			}
			if ( theFailLoop[0] < theExpectedDelays.length ) {
				assertEquals( theExpectedDelays[theFailLoop[0]], Math.round( theDelay ) );
			}
			else {
				throw new InternalServerErrorException( "Yet another loop we ignore for this test :-)" );
			}
			theFailLoop[0]++;
			if ( theClientId.equals( eClientId ) && theClientSecret.equals( eClientSecret ) && theUserName.equals( eUserName ) && thePasswort.equals( ePassword ) && theGrantType.getValue().equals( eGrantType ) ) {
				final HttpBodyMap theResponseBody = new HttpBodyMap();
				theResponseBody.put( OauthField.ACCESS_TOKEN.getPath(), theAccessToken1 );
				theResponseBody.put( OauthField.REFRESH_TOKEN.getPath(), theRefreshToken1 );
				theResponseBody.put( OauthField.TOKEN_TYPE.getPath(), TokenType.BEARER.getName() );
				theResponseBody.put( OauthField.EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds1 ) );
				theResponseBody.put( OauthField.REFRESH_EXPIRES_IN.getPath(), Integer.toString( theExpiresSeconds1 ) );
				theResponseBody.put( OauthField.GRANT_TYPE.getPath(), eGrantType );
				res.setResponse( theResponseBody );
				res.setHttpStatusCode( HttpStatusCode.OK );
			}
			else {
				throw new InternalServerErrorException( "Hick-up!" );
			}
		} ).open();
		theRestServer.open( thePort );
		// Our tiny KeyCloak <--|
		theStartTime[0] = System.currentTimeMillis();
		try {
			final OauthTokenHandler theTokenHandler = new OauthTokenHandler( theUrl, theClientId, theClientSecret, theUserName, thePasswort, theGrantType, 3, 1000, 5000 );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theTokenHandler.toString() );
			}
			Thread.sleep( 10000 );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theTokenHandler.toString() );
			}
		}
		catch ( HttpStatusException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( e.toMessage() );
				e.printStackTrace();
			}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}
}
