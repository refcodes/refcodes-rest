// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpServerResponse;
import org.refcodes.web.HttpStatusException;
import org.refcodes.web.JsonMediaTypeFactory;
import org.refcodes.web.MediaType;
import org.refcodes.web.MediaTypeFactory;
import org.refcodes.web.RequestCookie;
import org.refcodes.web.RequestHeaderFields;
import org.refcodes.web.Url;

public class LoopbackRestServerTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String LOCATOR = "/bla";
	private static final String BASE_LOCATOR = "/refcodes";
	private static final String COOKIE_A_NAME = "refcodes";
	private static final String COOKIE_A_VALUE = "org";
	private static final String COOKIE_A_VALUE_2 = "com";
	private static final String COOKIE_B_NAME = "funcodes";
	private static final String COOKIE_B_VALUE = "forever";
	private static final String LAST_NAME = "Bushnell";
	private static final String FIRST_NAME = "Nolan";

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testLoopbackRestServer() throws IOException, HttpStatusException, MarshalException, UnmarshalException {
		final LoopbackRestServer theRestServer = new LoopbackRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final Person thePerson = aRequest.getRequest( Person.class ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); } ).open();
		final MediaTypeFactory theFactory = new JsonMediaTypeFactory();
		final RequestHeaderFields theHeaderFields = new RequestHeaderFields().withAcceptTypes( MediaType.APPLICATION_JSON );
		final HttpServerResponse theResponse = new HttpServerResponse( theRestServer );
		theRestServer.onHttpRequest( null, null, HttpMethod.POST, new Url( BASE_LOCATOR + LOCATOR ), theHeaderFields, theFactory.fromUnmarshaled( new Person( FIRST_NAME, LAST_NAME ) ), theResponse );
		final Person thePerson = theFactory.toUnmarshaled( theResponse.toHttpBody(), Person.class );
		assertEquals( FIRST_NAME, thePerson.getFirstName() );
		assertEquals( LAST_NAME, thePerson.getLastName() );
	}

	@Test
	public void testServerCookie() throws IOException, HttpStatusException, MarshalException, UnmarshalException {
		final LoopbackRestServer theRestServer = new LoopbackRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final RequestCookie theCookie = aRequest.getHeaderFields().getFirstCookie( COOKIE_A_NAME ); assertEquals( COOKIE_A_NAME, theCookie.getKey() ); assertEquals( COOKIE_A_VALUE, theCookie.getValue() ); final Person thePerson = aRequest.getRequest( Person.class ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); } ).open();
		final MediaTypeFactory theFactory = new JsonMediaTypeFactory();
		final RequestHeaderFields theHeaderFields = new RequestHeaderFields().withAcceptTypes( MediaType.APPLICATION_JSON );
		theHeaderFields.addCookie( COOKIE_A_NAME, COOKIE_A_VALUE );
		final HttpServerResponse theResponse = new HttpServerResponse( theRestServer );
		theRestServer.onHttpRequest( null, null, HttpMethod.POST, new Url( BASE_LOCATOR + LOCATOR ), theHeaderFields, theFactory.fromUnmarshaled( new Person( FIRST_NAME, LAST_NAME ) ), theResponse );
		final Person thePerson = theFactory.toUnmarshaled( theResponse.toHttpBody(), Person.class );
		assertEquals( FIRST_NAME, thePerson.getFirstName() );
		assertEquals( LAST_NAME, thePerson.getLastName() );
	}

	@Test
	public void testServerCookies() throws IOException, UnmarshalException, HttpStatusException, MarshalException {
		final LoopbackRestServer theRestServer = new LoopbackRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> {
			final Person thePerson = aRequest.getRequest( Person.class );
			assertEquals( FIRST_NAME, thePerson.getFirstName() );
			assertEquals( LAST_NAME, thePerson.getLastName() );
			aResponse.setResponse( thePerson );
			final List<RequestCookie> theCookies = aRequest.getHeaderFields().getAllCookies();
			assertEquals( 2, theCookies.size() );
			int a = 0;
			int b = 1;
			if ( theCookies.get( a ).getValue().equals( COOKIE_A_VALUE_2 ) ) {
				a = 1;
				b = 0;
			}
			assertEquals( COOKIE_A_NAME, theCookies.get( a ).getKey() );
			assertEquals( COOKIE_A_VALUE, theCookies.get( a ).getValue() );
			assertEquals( COOKIE_A_NAME, theCookies.get( b ).getKey() );
			assertEquals( COOKIE_A_VALUE_2, theCookies.get( b ).getValue() );
		} ).open();
		final MediaTypeFactory theFactory = new JsonMediaTypeFactory();
		final RequestHeaderFields theHeaderFields = new RequestHeaderFields().withAcceptTypes( MediaType.APPLICATION_JSON );
		theHeaderFields.addCookie( COOKIE_A_NAME, COOKIE_A_VALUE );
		theHeaderFields.addCookie( COOKIE_A_NAME, COOKIE_A_VALUE_2 );
		final HttpServerResponse theResponse = new HttpServerResponse( theRestServer );
		theRestServer.onHttpRequest( null, null, HttpMethod.POST, new Url( BASE_LOCATOR + LOCATOR ), theHeaderFields, theFactory.fromUnmarshaled( new Person( FIRST_NAME, LAST_NAME ) ), theResponse );
		final Person thePerson = theFactory.toUnmarshaled( theResponse.toHttpBody(), Person.class );
		assertEquals( FIRST_NAME, thePerson.getFirstName() );
		assertEquals( LAST_NAME, thePerson.getLastName() );
	}

	@Test
	public void testAllServerCookies() throws IOException, UnmarshalException, HttpStatusException, MarshalException {
		final LoopbackRestServer theRestServer = new LoopbackRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> {
			final Person thePerson = aRequest.getRequest( Person.class );
			assertEquals( FIRST_NAME, thePerson.getFirstName() );
			assertEquals( LAST_NAME, thePerson.getLastName() );
			aResponse.setResponse( thePerson );
			final List<RequestCookie> theCookies = aRequest.getHeaderFields().getAllCookies();
			assertEquals( 2, theCookies.size() );
			int a = 0;
			int b = 1;
			if ( theCookies.get( a ).getKey().equals( COOKIE_B_NAME ) ) {
				a = 1;
				b = 0;
			}
			assertEquals( COOKIE_A_NAME, theCookies.get( a ).getKey() );
			assertEquals( COOKIE_A_VALUE, theCookies.get( a ).getValue() );
			assertEquals( COOKIE_B_NAME, theCookies.get( b ).getKey() );
			assertEquals( COOKIE_B_VALUE, theCookies.get( b ).getValue() );
		} ).open();
		final MediaTypeFactory theFactory = new JsonMediaTypeFactory();
		final RequestHeaderFields theHeaderFields = new RequestHeaderFields().withAcceptTypes( MediaType.APPLICATION_JSON );
		theHeaderFields.addCookie( COOKIE_A_NAME, COOKIE_A_VALUE );
		theHeaderFields.addCookie( COOKIE_B_NAME, COOKIE_B_VALUE );
		final HttpServerResponse theResponse = new HttpServerResponse( theRestServer );
		theRestServer.onHttpRequest( null, null, HttpMethod.POST, new Url( BASE_LOCATOR + LOCATOR ), theHeaderFields, theFactory.toMarshaled( new Person( FIRST_NAME, LAST_NAME ) ), theResponse );
		final Person thePerson = theFactory.toUnmarshaled( theResponse.toHttpBody(), Person.class );
		assertEquals( FIRST_NAME, thePerson.getFirstName() );
		assertEquals( LAST_NAME, thePerson.getLastName() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static class Person {
		@Override
		public String toString() {
			return "Person [firstName=" + firstName + ", lastName=" + lastName + "]";
		}

		private String firstName;
		private String lastName;

		public Person() {};

		public Person( String firstName, String lastName ) {
			this.firstName = firstName;
			this.lastName = lastName;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName( String firstName ) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName( String lastName ) {
			this.lastName = lastName;
		}
	}
}
