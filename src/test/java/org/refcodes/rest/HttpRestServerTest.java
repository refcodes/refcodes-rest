// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import static org.junit.jupiter.api.Assertions.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.regex.Pattern;
import org.junit.jupiter.api.Test;
import org.refcodes.data.License;
import org.refcodes.io.InputStreamStringBuilder;
import org.refcodes.net.PortManagerSingleton;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.VerboseTextBuilder;
import org.refcodes.web.BasicAuthCredentials;
import org.refcodes.web.BasicAuthFailureException;
import org.refcodes.web.BasicAuthRequiredException;
import org.refcodes.web.HeaderFields;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpStatusCode;
import org.refcodes.web.RequestCookie;

public class HttpRestServerTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String LOCATOR = "/bla";
	private static final String BASE_URL = "http://localhost";
	private static final String BASE_LOCATOR = "/refcodes";
	private static final String COOKIE_A_NAME = "refcodes";
	private static final String COOKIE_A_VALUE = "org";
	private static final String COOKIE_A_VALUE_2 = "com";
	private static final String COOKIE_B_NAME = "funcodes";
	private static final String COOKIE_B_VALUE = "forever";
	private static final String LAST_NAME = "Bushnell";
	private static final String FIRST_NAME = "Nolan";
	private static final String USER_NAME = "nobody";
	private static final String PASSWORD = "secret";
	private static final String KEY_LAST_NAME = "lastName";
	private static final String KEY_FIRST_NAME = "firstName";

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testHttpRestServer() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final Person thePerson = aRequest.getRequest( Person.class ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		theRestClient.onResponse( HttpMethod.POST, LOCATOR, ( aResponse ) -> {
			final Person thePerson = aResponse.getResponse( Person.class );
			assertEquals( FIRST_NAME, thePerson.getFirstName() );
			assertEquals( LAST_NAME, thePerson.getLastName() );
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new Person( FIRST_NAME, LAST_NAME ) ).open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testHttpRestServerPathPattern() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( "/bla/*/blub/${company}", ( aRequest, aResponse ) -> {
			final String[] theNames = aRequest.getWildcardNames();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Wildcard names = " + VerboseTextBuilder.asString( theNames ) );
			}
			assertEquals( 1, theNames.length );
			assertEquals( "company", theNames[0] );
			final String[] theReplacements = aRequest.getWildcardReplacements();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Wildcard replacements = " + VerboseTextBuilder.asString( theReplacements ) );
			}
			assertEquals( 2, theReplacements.length );
			assertEquals( "atari", theReplacements[0] );
			assertEquals( "commodore", theReplacements[1] );
			final Person thePerson = aRequest.getRequest( Person.class );
			assertEquals( FIRST_NAME, thePerson.getFirstName() );
			assertEquals( LAST_NAME, thePerson.getLastName() );
			aResponse.setResponse( thePerson );
		} ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		theRestClient.onResponse( HttpMethod.POST, "/bla/atari/blub/commodore", ( aResponse ) -> {
			final Person thePerson = aResponse.getResponse( Person.class );
			assertEquals( FIRST_NAME, thePerson.getFirstName() );
			assertEquals( LAST_NAME, thePerson.getLastName() );
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new Person( FIRST_NAME, LAST_NAME ) ).open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testHttpRestServerRegExp() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( Pattern.compile( "/bla/([^/]*)/blub/(?<company>[^/]*)" ), ( aRequest, aResponse ) -> {
			final String[] theNames = aRequest.getWildcardNames();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Wildcard names = " + VerboseTextBuilder.asString( theNames ) );
			}
			assertEquals( 1, theNames.length );
			assertEquals( "company", theNames[0] );
			final String[] theReplacements = aRequest.getWildcardReplacements();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Wildcard replacements = " + VerboseTextBuilder.asString( theReplacements ) );
			}
			assertEquals( 2, theReplacements.length );
			assertEquals( "atari", theReplacements[0] );
			assertEquals( "commodore", theReplacements[1] );
			final Person thePerson = aRequest.getRequest( Person.class );
			assertEquals( FIRST_NAME, thePerson.getFirstName() );
			assertEquals( LAST_NAME, thePerson.getLastName() );
			aResponse.setResponse( thePerson );
		} ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		theRestClient.onResponse( HttpMethod.POST, "/bla/atari/blub/commodore", ( aResponse ) -> {
			final Person thePerson = aResponse.getResponse( Person.class );
			assertEquals( FIRST_NAME, thePerson.getFirstName() );
			assertEquals( LAST_NAME, thePerson.getLastName() );
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new Person( FIRST_NAME, LAST_NAME ) ).open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testHttpRestServerAllMethods1() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onRequest( LOCATOR, ( aRequest, aResponse ) -> { final Person thePerson = aRequest.getRequest( Person.class ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		theRestClient.onResponse( HttpMethod.POST, LOCATOR, ( aResponse ) -> {
			final Person thePerson = aResponse.getResponse( Person.class );
			assertEquals( FIRST_NAME, thePerson.getFirstName() );
			assertEquals( LAST_NAME, thePerson.getLastName() );
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new Person( FIRST_NAME, LAST_NAME ) ).open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testHttpRestServerAllMethods2() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onRequest( LOCATOR, ( aRequest, aResponse ) -> { final Person thePerson = new Person( aRequest.getUrl().getQueryFields().getFirst( KEY_FIRST_NAME ), aRequest.getUrl().getQueryFields().getFirst( KEY_LAST_NAME ) ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		final RestResponseHandler theCaller = theRestClient.onResponse( HttpMethod.GET, LOCATOR, ( aResponse ) -> {
			final Person thePerson = aResponse.getResponse( Person.class );
			assertEquals( FIRST_NAME, thePerson.getFirstName() );
			assertEquals( LAST_NAME, thePerson.getLastName() );
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} );
		theCaller.getQueryFields().withPut( KEY_LAST_NAME, LAST_NAME ).withPut( KEY_FIRST_NAME, FIRST_NAME );
		theCaller.open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testHttpRestServerStreamA() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final InputStream theInputStream = aRequest.getRequest( InputStream.class ); final String theHttpBody = new InputStreamStringBuilder().withInputStream( theInputStream ).toString(); assertEquals( License.REFCODES_LICENSE.getText(), theHttpBody ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		theRestClient.onResponse( HttpMethod.POST, LOCATOR, ( aResponse ) -> {
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new ByteArrayInputStream( License.REFCODES_LICENSE.getText().getBytes( StandardCharsets.UTF_8 ) ) ).open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testHttpRestServerStreamB() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final InputStream theInputStream = aRequest.getRequest( InputStream.class ); final String theHttpBody = new InputStreamStringBuilder().withInputStream( theInputStream ).toString(); assertNotEquals( License.REFCODES_LICENSE.getText(), theHttpBody ); assertEquals( License.REFCODES_LICENSE.getText().replaceAll( "\\n", "\r\n" ), theHttpBody ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		theRestClient.onResponse( HttpMethod.POST, LOCATOR, ( aResponse ) -> {
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new ByteArrayInputStream( License.REFCODES_LICENSE.getText().replaceAll( "\\n", "\r\n" ).getBytes( StandardCharsets.UTF_8 ) ) ).open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testHttpRestServerStreamC() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final InputStream theInputStream = aRequest.getRequest( InputStream.class ); final String theHttpBody = new InputStreamStringBuilder().withInputStream( theInputStream ).toString(); assertNotEquals( License.REFCODES_LICENSE.getText(), theHttpBody ); assertEquals( ( License.REFCODES_LICENSE.getText() + "\n" ), theHttpBody ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		theRestClient.onResponse( HttpMethod.POST, LOCATOR, ( aResponse ) -> {
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new ByteArrayInputStream( ( License.REFCODES_LICENSE.getText() + "\n" ).getBytes( StandardCharsets.UTF_8 ) ) ).open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testServerCookie() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final RequestCookie theCookie = aRequest.getHeaderFields().getFirstCookie( COOKIE_A_NAME ); assertEquals( COOKIE_A_NAME, theCookie.getKey() ); assertEquals( COOKIE_A_VALUE, theCookie.getValue() ); final Person thePerson = aRequest.getRequest( Person.class ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		final RestResponseHandler theBuilder = theRestClient.onResponse( HttpMethod.POST, LOCATOR, ( aResponse ) -> {
			final Person thePerson = aResponse.getResponse( Person.class );
			assertEquals( FIRST_NAME, thePerson.getFirstName() );
			assertEquals( LAST_NAME, thePerson.getLastName() );
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new Person( FIRST_NAME, LAST_NAME ) );
		theBuilder.getHeaderFields().addCookie( COOKIE_A_NAME, COOKIE_A_VALUE );
		theBuilder.open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testServerCookies() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> {
			final Person thePerson = aRequest.getRequest( Person.class );
			assertEquals( FIRST_NAME, thePerson.getFirstName() );
			assertEquals( LAST_NAME, thePerson.getLastName() );
			aResponse.setResponse( thePerson );
			final List<RequestCookie> theCookies = aRequest.getHeaderFields().getAllCookies();
			assertEquals( 2, theCookies.size() );
			int a = 0;
			int b = 1;
			if ( theCookies.get( a ).getValue().equals( COOKIE_A_VALUE_2 ) ) {
				a = 1;
				b = 0;
			}
			assertEquals( COOKIE_A_NAME, theCookies.get( a ).getKey() );
			assertEquals( COOKIE_A_VALUE, theCookies.get( a ).getValue() );
			assertEquals( COOKIE_A_NAME, theCookies.get( b ).getKey() );
			assertEquals( COOKIE_A_VALUE_2, theCookies.get( b ).getValue() );
		} ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		final RestResponseHandler theBuilder = theRestClient.onResponse( HttpMethod.POST, LOCATOR, ( aResponse ) -> {
			final Person thePerson = aResponse.getResponse( Person.class );
			assertEquals( FIRST_NAME, thePerson.getFirstName() );
			assertEquals( LAST_NAME, thePerson.getLastName() );
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new Person( FIRST_NAME, LAST_NAME ) );
		theBuilder.getHeaderFields().addCookie( COOKIE_A_NAME, COOKIE_A_VALUE );
		theBuilder.getHeaderFields().addCookie( COOKIE_A_NAME, COOKIE_A_VALUE_2 );
		theBuilder.open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testAllServerCookies() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> {
			final Person thePerson = aRequest.getRequest( Person.class );
			assertEquals( FIRST_NAME, thePerson.getFirstName() );
			assertEquals( LAST_NAME, thePerson.getLastName() );
			aResponse.setResponse( thePerson );
			final List<RequestCookie> theCookies = aRequest.getHeaderFields().getAllCookies();
			assertEquals( 2, theCookies.size() );
			int a = 0;
			int b = 1;
			if ( theCookies.get( a ).getKey().equals( COOKIE_B_NAME ) ) {
				a = 1;
				b = 0;
			}
			assertEquals( COOKIE_A_NAME, theCookies.get( a ).getKey() );
			assertEquals( COOKIE_A_VALUE, theCookies.get( a ).getValue() );
			assertEquals( COOKIE_B_NAME, theCookies.get( b ).getKey() );
			assertEquals( COOKIE_B_VALUE, theCookies.get( b ).getValue() );
		} ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		final RestResponseHandler theBuilder = theRestClient.onResponse( HttpMethod.POST, LOCATOR, ( aResponse ) -> {
			final Person thePerson = aResponse.getResponse( Person.class );
			assertEquals( FIRST_NAME, thePerson.getFirstName() );
			assertEquals( LAST_NAME, thePerson.getLastName() );
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new Person( FIRST_NAME, LAST_NAME ) );
		theBuilder.getHeaderFields().addCookie( COOKIE_A_NAME, COOKIE_A_VALUE );
		theBuilder.getHeaderFields().addCookie( COOKIE_B_NAME, COOKIE_B_VALUE );
		theBuilder.open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testBasicAuthSuccess1() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onGet( LOCATOR, ( aRequest, aResponse ) -> {
			final BasicAuthCredentials theCredentials = aRequest.getHeaderFields().getBasicAuthCredentials();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theCredentials.toString() + " := " + theCredentials.toHttpAuthorization() );
			}
			assertEquals( USER_NAME, theCredentials.getIdentity() );
			assertEquals( PASSWORD, theCredentials.getSecret() );
			aResponse.setResponse( "Authentification success!" );
		} ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		final RestResponseHandler theCaller = theRestClient.onResponse( HttpMethod.GET, LOCATOR, ( aResponse ) -> {
			assertEquals( HttpStatusCode.OK, aResponse.getHttpStatusCode() );
			final String theMessage = aResponse.getResponse( String.class );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theMessage );
			}
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} );
		theCaller.getHeaderFields().putBasicAuthCredentials( USER_NAME, PASSWORD );
		theCaller.open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testBasicAuthSuccess2() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onGet( LOCATOR, ( aRequest, aResponse ) -> {
			final BasicAuthCredentials theCredentials = aRequest.getHeaderFields().getBasicAuthCredentials();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theCredentials.toString() + " := " + theCredentials.toHttpAuthorization() );
			}
			assertEquals( USER_NAME, theCredentials.getIdentity() );
			assertEquals( PASSWORD, theCredentials.getSecret() );
			aResponse.setResponse( "Authentification success!" );
		} ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) ).withBasicAuthCredentials( USER_NAME, PASSWORD );
		final RestResponseHandler theCaller = theRestClient.onResponse( HttpMethod.GET, LOCATOR, ( aResponse ) -> {
			assertEquals( HttpStatusCode.OK, aResponse.getHttpStatusCode() );
			final String theMessage = aResponse.getResponse( String.class );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theMessage );
			}
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} );
		theCaller.open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testBasicAuthFailure() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onGet( LOCATOR, ( aRequest, aResponse ) -> {
			final BasicAuthCredentials theCredentials = aRequest.getHeaderFields().getBasicAuthCredentials();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theCredentials.toString() + " := " + theCredentials.toHttpAuthorization() );
			}
			assertEquals( USER_NAME, theCredentials.getIdentity() );
			assertEquals( PASSWORD, theCredentials.getSecret() );
			throw new BasicAuthFailureException( "Authentication failed!" );
		} ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		final RestResponseHandler theCaller = theRestClient.onResponse( HttpMethod.GET, LOCATOR, ( aResponse ) -> {
			assertEquals( HttpStatusCode.UNAUTHORIZED, aResponse.getHttpStatusCode() );
			final String theAuth = aResponse.getHeaderFields().getAuthenticate();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Authenticate := " + theAuth );
			}
			final String theMessage = aResponse.getHttpBody();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Error message := " + theMessage );
			}
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} );
		theCaller.getHeaderFields().putBasicAuthCredentials( USER_NAME, PASSWORD );
		theCaller.open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testBasicAuthRequired() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onGet( LOCATOR, ( aRequest, aResponse ) -> {
			final BasicAuthCredentials theCredentials = aRequest.getHeaderFields().getBasicAuthCredentials();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theCredentials.toString() + " := " + theCredentials.toHttpAuthorization() );
			}
			assertEquals( USER_NAME, theCredentials.getIdentity() );
			assertEquals( PASSWORD, theCredentials.getSecret() );
			throw new BasicAuthRequiredException( "Authentification failed!" );
		} ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		final RestResponseHandler theCaller = theRestClient.onResponse( HttpMethod.GET, LOCATOR, ( aResponse ) -> {
			assertEquals( HttpStatusCode.UNAUTHORIZED, aResponse.getHttpStatusCode() );
			final String theAuth = aResponse.getHeaderFields().getAuthenticate();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Authenticate := " + theAuth );
			}
			assertNotNull( theAuth );
			assertTrue( theAuth.startsWith( HeaderFields.BASIC_REALM ) );
			final String theMessage = aResponse.getHttpBody();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Error message := " + theMessage );
			}
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} );
		theCaller.getHeaderFields().putBasicAuthCredentials( USER_NAME, PASSWORD );
		theCaller.open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testQueryFields() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { assertEquals( FIRST_NAME, aRequest.getUrl().getQueryFields().getFirst( KEY_FIRST_NAME ) ); assertEquals( LAST_NAME, aRequest.getUrl().getQueryFields().getFirst( KEY_LAST_NAME ) ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		final RestResponseHandler theCaller = theRestClient.onResponse( HttpMethod.POST, LOCATOR, ( aResponse ) -> {
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} );
		theCaller.getQueryFields().withAddTo( KEY_FIRST_NAME, FIRST_NAME ).withAddTo( KEY_LAST_NAME, LAST_NAME );
		theCaller.open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private String toBaseUrl( Integer thePort ) {
		return BASE_URL + ":" + thePort + BASE_LOCATOR;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static class Person {
		@Override
		public String toString() {
			return "Person [firstName=" + firstName + ", lastName=" + lastName + "]";
		}

		private String firstName;
		private String lastName;

		public Person() {};

		public Person( String firstName, String lastName ) {
			this.firstName = firstName;
			this.lastName = lastName;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName( String firstName ) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName( String lastName ) {
			this.lastName = lastName;
		}
	}
}
