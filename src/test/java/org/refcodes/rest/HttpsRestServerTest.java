// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import static org.junit.jupiter.api.Assertions.*;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.security.KeyStoreDescriptorBuilderImpl;
import org.refcodes.security.TrustStoreDescriptorBuilderImpl;
import org.refcodes.web.HttpMethod;

public class HttpsRestServerTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String LOCATOR = "/bla";
	private static final String BASE_URL = "https://localhost:5161/refcodes";
	private static final String BASE_LOCATOR = "/refcodes";
	private static final String LAST_NAME = "Bushnell";
	private static final String FIRST_NAME = "Nolan";
	private static final String KEY_STORE_A = "keystore_a.jks";
	private static final String KEY_STORE_A_PASSWORD = "keyStoreSecret";
	private static final String KEY_A_PASSWORD = "keySecret";
	private static final String KEY_STORE_B = "keystore_b.jks";
	private static final String KEY_STORE_B_PASSWORD = "keyStoreSecret";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private boolean _hasConnectionRequest;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * "keytool -genkey -keyalg RSA -alias localhost -keystore keystore_a.jks
	 * -storepass keyStoreSecret -keypass keySecret"
	 */
	@Test
	// Disabled("Either the one A or the other B test can be executed in a JVM instance.")
	public void testHttpsRestServerA() throws IOException, URISyntaxException {
		_hasConnectionRequest = false;
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final Person thePerson = aRequest.getRequest( Person.class ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); } ).open();
		theRestServer.onConnectionRequest( ( aLocalAddress, aRemoteAddress, aHttpsParams ) -> {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Local := " + aLocalAddress + ", remote := " + aRemoteAddress + ", HTTP params := " + aHttpsParams );
			}
			if ( _hasConnectionRequest ) {
				fail( "Expecting just one connection request." );
			}
			_hasConnectionRequest = true;
		} );
		theRestServer.open( new KeyStoreDescriptorBuilderImpl( toResourceFile( KEY_STORE_A ), KEY_STORE_A_PASSWORD, KEY_A_PASSWORD ), 5161 );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( BASE_URL );
		theRestClient.open( new TrustStoreDescriptorBuilderImpl( toResourceFile( KEY_STORE_A ), KEY_STORE_A_PASSWORD ) );
		theRestClient.onResponse( HttpMethod.POST, LOCATOR, ( aResponse ) -> {
			final Person thePerson = aResponse.getResponse( Person.class );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( thePerson.toString() );
			}
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new Person( FIRST_NAME, LAST_NAME ) ).open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		assertTrue( _hasConnectionRequest );
	}

	/**
	 * "keytool -genkey -keyalg RSA -alias localhost -keystore keystore_b.jks
	 * -storepass keyStoreSecret"
	 */
	@Test
	@Disabled("Either the one A or the other B test can be executed in a JVM instance.")
	public void testHttpsRestServerB() throws IOException, URISyntaxException {
		_hasConnectionRequest = false;
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final Person thePerson = aRequest.getRequest( Person.class ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); } ).open();
		theRestServer.onConnectionRequest( ( aLocalAddress, aRemoteAddress, aHttpsParams ) -> {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Local := " + aLocalAddress + ", remote := " + aRemoteAddress + ", HTTP params := " + aHttpsParams );
			}
			if ( _hasConnectionRequest ) {
				fail( "Expecting just one connection request." );
			}
			_hasConnectionRequest = true;
		} );
		theRestServer.open( new KeyStoreDescriptorBuilderImpl( toResourceFile( KEY_STORE_B ), KEY_STORE_B_PASSWORD ), 5161 );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( BASE_URL );
		theRestClient.open( new TrustStoreDescriptorBuilderImpl( toResourceFile( KEY_STORE_B ) ) );
		theRestClient.onResponse( HttpMethod.POST, LOCATOR, ( aResponse ) -> {
			final Person thePerson = aResponse.getResponse( Person.class );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( thePerson.toString() );
			}
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new Person( FIRST_NAME, LAST_NAME ) ).open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		assertTrue( _hasConnectionRequest );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	protected File toResourceFile( String aKeystoreFile ) throws URISyntaxException {
		return new File( HttpsRestServerTest.class.getResource( "/" + aKeystoreFile ).toURI() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * The Class Person.
	 */
	public static class Person {
		@Override
		public String toString() {
			return "Person [firstName=" + firstName + ", lastName=" + lastName + "]";
		}

		private String firstName;
		private String lastName;

		public Person() {};

		public Person( String firstName, String lastName ) {
			this.firstName = firstName;
			this.lastName = lastName;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName( String firstName ) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName( String lastName ) {
			this.lastName = lastName;
		}
	}
}
