// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.rest.HttpRestClientSugar.*;
import static org.refcodes.rest.HttpRestServerSugar.*;
import static org.refcodes.rest.HttpRestServerSugar.onPost;
import static org.refcodes.rest.HttpRestServerSugar.open;
import static org.refcodes.rest.RestPostClientSugar.onPost;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.refcodes.net.PortManagerSingleton;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.web.HttpResponseException;

public class HttpRestSugarTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String BASE_URL = "http://localhost";
	private static final String BASE_LOCATOR = "/refcodes";
	private static final String LOCATOR = "/bla";
	private static final String LAST_NAME = "Bushnell";
	private static final String FIRST_NAME = "Nolan";

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testAsynchronousRestSugar() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		withBaseLocator( BASE_LOCATOR );
		final RestEndpointBuilder theObserver = onPost( LOCATOR, ( aRequest, aResponse ) -> { final Person thePerson = aRequest.getRequest( Person.class ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); } );
		theObserver.open();
		open( thePort );
		withBaseUrl( toBaseUrl( thePort ) );
		onPost( LOCATOR, ( aResponse ) -> {
			final Person thePerson = aResponse.getResponse( Person.class );
			assertEquals( FIRST_NAME, thePerson.getFirstName() );
			assertEquals( LAST_NAME, thePerson.getLastName() );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( thePerson.toString() );
			}
			closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new Person( FIRST_NAME, LAST_NAME ) ).open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		unsubscribeObserver( theObserver );
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testSynchronousRestSugar() throws IOException, HttpResponseException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		withBaseLocator( BASE_LOCATOR );
		final RestEndpointBuilder theObserver = onPost( LOCATOR, ( aRequest, aResponse ) -> { final Person thePerson = aRequest.getRequest( Person.class ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); } );
		theObserver.open();
		open( thePort );
		withBaseUrl( toBaseUrl( thePort ) );
		final RestResponse theResponse = doPost( LOCATOR, new Person( FIRST_NAME, LAST_NAME ) );
		final Person thePerson = theResponse.getResponse( Person.class );
		assertEquals( FIRST_NAME, thePerson.getFirstName() );
		assertEquals( LAST_NAME, thePerson.getLastName() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( thePerson.toString() );
		}
		unsubscribeObserver( theObserver );
		closeQuietly();
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private String toBaseUrl( Integer thePort ) {
		return BASE_URL + ":" + thePort + BASE_LOCATOR;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static class Person {
		@Override
		public String toString() {
			return "Person [firstName=" + firstName + ", lastName=" + lastName + "]";
		}

		private String firstName;
		private String lastName;

		public Person() {};

		public Person( String firstName, String lastName ) {
			this.firstName = firstName;
			this.lastName = lastName;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName( String firstName ) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName( String lastName ) {
			this.lastName = lastName;
		}
	}
}
