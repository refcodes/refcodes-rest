// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.refcodes.net.PortManagerSingleton;
import org.refcodes.rest.HttpRestClientTest.Person;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpResponseException;

public class CorrelationRestClientTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String LOCATOR = "/bla";
	private static final String BASE_URL = "http://localhost";
	private static final String BASE_LOCATOR = "/refcodes";
	private static final String LAST_NAME = "Bushnell";
	private static final String FIRST_NAME = "Nolan";

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testCorreltionId() throws IOException, HttpResponseException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.addPreHttpInterceptor( ( req, res ) -> { res.getHeaderFields().putSessionId( req.getHeaderFields().getSessionId() ); res.getHeaderFields().putRequestId( req.getHeaderFields().getRequestId() ); } );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final Person thePerson = aRequest.getRequest( Person.class ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		theRestClient.addHttpInterceptor( new CorrelationClientInterceptor() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			theRestClient.addPreHttpInterceptor( ( req, res ) -> System.out.println( "HTTP-Request: Session-ID = " + req.getHeaderFields().getSessionId() + ", Request-ID = " + req.getHeaderFields().getRequestId() ) );
			theRestClient.addPostHttpInterceptor( ( req, res ) -> System.out.println( "HTTP-Response: Session-ID = " + res.getHeaderFields().getSessionId() + ", Request-ID = " + res.getHeaderFields().getRequestId() ) );
		}
		final RestResponse aResponse = theRestClient.doRequest( HttpMethod.POST, LOCATOR, new Person( FIRST_NAME, LAST_NAME ) );
		final Person thePerson = aResponse.getResponse( Person.class );
		final String theRequestId = aResponse.getHeaderFields().getRequestId();
		final String theSessionId = aResponse.getHeaderFields().getSessionId();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Request = " + theRequestId );
			System.out.println( "Session = " + theSessionId );
			System.out.println( thePerson.toString() );
		}
		theRestServer.closeQuietly();
		PortManagerSingleton.getInstance().unbindPort( thePort );
		assertNotNull( theRequestId );
		assertNotNull( theSessionId );
	}

	@Test
	public void testNoCorreltionId() throws IOException, HttpResponseException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final Person thePerson = aRequest.getRequest( Person.class ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		final RestResponse aResponse = theRestClient.doRequest( HttpMethod.POST, LOCATOR, new Person( FIRST_NAME, LAST_NAME ) );
		final Person thePerson = aResponse.getResponse( Person.class );
		final String theRequestId = aResponse.getHeaderFields().getRequestId();
		final String theSessionId = aResponse.getHeaderFields().getSessionId();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Request = " + theRequestId );
			System.out.println( "Session = " + theSessionId );
			System.out.println( thePerson.toString() );
		}
		theRestServer.closeQuietly();
		PortManagerSingleton.getInstance().unbindPort( thePort );
		assertNull( theRequestId );
		assertNull( theSessionId );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private String toBaseUrl( Integer thePort ) {
		return BASE_URL + ":" + thePort + BASE_LOCATOR;
	}
}
