// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.net.PortManagerSingleton;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.HorizAlignTextBuilder;
import org.refcodes.textual.HorizAlignTextMode;
import org.refcodes.textual.VerboseTextBuilder;
import org.refcodes.web.FormFields;
import org.refcodes.web.HeaderField;
import org.refcodes.web.HttpBodyMap;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.HttpResponseException;
import org.refcodes.web.HttpStatusCode;
import org.refcodes.web.MediaType;
import org.refcodes.web.ResponseCookie;

public class HttpRestClientTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int MOD_BYTE = 256;
	private static final String LOCATOR = "/bla";
	private static final String REDIRECT = "/blub";
	private static final String BASE_URL = "http://localhost";
	private static final String BASE_LOCATOR = "/refcodes";
	private static final String COOKIE_A_PATH = "/refcodes/bla";
	private static final String COOKIE_A_NAME = "refcodes";
	private static final String COOKIE_A_VALUE = "org";
	private static final String COOKIE_A_VALUE_2 = "com";
	private static final String COOKIE_B_PATH = "/refcodes/blub";
	private static final String COOKIE_B_NAME = "funcodes";
	private static final String COOKIE_B_VALUE = "forever";
	private static final String LAST_NAME = "Bushnell";
	private static final String FIRST_NAME = "Nolan";
	private static final String KEY_LAST_NAME = "lastName";
	private static final String KEY_FIRST_NAME = "firstName";

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testAsynchtronousRestClient() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final Person thePerson = aRequest.getRequest( Person.class ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		theRestClient.onResponse( HttpMethod.POST, LOCATOR, ( aResponse ) -> {
			final Person thePerson = aResponse.getResponse( Person.class );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( thePerson.toString() );
			}
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new Person( FIRST_NAME, LAST_NAME ) ).open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
		theRestClient.buildPost( "123" );
	}

	@Test
	public void testAsynchtronousRestClient2() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final Person thePerson = aRequest.getRequest( Person.class ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		theRestClient.onResponse( HttpMethod.POST, LOCATOR, new Person( FIRST_NAME, LAST_NAME ), ( aResponse ) -> {
			final Person thePerson = aResponse.getResponse( Person.class );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( thePerson.toString() );
			}
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testSynchtronousRestClient() throws IOException, HttpResponseException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final Person thePerson = aRequest.getRequest( Person.class ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		final RestResponse aResponse = theRestClient.doRequest( HttpMethod.POST, LOCATOR, new Person( FIRST_NAME, LAST_NAME ) );
		final Person thePerson = aResponse.getResponse( Person.class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( thePerson.toString() );
		}
		theRestServer.closeQuietly();
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testResponseBuilderOverClient() throws IOException, HttpResponseException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final Person thePerson = aRequest.getRequest( Person.class ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		final RestResponse theResponse = theRestClient.buildRequest( HttpMethod.POST, LOCATOR, new Person( FIRST_NAME, LAST_NAME ) ).toRestResponse();
		final Person thePerson = theResponse.getResponse( Person.class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( thePerson.toString() );
		}
		theRestServer.closeQuietly();
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testClientRedirect1() throws IOException, HttpResponseException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { aResponse.setHttpStatusCode( HttpStatusCode.TEMPORARY_REDIRECT ); aResponse.getHeaderFields().putLocation( BASE_LOCATOR + REDIRECT ); } ).open();
		theRestServer.onPost( REDIRECT, ( aRequest, aResponse ) -> { aResponse.setHttpStatusCode( HttpStatusCode.OK ); aResponse.setResponse( aRequest.getHttpBody() + " --> " + "Pong!" ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		final RestRequestBuilder theRequest = theRestClient.buildRequest( HttpMethod.POST, LOCATOR, "Ping?" ).withAddToHeaderFields( HeaderField.CONTENT_TYPE, MediaType.TEXT_PLAIN.toHttpMediaType() ).withAddToHeaderFields( HeaderField.ACCEPT, MediaType.TEXT_PLAIN.toHttpMediaType() );
		final RestResponse theResponse = theRequest.toRestResponse();
		final String theBody = theResponse.getHttpBody();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theBody );
		}
		assertEquals( "Ping? --> Pong!", theBody );
		theRestServer.closeQuietly();
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testClientRedirect2() throws IOException, HttpResponseException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { aResponse.setHttpStatusCode( HttpStatusCode.TEMPORARY_REDIRECT ); aResponse.getHeaderFields().putLocation( BASE_LOCATOR + REDIRECT ); } ).open();
		theRestServer.onPost( REDIRECT, ( aRequest, aResponse ) -> { aResponse.setHttpStatusCode( HttpStatusCode.OK ); aResponse.setResponse( aRequest.getHttpBody() + " --> " + "Pong!" ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		final RestRequestBuilder theRequest = theRestClient.buildRequest( HttpMethod.POST, LOCATOR, "Ping?" ).withRedirectDepth( -1 ).withAddToHeaderFields( HeaderField.CONTENT_TYPE, MediaType.TEXT_PLAIN.toHttpMediaType() ).withAddToHeaderFields( HeaderField.ACCEPT, MediaType.TEXT_PLAIN.toHttpMediaType() );
		final RestResponse theResponse = theRequest.toRestResponse();
		final String theBody = theResponse.getHttpBody();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theBody );
		}
		assertEquals( "Ping? --> Pong!", theBody );
		theRestServer.closeQuietly();
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testClientRedirect3() throws IOException, HttpResponseException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		// No header set!: aResponse.getHeaderFields().putLocation( BASE_LOCATOR + REDIRECT );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> aResponse.setHttpStatusCode( HttpStatusCode.TEMPORARY_REDIRECT ) ).open();
		theRestServer.onPost( REDIRECT, ( aRequest, aResponse ) -> { aResponse.setHttpStatusCode( HttpStatusCode.OK ); aResponse.setResponse( aRequest.getHttpBody() + " --> " + "Pong!" ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		final RestRequestBuilder theRequest = theRestClient.buildRequest( HttpMethod.POST, LOCATOR, "Ping?" ).withAddToHeaderFields( HeaderField.CONTENT_TYPE, MediaType.TEXT_PLAIN.toHttpMediaType() ).withAddToHeaderFields( HeaderField.ACCEPT, MediaType.TEXT_PLAIN.toHttpMediaType() );
		final RestResponse theResponse = theRequest.toRestResponse();
		final String theBody = theResponse.getHttpBody();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theBody );
		}
		assertEquals( HttpStatusCode.TEMPORARY_REDIRECT, theResponse.getHttpStatusCode() );
		theRestServer.closeQuietly();
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testClientCookie() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final Person thePerson = aRequest.getRequest( Person.class ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); aResponse.getHeaderFields().addCookie( COOKIE_A_NAME, COOKIE_A_VALUE ).withPath( COOKIE_A_PATH ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		theRestClient.onResponse( HttpMethod.POST, LOCATOR, ( aResponse ) -> {
			final Person thePerson = aResponse.getResponse( Person.class );
			assertEquals( FIRST_NAME, thePerson.getFirstName() );
			assertEquals( LAST_NAME, thePerson.getLastName() );
			final ResponseCookie theCookie = aResponse.getHeaderFields().getFirstCookie( COOKIE_A_NAME );
			assertEquals( COOKIE_A_NAME, theCookie.getKey() );
			assertEquals( COOKIE_A_VALUE, theCookie.getValue() );
			assertEquals( COOKIE_A_PATH, theCookie.getPath() );
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new Person( FIRST_NAME, LAST_NAME ) ).open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testClientCookies() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final Person thePerson = aRequest.getRequest( Person.class ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); aResponse.getHeaderFields().addCookie( COOKIE_A_NAME, COOKIE_A_VALUE ).withPath( COOKIE_A_PATH ); aResponse.getHeaderFields().addCookie( COOKIE_A_NAME, COOKIE_A_VALUE_2 ).withPath( COOKIE_A_PATH ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		theRestClient.onResponse( HttpMethod.POST, LOCATOR, ( aResponse ) -> {
			final Person thePerson = aResponse.getResponse( Person.class );
			assertEquals( FIRST_NAME, thePerson.getFirstName() );
			assertEquals( LAST_NAME, thePerson.getLastName() );
			final List<ResponseCookie> theCookies = aResponse.getHeaderFields().getCookies( COOKIE_A_NAME );
			assertEquals( 2, theCookies.size(), "Expecting two cookies" );
			int a = 0;
			int b = 1;
			if ( theCookies.get( a ).getValue().equals( COOKIE_A_VALUE_2 ) ) {
				a = 1;
				b = 0;
			}
			assertEquals( COOKIE_A_NAME, theCookies.get( a ).getKey() );
			assertEquals( COOKIE_A_VALUE, theCookies.get( a ).getValue() );
			assertEquals( COOKIE_A_PATH, theCookies.get( a ).getPath() );
			assertEquals( COOKIE_A_NAME, theCookies.get( b ).getKey() );
			assertEquals( COOKIE_A_VALUE_2, theCookies.get( b ).getValue() );
			assertEquals( COOKIE_A_PATH, theCookies.get( b ).getPath() );
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new Person( FIRST_NAME, LAST_NAME ) ).open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testAllClientCookies() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final Person thePerson = aRequest.getRequest( Person.class ); assertEquals( FIRST_NAME, thePerson.getFirstName() ); assertEquals( LAST_NAME, thePerson.getLastName() ); aResponse.setResponse( thePerson ); aResponse.getHeaderFields().addCookie( COOKIE_A_NAME, COOKIE_A_VALUE ).withPath( COOKIE_A_PATH ); aResponse.getHeaderFields().addCookie( COOKIE_B_NAME, COOKIE_B_VALUE ).withPath( COOKIE_B_PATH ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		theRestClient.onResponse( HttpMethod.POST, LOCATOR, ( aResponse ) -> {
			final Person thePerson = aResponse.getResponse( Person.class );
			assertEquals( FIRST_NAME, thePerson.getFirstName() );
			assertEquals( LAST_NAME, thePerson.getLastName() );
			final List<ResponseCookie> theCookies = aResponse.getHeaderFields().getAllCookies();
			assertEquals( 2, theCookies.size() );
			int a = 0;
			int b = 1;
			if ( theCookies.get( a ).getKey().equals( COOKIE_B_NAME ) ) {
				a = 1;
				b = 0;
			}
			assertEquals( COOKIE_A_NAME, theCookies.get( a ).getKey() );
			assertEquals( COOKIE_A_VALUE, theCookies.get( a ).getValue() );
			assertEquals( COOKIE_A_PATH, theCookies.get( a ).getPath() );
			assertEquals( COOKIE_B_NAME, theCookies.get( b ).getKey() );
			assertEquals( COOKIE_B_VALUE, theCookies.get( b ).getValue() );
			assertEquals( COOKIE_B_PATH, theCookies.get( b ).getPath() );
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new Person( FIRST_NAME, LAST_NAME ) ).open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testWebForm() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final FormFields theFormFields = aRequest.getRequest( FormFields.class ); assertEquals( FIRST_NAME, theFormFields.getFirst( KEY_FIRST_NAME ) ); assertEquals( LAST_NAME, theFormFields.getFirst( KEY_LAST_NAME ) ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		final FormFields theWebForm = new FormFields().withAddTo( KEY_FIRST_NAME, FIRST_NAME ).withAddTo( KEY_LAST_NAME, LAST_NAME );
		final RestResponseHandler theCaller = theRestClient.onResponse( HttpMethod.POST, LOCATOR, ( aResponse ) -> {
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( theWebForm );
		theCaller.getHeaderFields().putContentType( MediaType.APPLICATION_X_WWW_FORM_URLENCODED );
		theCaller.open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testRequestResult() throws IOException, InterruptedException {
		final String MESSAGE = "Hallo Welt!";
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final FormFields theFormFields = aRequest.getRequest( FormFields.class ); assertEquals( FIRST_NAME, theFormFields.getFirst( KEY_FIRST_NAME ) ); assertEquals( LAST_NAME, theFormFields.getFirst( KEY_LAST_NAME ) ); aResponse.setResponse( MESSAGE ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		final FormFields theWebForm = new FormFields().withAddTo( KEY_FIRST_NAME, FIRST_NAME ).withAddTo( KEY_LAST_NAME, LAST_NAME );
		final RestResponseResult theRequest = theRestClient.onResponse( HttpMethod.POST, LOCATOR ).withRequest( theWebForm );
		theRequest.getHeaderFields().putContentType( MediaType.APPLICATION_X_WWW_FORM_URLENCODED );
		theRequest.open();
		final RestResponse theResult = theRequest.getResult();
		assertEquals( "\"" + MESSAGE + "\"", theResult.getHttpBody() );
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testHttpRestClientStreamA() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> {
			final InputStream theInputStream = aRequest.getRequest( InputStream.class );
			try {
				int i = 0;
				int theCounter = 0;
				int eByte;
				StringBuilder theBuffer = new StringBuilder();
				while ( theInputStream.available() > 0 ) {
					eByte = theInputStream.read();
					assertEquals( theCounter % MOD_BYTE, eByte );
					theCounter++;
					theBuffer.append( new HorizAlignTextBuilder().withColumnWidth( 3 ).withFillChar( '0' ).withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).toString( Integer.toString( eByte ) ) + " " );
					i++;
					if ( i >= 8 || theInputStream.available() == 0 ) {
						if ( SystemProperty.LOG_TESTS.isEnabled() ) {
							System.out.println( theBuffer.toString() );
						}
						i = 0;
						theBuffer = new StringBuilder();
					}
				}
			}
			catch ( Exception e ) {
				fail( "Should not reach this code!" );
			}
		} ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		theRestClient.onResponse( HttpMethod.POST, LOCATOR, ( aResponse ) -> {
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new NumberSequenceInputStream() ).open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testHttpRestClientStreamB() throws IOException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> aResponse.setResponse( aRequest.getRequest( InputStream.class ) ) ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		theRestClient.onResponse( HttpMethod.POST, LOCATOR, ( aResponse ) -> {
			final InputStream theInputStream = aResponse.getResponse( InputStream.class );
			try {
				int i = 0;
				int theCounter = 0;
				int eByte;
				StringBuilder theBuffer = new StringBuilder();
				while ( theInputStream.available() > 0 ) {
					eByte = theInputStream.read();
					assertEquals( theCounter % MOD_BYTE, eByte );
					theCounter++;
					theBuffer.append( new HorizAlignTextBuilder().withColumnWidth( 3 ).withFillChar( '0' ).withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).toString( Integer.toString( eByte ) ) + " " );
					i++;
					if ( i >= 8 || theInputStream.available() == 0 ) {
						if ( SystemProperty.LOG_TESTS.isEnabled() ) {
							System.out.println( theBuffer.toString() );
						}
						i = 0;
						theBuffer = new StringBuilder();
					}
				}
			}
			catch ( Exception e ) {
				fail( "Should not reach this code!" );
			}
			theRestServer.closeQuietly();
			synchronized ( this ) {
				notifyAll();
			}
		} ).withRequest( new NumberSequenceInputStream() ).open();
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ignore ) {}
		}
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testHttpBodyMap1() throws IOException, HttpResponseException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final HttpBodyMap thePerson = aRequest.getRequest( HttpBodyMap.class ); assertEquals( FIRST_NAME, thePerson.get( "/firstName" ) ); assertEquals( LAST_NAME, thePerson.get( "/lastName" ) ); aResponse.setResponse( thePerson ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		final RestResponse aResponse = theRestClient.doRequest( HttpMethod.POST, LOCATOR, new Person( FIRST_NAME, LAST_NAME ) );
		final Person thePerson = aResponse.getResponse( Person.class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( thePerson.toString() );
		}
		theRestServer.closeQuietly();
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Test
	public void testHttpBodyMap2() throws IOException, HttpResponseException {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Using port <" + thePort + "> for testing ..." );
		}
		final RestfulHttpServer theRestServer = new HttpRestServer().withBaseLocator( BASE_LOCATOR );
		theRestServer.onPost( LOCATOR, ( aRequest, aResponse ) -> { final HttpBodyMap thePerson = aRequest.getRequest( HttpBodyMap.class ); assertEquals( FIRST_NAME, thePerson.get( "/firstName" ) ); assertEquals( LAST_NAME, thePerson.get( "/lastName" ) ); aResponse.setResponse( thePerson ); } ).open();
		theRestServer.open( thePort );
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( toBaseUrl( thePort ) );
		final RestResponse aResponse = theRestClient.doRequest( HttpMethod.POST, LOCATOR, new Person( FIRST_NAME, LAST_NAME ) );
		final HttpBodyMap thePerson = aResponse.getResponse( HttpBodyMap.class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( thePerson.toString() );
		}
		assertEquals( FIRST_NAME, thePerson.get( "/firstName" ) );
		assertEquals( LAST_NAME, thePerson.get( "/lastName" ) );
		theRestServer.closeQuietly();
		PortManagerSingleton.getInstance().unbindPort( thePort );
	}

	@Disabled
	@Test
	public void testBasePath() throws MalformedURLException, HttpResponseException {
		final RestfulHttpClient theClient = new HttpRestClient().withBaseUrl( "http://www.refcodes.org" );
		final RestRequestBuilder theBuilder = theClient.buildGet( "blog/the_tiny_bash_cheat_sheet" );
		final RestResponse theResponse = theBuilder.toRestResponse();
		System.out.println( theResponse.getHttpBody() );
	}

	@Disabled
	@Test
	public void testEdgeCase() throws IOException, HttpResponseException {
		final RestfulHttpClient theRestClient = new HttpRestClient().withBaseUrl( "https://www.heise.de" );
		final RestResponse theResponse = theRestClient.doGet( "thema/Linux-und-Open-Source" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theResponse.getHttpBody() );
			// if ( SystemProperty.LOG_TESTS.isEnabled() ) HttpBodyMap theBodyMap = theResponse.getResponse();
			// if ( SystemProperty.LOG_TESTS.isEnabled() ) System.out.println( VerboseTextBuilder.asString( theBodyMap ) );
		}
	}

	@Test
	@Disabled
	public void testHtml() throws HttpResponseException, MalformedURLException {
		final RestfulHttpClient theRestClient = new HttpRestClient();
		final RestResponse theResponse = theRestClient.doGet( "http://www.refcodes.org" );
		final HttpBodyMap theBodyMap = theResponse.getResponse();
		System.out.println( VerboseTextBuilder.asString( theBodyMap ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private String toBaseUrl( Integer thePort ) {
		return BASE_URL + ":" + thePort + BASE_LOCATOR;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static class Person {
		@Override
		public String toString() {
			return "Person [firstName=" + firstName + ", lastName=" + lastName + "]";
		}

		private String firstName;
		private String lastName;

		public Person() {};

		public Person( String firstName, String lastName ) {
			this.firstName = firstName;
			this.lastName = lastName;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName( String firstName ) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName( String lastName ) {
			this.lastName = lastName;
		}
	}

	private static class NumberSequenceInputStream extends InputStream {
		private int _counter = 0;

		@Override
		public int read() throws IOException {
			return available() <= 0 ? -1 : _counter++ % MOD_BYTE;
		}

		@Override
		public int available() throws IOException {
			return 1024 - _counter;
		}
	}
}
